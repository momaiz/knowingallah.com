### Knowing Allah ###

### What is this repository for? ###

* This repo for **Knowing Allah** site. This site was built using [CodeIgniter 2 Framework](http://codeigniter.com/userguide2/). The admin panel using [grocery CRUD](http://www.grocerycrud.com/).

-----

### Site URLs ###
- [Main Site](http://knowingallah.com/)
- [Develop Site](http://new.knowingallah.com/)

**Note:** *The database for both sites is one*

-----

### How do I get set up? ###

### 1) Downloading
#### 1.1) Clone the Repository
```
git clone https://bitbucket.org/midadedev/knowingallah.com
```
#### 1.2) [OR] Download the Repository
```
https://bitbucket.org/midadedev/knowingallah.com/get/5720c90e2b6a.zip
```

-----

### 2) Setup Database

- Now that you have project cloned with all the dependencies pre-installed, you need to create a database with the name "as-you-want" through PHPMyAdmin.
- Put database data on application/config/database.php
```
<?php
$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'root';
$db['default']['password'] = 'your-password';
$db['default']['database'] = 'as-you-want';
```


### 3) You are Done!

-----

### Important ###
In KnowingAdmin folder there is a folder named *temp_upload* this folder is very important and take 0777 permeation.
Any photo we upload to media domain we re-sized & compressed it. The new photo which re-sized & compressed saved in this folder until upload to media domain then it removed by 
```
#!PHP
unlike()
```
So any time you open this folder you should see index.html file only.

-----

### Login Data ###
## Main Site ##
- Admin URL [here](http://knowingallah.com/KnowingAdmin/)
- username: KnowingAllah_Admin
- password: k80W1n9@ll@h

## Develop Site ##
- Admin URL [here](http://new.knowingallah.com/KnowingAdmin/)
- username: KnowingAllah_Admin
- password: k80W1n9@ll@h

**Note:** if you change username or password in any site it will be change in the other because the database is one for the two sites

-----

## License

This software is released under the [Midade PSL](LICENSE) License.

© 2015 Midade LLC, All rights reserved.