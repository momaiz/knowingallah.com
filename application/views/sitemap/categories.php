<?php
header("Content-type: text/xml");
/**
 * Copyright (c) 2015 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author        programmer/ Mohammed Fawzy Zayan <admin@zayanit.com>
 * @link          http://www.zayanit.com
 * @version       1.0
 * Created by Sublime Text 2
 */

echo "<?xml version='1.0' encoding='UTF-8'?>";
?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

	<url>
		<loc><?= site_url().'/'.$model ?>s/category/</loc>
	</url>

	<?php foreach($categories as $category): ?>
		<url>
			<loc><?= site_url().'/'.$model ?>s/category/<?= $category[$model.'_cat_id'].'/' ?></loc>
		</url>
	<?php endforeach; ?>

</urlset>