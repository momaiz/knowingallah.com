<div class="col-md-9 col-sm-8 col-xs-12">

    <div class="jumbotron">
        <div class="well no-border text-center">
            <h1>404</h1>
            <h2><?= lang('notFound') ?></h2>
            <p>
                <?= lang('404message1') ?>
                <br />
                <?= lang('404message2') ?><a href="<?= site_url('home').'/' ?>"><?= lang('404homepage') ?></a>
            </p>
            
            <br />
            <hr />
            <br />

            <p><?= lang('search_message') ?></p>
            <style>
                #gsc-iw-id1{
                    height: 30px;
                    border-color: #898989  ;
                }
                .search-box, .search-box *, .search-box *:before, .search-box *:after {
                    -webkit-box-sizing: content-box;
                    -moz-box-sizing: content-box;
                    box-sizing: content-box;
                }
            </style>
            <div class="search-box">
                <script>
                (function() {
                    var cx = '015497087736326010529:rbazlqhvg7i';
                    var gcse = document.createElement('script');
                    gcse.type = 'text/javascript';
                    gcse.async = true;
                    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
                        '//cse.google.com/cse.js?cx=' + cx;
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(gcse, s);
                })();
                </script>
                <gcse:search></gcse:search>
            </div>

        </div>
    </div>

</div>
</div>
</div>
</div>