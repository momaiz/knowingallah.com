<div class="col-md-9 quran-way">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">

                    <div class="pathto text-center">
                        <ol class="breadcrumb list-inline">
                            <li class="active"><span class='fa fa-ban'></span></li>
                        </ol>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h3><?= $message ?></h3>
                        </div>
                    </div>

                </div>
            </div>
        </div><!--==== End Row =====-->
    </div><!--==== End panel-body ====-->

</div><!--==== End Row =====-->
</div><!--==== End Container =====-->
</div><!--==== End Component =====-->
