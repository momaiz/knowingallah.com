<?php

/**
 * Copyright (c) 2016 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author   Mohammed Zayan <zayanit@gmail.com>
 * @link     http://www.zayanit.com
 * @version  1.0
 * Created by PhpStorm
 */
 
?>

<div class="col-md-9 col-sm-8 col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?=site_url().'/home/'?>"><i class="fa fa-home"></i><?=lang('homepage')?></a></li>
        <li><?=lang('books')?></li>
    </ol>
    <?php if (is_array($allBooks) && !empty($allBooks)) : ?>
    <ul class="list-unstyled no-margin clearfix">
        <?php foreach ($allBooks as $book) : ?>
            <li class="col-md-4 col-sm-4 col-xs-12 col-padding-5">
                <div class="card book-item">
                    <div class="book-img">
                        <a href="<?=site_url('books').'/'.$book['slug']?>">
                            <?php if ($book['photo'] != '') : ?>
                                <img src="<?=$book['photo']?>" class="img-responsive align-center" alt="<?=$book['name']?>">
                            <?php else : ?>
                                <img src="<?=base_url('assets/images/book-sample.png')?>" class="img-responsive align-center" alt="<?=$book['name']?>">
                            <?php endif; ?>
                        </a>
                    </div>
                    <div class="desc">
                        <h4>
                            <a href="<?=site_url('books').'/'.$book['slug']?>" title="<?=$book['name']?>">
                                <?=$book['name']?>
                            </a>
                        </h4>
                        <?php if ($book['author'] != '') : ?>
                            <div class="author"><i class="fa fa-user"></i><?=$book['author']?></div>
                        <?php else : ?>
                            <div class="author"><i class="fa fa-user"></i><?=lang('siteTeam')?></div>
                        <?php endif; ?>
                        <span><i class="fa fa-clock-o"></i><?=changeDateFormate($book['creation_date'])?></span>
                        <span><i class="fa fa-eye"></i><?=$book['hits']?></span>
                    </div>
                </div>
            </li>
            <?php endforeach; ?>
    </ul>
    <div class="text-center">
        <?=$this->pagination->create_links();?>
    </div>
<?php else : ?>
    <div class="card home-article inner">
        <div class="media">
                <p><?=lang('noBooks')?></p>
            </div>
    </div>
<?php endif; ?>
</div>

<?php $this->load->view('partials/side_menus/books_mobile_side_menu'); ?>

</div>
</div>
</main>