<?php

/**
 * Copyright (c) 2016 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author   Mohammed Zayan <zayanit@gmail.com>
 * @link     http://www.zayanit.com
 * @version  1.0
 * Created by PhpStorm
 */
 
?>

<div class="col-md-9 col-sm-8 col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?=site_url().'/'?>"><i class="fa fa-home"></i><?=lang('homepage')?></a></li>
        <li><a href="<?=site_url('books').'/'?>"><?=lang('books')?></a></li>
        <li><?=$book['name']?></li>
    </ol>
    <div class="card view-item">
        <div class="article">
            <?php if ($book['photo'] != '') : ?>
                <img src="<?=$book['photo']?>" title="<?=$book['name']?>" class="img-responsive align-center" width="640" height="330" alt="<?=$book['name']?>">
            <?php endif; ?>
        </div>
        <div class="item-title">
            <h1><?=$book['name']?></h1>
            <div class="author">
                <i class="fa fa-user"></i>
                <?=$book['author'] != '' ? $book['author'] : lang('siteTeam')?>
            </div>
        </div>
        <article>
            <div class="download-btn">
                <a href="<?=$book['download_link']?>" target="_blank" class="btn btn-rounded" onclick="incrementDownloads(<?=$book['id']?>)">
                    <i class="fa fa-download"></i><?=lang('downloadTheBook')?>
                </a>
            </div>
        </article>
        <div class="share-item clearfix">
            <div class="info">
                <span><i class="fa fa-clock-o"></i><?=changeDatePublished($book['creation_date'])?></span>
                <span><i class="fa fa-eye"></i><?=$book['hits']?></span>
            </div>
            <div class="share">
                <h5><?=lang('shareBookIn')?></h5>
                <button class="btn-sm-full fb-bg btn-rounded waves-effect waves-light" onclick="shareFunction('http://www.facebook.com/share.php?u=http://<?= $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>&title=<?= $book['name'] ?>')">
                    <i class="fa fa-facebook"> </i> <span>Facebook</span>
                </button>
                <button class="btn-sm-full tw-bg btn-rounded waves-effect waves-light" onclick="shareFunction('http://twitter.com/home?status=<?= $book['name'].'+http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>')">
                    <i class="fa fa-twitter"> </i> <span>Twitter</span>
                </button>
                <button class="btn-sm-full gplus-bg btn-rounded waves-effect waves-light" onclick="shareFunction('https://plus.google.com/share?url=http://<?= $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>')">
                    <i class="fa fa-google-plus"> </i> <span>Google +</span>
            </div>
        </div>
    </div>
    <?php if (is_array($relatedBooks) && !empty($relatedBooks)) : ?>
    <div class="section-title">
        <h3><?=lang('relatedBooks')?></h3>
    </div>
    <ul class="list-unstyled no-margin clearfix">
        <?php foreach ($relatedBooks as $book) : ?>
            <li class="col-md-4 col-sm-4 col-xs-12 col-padding-5">
                <div class="card book-item">
                    <div class="book-img">
                        <a href="<?=site_url('books').'/'.$book['slug']?>">
                            <?php if ($book['photo'] != '') : ?>
                                <img src="<?=$book['photo']?>" class="img-responsive align-center" alt="<?=$book['name']?>">
                            <?php else : ?>
                                <img src="<?=base_url('assets/images/book-sample.png')?>" class="img-responsive align-center" alt="<?=$book['name']?>">
                            <?php endif; ?>
                        </a>
                    </div>
                    <div class="desc">
                        <h4>
                            <a href="<?=site_url('books').'/'.$book['slug']?>" title="<?=$book['name']?>">
                                <?=$book['name']?>
                            </a>
                        </h4>
                        <?php if ($book['author'] != '') : ?>
                            <div class="author"><i class="fa fa-user"></i><?=$book['author']?></div>
                        <?php else : ?>
                            <div class="author"><i class="fa fa-user"></i><?=lang('siteTeam')?></div>
                        <?php endif; ?>
                        <span><i class="fa fa-clock-o"></i><?=changeDateFormate($book['creation_date'])?></span>
                        <span><i class="fa fa-eye"></i><?=$book['hits']?></span>
                    </div>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
    <?php endif; ?>
</div>
</div>
</div>
</main>