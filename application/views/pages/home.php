<?php

$this->load->view('pages/home_parts/site_new');
if (is_array($books) && !empty($books)) {
    $this->load->view('pages/home_parts/books');
}
if (is_array($questionsAboutGod) && !empty($questionsAboutGod)) {
    $this->load->view('pages/home_parts/questions_about_allah');
}
$this->load->view('pages/home_parts/videos_sounds');
$this->load->view('pages/home_parts/home_articles');
