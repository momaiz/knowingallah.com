<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= lang('websiteName'); ?></title>
    <style>
        body{
            direction:<?= $dir ?>;
        }
        p ,span{
            font-size: 20px !important;
        }
    </style>
</head>
<body>
            <h1 align="center"><?= $article['title'] ?></h1>
            <h3 align="center"><?=lang('underCategory')?>: <?=$article['category']?></h3>
            <p><?= $article['content'] ?></p>
</body>
</html>