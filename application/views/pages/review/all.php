<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= lang('websiteName'); ?></title>
    <style>
        body{
            direction:<?= $dir ?>;
        }
        table{
            border: 1px solid;
        }
        td {
            border-bottom: 1px solid;
            padding: 0 10px;
        }
    </style>
</head>
<body>
<h1><?= lang('articles') ?></h1>
    <table >
        <?php foreach($articles as $article): ?>
            <tr>
                <td>
                    <a href="<?= site_url("review/article/".$article['article_id']) ?>">
                        <h4><?= $article['title'] ?></h4>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
        <?= $this->pagination->create_links(); ?>
</body>
</html>