<div class="col-md-9 quran-way">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">

                <div class="pathto">
                    <ol class="breadcrumb list-inline">
                        <?php
                        switch(uri_string()){
                            case $current_lang_abbr.'/articles/category':
                            case $current_lang_abbr.'/articles':
                                $mainCategory = lang('articles');
                            break;
                            case $current_lang_abbr.'/videos/category':
                            case $current_lang_abbr.'/videos':
                                $mainCategory = lang('videos');
                            break;
                            case $current_lang_abbr.'/audios/category':
                            case $current_lang_abbr.'/audios':
                                $mainCategory = lang('audios');
                            break;
                            default:
                                $mainCategory = '';
                            break;
                        }
                        ?>
                        <li class="active"><?=$mainCategory?></li>
                    </ol>
                </div>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th colspan="4"><?= lang('sub_cats') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $n = 1;
                    foreach (array_chunk($query_sub_cats, 2) as $subCats) :
                    ?>
                        <tr>
                            <?php foreach ($subCats as $row) : ?>
                                <td scope="row"><?=$n?></td>
                                <td>
                                    <a href="<?=site_url($model."/category/".$row['id']).'/'?>" class="categoryLink">
                                        <?= $row['description'] ?>
                                    </a>
                                </td>
                            <?php
                                $n++;
                            endforeach;
                            ?>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>

                    <!--          <hr>
            <div class="well text-center">-->
<!--                <div class="fb-comments" data-href="--><?//= current_url()?><!--" data-numposts="5" data-colorscheme="light" data-mobile></div>-->
<!--            </div>-->
            </div>
            </div>
            </div><!--==== End Row =====-->
        </div><!--==== End panel-body ====-->

    </div><!--==== End Row =====-->
</div><!--==== End Container =====-->
</div><!--==== End Component =====-->