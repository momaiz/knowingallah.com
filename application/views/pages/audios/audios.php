<div class="col-md-9 quran-way">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">



                    <h4 class="no-margin-top"><?= lang('audios') ?></h4>


                    <div class="col-md-12">
                        <?php
                        $num_rows=count($audios);
                        $rows_per_column=$num_rows/3;
                        foreach(array_chunk($audios,ceil($rows_per_column)) as $audios_column):
                            ?>
                            <div class='col-md-4'>
                                <div class="panel panel-default panel-sound">
                                    <div class="panel-heading"><?= lang('audios') ?></div>
                                    <div class="panel-body">
                                        <ul class="list-unstyled">
                                            <?php foreach($audios_column as $audio): ?>
                                                <li><a href="<?= site_url('audios/'.$audio['slug']).'/' ?>">
                                                        <h4><i class="fa fa-volume-up "></i> <?= $audio['title'] ?></h4>
                                                        <p><?= $audio['author'] ?></p>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="pagination-wrap">
                        <div class="text-center">
                            <?= $this->pagination->create_links(); ?>
                        </div>
                    </div>







                </div>
            </div>
        </div><!--==== End Row =====-->
    </div><!--==== End panel-body ====-->

</div><!--==== End Row =====-->
</div><!--==== End Container =====-->
</div><!--==== End Component =====-->