<div class="col-md-9 col-sm-8 col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?=site_url('home').'/'?>"><i class="fa fa-home"></i><?=lang('homepage')?></a></li>
        <li><a href="<?=site_url('audios/category').'/'.$audio_row['audio_cat_id'].'/'?>"><?=$audio_row['category']?></a></li>
        <li><?= $audio_row['title'] ?></li>
    </ol>
    <div class="card view-item">
        <div class="sound-item">
<!--            <div id="jquery_jplayer_1" class="jp-jplayer"></div>-->
<!--            <div id="jp_container_1" class="jp-audio" role="application" aria-label="media player">-->
<!--                <div class="jp-type-single">-->
<!--                    <div class="jp-gui jp-interface">-->
<!--                        <div class="jp-controls">-->
<!--                            <button class="jp-play" role="button" tabindex="0">play</button>-->
<!--                        </div>-->
<!--                        <div class="jp-progress">-->
<!--                            <div class="jp-seek-bar">-->
<!--                                <div class="jp-play-bar"></div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="jp-volume-controls">-->
<!--                            <button class="jp-mute" role="button" tabindex="0"> <i class="fa fa-volume-off"></i> </button>-->
<!--                            <button class="jp-volume-max" role="button" tabindex="0"><i class="fa fa-volume-up"></i></button>-->
<!--                            <div class="jp-volume-bar">-->
<!--                                <div class="jp-volume-bar-value"></div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="jp-time-holder">-->
<!--                            <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>-->
<!--                            <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
            <?php if(trim($audio_row['link']) != ''):
            if($file_ext == "rm"): ?>
            <EMBED  src="<?= $audio_row['link'] ?>" width="343" height="43" type="audio/x-pn-realaudio-plugin" autostart="true" controls="ControlPanel">
            <?php elseif($file_ext == "mp3"): ?>
                    <audio class="audioplayer" preload="auto" controls>
                        <source src="<?= $audio_row['link'] ?>" type="audio/mpeg">
                        <source src="<?= $audio_row['link'] ?>" type="audio/ogg">
                        <source src="<?= $audio_row['link'] ?>" type="audio/wav">
                    </audio>

            <?php
                else:
                    $audio_link = getRedirectUrl($audio_row['link']);
            ?>
                    <audio class="audioplayer" preload="auto" controls>
                        <source src="<?= $audio_link ?>" type="audio/mpeg">
                        <source src="<?= $audio_link ?>" type="audio/ogg">
                        <source src="<?= $audio_link ?>" type="audio/wav">
                    </audio>
            <?php endif; ?>
            <?php endif; ?>
            <br />
            <a href="<?php echo $audio_row['link'] ?>" class="btn btn-block btn-success" download="<?php echo lang('download');?>"><?php echo lang('download');?></a>
        </div>
        <div class="item-title">
            <h1><?=$audio_row['title']?></h1>

            <p><?=$audio_row['description']?></p>

            <div class="author">
                <i class="fa fa-user"></i>
                <?=$audio_row['author'] != '' ? $audio_row['author'] : lang('siteTeam')?>
            </div>
        </div>
        <div class="share-item clearfix">
            <div class="info">
                <span><i class="fa fa-clock-o"></i><?=changeDateFormate($audio_row['creation_date'])?></span>
                <span><i class="fa fa-eye"></i><?=$audio_row['hits']?></span>
            </div>
            <div class="share">
                <h5><?=lang('shareVedio')?></h5>
                <button class="btn-sm-full fb-bg btn-rounded waves-effect waves-light" onclick="shareFunction('http://www.facebook.com/share.php?u=http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>&title=<?=$audio_row['title']?>')">
                    <i class="fa fa-facebook"> </i> <span>Facebook</span>
                </button>
                <button class="btn-sm-full tw-bg btn-rounded waves-effect waves-light" onclick="shareFunction('http://twitter.com/home?status=<?=$audio_row['title'].'+http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>')">
                    <i class="fa fa-twitter"> </i> <span>Twitter</span>
                </button>
                <button class="btn-sm-full gplus-bg btn-rounded waves-effect waves-light" onclick="shareFunction('https://plus.google.com/share?url=http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>')">
                    <i class="fa fa-google-plus"> </i> <span>Google +</span>
                </button>
            </div>
        </div>
    </div>
    <?php if (is_array($relatedAudios) && !empty($relatedAudios)) : ?>
        <div class="section-title">
            <h3><?=lang('relatedAudios')?></h3>
        </div>
        <ul class="list-unstyled no-margin clearfix">
            <?php foreach ($relatedAudios as $audio) : ?>
                <li>
                    <div class="home-sound-item inner">
                        <div class="card">
                            <div class="media">
                                <div class="media-left">
                                    <a href="<?=site_url('audios').'/'.$audio['slug'].'/'?>">
                                        <i class="fa fa-play media-object"></i>
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4>
                                        <a href="<?=site_url('audios').'/'.$audio['slug'].'/'?>" title="<?=$audio['title']?>
        ">
                                            <?=$audio['title']?>
                                        </a>
                                    </h4>
                                    <div class="author">
                                        <i class="fa fa-user"></i>
                                        <?=$audio['author'] != '' ? $audio['author'] : lang('siteTeam')?>
                                    </div>
                                    <span><i class="fa fa-clock-o"></i><?=changeDateFormate($audio['creation_date'])?></span>
                                    <span><i class="fa fa-eye"></i><?=$audio['hits']?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>
</div>
</div>
</main>