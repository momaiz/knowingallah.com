<div class="col-md-9 col-sm-8 col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?=site_url('home').'/'?>"><i class="fa fa-home"></i><?=lang('homepage')?></a></li>
        <li><a href="<?=site_url('audios/category').'/'?>"><?=lang('audios')?></a></li>
        <li><?=$cat_name?></li>
    </ol>
    <?php if ($query_other_languages || $query_sub_cats) : ?>
        <div class="card section-info">
            <?php if ($query_other_languages): ?>
                <div class="translated-to">
                    <span><?=lang('translated_to_other_langs')?></span>
                    <button type="button" class="btn dropdown-button" href="#" data-activates="dropdown2">
                        <?=lang('catTranslation')?>
                        <i class="fa fa-angle-down"></i>
                    </button>
                    <!-- Dropdown Structure -->
                    <ul id="dropdown2" class="dropdown-content">
                        <?php foreach ($query_other_languages as $row): ?>
                            <li>
                                <a hreflang="<?=$row['code']?>" href="<?=site_url("../".$row['code']."/audios/category/".$categoryId).'/'?>">
                                    <?=$row['description']?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <?php
            endif;
            if ($query_sub_cats):
                ?>
                <div class="translated-to">
                    <span><?=lang('sub_cats')?></span>
                    <button type="button" class="btn dropdown-button" href="#" data-activates="dropdown3">
                        <?=lang('sub_cats')?>
                        <i class="fa fa-angle-down"></i>
                    </button>
                    <!-- Dropdown Structure -->
                    <ul id="dropdown3" class="dropdown-content">
                        <?php foreach ($query_sub_cats as $row): ?>
                            <li>
                                <a hreflang="<?=$current_lang_abbr?>" href="<?=site_url("audios/category/".$row['id']).'/'?>" class="btn">
                                    <?=$row['description']?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <ul class="list-unstyled no-margin clearfix">
        <?php foreach ($query_audios as $audio) : ?>
            <li class="col-md-6 col-sm-4 col-xs-12 col-padding-5">
                <div class="home-sound-item inner">
                    <div class="card">
                        <div class="media">
                            <div class="media-left">
                                <a href="<?=site_url('audios').'/'.$audio['slug'].'/'?>">
                                    <i class="fa fa-play media-object"></i>
                                </a>
                            </div>
                            <div class="media-body">
                                <h4>
                                    <a href="<?=site_url('audios').'/'.$audio['slug'].'/'?>" title="<?=$audio['title']?>
    ">
                                        <?=$audio['title']?>
                                    </a>
                                </h4>
                                <div class="author">
                                    <i class="fa fa-user"></i>
                                    <?=$audio['author'] != '' ? $audio['author'] : lang('siteTeam')?>
                                </div>
                                <span><i class="fa fa-clock-o"></i><?=changeDateFormate($audio['creation_date'])?></span>
                                <span><i class="fa fa-eye"></i><?=$audio['hits']?></span>
                        </div>
                    </div>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
    <div class="text-center">
        <?=$this->pagination->create_links();?>
    </div>
</div>

<?php $this->load->view('partials/side_menus/audios_mobile_side_menu'); ?>

</div>
</div>
</main>