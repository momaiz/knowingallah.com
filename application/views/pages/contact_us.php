<div class="col-md-9 col-sm-8 col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?=site_url('home').'/'?>"><i class="fa fa-home"></i><?=lang('homepage')?></a></li>
        <li><?=lang('contactus')?></li>
    </ol>
    <div class="card view-item">
        <h1><?= lang('contactus') ?></h1>
        <hr />
        <form action="<?=site_url('contactSend')?>" method="post">
            <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
            <div class="form-group">
                <label for="name"><?=lang('yourName')?></label>
                <input type="text" name="name" id="name" class="form-control" required />
            </div>
            <div class="form-group">
                <label for="email"><?=lang('email')?></label>
                <input type="email" name="email" id="email" class="form-control" required />
            </div>
            <div class="form-group">
                <label for="message"><?=lang('message')?></label>
                <textarea name="message" id="message" rows="10" class="form-control" required></textarea>
            </div>
            <div class="form-group">
                <input type="submit" name="submit" id="submit" class="btn btn-primary form-control" value="<?=lang('submitText')?>" />
            </div>
        </form>
    </div>
</div>
</div>
</main>