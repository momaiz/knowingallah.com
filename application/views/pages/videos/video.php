<div class="col-md-9 col-sm-8 col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?=site_url().'/'?>"><i class="fa fa-home"></i><?=lang('homepage')?></a></li>
        <li><a href="<?=site_url('videos/category').'/'?>"><?=lang('videos')?></a></li>
        <li><a href="<?=site_url('videos/category').'/'.$video_row['video_cat_id'].'/'?>"><?=$video_row['category']?></a></li>
        <li><?=$video_row['title']?></li>
    </ol>
    <div class="card view-item">
        <div class="video">
            <?php if ($video_row['youtube_code'] != '') { ?>
            <iframe src="https://www.youtube.com/embed/<?=$video_row['youtube_code']?>?rel=0" width="770" height="450" frameborder="0" allowfullscreen=""></iframe>
            <?php
            } elseif (trim($video_row['link']) != '') {
                if ($file_ext == "rm" || $file_ext == "ram" || $file_ext == "rmvb" || $file_ext == "rv" || $file_ext == "asx")
                {
            ?>
                    <embed class="col-md-12" name="64379" width="500" height="400" pluginspage="http://www.real.com/player" src="<?=$video_row['link']?>" type="audio/x-pn-realaudio-plugin" nojava="true" autostart="true" maintainaspect="false" controls="imagewindow" console="64379" vspace="0" hspace="0" border="0">
                    <embed class="col-md-12" width="500" height="60" name="64379" pluginspage="http://www.real.com/player" type="audio/x-pn-realaudio-plugin" nojava="true" autostart="true" maintainaspect="false" controls="controlpanel,statusbar" console="64379" vspace="0" hspace="0" border="0"></embed>
            <?php
                }
            } elseif ($video_row['content'] != '') {
                echo $video_row['content'];
            }
            ?>
        </div>
        <div class="item-title">
            <p><br></p>
            <h1><?=$video_row['title']?></h1>
            <div class="author">
                <i class="fa fa-user"></i>
                <?=$video_row['author'] != '' ? $video_row['author'] : lang('siteTeam')?>
            </div>
            <div>
                 <?=$video_row['description']?>
            </div>
        </div>
        <div class="share-item clearfix">
            <div class="info">
                <span><i class="fa fa-clock-o"></i><?=changeDateFormate($video_row['creation_date'])?></span>
                <span><i class="fa fa-eye"></i><?=$video_row['hits']?></span>
            </div>

            <div class="share">
                <h5><?=lang('shareVedio')?></h5>
                <button class="btn-sm-full fb-bg btn-rounded waves-effect waves-light" onclick="shareFunction('http://www.facebook.com/share.php?u=http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>&title=<?= $video_row['title']?>')">
                    <i class="fa fa-facebook"> </i> <span>Facebook</span>
                </button>
                <button class="btn-sm-full tw-bg btn-rounded waves-effect waves-light" onclick="shareFunction('http://twitter.com/home?status=<?=$video_row['title'].'+http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>')">
                    <i class="fa fa-twitter"> </i> <span>Twitter</span>
                </button>
                <button class="btn-sm-full gplus-bg btn-rounded waves-effect waves-light" onclick="shareFunction('https://plus.google.com/share?url=http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>')">
                    <i class="fa fa-google-plus"> </i> <span>Google +</span>
                </button>
            </div>
        </div>
    </div>
    <?php if (is_array($relatedVideos) && !empty($relatedVideos)) : ?>
        <div class="section-title">
            <h3><?=lang('relatedVideos')?></h3>
        </div>
        <ul class="list-unstyled no-margin clearfix">
            <?php foreach ($relatedVideos as $video) : ?>
            <li class="col-md-4 col-sm-4 col-xs-12 col-padding-5">
                <div class="video-item">
                    <div class="elegant-card z-depth-1 hoverable">
                        <div class="card-up view overlay hm-zoom">
                            <a href="<?=site_url('videos/'.$video['slug']).'/'?>">
                                <?php  if ($video['youtube_code'] != '') : ?>
                                    <img src="http://img.youtube.com/vi/<?=$video['youtube_code']?>/0.jpg" class="img-responsive">
                                <?php else : ?>
                                    <img src="http://img.youtube.com/vi/oJuTIb50fc8/0.jpg" class="img-responsive">
                                <?php endif; ?>
                            </a>
                        </div>
                        <a href="<?=site_url('videos/'.$video['slug']).'/'?>" class="btn-floating btn-large waves-effect waves-light"><i class="fa fa-play"></i></a>
                        <div class="card-content">
                            <h4>
                                <a href="<?=site_url('videos/'.$video['slug']).'/'?>" title="">
                                    <?=$video['title']?>
                                </a>
                            </h4>
                            <div class="author">
                                <i class="fa fa-user"></i>
                                <?=$video['author'] != '' ? $video['author'] : lang('siteTeam')?>
                            </div>
                            <span><i class="fa fa-clock-o"></i><?=changeDateFormate($video['creation_date'])?></span>
                            <span><i class="fa fa-eye"></i><?=$video['hits']?></span>
                    </div>
                </div>
            </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>
</div>
</div>
</main>