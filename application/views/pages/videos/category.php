<div class="col-md-9 col-sm-8 col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?=site_url().'/'?>"><i class="fa fa-home"></i><?=lang('homepage')?></a></li>
        <li><a href="<?=site_url('videos/category').'/'?>"><?=lang('videos')?></a></li>
        <li><?=$cat_name?></li>
    </ol>
    <?php if ($query_other_languages || $query_sub_cats) : ?>
        <div class="card section-info">
            <?php if ($query_other_languages): ?>
                <div class="translated-to">
                    <span><?=lang('translated_to_other_langs')?></span>
                    <button type="button" class="btn dropdown-button" href="#" data-activates="dropdown2">
                        <?=lang('catTranslation')?>
                        <i class="fa fa-angle-down"></i>
                    </button>
                    <!-- Dropdown Structure -->
                    <ul id="dropdown2" class="dropdown-content">
                        <?php foreach ($query_other_languages as $row): ?>
                            <li>
                                <a hreflang="<?=$row['code']?>" href="<?=site_url("../".$row['code']."/videos/category/".$categoryId).'/'?>">
                                    <?=$row['description']?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <?php
            endif;
            if ($query_sub_cats):
                ?>
                <div class="translated-to">
                    <span><?=lang('sub_cats')?></span>
                    <button type="button" class="btn dropdown-button" href="#" data-activates="dropdown3">
                        <?=lang('sub_cats')?>
                        <i class="fa fa-angle-down"></i>
                    </button>
                    <!-- Dropdown Structure -->
                    <ul id="dropdown3" class="dropdown-content">
                        <?php foreach ($query_sub_cats as $row): ?>
                            <li>
                                <a hreflang="<?=$current_lang_abbr?>" href="<?=site_url("videos/category/".$row['id']).'/'?>" class="btn">
                                    <?=$row['description']?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <ul class="list-unstyled no-margin clearfix">
        <?php foreach ($query_videos as $video): ?>
            <li class="col-md-4 col-sm-4 col-xs-12 col-padding-5">
                <div class="video-item">
                    <div class="elegant-card z-depth-1 hoverable">
                        <div class="card-up view overlay hm-zoom">
                            <a href="<?= site_url('videos').'/'.$video['slug'].'/'?>">
                                <?php if ($video['youtube_code'] != '') : ?>
                                    <img src="https://img.youtube.com/vi/<?= $video['youtube_code'] ?>/0.jpg" class="img-responsive">
                                <?php else : ?>
                                    <img src="<?=base_url('assets/images/deafult-video-img.png')?>" class="img-responsive">
                                <?php endif; ?>
                            </a>
                        </div>
                        <a href="<?= site_url('videos').'/'.$video['slug'].'/'?>" class="btn-floating btn-large waves-effect waves-light"><i class="fa fa-play"></i></a>
                        <div class="card-content">
                            <h4>
                                <a href="<?= site_url('videos').'/'.$video['slug'].'/'?>" title="<?= $video['title']?>">
                                    <?= $video['title']?>
                                </a>
                            </h4>
                            <div class="author">
                                <i class="fa fa-user"></i>
                                <?=$video['author'] != '' ? $video['author'] : lang('siteTeam')?>
                            </div>
                            <?php if ($video['description'] != '') : ?>
                                <p><?=$video['description']?></p>
                            <?php endif; ?>
                            <span><i class="fa fa-clock-o"></i><?=changeDateFormate($video['creation_date'])?></span>
                            <span><i class="fa fa-eye"></i><?=$video['hits']?></span>
                        </div>
                    </div>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
    <div class="text-center">
        <?=$this->pagination->create_links();?>
    </div>
</div>

<?php $this->load->view('partials/side_menus/videos_mobile_side_menu'); ?>

</div>
</div>
</main>