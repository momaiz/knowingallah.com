<div class="component pos-relative"><!--==== Start Component =====-->

    <div class="container"><!--==== Start Container =====-->

        <div class="row padding-top-md"><!--==== Start Row =====-->



<div class="col-md-9">

    <div class="panel panel-default">

        <div class="panel-body text-center">

            <div class="row heading">

                <div class="col-md-12">

                    <h4 class="margin-top-md text-center"><a href="<?= site_url('videos/category').'/' ?>"><?= lang('videos') ?></a></h4>

                </div>

            </div>
            <?php
            $x = 2;
            foreach($videos as $video):
                if($x %2 == 0):
            ?>
            <div class="row margin-top-xl">
            <?php endif; ?>

                <div class="col-md-6 col-sm-6 col-xs-12">

                    <figure class="full-width no-overflow">

                        <a href="<?= site_url('videos/'.$video['slug']).'/' ?>">

                            <img src="http://img.youtube.com/vi/<?= $video['youtube_code'] ?>/0.jpg" class="align-center full-width img-responsive">

                        </a>

                        <figcaption>

                            <h4><?= $video['title'] ?></h4>

                            <p><?= $video['author'] ?></p>

                            <a href="<?= site_url('videos/'.$video['slug']).'/' ?>" class="btn btn-primary margin-top-md"><?= lang('playVideo') ?></a>

                        </figcaption>

                    </figure>


                </div>

            <?php if($x %2 == 1): ?>
            </div>
            <?php
            endif;
            $x++;
            endforeach;
            ?>
        </div>

    </div>

    <?php if(!isset($latest_videos)): ?>
    <div class="row">
        <div class="col-md-12 text-center">
            <?= $this->pagination->create_links(); ?>
        </div>
    </div>
    <?php endif; ?>

</div>












</div>
</div>
</div><!--==== End Row =====-->

</div><!--==== End Container =====-->

</div><!--==== End Component =====-->