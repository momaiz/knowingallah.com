<div class="main-section text-center">
    
    <div class="container">
        
        <div class="row">
            
            <div class="col-md-3 col-sm-3 col-xs-12">   
                <div class="thumbnail no-margin">
                    <img src="<?= base_url('assets/images/whoIsAllah.png') ?>" class="img-responsive img-circle align-center">
                    <div class="caption">
                        <h3><?= lang('whoIsAllah') ?></h3>
                        <p><?= lang('whoIsAllahDesc') ?></p>

                        <a href="http://knowingallah.com/ar/articles/category/1/" class="btn btn-primary btn-block"><?= lang('browse') ?></a>

                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">   
                <div class="thumbnail no-margin">
                    <img src="<?= base_url('assets/images/existence-god.png') ?>" class="img-responsive img-circle align-center">
                    <div class="caption">
                        <h3><?= lang('existenceGod') ?></h3>
                        <p><?= lang('existenceGodDesc') ?></p>

                        <a href="http://knowingallah.com/ar/articles/category/3/" class="btn btn-primary btn-block"><?= lang('browse') ?></a>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">   
                <div class="thumbnail no-margin">
                    <img src="<?= base_url('assets/images/god-and-man.png') ?>" class="img-responsive img-circle align-center">
                    <div class="caption">
                        <h3><?= lang('godAndMan') ?></h3>
                        <p><?= lang('godAndManDesc') ?></p>

                        <a href="http://knowingallah.com/ar/articles/category/4/" class="btn btn-primary btn-block"><?= lang('browse') ?></a>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">   
                <div class="thumbnail no-margin">
                    <img src="<?= base_url('assets/images/my-son-atheist.png') ?>" class="img-responsive img-circle align-center">
                    <div class="caption">
                        <h3><?= lang('sonAtheist') ?></h3>
                        <p><?= lang('sonAtheistDesc') ?></p>

                        <a href="http://knowingallah.com/ar/articles/category/579/" class="btn btn-primary btn-block"><?= lang('browse') ?></a>
                        
                    </div>
                </div>
            </div>


        </div>

    </div>

</div>