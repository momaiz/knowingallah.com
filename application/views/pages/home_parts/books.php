<?php

/**
 * Copyright (c) 2017 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author   Mohammed Zayan <zayanit@gmail.com>
 * @link     http://www.zayanit.com
 * @version  1.0
 * Created by PhpStorm
 */

?>

<div class="books">
    <div class="section-title">
        <h3><?=lang('books')?></h3>
        <a href="<?=site_url('books').'/'?>"><?=lang('others')?></a>
    </div>
    <ul class="list-unstyled no-margin clearfix">
        <?php foreach ($books as $book) : ?>
            <li class="col-md-3 col-sm-3 col-xs-12 col-padding-5">
                <div class="card book-item">
                    <div class="book-img">
                        <?php if ($book['photo'] != '') : ?>
                            <a href="<?=site_url('books').'/'.$book['slug']?>">
                                <img src="<?=$book['photo']?>" class="img-responsive align-center" width="181" height="197" alt="<?=$book['name']?>">
                            </a>
                        <?php else : ?>
                            <a href="<?=site_url('books').'/'.$book['slug']?>">
                                <img src="<?=base_url('assets/images/book-sample.png')?>" class="img-responsive align-center" width="181" height="197" alt="<?=$book['name']?>">
                            </a>
                        <?php endif; ?>
                    </div>
                    <div class="desc">
                        <h4>
                            <a href="<?=site_url('books').'/'.$book['slug']?>" title="<?=$book['name']?>">
                                <?=$book['name']?>
                            </a>
                        </h4>
                        <?php if ($book['author'] != '') : ?>
                            <div class="author"><i class="fa fa-user"></i><?=$book['author']?></div>
                        <?php else : ?>
                            <div class="author"><i class="fa fa-user"></i><?=lang('siteTeam')?></div>
                        <?php endif; ?>
                        <span><i class="fa fa-clock-o"></i><?=changeDateFormate($book['creation_date'])?></span>
                        <span><i class="fa fa-eye"></i><?=$book['hits']?></span>
                    </div>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>