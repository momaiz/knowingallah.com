<?php

/**
 * Copyright (c) 2016 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author   Mohammed Zayan <zayanit@gmail.com>
 * @link     http://www.zayanit.com
 * @version  1.0
 * Created by PhpStorm
 */
 
?>

<div class="questions-about-allah">
    <div class="section-title">
        <h3><?=lang('questionsAboutGod')?></h3>
        <a href="<?=site_url('articles/category/198').'/'?>"><?=lang('others')?></a>
    </div>
    <ul class="list-unstyled no-margin">
        <?php foreach ($questionsAboutGod as $article) : ?>
            <li class="col-md-6 col-sm-6 col-xs-12 col-padding-5">
                <div class="card question-item">
                    <div class="media">
                        <div class="media-left">
                            <?php
                                if ($article['article_photo'] != '') :
                                    $article_photos = explode(',', $article['article_photo']);
                            ?>
                                    <img src="<?=$article_photos[3]?>" class="media-object" width="60" height="70" alt="<?=$article['title']?>">
                            <?php else : ?>
                                <img src="<?=base_url('assets/images/allah-word.png')?>" class="media-object" width="60" height="60" alt="<?=$article['title']?>">
                            <?php endif; ?>
                        </div>
                        <div class="media-body">
                            <h4>
                                <a href="<?=site_url('articles').'/'.$article['slug'].'/'?>" title="<?=$article['title']?>">
                                    <?=$article['title']?>
                                </a>
                            </h4>
                            <div class="author">
                                <i class="fa fa-user"></i>
                                <?=$article['author'] != '' ? $article['author'] : lang('siteTeam')?>
                            </div>
                            <span><i class="fa fa-clock-o"></i><?=changeDateFormate($article['creation_date'])?></span>
                            <span><i class="fa fa-eye"></i><?=$article['hits']?></span>
                        </div>
                    </div>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>