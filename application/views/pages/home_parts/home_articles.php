<?php

/**
 * Copyright (c) 2016 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author   Mohammed Zayan <zayanit@gmail.com>
 * @link     http://www.zayanit.com
 * @version  1.0
 * Created by PhpStorm
 */

?>

<div class="home-articles">
    <div class="section-title">
        <h3><?=$current_lang_id == 1 ? lang('homeArticles') : lang('articles')?></h3>
        <a href="<?=site_url('articles/category').'/'?>"><?=lang('others')?></a>
    </div>
    <ul class="list-unstyled no-margin">
        <?php foreach ($special_articles as $article) : ?>
            <li>
                <div class="card home-article">
                    <div class="media">
                        <div class="media-left">
                            <a href="<?=site_url('articles').'/'.$article['slug'].'/'?>">
                                <?php
                                if ($article['article_photo'] != '') :
                                    $article_photos = explode(',', $article['article_photo']);
                                ?>
                                    <img src="<?=$article_photos[3]?>" alt="<?=$article['title']?>" width="70" height="70" class="media-object">
                                <?php else : ?>
                                    <img src="<?=base_url('assets/images/home-article-thum.png')?>" alt="<?=$article['title']?>" width="70" height="70" class="media-object">
                                <?php endif; ?>
                            </a>
                        </div>
                        <div class="media-body">
                            <h4>
                                <a href="<?=site_url('articles').'/'.$article['slug'].'/'?>" title="<?=$article['title']?>">
                                    <?=$article['title']?>
                                </a>
                            </h4>
                            <div class="author">
                                <i class="fa fa-user"></i>
                                <?=$article['author'] != '' ? $article['author'] : lang('siteTeam')?>
                            </div>
                            <?php if ($article['description'] != '') : ?>
                                <p><?=$article['description']?></p>
                            <?php else : ?>
                                <p><?=substr_word_end($article['content'], 0, 115)?></p>
                            <?php endif; ?>
                            <span><i class="fa fa-clock-o"></i><?=changeDateFormate($article['creation_date'])?></span>
                            <span><i class="fa fa-eye"></i><?=$article['hits']?></span>
                        </div>
                    </div>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
</div>
<?php $this->load->view("partials/mobile_pre_footer"); ?>
</div>
</div>
</main>