<div class="videos-sounds">
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="section-title">
                <h3><?=$current_lang_id == 1 ? lang('homeVideos') : lang('videos')?></h3>
                <a href="<?=site_url('videos/category').'/'?>"><?=lang('others')?></a>
            </div>
            <div class="home-videos">
                <div class="card">
                        <div class="big-video">
                            <div class="video-img">
                                <?php if ($latest_videos[0]['youtube_code'] != '') : ?>
                                    <img src="https://img.youtube.com/vi/<?=$latest_videos[0]['youtube_code']?>/0.jpg" class="img-responsive align-center" width="400" height="220" alt="<?=$latest_videos[0]['title']?>">
                                <?php else : ?>
                                    <img src="<?=base_url('assets/images/video-big-img.jpg')?>" class="img-responsive align-center" width="400" height="220" alt="<?=$latest_videos[0]['title']?>">
                                <?php endif; ?>
                                <div class="play-icon">
                                    <a href="<?=site_url('videos').'/'.$latest_videos[0]['slug'].'/'?>" class="fa fa-play"></a>
                                </div>
                            </div>
                            <div class="desc">
                                <h4>
                                    <a href="<?=site_url('videos').'/'.$latest_videos[0]['slug'].'/'?>" title="<?=$latest_videos[0]['title']?>">
                                        <?=$latest_videos[0]['title']?>
                                    </a>
                                </h4>
                                <div class="author">
                                    <i class="fa fa-user"></i>
                                    <?=$latest_videos[0]['author'] != '' ? $latest_videos[0]['author'] : lang('siteTeam')?>
                                </div>
                                <span><i class="fa fa-clock-o"></i><?=changeDateFormate($latest_videos[0]['creation_date'])?></span>
                                <span><i class="fa fa-eye"></i><?=$latest_videos[0]['hits']?></span>
                            </div>
                        </div>
                    <div class="small-videos-list">
                        <ul class="list-unstyled no-margin">
                            <?php foreach (array_slice($latest_videos, 1) as $video) : ?>
                                <li>
                                    <div class="small-video-item">
                                        <div class="media">
                                            <div class="media-left">
                                                <?php if ($video['youtube_code'] != '') : ?>
                                                    <img src="https://img.youtube.com/vi/<?=$video['youtube_code']?>/0.jpg" class="media-object" width="90" height="67" alt="<?=$video['title']?>">
                                                <?php else : ?>
                                                    <img src="<?=base_url('assets/images/default-sm-video.png')?>" class="media-object" width="90" height="67" alt="<?=$video['title']?>">
                                                <?php endif; ?>
                                                <div class="play-icon">
                                                    <a href="<?=site_url('videos').'/'.$video['slug'].'/'?>" class="fa fa-play"></a>
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <h4>
                                                    <a href="<?=site_url('videos').'/'.$video['slug'].'/'?>" title="<?=$video['title']?>">
                                                        <?=$video['title']?>
                                                    </a>
                                                </h4>
                                                <div class="author">
                                                    <i class="fa fa-user"></i>
                                                    <?=$video['author'] != '' ? $video['author'] : lang('siteTeam')?>
                                                </div>
                                                <span><i class="fa fa-clock-o"></i><?=changeDateFormate($video['creation_date'])?></span>
                                                <span><i class="fa fa-eye"></i><?=$video['hits']?></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="section-title">
                <h3><?=$current_lang_id == 1 ? lang('homeAudios') : lang('audios')?></h3>
                <a href="<?=site_url('audios/category').'/'?>"><?=lang('others')?></a>
            </div>
            <div class="card home-sounds">
                <ul class="list-unstyled no-margin">
                    <?php foreach ($latestAudios as $audio) : ?>
                        <li>
                            <div class="home-sound-item">
                                <div class="card">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="<?=site_url('audios').'/'.$audio['slug'].'/'?>"><i class="fa fa-play media-object"></i></a>
                                        </div>
                                        <div class="media-body">
                                            <h4>
                                                <a href="<?=site_url('audios').'/'.$audio['slug'].'/'?>" title="<?=$audio['title']?>">
                                                    <?=$audio['title']?>
                                                </a>
                                            </h4>
                                            <div class="author">
                                                <i class="fa fa-user"></i>
                                                <?=$audio['author'] != '' ? $audio['author'] : lang('siteTeam')?>
                                            </div>
                                            <span>
                                                <i class="fa fa-clock-o"></i>
                                                <?=changeDateFormate($audio['creation_date'])?>
                                            </span>
                                            <span><i class="fa fa-eye"></i><?=$audio['hits']?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>