<div class="top-slider pos-relative"><!--==== Start Component =====-->
    
    <div class="container"><!--==== Start Container =====-->
        
        <div class="row"><!--==== Start Row =====-->
                
            <div class="col-md-12">
                
                <div id="component-slider" class="carousel slide compo-slide" data-ride="carousel">
                    
                    <div class="slider-control">
                                                                
                        <a class="left carousel-control" href="#component-slider" role="button" data-slide="prev">

                            <span class="fa fa-angle-right" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>

                        </a>
                                                                    
                        <a class="right carousel-control" href="#component-slider" role="button" data-slide="next">

                            <span class="fa fa-angle-left" aria-hidden="true"></span>
                             <span class="sr-only">Next</span>

                        </a>

                    </div>

                    <div class="carousel-inner" role="listbox">
                        <?php
                        $n = 0;
                        foreach ($special_articles as $article):
                            if($n %4 == 0):
                        ?>
                        <div class="item<?= $n == 0 ? ' active' : '' ?>">

                            <ul class="list-unstyled">
                        <?php endif; ?>
                                <li class="col-md-6 col-xs-6">
                                    <a href="<?= site_url('articles').'/'.$article['slug'].'/' ?>">
                                        <figure>
                                            <?php 
                                            if($article['article_photo'] != ''): 
                                                $article_photos = explode(',', $article['article_photo']);
                                            ?>
                                            <img src="<?= $article_photos[0] ?>" class="img-responsive" alt="<?= $article['title'] ?>">
                                            <?php else: ?>
                                                <img src="<?= base_url('assets/images/article_photo.jpg') ?>" class="img-responsive" alt="<?= $article['title'] ?>">
                                            <?php endif; ?>
                                            <figcaption>
                                                <span><i class="fa fa-clock"></i><?= changeDateFormate($article['creation_date']) ?></span>
                                                <h3><?= $article['title'] ?></h3>
                                                <p><?= substr_word_end($article['content'], 0, 180) ?></p>
                                            </figcaption>
                                        </figure>
                                    </a>
                                </li>
                        <?php if($n %4 == 3): ?>
                            </ul>
                        </div>
                        <?php
                            endif;
                            $n++;
                        endforeach;
                        ?>
                        
                    </div>
    
                </div><!--===== End Carousel-Slider ========-->

            </div>  



        </div><!--==== End Row =====-->

    </div><!--==== End Container =====-->

</div><!--==== End Component =====-->