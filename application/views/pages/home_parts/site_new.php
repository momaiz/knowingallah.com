<div class="col-md-9 col-sm-8 col-xs-12">
    <div class="latest-added">
        <div class="section-title">
            <h3><?=lang('site_new')?></h3>
            <a href="<?=site_url('latest').'/'?>"><?=lang('others')?></a>
        </div>
        <div class="card">
            <div class="row">
                <?php
                switch ($site_new[0]['tp']) {
                    case 'articles/article' :
                        if ($site_new[0]['photo'] != '') {
                            $article_photos = explode(',', $site_new[0]['photo']);
                            $figureImg      = $article_photos[1];
                        } else {
                            $figureImg = base_url('assets/images/home-article-thum.png');
                        }
                        $modle = 'articles';
                        break;
                    case 'videos/video' :
                        if ($site_new[0]['photo'] != '') {
                            $figureImg = "https://img.youtube.com/vi/".$site_new[0]['photo']."/0.jpg";
                        } else {
                            $figureImg = base_url('assets/images/default-sm-video.png');
                        }
                        $modle = 'videos';
                        break;
                    case 'audios/audio':
                        $figureImg = base_url('assets/images/home-sound-thum.png');
                        $modle = 'audios';
                        break;
                }
                ?>
                <div class="col-md-5 col-sm-6 col-xs-12">
                    <div class="big-thum">
                        <img src="<?=$figureImg?>" class="img-responsive align-center" width="318" height="185" alt="<?=$site_new[0]['title']?>">
                        <?php if (isset($modle) && $modle == 'videos') : ?>
                        <div class="play-icon">
                            <a href="<?=site_url($modle."/".$site_new[0]['slug']).'/'?>" class="fa fa-play"></a>
                        </div>
                        <?php endif; ?>
                        <h4>
                            <a href="<?=site_url($modle."/".$site_new[0]['slug']).'/'?>">
                                <?=$site_new[0]['title']?>
                            </a>
                        </h4>
                        <div class="author">
                            <i class="fa fa-user"></i>
                            <?=$site_new[0]['author'] != '' ? $site_new[0]['author'] : lang('siteTeam')?>
                        </div>
                        <?php if ($site_new[0]['description'] != '') : ?>
                            <p><?=substr($site_new[0]['description'], 0, 50).' ...'?></p>
                        <?php endif; ?>
                        <span><i class="fa fa-clock-o"></i><?=changeDateFormate($site_new[0]['creation_date'])?></span>
                        <span><i class="fa fa-eye"></i><?=$site_new[0]['hits']?></span>
                    </div>
                </div>
                <div class="col-md-7 col-sm-6 col-xs-12">
                    <ul class="list-unsryled no-margin">
                        <?php
                        foreach (array_slice($site_new, 1) as $row) :
                        switch ($row['tp']) {
                            case 'articles/article' :
                                if ($row['photo'] != '') {
                                    $article_photos = explode(',', $row['photo']);
                                    $img            = $article_photos[1];
                                } else {
                                    $img = base_url('assets/images/home-article-thum.png');
                                }
                                $modle = 'articles';
                            break;
                            case 'videos/video' :
                                if ($row['photo'] != '')
                                    $img = 'http://img.youtube.com/vi/'.$row['photo'].'/1.jpg';
                                else
                                    $img = base_url('assets/images/default-sm-video.png');
                                $modle = 'videos';
                            break;
                            case 'audios/audio' :
                                $img   = base_url('assets/images/home-sound-thum.png');
                                $modle = 'audios';
                            break;
                        }
                        ?>
                        <li>
                            <div class="new-small-added-item">
                                <div class="media">
                                    <div class="media-left">
                                        <a href="<?=site_url($modle."/".$row['slug']).'/'?>">
                                            <img src="<?=$img?>" class="media-object" width="60" height="60" alt="<?=$row['title']?>">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4>
                                            <a href="<?=site_url($modle."/".$row['slug']).'/'?>">
                                                <?= $row['title'] ?>
                                            </a>
                                        </h4>
                                        <div class="author">
                                            <i class="fa fa-user"></i>
                                            <?=$row['author'] != '' ? $row['author'] : lang('siteTeam')?>
                                        </div>
                                        <span><i class="fa fa-clock-o"></i><?=changeDateFormate($row['creation_date'])?></span>
                                        <span><i class="fa fa-eye"></i><?=$row['hits']?></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>