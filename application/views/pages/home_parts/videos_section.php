<div class="row section-three">
    <div class="col-md-12">
        <h3 class="margin-bottom-md"><?= lang('videos') ?></h3>
        <div class="stripe-line"></div>
        <div class="clearfix"></div>

        <div class="well bg-white">
            <div class="media">

                <div class="media-left">
                    <a href="<?= site_url('videos').'/'.$latest_videos[0]['slug'].'/' ?>">
                    <?php if($latest_videos[0]['youtube_code'] != ''): ?>
                        <img src="http://img.youtube.com/vi/<?= $latest_videos[0]['youtube_code'] ?>/0.jpg" class="media-object" alt="<?= $latest_videos[0]['title'] ?>">
                    <?php else: ?>
                        <img src="<?= base_url('assets/images/videoPlayer.png') ?>" class="media-object" alt="<?= $latest_videos[0]['title'] ?>">
                    <?php endif; ?>
                    </a>
                </div>

                <div class="media-body">
                    <h4 class="media-heading"><a href="<?= site_url('videos').'/'.$latest_videos[0]['slug'].'/' ?>"><?= $latest_videos[0]['title'] ?></a></h4>
                    <p><?= $latest_videos[0]['description'] ?></p>
                    <a href="<?= site_url('videos').'/'.$latest_videos[0]['slug'].'/' ?>" class="btn btn-primary"><?= lang('watchVideo') ?></a>
                </div>

            </div>
        </div>

        <div class="well sm-videos">
            <ul class="list-unstyled no-margin no-padding">
                <?php foreach (array_slice($latest_videos, 1) as $video): ?>
                <li class="col-md-6 col-xs-6">
                    <div class="media">
                        <div class="media-left">
                            <a href="<?= site_url('videos').'/'.$video['slug'].'/' ?>">
                            <?php if($video['youtube_code'] != ''): ?>
                                <img src="http://img.youtube.com/vi/<?= $video['youtube_code'] ?>/0.jpg" class="media-object" alt="<?= $video['title'] ?>">
                            <?php else: ?>
                                <img src="<?= base_url('assets/images/videoPlayer.png') ?>" class="media-object" alt="<?= $video['title'] ?>">
                            <?php endif; ?>
                                <i class="fa fa-play"></i>
                            </a>
                        </div>
                        <div class="media-body">
                            <h5 class="media-heading"></h5>
                            <p><a href="<?= site_url('videos').'/'.$video['slug'].'/' ?>"><?= $video['title'] ?></a></p>
                            <span><i class="fa fa-clock-o"></i><?= changeDateFormate($video['creation_date']) ?></span>
                        </div>
                    </div>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>

</div>

</div>