<div class="col-md-4 col-xs-12 main-sidebar">
    <div class="sm-banner">

        <?php if(!empty($side_banner)): ?>
            <a href="<?= $side_banner['banner_link'] ?>">
                <img src="<?= $side_banner['banner_photo'] ?>" class="img-responsive" alt="banner-img">
            </a>
        <?php else: ?>
            <a href="">
                <img src="http://placehold.it/360x250" class="img-responsive" alt="banner-img">
            </a>
        <?php endif; ?>
    </div>

    <div class="sm-video">
        <h3><?= $home_video['video_title'] ?></h3>
        <div class="stripe-line"></div>
        <div class="clearfix"></div>
        <iframe src="https://www.youtube.com/embed/<?= $home_video['youtube_code'] ?>?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>

    <div class="new-muslims">
        <h3><a href="<?= site_url('articles/category/69').'/' ?>"><?= lang('returnersToAllah') ?></a></h3>
        <div class="stripe-line"></div>
        <div class="clearfix"></div>

        <div id="new-muslims" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <?php
                $n = 1;
                foreach ($islamStory as $article):
                ?>
                <div class="item<?= $n == 1 ? ' active' : '' ?>">
                  <a href="<?= site_url('articles').'/'.$article['slug'].'/' ?>">
                    <?php
                    if($article['article_photo'] != ''):
                        $article_photos = explode(',', $article['article_photo']);
                    ?>
                        <img src="<?= $article_photos[1] ?>" alt="<?= $article['title'] ?>">
                    <?php else: ?>
                        <img src="<?= base_url('assets/images/article_photo_relat.jpg') ?>" alt="<?= $article['title'] ?>">
                    <?php endif; ?>
                  </a>
                  <div class="carousel-caption">
                    <p><a href="<?= site_url('articles').'/'.$article['slug'].'/' ?>"><?= $article['title'] ?></a></p>
                  </div>
                </div>
                <?php
                    $n++;
                endforeach;
                ?>
            </div>

            <a class="left carousel-control" href="#new-muslims" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#new-muslims" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>

        </div>

    </div>

    <div class="most-visited">

        <h3><?= lang('mostVisited') ?></h3>
        <div class="stripe-line"></div>
        <div class="clearfix"></div>

        <ul class="list-unstyled no-margin no-padding">
            <?php foreach ($most_viwed_articles as $article): ?>
            <li>
                <div class="media">
                    <div class="media-left">
                        <a href="<?= site_url('articles').'/'.$article['slug'].'/' ?>">
                        <?php
                        if($article['article_photo'] != ''):
                            $article_photos = explode(',', $article['article_photo']);
                        ?>
                        <img src="<?= $article_photos[3] ?>" alt="<?= $article['title'] ?>" class="media-boject">
                        <?php else: ?>
                            <img src="<?= base_url('assets/images/article_photo_last.jpg') ?>" alt="<?= $article['title'] ?>" class="media-boject">
                        <?php endif; ?>
                        </a>
                    </div>
                    <div class="media-body">
                        <a href="<?= site_url('articles').'/'.$article['slug'].'/' ?>" class="media-heading"><h4><?= $article['title'] ?></h4></a>
                        <p>
                            <span style="margin-right: 6px;"><i style="margin-left: 4px;" class="fa fa-clock-o"></i><?= changeDateFormate($article['creation_date']) ?></span>
                            <span><i style="margin-right: 6px; margin-left: 6px;" class="fa fa-eye"></i><?= $article['hits'] ?></span>
                        </p>
                    </div>
                </div>
            </li>
            <?php endforeach; ?>
        </ul>

    </div>

    <div class="sounds">

        <h3><?= lang('followUs') ?></h3>
        <div class="stripe-line"></div>
        <div class="clearfix"></div>

        <?php
        if($current_lang_abbr == 'ar')
            $facebook = 'https://www.facebook.com/Knowing.Allah';
        else
            $facebook = 'https://www.facebook.com/KnowingAllah.en';
        ?>
        <div class="fb-page" data-href="<?= $facebook ?>" data-tabs="timeline" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="<?= $facebook ?>"><a href="<?= $facebook ?>">Facebook</a></blockquote></div></div>

    </div>

</div>

</div>



</div>

</main>