<?php
switch($page) {
    case 'search' :
        $mainPage = 'search';
        break;
    case 'site_new' :
        $mainPage = 'site_new';
        $siteNewClass = ' site-new';
        break;
    case 'gallery' :
        $mainPage = 'image_gallery';
        break;
    default :
        $mainPage = 'custom_page';
        break;
}
?>

<div class="col-md-9 col-sm-8 col-xs-12">

<?php $this->load->view('pages/sub_pages/'.$mainPage); ?>

</div>
</div>
</div>