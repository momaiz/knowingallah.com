<div class="col-md-9 col-sm-8 col-xs-12">
<ol class="breadcrumb">
    <li><a href="<?=site_url().'/'?>"><i class="fa fa-home"></i><?=lang('homepage')?></a></li>
    <li><a href="<?=site_url('articles/category').'/'?>"><?=lang('articles')?></a></li>
    <li><?=$cat_name?></li>
</ol>
<?php if ($query_other_languages || $query_sub_cats) : ?>
<div class="card section-info">
    <?php if ($query_other_languages): ?>
    <div class="translated-to">
        <span><?=lang('translated_to_other_langs')?></span>
        <button type="button" class="btn dropdown-button" href="#" data-activates="dropdown2">
            <?=lang('catTranslation')?>
            <i class="fa fa-angle-down"></i>
        </button>
        <!-- Dropdown Structure -->
        <ul id="dropdown2" class="dropdown-content">
            <?php foreach ($query_other_languages as $row): ?>
                <li>
                    <a hreflang="<?=$row['code']?>" href="<?=site_url("../".$row['code']."/articles/category/".$categoryId).'/'?>">
                        <?=$row['description']?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <?php
    endif;
    if ($query_sub_cats):
    ?>
    <div class="translated-to">
        <span><?=lang('sub_cats')?></span>
        <button type="button" class="btn dropdown-button" href="#" data-activates="dropdown3">
            <?=lang('sub_cats')?>
            <i class="fa fa-angle-down"></i>
        </button>
        <!-- Dropdown Structure -->
        <ul id="dropdown3" class="dropdown-content">
            <?php foreach ($query_sub_cats as $row): ?>
                <li>
                    <a hreflang="<?=$current_lang_abbr?>" href="<?=site_url("articles/category/".$row['id']).'/'?>" class="btn">
                        <?=$row['description']?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <?php endif; ?>
</div>
<?php endif; ?>
<ul class="list-unstyled no-margin clearfix">
    <?php foreach ($query_articles as $article) : ?>
    <li>
        <div class="card home-article inner">
            <div class="media">
                <div class="media-left">
                    <a href="<?=site_url('articles').'/'.$article['slug'].'/'?>">
                        <?php
                        if($article['article_photo'] != ''):
                            $article_photos = explode(',', $article['article_photo']);
                        ?>
                            <img src="<?=$article_photos[3]?>" alt="<?=$article['title']?>" class="media-object" width="70" height="70">
                        <?php else : ?>
                            <img src="<?=base_url('assets/images/site-new-article-img.png')?>" alt="<?=$article['title']?>" class="media-object">
                        <?php endif; ?>
                    </a>
                </div>
                <div class="media-body">
                    <h4>
                        <a href="<?=site_url('articles').'/'.$article['slug'].'/'?>" title="<?=$article['title']?>">
                            <?=$article['title']?>
                        </a>
                    </h4>
                    <div class="author">
                        <i class="fa fa-user"></i>
                        <?=$article['author'] != '' ? $article['author'] : lang('siteTeam')?>
                    </div>
                    <?php if ($article['description'] != '') : ?>
                        <p><?=$article['description']?></p>
                    <?php else : ?>
                        <p><?=substr_word_end($article['content'], 0, 115)?></p>
                    <?php endif; ?>
                    <span><i class="fa fa-clock-o"></i><?=changeDateFormate($article['creation_date'])?></span>
                    <span><i class="fa fa-eye"></i><?=$article['hits']?></span>
                </div>
            </div>
        </div>
    </li>
    <?php endforeach; ?>
</ul>
<div class="text-center">
    <?=$this->pagination->create_links();?>
</div>
</div>

<?php $this->load->view('partials/side_menus/articles_mobile_side_menu'); ?>

</div>
</div>
</main>