<div class="col-md-9 quran-way">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">



                    <h4 class="no-margin-top"><?= $page_title ?></h4>

                    <div class="gridMe">
                        <?php foreach($articles as $article): ?>
                            <div class="col-md-4">
                                <div class="art-item">
                                    <h4><?= mb_substr(strip_tags($article['title']), 0, 25, 'UTF-8') ?></h4>
                                    <p>
                                        <?= mb_substr(strip_tags($article['content']), 0, 130, 'UTF-8') ?>....
                                    </p>
                                    <a href="<?= site_url("articles/".$article['slug']).'/' ?>" class="pull-right">
                                        <?= lang('others') ?>
                                    </a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <?php if(!isset($latest_articles)): ?>
                    <div class="pagination-wrap">
                        <div class="text-center">
                            <?= $this->pagination->create_links(); ?>
                        </div>
                    </div>
                    <?php endif; ?>







                </div>
            </div>
        </div><!--==== End Row =====-->
    </div><!--==== End panel-body ====-->

</div><!--==== End Row =====-->
</div><!--==== End Container =====-->
</div><!--==== End Component =====-->