<div class="col-md-9 col-sm-8 col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?=site_url().'/'?>"><i class="fa fa-home"></i><?=lang('homepage')?></a></li>
        <li><a href="<?=site_url('articles/category').'/'?>"><?=lang('articles')?></a></li>
        <li><a href="<?= site_url('articles/category').'/'.$article_row['article_cat_id'].'/' ?>"><span itemprop="articleSection"><?= $article_row['category'] ?></span></a></li>
        <li><?= $article_row['title'] ?></li>
    </ol>
    <div class="card view-item">
        <div class="article">
            <?php
            if($article_row['article_photo'] != ''):
                $article_photos = explode(',', $article_row['article_photo']);
            ?>
                <img src="<?=$article_photos[0]?>" title="<?=$article_row['title']?>" class="img-responsive align-center" width="640" height="330" alt="<?=$article_row['title']?>">
            <?php endif; ?>
        </div>
        
        <article class="article-typo islamic-typography">
            <div class="item-title">
              <h1><?=$article_row['title']?></h1>
                <div class="author">
                    <i class="fa fa-user"></i>
                    <?=$article_row['author'] != '' ? $article_row['author'] : lang('siteTeam')?>
                </div>
            </div>
            <?=$article_row['content']?>
        </article>
        <div class="share-item clearfix">
            <div class="info">
                <span><i class="fa fa-clock-o"></i><?=changeDatePublished($article_row['creation_date'])?></span>
                <span><i class="fa fa-eye"></i><?=$article_row['hits']?></span>
            </div>
            <div class="share">
                <h5><?=lang('shareArticle')?> :</h5>
                <button class="btn-sm-full fb-bg btn-rounded waves-effect waves-light" onclick="shareFunction('http://www.facebook.com/share.php?u=http://<?= $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>&title=<?= $article_row['title'] ?>')">
                    <i class="fa fa-facebook"> </i> <span>Facebook</span>
                </button>
                <button class="btn-sm-full tw-bg btn-rounded waves-effect waves-light" onclick="shareFunction('http://twitter.com/home?status=<?= $article_row['title'].'+http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>')">
                    <i class="fa fa-twitter"> </i> <span>Twitter</span>
                </button>
                <button class="btn-sm-full gplus-bg btn-rounded waves-effect waves-light" onclick="shareFunction('https://plus.google.com/share?url=http://<?= $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>')">
                    <i class="fa fa-google-plus"> </i> <span>Google +</span>
                </button>
            </div>
        </div>
    </div>
    <div class="section-title">
        <h3><?=lang('relatedArticles').' '.$article_row['title']?></h3>
    </div>
    <ul class="list-unstyled no-margin clearfix">
        <?php foreach($related_articles as $article): ?>
            <li>
                <div class="card home-article inner">
                    <div class="media">
                        <div class="media-left">
                            <a href="<?=site_url('articles').'/'.$article['slug'].'/'?>">
                                <?php
                                if ($article['article_photo'] != '') :
                                    $article_photos = explode(',', $article['article_photo']);
                                ?>
                                    <img src="<?=$article_photos[3]?>" alt="<?=$article['title']?>" width="70" height="70" class="media-object">
                                <?php else : ?>
                                    <img src="<?=base_url('assets/images/home-article-thum.png')?>" alt="<?=$article['title']?>" width="70" height="70" class="media-object">
                                <?php endif; ?>
                            </a>
                        </div>
                        <div class="media-body">
                            <h4>
                                <a href="<?=site_url('articles').'/'.$article['slug'].'/'?>" title="<?=$article['title']?>">
                                    <?=$article['title']?>
                                </a>
                            </h4>
                            <div class="author">
                                <i class="fa fa-user"></i>
                                <?=$article['author'] != '' ? $article['author'] : lang('siteTeam')?>
                            </div>
                            <?php if ($article['description'] != '') : ?>
                                <p><?=$article['description']?></p>
                            <?php else : ?>
                                <p><?=substr_word_end($article['content'], 0, 115)?></p>
                            <?php endif; ?>
                            <span><i class="fa fa-clock-o"></i><?=changeDateFormate($article['creation_date'])?></span>
                            <span><i class="fa fa-eye"></i><?=$article['hits']?></span>
                        </div>
                    </div>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
</div>
</div>
</main>