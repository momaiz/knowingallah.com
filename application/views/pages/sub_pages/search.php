<div class="col-md-12">
    <div class="pathto">
        <ol class="breadcrumb list-inline">
            <li class="active"><?= lang('search') ?></li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-12">
            <style>
                #gsc-iw-id1{
                    height: 30px;
                    border-color: #898989  ;
                }
                .search-box, .search-box *, .search-box *:before, .search-box *:after {
                    -webkit-box-sizing: content-box;
                    -moz-box-sizing: content-box;
                    box-sizing: content-box;
                }
            </style>
            <div class="search-box">
                <script>
                    (function() {
                        var cx = '015497087736326010529:rbazlqhvg7i';
                        var gcse = document.createElement('script');
                        gcse.type = 'text/javascript';
                        gcse.async = true;
                        gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
                        '//www.google.com/cse/cse.js?cx=' + cx;
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(gcse, s);
                    })();
                </script>
                <gcse:search></gcse:search>
            </div>
            <br /><br />
        </div>
    </div>
</div>