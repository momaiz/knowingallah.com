<div class="col-md-12">
    <div class="pathto">
        <ol class="breadcrumb list-inline">
            <li class="active"><?= lang('photoalbum') ?></li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-12 center-text">
            <?php if(isset($gallery_photos)): ?>
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
                <script src="<?= base_url('assets/galleria/galleria-1.2.9.min.js'); ?>"></script>
                <style>
                    #galleria{ width: 800px; height: 500px; background: #172b5e; margin: auto; }
                </style>
                <div id="galleria">
                    <?php foreach($gallery_photos as $photo): ?>
                        <a href="<?= $photo['photo']; ?>"><img src="<?= $photo['photo_thumb']?>" alt="" /></a>
                    <?php endforeach; ?>
                </div>
                <script>
                    Galleria.loadTheme('<?php echo base_url(); ?>assets/galleria/themes/twelve/galleria.twelve.min.js');
                    Galleria.run('#galleria');
                </script>
            <?php endif; ?>
        </div>
    </div>
</div>