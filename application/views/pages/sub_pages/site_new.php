<ol class="breadcrumb">
  <li><a href="<?=site_url('home').'/'?>"><i class="fa fa-home"></i><?=lang('homepage')?></a></li>
  <li><?=lang('site_new')?></li>
</ol>
<ul class="list-unstyled no-margin clearfix">
  <?php
  foreach($site_new as $row):
    switch($row['tp'])
    {
      case 'articles/article':
        $modle   = 'articles';
        $catName = lang('articles');
        $link    = site_url('articles/category/1').'/';
        $photo   = base_url('assets/images/site-new-article-img.png');
        break;
      case 'audios/audio':
        $modle   = 'audios';
        $catName = lang('audios');
        $link    = site_url('audios/category/19').'/';
        $photo   = base_url('assets/images/site-new-audio-img.png');
        break;
      case 'videos/video':
        $modle   = 'videos';
        $catName = lang('videos');
        $link    = site_url('videos/category/23').'/';
        $photo   = base_url('assets/images/site-new-video-img.png');
        break;
    }
  ?>
  <li>
    <div class="card home-article inner">
      <div class="media">
        <div class="media-left">
          <a href="<?= site_url($modle.'/'.$row['slug']).'/' ?>">
            <img src="<?=$photo?>" alt="<?= $row['title'] ?>" class="media-object"></a>
        </div>
        <div class="media-body">
          <h4>
            <a href="<?= site_url($modle.'/'.$row['slug']).'/' ?>" title="<?= $row['title'] ?>">
              <?= $row['title'] ?>
            </a>
          </h4>
          <div class="author">
            <i class="fa fa-user"></i><?= $row['author'] ?>
          </div>
          <p><?=lang('category')?> :<a href="<?=$link?>"><?=$catName?></a></p> 
          <span><i class="fa fa-clock-o"></i><?=changeDateFormate($row['creation_date'])?></span> 
          <span><i class="fa fa-eye"></i><?=$row['hits']?></span> </div>
      </div>
    </div>
  </li>
  <?php endforeach; ?>
</ul>