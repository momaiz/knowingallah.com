<div class="col-md-9">
    <div class="jumbotron">
        <div class="well no-border">
            <h1><?= $page['title'] ?></h1>
            <p>
                <?= $page['content'] ?>
            </p>

        </div>
    </div>
</div>

<!-- sidebar -->
<div class="col-md-3 sidebar-content">

    <div class="articles-in-section">
        <h4><?= lang('mostWatched') ?></h4>
        <ul class="list-unstyled no-margin no-padding">
            <?php foreach($mostViewed as $article): ?>
            <li>
                <figure>
                    <a href="<?= site_url('articles').'/'.$article['slug'].'/' ?>">
                    <?php
                    if($article['article_photo'] != ''):
                        $article_photos = explode(',', $article['article_photo']);
                    ?>
                        <img src="<?= $article_photos[2] ?>" class="img-responsive" alt="<?= $article['title'] ?>">
                    <?php else: ?>
                        <img src="<?= base_url('assets/images/article_photo_sam.jpg') ?>" class="img-responsive" alt="<?= $article['title'] ?>">
                    <?php endif; ?>
                    </a>
                    <figcaption>
                        <p><a href="<?= site_url('articles').'/'.$article['slug'].'/' ?>"><?= $article['title'] ?></a></p>
                    </figcaption>
                </figure>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
 </div>