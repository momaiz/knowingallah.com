<?php

$this->load->view("partials/header");
$this->load->view("partials/navbar");
$this->load->view("partials/side_menu");
$this->load->view($main_content);
$this->load->view("partials/footer");