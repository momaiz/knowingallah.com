<?php

/**
 * Copyright (c) 2017 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @copyright     copyright 2017
 * @author        Mohammed Zayan <zayanit@gmail.com>
 * @link          http://www.zayanit.com
 * @version       1.0
 * Created by PhpStorm
 */

?>

<!doctype html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>معرفة الله  | علم وعَمل</title>
    <meta name="description" content="<?php echo lang('meta_description');?>"  />

    <!--=====Icon-Font======-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url('assets/css/font-awesome.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/plugins/owl.carousel/owl.carousel.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/mdb.css')?>">

    <!--=====Styles=====-->
    <link rel="stylesheet" href="<?=base_url('assets/css/style.css')?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!-- [if lt IE 9]>
        <script src="../assets/js/html5shiv-3.7.1.js"></script>
        <script src="../assets/js/respond-1.4.2.js"></script>
    <![5endif] -->

</head>

<body>

<div id="wrapper">
    <div class="intro">
        <div class="pattern-ovelary">
            <div class="container">
                <div class="caption">
                    <div class="logo">
                        <img src="<?=base_url('assets/images/white-logo.png')?>" class="img-responsive align-center" alt="logo image">
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
                            <div class="langs">
                                <a href="<?=base_url('jp').'/home/'?>" class="hoverable" data-toggle="tooltip" data-placement="bottom" title="日本の" data-original-title="日本の">
                                    <img src="<?=base_url('assets/images/lang-12.gif')?>" class="img-responsive" alt="日本の" width="30" height="25">
                                </a>
                                <a href="<?=base_url('hi').'/home/'?>" class="hoverable" data-toggle="tooltip" data-placement="bottom" title="हिन्दी" data-original-title="हिन्दी">
                                    <img src="<?=base_url('assets/images/lang-11.gif')?>" class="img-responsive" alt="हिन्दी" width="30" height="25">
                                </a>
                                <a href="<?=base_url('id').'/home/'?>" class="hoverable" data-toggle="tooltip" data-placement="bottom" title="Indonesia" data-original-title="Indonesia">
                                    <img src="<?=base_url('assets/images/lang-10.gif')?>" class="img-responsive" alt="Indonesia" width="30" height="25">
                                </a>
                                <a href="<?=base_url('ru').'/home/'?>" class="hoverable" data-toggle="tooltip" data-placement="bottom" title="Русский" data-original-title="Русский">
                                    <img src="<?=base_url('assets/images/lang-9.gif')?>" class="img-responsive" alt="Русский" width="30" height="25">
                                </a>
                                <a href="<?=base_url('fr').'/home/'?>" class="hoverable" data-toggle="tooltip" data-placement="bottom" title="Français" data-original-title="Français">
                                    <img src="<?=base_url('assets/images/lang-8.gif')?>" class="img-responsive" alt="Français" width="30" height="25">
                                </a>
                                <a href="<?=base_url('es').'/home/'?>" class="hoverable" data-toggle="tooltip" data-placement="bottom" title="Español" data-original-title="Español">
                                    <img src="<?=base_url('assets/images/lang-7.gif')?>" class="img-responsive" alt="Español" width="30" height="25">
                                </a>
                                <a href="<?=base_url('zh').'/home/'?>" class="hoverable" data-toggle="tooltip" data-placement="bottom" title="中文" data-original-title="中文">
                                    <img src="<?=base_url('assets/images/lang-6.gif')?>" class="img-responsive" alt="中文" width="30" height="25">
                                </a>
                                <a href="<?=base_url('it').'/home/'?>" class="hoverable" data-toggle="tooltip" data-placement="bottom" title="Italiano" data-original-title="Italiano">
                                    <img src="<?=base_url('assets/images/lang-5.gif')?>" class="img-responsive" alt="Italiano" width="30" height="25">
                                </a>
                                <a href="<?=base_url('de').'/home/'?>" class="hoverable" data-toggle="tooltip" data-placement="bottom" title="Deutsch" data-original-title="Deutsch">
                                    <img src="<?=base_url('assets/images/lang-4.gif')?>" class="img-responsive" alt="Deutsch" width="30" height="25">
                                </a>
                                <a href="<?=base_url('he').'/home/'?>" class="hoverable" data-toggle="tooltip" data-placement="bottom" title="Hebrew" data-original-title="Hebrew">
                                    <img src="<?=base_url('assets/images/lang-3.gif')?>" class="img-responsive" alt="Hebrew" width="30" height="25">
                                </a>
                                <a href="<?=base_url('en').'/home/'?>" class="hoverable" data-toggle="tooltip" data-placement="bottom" title="English" data-original-title="English">
                                    <img src="<?=base_url('assets/images/lang-2.gif')?>" class="img-responsive" alt="English" width="30" height="25">
                                </a>
                                <a href="<?=base_url('ar').'/home/'?>" class="hoverable" data-toggle="tooltip" data-placement="bottom" title="العربية" data-original-title="العربية">
                                    <img src="<?=base_url('assets/images/lang-1.gif')?>" class="img-responsive" alt="العربية" width="30" height="25">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="social">
                        <h1>follow us :</h1>
                        <?php
                        if($current_lang_abbr == 'ar' || 1==1)
                            $facebook = 'https://www.facebook.com/Knowing.Allah';
                        else
                            $facebook = 'https://www.facebook.com/KnowingAllah.en';
                        ?>
                        <a href="<?=$facebook?>" class="btn-sm fb-bg square waves-effect waves-light" target="_blank">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="https://twitter.com/knowingallah" class="btn-sm tw-bg square waves-effect waves-light" target="_blank">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="https://plus.google.com/+knowingAllah1/videos" class="btn-sm gplus-bg square waves-effect waves-light" target="_blank">
                            <i class="fa fa-google-plus"></i>
                        </a>
                        <a href="https://www.youtube.com/user/knowingAllah1" class="btn-sm yt-bg square waves-effect waves-light" target="_blank">
                            <i class="fa fa-youtube"></i>
                        </a>
                        <a href="https://www.instagram.com/KnowingAllah1/" class="btn-sm ins-bg square waves-effect waves-light" target="_blank">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?=base_url('assets/js/jquery.min.js')?>"></script>
<script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
<script src="<?=base_url('assets/js/mdb.js')?>"></script>
<script src="<?=base_url('assets/plugins/owl.carousel/owl.carousel.min.js')?>"></script>
<script src="<?=base_url('assets/js/scripts.0.0.1.js')?>"></script>
</body>
</html>
