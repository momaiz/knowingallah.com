<?php

/**
 * Copyright (c) 2017 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author   Mohammed Zayan <zayanit@gmail.com>
 * @link     http://www.zayanit.com
 * @version  1.0
 * Created by PhpStorm
 */
 
 ?>

<main class="main-content">
<div class="container">
<div class="row">

<div class="col-md-3 col-sm-4 col-xs-12 hidden-xs">
    <aside class="home-sidebar">
        <div class="side-menu">
            <h3><i class="fa fa-bars"></i><?=lang('menu')?></h3>
            <ul class="list-unstyled no-margin">
                <?php if (!empty($menu)) :
                    foreach ($menu as $item) :
                        if (isset($item->sub_menu)) : ?>
                            <li>
                                <a href="#"><?=$item->title?></a>
                                <ul class="list-unstyled no-margin">
                                    <?php foreach ($item->sub_menu as $sub_item) : ?>
                                        <li>
                                            <a href="<?=site_url($sub_item->link)?>">
                                                <?=$sub_item->title?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                        <?php else: ?>
                            <li>
                                <a href="<?=site_url($item->link)?>">
                                    <?=$item->title?>
                                </a>
                            </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
        </div>
        <?php
        switch ($this->uri->segment(1)) {
            case 'home':
                $this->load->view('partials/side_menus/home_menu');
            break;
            case 'articles':
                $this->load->view('partials/side_menus/articles_side_menu');
            break;
            case 'books':
                $this->load->view('partials/side_menus/books_side_menu');
            break;
            case 'videos':
                $this->load->view('partials/side_menus/videos_menu');
            break;
            case 'audios':
                $this->load->view('partials/side_menus/audios_menu');
            break;
            case 'latest':
                $this->load->view('partials/side_menus/latest_side_menu');
            break;
        }
        ?>
    </aside>
</div>