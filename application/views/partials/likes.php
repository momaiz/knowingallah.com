<?php

/**
 * Copyright (c) 2015 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author        Mohammed Zayan <admin@zayanit.com>
 * @link          http://www.zayanit.com
 * @version       1.0
 * Created by Sublime Text 2
 */

?>
<script>
function like_add(item_id, module, lang) {
	$.post('<?= site_url("add_like") ?>', {item_id: item_id, module: module, lang: lang, '<?= $this->security->get_csrf_token_name() ?>' : '<?= $this->security->get_csrf_hash() ?>'}, function(data){
		if (data == 'success') {
			likes_get(item_id, module, lang);
		} else {
			alert(data);
		}
	});
}
function likes_get(item_id, module, lang) {
	$.post('<?= site_url("likes_get") ?>', {item_id: item_id, module: module, lang: lang, '<?= $this->security->get_csrf_token_name() ?>' : '<?= $this->security->get_csrf_hash() ?>'}, function(data){
		$('#likes').text(data);
	});
}
function add_dislike(item_id, module, lang) {
	$.post('<?= site_url("add_dislike") ?>', {item_id: item_id, module: module, lang:lang, '<?= $this->security->get_csrf_token_name() ?>' : '<?= $this->security->get_csrf_hash() ?>'}, function(data){
		if (data == 'success') {
			dislikes_get(item_id, module, lang);
		} else {
			alert(data);
		}
	});
}
function dislikes_get(item_id, module, lang) {
	$.post('<?= site_url("dislikes_get") ?>', {item_id: item_id, module: module, lang:lang, '<?= $this->security->get_csrf_token_name() ?>' : '<?= $this->security->get_csrf_hash() ?>'}, function(data){
		$('#dislikes').text(data);
	});
}
</script>