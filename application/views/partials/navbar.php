<header class="main-header">
    <div class="top-header clearfix">
        <div class="container">
            <div class="social">
                <?php
                if($current_lang_abbr == 'ar')
                    $facebook = 'https://www.facebook.com/Knowing.Allah';
                else
                    $facebook = 'https://www.facebook.com/KnowingAllah.en';
                ?>
                <a href="<?=$facebook?>" class="fa fa-facebook" target="_blank"></a>
                <a href="https://twitter.com/knowingallah" class="fa fa-twitter" target="_blank"></a>
                <a href="https://plus.google.com/+knowingAllah1/videos" class="fa fa-google-plus" target="_blank"></a>
                <a href="https://www.youtube.com/user/knowingAllah1" class="fa fa-youtube" target="_blank"></a>
                <a href="https://www.instagram.com/KnowingAllah1/" class="fa fa-instagram" target="_blank"></a>
            </div>
            <div class="lang">
                <button type="button" class="btn dropdown-button" data-activates="dropdown1">
                    <i class="fa fa-flag"></i><?=lang('choose_lang')?>
                </button>
                <!-- Dropdown Structure -->
                <ul id="dropdown1" class="dropdown-content">
                    <li><a href="<?=base_url('ar').'/home/'?>">العربية</a></li>
                    <li><a href="<?=base_url('en').'/home/'?>">English</a></li>
                    <li><a href="<?=base_url('he').'/home/'?>">עברית</a></li>
                    <li><a href="<?=base_url('de').'/home/'?>">Deutsch</a></li>
                    <li><a href="<?=base_url('it').'/home/'?>">Italiano</a></li>
                    <li><a href="<?=base_url('zh').'/home/'?>">中文</a></li>
                    <li><a href="<?=base_url('es').'/home/'?>">Español</a></li>
                    <li><a href="<?=base_url('fr').'/home/'?>">Français</a></li>
                    <li><a href="<?=base_url('ru').'/home/'?>">Русский</a></li>
                    <li><a href="<?=base_url('id').'/home/'?>">Indonesia</a></li>
                    <li><a href="<?=base_url('hi').'/home/'?>">हिन्दी</a></li>
                    <li><a href="<?=base_url('jp').'/home/'?>">日本の</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>

<?php if ($this->uri->segment(1) == 'home') : ?>
<div class="main-slider">
    <div class="owl-carousel">
        <div class="item">
            <img src="<?=base_url('assets/images/slide-1.png')?>" class="img-responsive align-center" alt="slider imgae" width="1920" height="400">
        </div>
        <div class="item">
            <img src="<?=base_url('assets/images/slide-2.png')?>" class="img-responsive align-center" alt="slider imgae" width="1920" height="400">
        </div>
        <div class="item">
            <img src="<?=base_url('assets/images/slide-3.png')?>" class="img-responsive align-center" alt="slider imgae" width="1920" height="400">
        </div>
    </div>
</div>
<?php else : ?>
<div class="banner">
    <div class="pattern-ovelary">
        <div class="container">
            <div class="caption">
                <div class="logo">
                    <img src="<?=base_url('assets/images/white-logo.png')?>" class="img-responsive align-center" alt="logo image" width="221" height="250">
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<nav class="navbar main-nav">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <ul class="nav navbar-nav">
                <li class="<?php echo $this->uri->segment(1) == 'home' ? 'active' : ''; ?>">
                    <a href="<?=site_url('home').'/'?>" class="waves-effect">
                        <i class="fa fa-home"></i><?=lang('homepage')?>
                    </a>
                </li>
                <li class="dropdown <?php echo $this->uri->segment(1) == 'articles' ? 'active' : ''; ?>">
                    <a href="#" class="dropdown-toggle waves-effect" data-toggle="dropdown" role="button" aria-expanded="false">
                        <i class="fa fa-pencil"></i><?=lang('articles')?> <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <?php foreach ($articles_categories as $cat) : ?>
                            <li>
                                <a href="<?=site_url('articles/category').'/'.$cat['article_cat_id'].'/'?>">
                                    <?=$cat['description']?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
                <li class="<?php echo $this->uri->segment(1) == 'books' ? 'active' : ''; ?>">
                    <a href="<?=site_url('books').'/'?>" class="waves-effect">
                        <i class="fa fa-book"></i><?=lang('books')?>
                    </a>
                </li>
                <li class="dropdown <?php echo $this->uri->segment(1) == 'videos' ? 'active' : ''; ?>">
                    <a href="#" class="dropdown-toggle waves-effect" data-toggle="dropdown" role="button" aria-expanded="false">
                        <i class="fa fa-video-camera"></i><?=lang('videos')?><span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <?php foreach ($videosCategories as $category) : ?>
                        <li>
                            <a href="<?=site_url('videos/category').'/'.$category['video_cat_id'].'/'?>">
                                <?=$category['description']?>
                            </a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
                <li class="dropdown <?php echo $this->uri->segment(1) == 'audios' ? 'active' : ''; ?>">
                    <a href="#" class="dropdown-toggle waves-effect" data-toggle="dropdown" role="button" aria-expanded="false">
                        <i class="fa fa-microphone"></i><?=lang('audios')?><span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <?php foreach ($audiosCategories as $category) : ?>
                            <li>
                                <a href="<?=site_url('audios/category').'/'.$category['audio_cat_id'].'/'?>">
                                    <?=$category['description']?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
                <li class="<?php echo $this->uri->segment(1) == 'contact' ? 'active' : ''; ?>">
                    <a href="<?=site_url('contact').'/'?>" class="waves-effect">
                        <i class="fa fa-envelope"></i><?=lang('contactus')?>
                    </a>
                </li>
                <li width="365" height="50" align="right" valign="top" style="width: 285px; cursor: text; background-position: center top;">
                    <script>
                      (function() {
                        var cx = '015497087736326010529:rbazlqhvg7i';
                        var gcse = document.createElement('script');
                        gcse.type = 'text/javascript';
                        gcse.async = true;
                        gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(gcse, s);
                      })();
                    </script>
                    <gcse:search></gcse:search>
                    <style type="text/css">
                        .cse .gsc-search-button input.gsc-search-button-v2,
                        input.gsc-search-button-v2 {
                            height: 25px !important;
                            margin-top: 0 !important;
                            min-width: 13px !important;
                            padding: 5px 25px !important;
                            width: 65px !important;
                        }
                    </style>
                </li>
            </ul>
        </div>
    </div>

    <!-- Start mobile sidebar menu-->
    <div class="mobile-menu visible-xs">
        <a href="#" data-activates="slide-out" class="navbar-brand button-collapse"><i class="material-icons">menu</i><?=lang('menu')?></a>
        <ul id="slide-out" class="side-nav">
            <!-- Logo -->
            <div class="logo-wrapper">
                <a href="#"><img src="<?=base_url('assets/images/white-logo.png')?>" class="img-responsive align-center"></a>
            </div>
            <!--/. Logo -->

            <!-- Side navigation links -->
            <ul class="collapsible collapsible-accordion">
                <?php if (!empty($menu)) :
                    foreach ($menu as $item) :
                        if (isset($item->sub_menu)) : ?>
                            <li>
                                <a class="collapsible-header"><?=$item->title?></a>
                                <div class="collapsible-body">
                                    <ul>
                                        <?php foreach ($item->sub_menu as $sub_item) : ?>
                                            <li>
                                                <a href="<?=site_url($sub_item->link)?>">
                                                    <?=$sub_item->title?>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </li>
                        <?php else: ?>
                            <li>
                                <a href="<?=site_url($item->link)?>">
                                    <?=$item->title?>
                                </a>
                            </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
            <!--/. Side navigation links -->

        </ul>
      </div>
      <!-- End mobile sidebar menu-->

</nav>