<!doctype html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-site-verification" content="X_fLn7lcIEJ_aFeLnGdZbW7-B7-V0lGgYHlYT3cEvKA" />

    <?php
    if (!isset($page_title))
        $page_title = '';
    switch ($this->uri->segment(1)) {
        case '':
        case 'pages':
            $title = lang('websiteName');
        break;
        case 'home':
            $title = lang('websiteName') . " - " . lang('homepage');
        break;
        case 'articles':
        case 'videos':
        case 'audios':
            $title = $page_title,' | '.lang('websiteName') ;
        break;
        default:
            $title =  $page_title,' | '.lang('websiteName');
        break;
    }
    if ($this->uri->segment(2) == 'category')
        $title = $page_title . " - " . lang('websiteName');
    ?>
    <title><?=$title?></title>

    <!--=====Icon-Font======-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url('assets/css/font-awesome.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/plugins/owl.carousel/owl.carousel.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/mdb.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/developer.css')?>">
    <?php if ($this->uri->segment(1) == 'audios') : ?>
        <link rel="stylesheet" href="<?=base_url('assets/plugins/jPlayer-2.9.2/dist/skin/blue.monday/css/jplayer.blue.monday.min.css')?>">
    <?php endif; ?>
    <!--=====Styles=====-->
    <link rel="stylesheet" href="<?=base_url('assets/css/style.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/islamic-typo.css')?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!-- [if lt IE 9]>
        <script src="../assets/js/html5shiv-3.7.1.js"></script>
        <script src="../assets/js/respond-1.4.2.js"></script>
    <![5endif] -->

    <?php
    if(isset($site_description))
        $meta_description = $site_description;
    else
        $meta_description = lang('meta_description');
    ?>
    <?php
    if ($this->uri->segment(2) != '' && $this->uri->segment(2) != 'category') :
        $host     = $_SERVER['HTTP_HOST'];
        $segment1 = $this->uri->segment(1);
        $segment2 = $this->uri->segment(2);
        if ($this->uri->segment(1) == 'articles' && $this->uri->segment(2) != 'category') :
    ?>
        <meta property="og:type" content="article" />
        <meta property="og:title" content="<?=$article_row['title']?>" />
        <meta name="twitter:title" content="<?=$article_row['title']?>" />
        <?php
        if ($article_row['article_photo'] != '') {
            $article_photos = explode(',', $article_row['article_photo']);
            $photo = $article_photos[0];
        } else {
            $photo = base_url('assets/images/home-article-thum.png');
        }
        ?>
        <meta property="og:image" content="<?=$photo?>" />
        <meta name="twitter:image" content="<?=$photo?>" />
    <?php endif; ?>

    <?php if ($this->uri->segment(1) == 'videos' && $this->uri->segment(2) != 'category') : ?>
        <meta property="og:type" content="video" />
        <meta property="og:title" content="<?=$video_row['title']?>" />
        <meta name="twitter:title" content="<?=$article_row['title']?>" />
    <?php endif; ?>

    <?php if ($this->uri->segment(1) == 'audios' && $this->uri->segment(2) != 'category') : ?>
        <meta property="og:type" content="audio" />
        <meta property="og:title" content="<?=$audio_row['title']?>" />
        <meta name="twitter:title" content="<?=$article_row['title']?>" />
    <?php endif; ?>

        <meta property="og:description" content="<?=$meta_description?>" />
        <meta property="og:url" content="http://<?= $host.'/'.$current_lang_abbr.'/'.$segment1.'/'.$segment2.'/' ?>" />
        <meta property="og:site_name" content="<?=lang('websiteName')?>" />
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@knowingallah">
        <meta name="twitter:description" content="<?=$meta_description?>">
        <meta property="article:publisher" content="https://www.facebook.com/Knowing.Allah/" />
        <meta property="article:author" content="https://www.facebook.com/Knowing.Allah" />
    <?php endif; ?>

    <meta name="description" content="<?=$meta_description?>"/>
    <meta name="ROBOTS" content="INDEX,FOLLOW" />
    <meta name="REVISIT-AFTER" content="5 days" />

    <?php if (isset($video_row)) : ?>
        <!--<script src="https://jwpsrv.com/library/zOwXMu2DEeSsYhJtO5t17w.js"></script>-->
    <?php endif; ?>

    <?php
    $host     = $_SERVER['HTTP_HOST'];
    $segment1 = $this->uri->segment(1);
    switch ($this->uri->segment(1)) {
        case 'articles':
        case 'videos':
        case 'audios':
            $segment2  = $this->uri->segment(2);
            $canonical = "<link rel=\"canonical\" href=\"http://$host/$current_lang_abbr/$segment1/$segment2/\">";
            break;
        default:
            $canonical = "<link rel=\"canonical\" href=\"http://$host/$current_lang_abbr/$segment1\">";
            break;
    }
    if ($this->uri->segment(2) == 'category') :
        $segment3  = $this->uri->segment(3);
        $canonical = "<link rel=\"canonical\" href=\"http://$host/$current_lang_abbr/$segment1/category/$segment3/\">";
    endif;
    echo $canonical;
    ?>
    <?php if ($this->uri->segment(2) == 'category') : ?>
        <link rel="alternate" hreflang="<?= $current_lang_abbr ?>" href="http://<?= $_SERVER['HTTP_HOST'].'/'.$current_lang_abbr.'/'.$this->uri->segment(1).'/category/'.$categoryId.'/' ?>" />
        <?php foreach ($query_other_languages as $row) : ?>
            <link rel="alternate" hreflang="<?=$row['code']?>" href="http://<?= $_SERVER['HTTP_HOST'].'/'.$row['code'].'/'.$this->uri->segment(1).'/category/'.$categoryId.'/' ?>" />
        <?php endforeach;
    endif; ?>
    <?php
    if (isset($article_row) || isset($audio_row) || isset($video_row)) :
        switch ($this->uri->segment(1)) {
            case 'articles':
                $itemID = $articleId;
                $module = 'articles';
            break;
            case 'videos':
                $itemID = $videoId;
                $module = 'videos';
            break;
            case 'audios':
                $itemID = $audioId;
                $module = 'audios';
            break;
        }
    ?>
    <link rel="alternate" hreflang="<?= $current_lang_abbr ?>" href="http://<?= $_SERVER['HTTP_HOST'].'/'.$current_lang_abbr.'/'.$module.'/'.$itemID.'/' ?>" />
    <?php foreach ($query_other_languages as $row): ?>
        <link rel="alternate" hreflang="<?=$row['code']?>" href="http://<?= $_SERVER['HTTP_HOST'].'/'.$row['code'].'/'.$module.'/'.$itemID.'/' ?>" />
    <?php endforeach;
    endif; ?>

    <script type="application/ld+json">
    {
     "@context": "http://schema.org",
     "@type": "WebSite",
     "url": "http://knowingallah.com/<?= $current_lang_abbr ?>/search/",
     "potentialAction": {
       "@type": "SearchAction",
       "target": "http://knowingallah.com/<?= $current_lang_abbr ?>/search?q={search_term_string}",
       "query-input": "required name=search_term_string"
     }
    }
    </script>

</head>

<body class="<?=$dir?>">

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-12983725-1', 'auto');
        ga('send', 'pageview');

    </script>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <div id="wrapper">