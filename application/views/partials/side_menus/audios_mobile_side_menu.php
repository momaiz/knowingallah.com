<?php

/**
 * Copyright (c) 2018 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author   Mohammed Zayan <zayanit@gmail.com>
 * @link     http://www.zayanit.com
 * @version  1.0
 * Created by Sublime Text 3
 */

if (is_array($mostViewed) && !empty($mostViewed)) :
?>

<div class="col-md-3 col-sm-4 col-xs-12 visible-xs">
<aside class="home-sidebar">
  <div class="most-visited">
    <div class="section-title">
      <h3><?=lang('mostListen')?></h3> </div>
    <div class="card">
      <ul class="list-unstyled no-margin">
      	<?php foreach ($mostViewed as $audio) : ?>
	        <li>
	          <div class="new-small-added-item">
	            <div class="media">
	              <div class="media-left">
	                <a href="<?=site_url('audios').'/'.$audio['slug'].'/'?>">
	                	<img src="<?=base_url('assets/images/home-sound-thum.png')?>" class="media-object" alt="<?=$audio['title']?>">
	                </a>
	              </div>
	              <div class="media-body">
	                <h4>
	                	<a href="<?=site_url('audios').'/'.$audio['slug'].'/'?>" title="<?=$audio['title']?>">
	                		<?=$audio['title']?>
	                	</a>
	                </h4>
	                <div class="author">
	                	<i class="fa fa-user"></i>
	                	<?=$audio['author'] != '' ? $audio['author'] : lang('siteTeam')?>
	                </div> 
	                <span><i class="fa fa-clock-o"></i><?=changeDateFormate($audio['creation_date'])?></span> 
	                <span><i class="fa fa-eye"></i><?=$audio['hits']?></span> 
	            </div>
	            </div>
	          </div>
	        </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
</aside>
</div>
<?php endif; ?>