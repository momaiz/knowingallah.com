<?php

/**
 * Copyright (c) 2018 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author   Mohammed Zayan <zayanit@gmail.com>
 * @link     http://www.zayanit.com
 * @version  1.0
 * Created by Sublime Text 3
 */
 
?>
<?php if (is_array($mostViewed) && !empty($mostViewed)) : ?>
<div class="col-md-3 col-sm-4 col-xs-12 visible-xs">
<aside class="home-sidebar">
  <div class="most-visited">
    <div class="section-title">
      <h3><?=lang('mostViewedArticles')?></h3> 
  	</div>
    <div class="card">
      <ul class="list-unstyled no-margin">
        <?php foreach ($mostViewed as $vArticle) : ?>
        <li>
          <div class="new-small-added-item">
            <div class="media">
              <div class="media-left">
                <a href="<?=site_url('articles').'/'.$vArticle['slug'].'/'?>">
            	<?php
                if ($vArticle['article_photo'] != '') :
                    $article_photos = explode(',', $vArticle['article_photo']);
                ?>
                    <img src="<?=$article_photos[3]?>" class="media-object" alt="<?=$vArticle['title']?>">
                <?php else : ?>
                    <img src="<?=base_url('assets/images/home-article-thum.png')?>" class="media-object" alt="<?=$vArticle['title']?>">
                <?php endif; ?>
                </a>
              </div>
              <div class="media-body">
                <h4>
                	<a href="<?=site_url('articles').'/'.$vArticle['slug'].'/'?>" title="<?=$vArticle['title']?>">
                		<?=$vArticle['title']?>
            		</a>
            	</h4>
                <div class="author">
                	<i class="fa fa-user"></i><?=$vArticle['author'] != '' ? $vArticle['author'] : lang('siteTeam')?>
                </div> 
                <span><i class="fa fa-clock-o"></i><?=changeDateFormate($vArticle['creation_date'])?></span> 
                <span><i class="fa fa-eye"></i><?=$vArticle['hits']?></span> 
            </div>
            </div>
          </div>
        </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
</aside>
</div>
<?php endif; ?>