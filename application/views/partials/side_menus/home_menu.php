<?php

/**
 * Copyright (c) 2016 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author   Mohammed Zayan <zayanit@gmail.com>
 * @link     http://www.zayanit.com
 * @version  1.0
 * Created by PhpStorm
 */
 
?>

<div class="back-to-allah">
    <div class="section-title">
        <h3><?=lang('returnersToAllah')?></h3>
        <a href="<?=site_url('articles/category/69').'/'?>"><?=lang('others')?></a>
    </div>
    <div class="owl-carousel">
        <?php foreach ($islamStory as $article) : ?>
            <div class="item">
                <a href="<?=site_url('articles').'/'.$article['slug'].'/'?>">
                <?php
                if($article['article_photo'] != ''):
                    $article_photos = explode(',', $article['article_photo']);
                ?>
                    <img src="<?=$article_photos[1]?>" width="263" height="203" class="img-responsive align-center" alt="<?=$article['title']?>">
                <?php else : ?>
                    <img src="<?=base_url('assets/images/default-article.png')?>" width="263" height="203" class="img-responsive align-center" alt="<?=$article['title']?>">
                <?php endif; ?>
                </a>
                <h4>
                    <a href="<?=site_url('articles').'/'.$article['slug'].'/'?>">
                        <?=$article['title']?>
                    </a>
                </h4>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<div class="special-video">
    <div class="section-title">
        <h3><?=$home_video['video_title']?></h3>
    </div>
    <iframe src="https://www.youtube.com/embed/<?=$home_video['youtube_code']?>?rel=0"></iframe>
</div>

<div class="most-visited">
    <div class="section-title">
        <h3><?=lang('mostVisited')?></h3>
    </div>
    <div class="card">
        <ul class="list-unstyled no-margin">
            <?php
            foreach ($mostViewedItems as $item) :
                switch ($item['tp']) {
                    case 'articles/article' :
            ?>
                        <li>
                        <div class="new-small-added-item">
                            <div class="media">
                                <div class="media-left">
                                    <a href="<?=site_url('articles').'/'.$item['slug']?>">
                                        <?php
                                        if ($item['photo'] != '') :
                                            $article_photos = explode(',', $item['photo']);
                                        ?>
                                            <img src="<?=$article_photos[3]?>" class="media-object" width="50" height="50" alt="<?=$item['title']?>">
                                        <?php else : ?>
                                            <img src="<?=base_url('assets/images/home-article-thum.png')?>" class="media-object" width="50" height="50" alt="<?=$item['title']?>">
                                        <?php endif; ?>
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4>
                                        <a href="<?=site_url('articles').'/'.$item['slug']?>" title="<?=$item['title']?>">
                                            <?=$item['title']?>
                                        </a>
                                    </h4>
                                    <div class="author">
                                        <i class="fa fa-user"></i>
                                        <?=$item['author'] != '' ? $item['author'] : lang('siteTeam')?>
                                    </div>
                                    <span><i class="fa fa-clock-o"></i><?=changeDateFormate($item['creation_date'])?></span>
                                    <span><i class="fa fa-eye"></i><?=$item['hits']?></span>
                                </div>
                            </div>
                        </div>
                    </li>
            <?php
                        break;
                    case 'videos/video' :
            ?>
                        <li>
                            <div class="new-small-added-item">
                                <div class="media">
                                    <div class="media-left video-thum">
                                        <?php if ($item['photo'] != '') : ?>
                                            <img src="https://img.youtube.com/vi/<?=$item['photo']?>/1.jpg" class="media-object" width="70" height="77" alt="<?=$item['title']?>">
                                        <?php else : ?>
                                            <img src="<?=base_url('assets/images/default-sm-video.png')?>" class="media-object" width="70" height="77" alt="<?=$item['title']?>">
                                        <?php endif; ?>
                                    </div>
                                    <div class="media-body">
                                        <h4>
                                            <a href="<?=site_url('videos').'/'.$item['slug']?>">
                                                <?=$item['title']?>
                                            </a>
                                        </h4>
                                        <div class="author">
                                            <i class="fa fa-user"></i>
                                            <?=$item['author'] != '' ? $item['author'] : lang('siteTeam')?>
                                        </div>
                                        <span><i class="fa fa-clock-o"></i><?=changeDateFormate($item['creation_date'])?></span>
                                        <span><i class="fa fa-eye"></i><?=$item['hits']?></span>
                                    </div>
                                </div>
                            </div>
                        </li>
            <?php
                        break;
                    case 'audios/audio' :
            ?>
                        <li>
                            <div class="new-small-added-item">
                                <div class="media">
                                    <div class="media-left">
                                        <a href="<?=site_url('audios').'/'.$item['slug']?>">
                                            <img src="<?=base_url('assets/images/home-sound-thum.png')?>" class="media-object" width="50" height="50" alt="<?=$item['title']?>">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4>
                                            <a href="<?=site_url('audios').'/'.$item['slug']?>" title="<?=$item['title']?>">
                                                <?=$item['title']?>
                                            </a>
                                        </h4>
                                        <div class="author">
                                            <i class="fa fa-user"></i>
                                            <?=$item['author'] != '' ? $item['author'] : lang('siteTeam')?>
                                        </div>
                                        <span><i class="fa fa-clock-o"></i><?=changeDateFormate($item['creation_date'])?></span>
                                        <span><i class="fa fa-eye"></i><?=$item['hits']?></span>
                                    </div>
                                </div>
                            </div>
                        </li>
            <?php
                        break;
                }
            endforeach;
            ?>
        </ul>
    </div>
</div>