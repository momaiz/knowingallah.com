<?php

/**
 * Copyright (c) 2018 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author   Mohammed Zayan <zayanit@gmail.com>
 * @link     http://www.zayanit.com
 * @version  1.0
 * Created by Sublime Text 3
 */
 
?>

<?php if (is_array($booksMoreDownloaded) && !empty($booksMoreDownloaded)) : ?>
<div class="col-md-3 col-sm-4 col-xs-12 visible-xs">
  <aside class="home-sidebar">
    <div class="most-visited">
      <div class="section-title">
        <h3><?=lang('moreDownloaded')?></h3> 
      </div>
      <div class="card">
        <ul class="list-unstyled no-margin">
          <?php foreach ($booksMoreDownloaded as $book) : ?>
            <li>
              <div class="new-small-added-item">
                <div class="media">
                  <div class="media-left">
                    <a href="<?=site_url('books').'/'.$book['slug']?>">
                      <?php if ($book['photo'] != '') : ?>
                        <img src="<?=$book['photo']?>" class="media-object" alt="<?=$book['name']?>">
                      <?php else : ?>
                        <img src="<?=base_url('assets/images/book-thum.png')?>" class="media-object" alt="<?=$book['name']?>">
                      <?php endif; ?>
                    </a>
                  </div>
                  <div class="media-body">
                    <h4>
                      <a href="<?=site_url('books').'/'.$book['slug']?>" title="<?=$book['name']?>">
                        <?=$book['name']?>
                      </a>
                    </h4>
                    <div class="author">
                      <i class="fa fa-user"></i><?=$book['author'] != '' ? $book['author'] : lang('siteTeam') ?>
                    </div> 
                    <span><i class="fa fa-clock-o"></i><?=changeDateFormate($book['creation_date'])?></span> 
                    <span><i class="fa fa-eye"></i><?=$book['hits']?></span> 
                  </div>
                </div>
              </div>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>
  </aside>
</div>
<?php endif; ?>