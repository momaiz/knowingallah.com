<?php

/**
 * Copyright (c) 2016 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author   Mohammed Zayan <zayanit@gmail.com>
 * @link     http://www.zayanit.com
 * @version  1.0
 * Created by PhpStorm
 */
 
?>

<?php if (is_array($booksMoreDownloaded) && !empty($booksMoreDownloaded)) : ?>
<div class="most-visited">
    <div class="section-title">
        <h3><?=lang('moreDownloaded')?></h3>
    </div>
    <div class="card">
        <ul class="list-unstyled no-margin">
            <?php foreach ($booksMoreDownloaded as $book) : ?>
                <li>
                    <div class="new-small-added-item">
                        <div class="media">
                            <div class="media-left">
                                <a href="<?=site_url('books').'/'.$book['slug']?>">
                                    <?php if ($book['photo'] != '') : ?>
                                        <img src="<?=$book['photo']?>" class="media-object" alt="<?=$book['name']?>">
                                    <?php else : ?>
                                        <img src="<?=base_url('assets/images/book-thum.png')?>" class="media-object" alt="<?=$book['name']?>">
                                    <?php endif; ?>
                                </a>
                            </div>
                            <div class="media-body">
                                <h4>
                                    <a href="<?=site_url('books').'/'.$book['slug']?>" title="<?=$book['name']?>">
                                        <?=$book['name']?>
                                    </a>
                                </h4>
                                <?php if ($book['author'] != '') : ?>
                                    <div class="author"><i class="fa fa-user"></i><?=$book['author']?></div>
                                <?php else : ?>
                                    <div class="author"><i class="fa fa-user"></i><?=lang('siteTeam')?></div>
                                <?php endif; ?>
                                <span><i class="fa fa-clock-o"></i><?=changeDateFormate($book['creation_date'])?></span>
                                <span><i class="fa fa-eye"></i><?=$book['hits']?></span>
                            </div>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<?php endif; ?>