<?php

/**
 * Copyright (c) 2016 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author   Mohammed Zayan <zayanit@gmail.com>
 * @link     http://www.zayanit.com
 * @version  1.0
 * Created by PhpStorm
 */
 
?>

<?php if (is_array($mostViewed) && !empty($mostViewed)) : ?>
<div class="most-visited">
<div class="section-title">
    <h3><?=lang('mostViewedVideos')?></h3>
</div>
<div class="card">
    <ul class="list-unstyled no-margin">
        <?php foreach ($mostViewed as $sVideo) : ?>
            <li>
                <div class="new-small-added-item">
                    <div class="media">
                        <div class="media-left video-thum">
                            <?php if ($sVideo['youtube_code'] != '') : ?>
                                <img src="https://img.youtube.com/vi/<?=$sVideo['youtube_code']?>/0.jpg" class="media-object" alt="<?=$sVideo['title']?>">
                            <?php else : ?>
                                <img src="<?=base_url('assets/images/default-sm-video.png')?>" class="media-object" alt="<?=$sVideo['title']?>">
                            <?php endif; ?>
                        </div>
                        <div class="media-body">
                            <h4>
                                <a href="<?=site_url('videos').'/'.$sVideo['slug'].'/'?>" title="<?=$sVideo['title']?>">
                                    <?=$sVideo['title']?>
                                </a>
                            </h4>
                            <div class="author">
                                <i class="fa fa-user"></i>
                                <?=$sVideo['author'] != '' ? $sVideo['author'] : lang('siteTeam')?>
                            </div>
                            <span><i class="fa fa-clock-o"></i><?=changeDateFormate($sVideo['creation_date'])?></span>
                            <span><i class="fa fa-eye"></i><?=$sVideo['hits']?></span>
                        </div>
                    </div>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
</div>
<?php endif; ?>