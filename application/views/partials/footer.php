<footer class="main-footer">
    <div class="top-footer">
        <div class="container">
            <div class="logo">
                <a href="<?=site_url('home').'/'?>"><img src="<?=base_url('assets/images/white-logo.png')?>" class="img-responsive align-center" width="115" height="130" alt="logo image"></a>
                <p><?=lang('meta_description')?></p>
            </div>
            <div class="social">
                <?php
                if($current_lang_abbr == 'ar')
                    $facebook = 'https://www.facebook.com/Knowing.Allah';
                else
                    $facebook = 'https://www.facebook.com/KnowingAllah.en';
                ?>
                <a href="<?=$facebook?>" class="fa fa-facebook"></a>
                <a href="https://twitter.com/knowingallah" class="fa fa-twitter"></a>
                <a href="https://plus.google.com/+knowingAllah1/videos" class="fa fa-google-plus"></a>
                <a href="https://www.youtube.com/user/knowingAllah1" class="fa fa-youtube"></a>
                <a href="https://www.instagram.com/KnowingAllah1/" class="fa fa-instagram"></a>
                <p><?=lang('copyright')?></p>
                <!-- Start of IslamChat code -->
                <script type="text/javascript">
                    window.__lc = window.__lc || {};
                    window.__lc.license = 7499411;
                    window.__lc.chat_between_groups = false;
                    (function() {
                        var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
                        lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
                    })();
                </script>
                <!-- End of IslamChat code -->
            </div>
        </div>
    </div>
    <div class="bottom-footer">
        <div class="container">
            <p><a href="http://midade.com/home/" target="_blank">All Rights Reserved © Midade <?=date('Y')?></a></p>
        </div>
    </div>
</footer>
</div>

<script src="<?=base_url('assets/js/jquery.min.js')?>"></script>
<script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
<script src="<?=base_url('assets/js/mdb.js')?>"></script>
<script src="<?=base_url('assets/plugins/owl.carousel/owl.carousel.min.js')?>"></script>
<script src="<?=base_url('assets/js/scripts.0.0.1.js')?>"></script>
<?php
/*if(isset($article_row) || isset($audio_row) || isset($video_row))
    $this->load->view('partials/likes');*/

if (isset($article_row) || isset($video_row) || isset($audio_row) || isset($book)) :
    ?>
    <script type="text/javascript">
        function shareFunction(url){window.open(url, "Share", "width=500, height=500, left=400, top=50");}
    </script>
<?php
endif;

if (isset($book)) :
?>
<script>
    function incrementDownloads(bookID) {
        $.post(
            "<?=site_url('books/incrementDownloads')?>",
            {bookID: bookID, '<?= $this->security->get_csrf_token_name() ?>' : '<?= $this->security->get_csrf_hash() ?>'}
        );
    }
</script>
<?php endif; ?>
</body>
</html>