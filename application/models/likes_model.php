<?php

/**
 * Copyright (c) 2015 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author        Mohammed Zayan <admin@zayanit.com>
 * @link          http://www.zayanit.com
 * @version       1.0
 * Created by Sublime Text 2
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Likes_model extends CI_Model
{

	function checkItem($item_id, $module, $lang)
	{
		$lang_id = $this->get_language_id_by_code($lang);
		# Check if item exist
		$this->db->where(array($module.'_id' => $item_id, 'language_id' => $lang_id));
		$result = $this->db->get($module.'_translation');
		if($result->num_rows() == 1){
			# Check if this ip make like
			$user_ip = $_SERVER['REMOTE_ADDR'];
			$this->db->where(array('user_ip' => $user_ip, $module.'_id' => $item_id, 'language_id' => $lang_id));
			$result = $this->db->get($module.'_likes');
			if($result->num_rows() == 1){
				return 'liked';
			}else{
				return 'success';
			}
		}else{
			return 'fail';
		}
	}

	function increse_likes($item_id, $module, $lang)
	{
		$lang_id = $this->get_language_id_by_code($lang);
		$this->db->set('likes', 'likes + 1', FALSE);
        $this->db->where(array($module."_id" =>$item_id, 'language_id' =>$lang_id));
        $result = $this->db->update($module.'_translation');
        if($result){
        	$data = array(
    			$module.'_id' => $item_id,
    			'user_ip'     => $_SERVER['REMOTE_ADDR'],
    			'language_id' => $lang_id
    		);
    		$insert = $this->db->insert($module.'_likes', $data);
    		if($insert){
    			$this->db->where(array($module."_id" =>$item_id, 'language_id' =>$lang_id));
		        $this->db->select('likes');
		        $likes = $this->db->get($module.'_translation')->row()->likes;
		        return $likes;
    		}
        }
	}

	function checkElement($item_id, $module, $lang)
	{
		$lang_id = $this->get_language_id_by_code($lang);
		# Check if item exist
		$this->db->where(array($module.'_id' => $item_id, 'language_id' => $lang_id));
		$result = $this->db->get($module.'_translation');
		if($result->num_rows() == 1){
			# Check if this ip make like
			$user_ip = $_SERVER['REMOTE_ADDR'];
			$this->db->where(array('user_ip' => $user_ip, $module.'_id' => $item_id, 'language_id' => $lang_id));
			$result = $this->db->get($module.'_dislikes');
			if($result->num_rows() == 1){
				return 'disliked';
			}else{
				return 'success';
			}
		}else{
			return 'fail';
		}
	}

	function increse_dislikes($item_id, $module, $lang)
	{
		$lang_id = $this->get_language_id_by_code($lang);
		$this->db->set('dislikes', 'dislikes + 1', FALSE);
        $this->db->where(array($module."_id" =>$item_id, 'language_id' =>$lang_id));
        $result = $this->db->update($module.'_translation');
        if($result){
        	$data = array(
    			$module.'_id' => $item_id,
    			'user_ip'     => $_SERVER['REMOTE_ADDR'],
    			'language_id' => $lang_id
    		);
    		$insert = $this->db->insert($module.'_dislikes', $data);
    		if($insert){
    			$this->db->where(array($module."_id" =>$item_id, 'language_id' =>$lang_id));
		        $this->db->select('dislikes');
		        $dislikes = $this->db->get($module.'_translation')->row()->dislikes;
		        return $dislikes;
    		}
        }
	}

	function get_language_id_by_code($lang_code)
    {
        $this->db->select('id');
        $this->db->where('code',$lang_code);
        $query=$this->db->get('language')->row();
        return $query->id;
    }

}