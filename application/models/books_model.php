<?php

/**
 * Copyright (c) 2016 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author   Mohammed Zayan <zayanit@gmail.com>
 * @link     http://www.zayanit.com
 * @version  1.0
 * Created by PhpStorm
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Books_model extends CI_Model
{
    /**
     * Get latest added books.
     * @param $langID
     * @param $limit
     * @return mixed
     */
    function getLastestBooks($langID, $limit)
    {
        $this->db->select('id, name, slug, photo, author, creation_date, hits');
        $this->db->from('books_for_dowinload');
        $this->db->where(['language_id =' => $langID, 'visible =' => 'yes']);
        $this->db->order_by('id', 'desc');
        $this->db->limit($limit);
        $result = $this->db->get();
        if ($result)
            return $result->result_array();
    }

    /**
     * Get the most downloaded books.
     * @param $langID
     * @param int $limit
     * @return mixed
     */
    function booksMoreDownloaded($langID, $limit = 4)
    {
        $this->db->select('id, name, slug, photo, author, creation_date, hits, downloads');
        $this->db->from('books_for_dowinload');
        $this->db->where(['language_id =' => $langID, 'visible =' => 'yes']);
        $this->db->order_by('downloads', 'desc');
        $this->db->limit($limit);
        $result = $this->db->get();
        if ($result)
            return $result->result_array();
    }

    /**
     * Get books number un a language.
     * @param $langID
     * @return mixed
     */
    function getAllRowsNum($langID)
    {
        $this->db->from('books_for_dowinload');
        $this->db->where(['language_id =' => $langID, 'visible =' => 'yes']);
        $result = $this->db->get();
        if ($result)
            return $result->num_rows();
    }

    /**
     * Get books as pagination.
     * @param $langID
     * @param null $limit
     * @param null $from
     * @return mixed
     */
    function getBooks($langID, $limit=null, $from=null)
    {
        $this->db->select('id, name, slug, photo, author, creation_date, hits');
        $this->db->from('books_for_dowinload');
        $this->db->where(['language_id =' => $langID, 'visible =' => 'yes']);
        $this->db->order_by('creation_date', 'desc');
        if ($from)
            $this->db->limit($limit, $from);
        else
            $this->db->limit($limit);
        $result = $this->db->get();
        if ($result)
            return $result->result_array();
    }

    /**
     * Get book data by it's slug & language.
     * @param $slug
     * @param $langID
     * @return mixed
     */
    function getBookBySlug($slug, $langID)
    {
        $slug = urldecode($slug);
        $this->db->select('id, name, download_link, photo, author, creation_date, hits');
        $this->db->from('books_for_dowinload');
        $this->db->where(['language_id =' => $langID, 'slug =' => $slug]);
        $result = $this->db->get();
        if ($result)
            return $result->row_array();
    }

    /**
     * Get related Books with the selected book.
     * @param $slug
     * @param $langID
     * @param $limit
     * @return mixed
     */
    function getRelatedBooks($slug, $langID, $limit)
    {
        $slug = urldecode($slug);
        $this->db->select('id, name, slug, photo, author, creation_date, hits');
        $this->db->from('books_for_dowinload');
        $this->db->where(['language_id =' => $langID, 'slug !=' => $slug]);
        $this->db->limit($limit);
        $this->db->order_by('rand()');
        $result = $this->db->get();
        if ($result)
            return $result->result_array();
    }

    /**
     * Increment books hits when load book page.
     * @param $id
     * @param $lang_id
     */
    function increment_hits($id, $lang_id)
    {
        $this->db->set('hits', 'hits + 1', FALSE);
        $this->db->where(array('id' => $id, 'language_id' => $lang_id));
        $this->db->update('books_for_dowinload');
    }

    /**
     * Increment book downloads number.
     * @param  $id
     */
    function incrementDownloads($id)
    {
        $this->db->set('downloads', 'downloads + 1', FALSE);
        $this->db->where(array('id' => $id));
        $this->db->update('books_for_dowinload');
    }
}
