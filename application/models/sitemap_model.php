<?php

/**
 * Copyright (c) 2015 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author        programmer/ Mohammed Fawzy Zayan <admin@zayanit.com>
 * @link          http://www.zayanit.com
 * @version       1.0
 * Created by Sublime Text 2
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sitemap_model extends CI_Model
{

	function get_lang_id()
	{
		$current_lang_abbr = $this->config->item('language_abbr');
		$this->db->select('id');
        $this->db->where('code', $current_lang_abbr);
        $query=$this->db->get('language')->row();
        return $query->id;
	}

	function get_categories($lang_id, $model)
	{
		$this->db->select($model.'_cat_id');
		$this->db->where('language_id', $lang_id);
		$result = $this->db->get($model.'_cat_translation');
		if($result->num_rows() > 0)
			return $result->result_array();
	}

	function get_rows($lang_id, $model)
	{
		$this->db->select('slug, creation_date');
		$this->db->where('language_id', $lang_id);
		$result = $this->db->get($model.'_translation');
		if($result->num_rows() > 0)
			return $result->result_array();
	}

}