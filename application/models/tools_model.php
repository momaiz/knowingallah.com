<?php

class tools_model extends CI_Model
{
    function get_language_id_by_code($lang_code)
    {
        $this->db->select('id');
        $this->db->where('code',$lang_code);
        $query=$this->db->get('language')->row();
        return $query->id;
    }

    function get_current_lang_description($lang_code)
    {
        $this->db->select('description');
        $this->db->where('code',$lang_code);
        $query=$this->db->get('language')->row();
        return $query->description;
    }

    function get_current_language_config()
    {
        $data['current_lang_abbr']=$this->config->item('language_abbr');

        if($data['current_lang_abbr'] == "ar" || $data['current_lang_abbr'] == "he") {
            $data['dir'] = "rtl";
        }else{
            $data['dir'] = "ltr";
        }
        $data['current_lang_id'] = (int) $this->get_language_id_by_code($data['current_lang_abbr']);
        $data['current_lang_description']=$this->get_current_lang_description($data['current_lang_abbr']);
        //Increment Website hits
        $this->db->set('hits', 'hits + 1', FALSE);
        $this->db->update('sys_config');

        $this->db->select('hits');
        $query=$this->db->get('sys_config')->row();
        $data['website_hits'] = $query->hits;
        return $data;
    }

    /**
     * This method get the required data (menus data) for every page in the site.
     * @return mixed
     */
    function get_page_config()
    {
        $data                        = $this->get_current_language_config();
        $data['menu']                = $this->push_menu($data['current_lang_id']);
        //$data['top_banner']          = $this->get_banners($data['current_lang_id'], 'top');
        $data['articles_categories'] = $this->get_articles_categories($data['current_lang_id'], 6);
        $data['videosCategories']    = $this->getVideosCategories($data['current_lang_id'], 6);
        $data['audiosCategories']    = $this->getAudiosCategories($data['current_lang_id'], 6);
        //$data['menu_articles']       = $this->get_menu_articles($data['articles_categories'], $data['current_lang_id'], 3);
        $data['last_videos']         = $this->get_last_rows($data['current_lang_id'], 4, 'video');
        $data['audios_cats']         = $this->get_audios_cats($data['current_lang_id']);
        //$data['side_banner']         = $this->get_banners($data['current_lang_id'], 'side');
        //$data['books']               = $this->get_books($data['current_lang_id']);
        $data['last_article']        = $this->get_last_rows($data['current_lang_id'], 1, 'article');
        $data['last_video']          = $this->get_last_rows($data['current_lang_id'], 1, 'video');
        $data['last_audio']          = $this->get_last_rows($data['current_lang_id'], 1, 'audio');
        return $data;
    }

    /**
     * Get site side menu.
     * @param $current_lang_id
     * @return array
     */
    function push_menu($current_lang_id)
    {
        $menu     = $this->get_site_menu($current_lang_id);
        $sub_menu = $this->get_site_sub_menu($current_lang_id);
        if ($menu) {
            foreach ($menu as $item) {
                if (isset($sub_menu)) {
                    foreach ($sub_menu as $sub_item) {
                        if ($sub_item->parent_id == $item->id) {
                            $item->sub_menu[] = $sub_item;
                        }
                    }
                }
                $x[] = $item;
            }
            return $x;
        }
    }

    /**
     * Get main urls in the side menu.
     * @param $current_lang_id
     * @return mixed
     */
    function get_site_menu($current_lang_id)
    {
        $this->db->where('language_id', $current_lang_id);
        $this->db->where('parent_id', '0');
        $query = $this->db->get('site_menu');
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    /**
     * Get child urls in the side menu.
     * @param $current_lang_id
     * @return mixed
     */
    function get_site_sub_menu($current_lang_id)
    {
        $this->db->where('language_id', $current_lang_id);
        $this->db->where('parent_id >', '0');
        $query = $this->db->get('site_menu');
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    /**
     * Select articles categories which have more articles.
     * @param $current_lang_id
     * @param $limit
     * @return array
     */
    function get_articles_categories($current_lang_id, $limit)
    {
        $query = "SELECT `article`.`article_cat_id`, COUNT(`article`.`article_cat_id`) AS num
                    FROM `article` INNER JOIN `article_cat_translation`
                    ON `article`.`article_cat_id` =  `article_cat_translation`.`article_cat_id`
                    WHERE `article_cat_translation`.`language_id` = $current_lang_id
                    GROUP BY `article`.`article_cat_id` ORDER BY num DESC LIMIT $limit";
        $categories_id = $this->db->query($query)->result_array();
        foreach ($categories_id as $row) {
            $this->db->select('description, parent_id, cat_description, article_cat_id');
            $this->db->from('article_cat_translation');
            $this->db->join('article_cat', 'article_cat_translation.' . 'article_cat_id = ' . 'article_cat.id', 3);
            $this->db->where(array('article_cat_id' => $row['article_cat_id'], "language_id =" => $current_lang_id));
            $result = $this->db->get()->row_array();
            array_push($result, $row['article_cat_id']);
            $categories_data[] = $result;
        }
        return $categories_data;
    }

    /**
     * Select videos categories which have more videos.
     * @param $current_lang_id
     * @param $limit
     * @return array
     */
    function getVideosCategories($current_lang_id, $limit)
    {
        $query = "SELECT `video`.`video_cat_id`, COUNT(`video`.`video_cat_id`) AS num
                    FROM `video` INNER JOIN `video_cat_translation`
                    ON `video`.`video_cat_id` =  `video_cat_translation`.`video_cat_id`
                    WHERE `video_cat_translation`.`language_id` = $current_lang_id
                    AND `video`.`video_cat_id` != 23
                    GROUP BY `video`.`video_cat_id` ORDER BY num DESC LIMIT $limit";
        $categories_id = $this->db->query($query)->result_array();
        foreach ($categories_id as $row) {
            $this->db->select('description, parent_id, cat_description, video_cat_id');
            $this->db->from('video_cat_translation');
            $this->db->join('video_cat', 'video_cat_translation.' . 'video_cat_id = ' . 'video_cat.id', 3);
            $this->db->where(array('video_cat_id' => $row['video_cat_id'], "language_id =" => $current_lang_id));
            $result = $this->db->get()->row_array();
            array_push($result, $row['video_cat_id']);
            $categories_data[] = $result;
        }
        return $categories_data;
    }

    /**
     * Select audios categories which have more audios.
     * @param $current_lang_id
     * @param $limit
     * @return array
     */
    function getAudiosCategories($current_lang_id, $limit)
    {
        $query = "SELECT `audio`.`audio_cat_id`, COUNT(`audio`.`audio_cat_id`) AS num
                    FROM `audio` INNER JOIN `audio_cat_translation`
                    ON `audio`.`audio_cat_id` =  `audio_cat_translation`.`audio_cat_id`
                    WHERE `audio_cat_translation`.`language_id` = $current_lang_id
                    GROUP BY `audio`.`audio_cat_id` ORDER BY num DESC LIMIT $limit";
        $categories_id = $this->db->query($query)->result_array();
        foreach ($categories_id as $row) {
            $this->db->select('description, parent_id, cat_description, audio_cat_id');
            $this->db->from('audio_cat_translation');
            $this->db->join('audio_cat', 'audio_cat_translation.' . 'audio_cat_id = ' . 'audio_cat.id', 3);
            $this->db->where(array('audio_cat_id' => $row['audio_cat_id'], "language_id =" => $current_lang_id));
            $result = $this->db->get()->row_array();
            array_push($result, $row['audio_cat_id']);
            $categories_data[] = $result;
        }
        return $categories_data;
    }

    /**
     * [get_menu_articles description]
     * @param  [array] $categories [description]
     * @param  [int] $lang_id    [description]
     * @param  [int] $limit      [description]
     * @return [array]             [description]
     */
    function get_menu_articles($categories, $lang_id, $limit)
    {
        foreach($categories as $cat){
            $this->db->select('article_translation.article_id, article_translation.title, article_translation.slug, article_translation.creation_date, article_translation.article_photo');
            $this->db->from('article_translation');
            $this->db->join('article', 'article_translation.article_id = article.id');
            $this->db->where(array('article_cat_id' => $cat[0], 'language_id'=>$lang_id, 'special'=>'no', 'visible'=>1));
            $this->db->order_by('article_translation.creation_date','desc');
            $this->db->limit($limit);
            $menu_articles[] = $this->db->get()->result_array();
        }
        return $menu_articles;
    }

    function get_last_rows($current_lang_id, $limit, $model)
    {
        $this->db->where('language_id', $current_lang_id);
        $this->db->where('visible', 1);
        $this->db->order_by('creation_date','desc');
        $this->db->limit($limit);
        $query=$this->db->get($model.'_translation');
        if ($query) {
            return $query->result_array();
        }
    }

    function get_audios_cats($lang_id)
    {
        $this->db->select('description, audio_cat.id');
        $this->db->from('audio_cat_translation');
        $this->db->join('audio_cat', 'audio_cat_translation.audio_cat_id = audio_cat.id');
        $this->db->where(array('parent_id' => 0, "language_id =" => $lang_id));
        $query = $this->db->get();
        if ($query) {
            return $query->result_array();
        }
    }

    function get_page_detail($id)
    {
        $this->db->where('id',$id);
        $query=$this->db->get('page');
        return $query->row_array();
    }

    function get_file_extension_from_url($filename)
    {
        return strtolower(substr(strrchr($filename, '.'), 1));
    }

    function get_application_path()
    {
        return substr(BASEPATH, 0, strrpos(BASEPATH, "/"));
    }

    /**
     * Get photos for photo gallery
     * @param $current_lang_id
     * @param null $limit
     * @return mixed
     */
    function get_photos($current_lang_id, $limit = null)
    {
        $this->db->where('language_id', $current_lang_id);
        $this->db->order_by('id','desc');
        if($limit) $this->db->limit($limit);
        $query = $this->db->get('photo_gallery');
        if($query) return $query->result_array();
    }

    /**
     * Get latest rows
     * @param $current_lang_id
     * @param $limit
     * @return mixed
     */
    function get_site_new($current_lang_id, $limit)
    {
        $sql_statment = "select * from (
		(select article_id as id, title, slug, creation_date, author, article_photo as photo, description, hits, 'articles/article' as tp
		from article_translation
		where language_id = " . $current_lang_id . " and visible = 1 order by creation_date DESC limit 0,$limit)
		union
		(select video_id as id, title, slug, creation_date, author, youtube_code as photo, description, hits, 'videos/video' as tp
		from video_translation
		where language_id = " . $current_lang_id . " and visible = 1 order by creation_date DESC limit 0,$limit)
		union
		(select audio_id as id, title, slug, creation_date, author, audio_code as photo, description, hits, 'audios/audio' as tp
		from audio_translation
		where language_id = " . $current_lang_id . " and visible = 1 order by creation_date DESC limit 0,$limit)
		) t1 order by creation_date DESC limit 0,$limit
		";
        $result = $this->db->query($sql_statment);
        return $result->result_array();
    }

    /**
     * Get Most Viewed articles, Videos & Audios Random.
     * @param null $cat_id
     * @param $lang_id
     * @param null $limit
     * @param null $from
     * @return mixed
     */
    function getMostViewedItems($lang_id, $limit=null, $from=null)
    {
        $sql_statment = "select * from (
		(select article_id as id, title, slug, creation_date, author, article_photo as photo, hits, 'articles/article' as tp
		from article_translation
		where language_id = " . $lang_id . " and visible = 1 and hits > 25000 order by hits DESC limit 0,$limit)
		union
		(select video_id as id, title, slug, creation_date, author, youtube_code as photo, hits, 'videos/video' as tp
		from video_translation
		where language_id = " . $lang_id . " and visible = 1 order by hits DESC limit 0,$limit)
		union
		(select audio_id as id, title, slug, creation_date, author, audio_code as photo, hits, 'audios/audio' as tp
		from audio_translation
		where language_id = " . $lang_id . " and visible = 1 order by hits DESC limit 0,$limit)
		) t1 order by rand() DESC limit 0,$limit
		";
        $result = $this->db->query($sql_statment);
        return $result->result_array();
    }

    function get_slider_text($current_lang_id)
    {
        $this->db->where('language_id', $current_lang_id);
        $result = $this->db->get('slider');
        if($result) return $result->result_array();
    }

    function get_banners($current_lang_id, $place)
    {
        $this->db->where(array('language_id'=>$current_lang_id, 'banner_place'=>$place));
        $this->db->limit(1);
        $this->db->order_by('id', 'desc');
        $result = $this->db->get('banners');
        if($result) return $result->row_array();
    }

    function get_books($current_lang_id)
    {
        $this->db->where(array('language_id'=>$current_lang_id, 'visible'=>'yes'));
        $result = $this->db->get('books_for_dowinload');
        if($result) return $result->result_array();
    }

    function get_intro_text()
    {
        $this->db->where('visible', 'yes');
        $this->db->limit(1);
        $this->db->order_by('id', 'desc');
        $result = $this->db->get('intro_text');
        if($result) return $result->row_array();
    }

    function get_intro_video()
    {
        $this->db->where('visible', 'yes');
        $this->db->limit(1);
        $this->db->order_by('id', 'RANDOM');
        $result = $this->db->get('intro_videos');
        if($result) return $result->row_array();
    }

    function get_home_video($current_lang_id, $exact_lang='no')
    {
        $this->db->where(array('visible'=>'yes', 'language_id'=>$current_lang_id));
        if($exact_lang == 'no')
            $this->db->or_where('language_id', null);
        $this->db->limit(1);
        $this->db->order_by('id', 'RANDOM');
        $result = $this->db->get('home_videos');
        if($result) return $result->row_array();
    }

    function get_related_articles($title, $langID, $limit)
    {
        $sql = "SELECT *, MATCH(`title`, `content`) AGAINST('$title') AS score
                FROM `article_translation`
                WHERE MATCH(`title`, `content`) AGAINST('$title') AND  `title` != '$title' AND `language_id` = $langID
                ORDER BY score DESC LIMIT $limit";
        $result = $this->db->query($sql);
        if($result)
            return $result->result_array();
    }

    /**
     * Select all articles form db which doesn't have a photo.
     * @return array
     */
    function getAllArticlesWithDefaultPhoto()
    {
        $sql = "SELECT `article_translation`.`title` AS 'articleTitle', CONCAT('http://knowingallah.com/',`language`.`code`,'/articles/', `article_translation`.`slug`) AS 'articleLink', `language`.`code` AS 'laguageCode' FROM `article_translation` INNER JOIN `language` ON `article_translation`.`language_id` = `language`.`id` WHERE `article_photo` = '' ORDER BY `language`.`code`;";
        $result = $this->db->query($sql);
        if ($result)
            return $result->result_array();
    }

    /**
     * Get related Videos with the selected video.
     * @param $title
     * @param $langID
     * @param $limit
     * @return mixed
     */
    function getRelatedVideos($title, $langID, $limit)
    {
        $sql = "SELECT *, MATCH(`title`) AGAINST('$title') AS score
                FROM `video_translation`
                WHERE MATCH(`title`) AGAINST('$title') AND  `title` != '$title' AND `language_id` = $langID
                ORDER BY score DESC LIMIT $limit";
        $result = $this->db->query($sql);
        if($result)
            return $result->result_array();
    }
}