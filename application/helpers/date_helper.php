<?php

/**
 * Copyright (c) 2015 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author        programmer/ Mohammed Fawzy Zayan <admin@zayanit.com>
 * @link          http://www.zayanit.com
 * @version       1.0
 * Created by Sublime Text 2
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Change date formate to a formate you want.
 *
 * @param $date date or datetime
 * @return date
*/
function changeDateFormate($date)
{
	$newDate = strtotime($date);
	return date('d/m/Y', $newDate);
}

/**
 * Change date formate to Google Sitemap Date Format.
 *
 * @param $date date or datetime
 * @return date
*/
function sitemapDateFormate($date)
{
	$newDate = strtotime($date);
	return date('c', $newDate);
}

function changeDatePublished($date)
{
	$newDate = strtotime($date);
	return date('Y/m/d', $newDate);
}