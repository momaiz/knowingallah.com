<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @param $url
 * @return bool
 */
function getRedirectUrl ($url) {
    stream_context_set_default(array(
        'http' => array(
            'method' => 'HEAD'
        )
    ));
    $headers = get_headers($url, 1);
    if ($headers !== false && isset($headers['Location'])) {
        return $headers['Location'];
    }
    return false;
}