<?php 

/**
 * Copyright (c) 2015 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author        programmer/ Mohammed Fawzy Zayan <admin@zayanit.com>
 * @link          http://www.zayanit.com
 * @version       1.0
 * Created by Sublime Text 2
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *  Remove some html tags and leave some.
 * @param $string string
 * @return string
 */
function clean_text($string)
{
    $string = trim($string);
    $string = strip_tags($string, '<a><p><h1><h2><h3><h4><h5><h6><div><br><img><blockquote><caption><cite><u><ul><li><hr>');
    return $string;
}

/**
 * Like php substr function but stop at word end.
 * @param $string string
 * @param $start integer
 * @param $end integer
 * @return string
 */
function substr_word_end($string, $start, $end)
{
    $string = mb_substr(strip_tags($string), $start, $end, 'UTF-8');
    $string = substr($string, $start, strrpos($string, ' '));
    return $string;
}