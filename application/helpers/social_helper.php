<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @param $user
 * @param $apikey
 * @return google+ followers count
 */
function googleplus_followers_count( $user, $apikey ) {
    $google = file_get_contents( 'https://www.googleapis.com/plus/v1/people/' . $user . '?key=' . $apikey );
    return json_decode( $google )->circledByCount;
}

/**
 * @param $page_name
 * @return facebook page fans count
 */
function facebook_fans_count($page_name)
{
    $ch = curl_init("http://graph.facebook.com/$page_name");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $raw = curl_exec($ch);
    curl_close($ch);
    return $fb = json_decode($raw)->likes;
}

/**
 * @param $user
 * @return twitter followers count
 * @throws Exception
 */
function twitter_followers_count( $user ) {
    require_once (APPPATH.'libraries/TwitterAPIExchange.php');
    /** Set access tokens here - see: https://dev.twitter.com/apps/ **/
    $settings      = array(
        'oauth_access_token'        => "334601537-vhWLUVwB1bPusqWnhHCmdXI49sixIBLRxOsH4Be2",
        'oauth_access_token_secret' => "y50vTFXrPAsU4drEDvLtfp16aEG1D1DHCcScK0KTYVI",
        'consumer_key'              => "Rje13aaKmhsJftt11tcfQ",
        'consumer_secret'           => "cgSL6jr8lrRcd8hos0XTDI7nKX8wWDOaHoMji6OjvZw"
    );
    $url           = 'https://api.twitter.com/1.1/users/show.json';
    $getfield      = '?screen_name=' . $user;
    $requestMethod = 'GET';
    $twitter       = new TwitterAPIExchange( $settings );
    $follow_count  = $twitter->setGetfield( $getfield )
        ->buildOauth( $url, $requestMethod )
        ->performRequest();
    $get_count     = json_decode( $follow_count, true );

    return $get_count['followers_count'];
}

/**
 * @param $url_or_id
 * @return facebook page fans count or shares
 */
function shinra_fb_count( $url_or_id ) {
    /* url or id (for optional validation) */
    if( is_int( $url_or_id ) ) $url = 'http://graph.facebook.com/' . $url_or_id;
    else $url = 'http://graph.facebook.com/' .  $url_or_id;
 
    /* get json */
    $json = json_decode( file_get_contents( $url ), false );
 
    /* has likes or shares? */
    if( isset( $json->likes ) ) return (int) $json->likes;
    elseif( isset( $json->shares ) ) return (int) $json->shares;
 
    return 0; // otherwise zed
}