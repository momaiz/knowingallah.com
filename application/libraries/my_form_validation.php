<?php if (!defined('BASEPATH')) exit('No direct script access allowed.');

class MY_Form_validation extends CI_Form_validation {

	function __construct() {
		parent::__construct();
	}


	function isWeb($web='')
	{
		$strPattern = "/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i";
		if(preg_match($strPattern, $web))
		{
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function is_blocked($url=''){
		$domain=get_domain($url);
		$this->CI->db->like('domain',$domain);
		$result=$this->CI->db->get('blacklist');
		if($result->num_rows() > 0){
			return FALSE;
		}else{
			return TRUE;
		}

	}



}