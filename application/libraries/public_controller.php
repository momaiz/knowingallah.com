<?php
/**
 * user controller
 *
 * PHP version 5
 *
 * @category   Library
 * @package    SAMA
 * @subpackage Public_Controller
 * @author     Ahmed Abu ElDahab <DahabIT@gmail.com>
 * @license    Copyright (c) 2010 - 2011, WasmMedia, Inc.
 * @version    SVN: 0.2
 * @link       https://tracsamafw.orood.com/
 */

/**
 * user controller
 *
 * @category   Controller
 * @package    Mojaz
 * @subpackage mojaz_Core
 * @author     Ahmed Abu ElDahab <DahabIT@gmail.com>
 * @license    Copyright (c) 2010 - 2011, WasmMedia, Inc.
 * @version    Release: @package_version@
 * @link       https://mojaz.wasmdev.com/
 */
class Public_Controller extends CI_Controller
{
    /**
     *
     * Declare VARS
     * @var $_CI private var to get core codeigniter options
     */
    private $_ci;

     /**
     * welcome
     */
    function __construct ()
    {
        parent::__construct();
       /* $this->_ci = &get_instance();
       	$this->_ci->load->library('tank_auth');
       	$this->_ci->load->library('template');
		$this->_ci->lang->load('tank_auth');
		$this->_ci->lang->load('main');
		//$this->template->set_layout('default');
		//$this->carabiner->css('main.css');
		*/
     
    }
}