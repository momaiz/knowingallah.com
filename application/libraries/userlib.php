<?php
class Userlib
{
	private $CI;
	function __construct() {

		$this->CI = &get_instance();
	}



	function check_ie(){
		$this->CI->load->library('user_agent');
		$iever=(int) $this->CI->agent->version();
		//do_dump($this->CI->agent->browser());
		//die();
		if($this->CI->agent->browser() == "Internet Explorer"){
			return  $iever;
		}else{
			return FALSE;
		}
	}


	function notify_email($type, $email, &$data)
	{
		$this->CI->load->library('email');
		$this->CI->email->from($this->CI->config->item('webmaster_email', 'tank_auth'), $this->CI->config->item('website_name', 'tank_auth'));
		$this->CI->email->reply_to($this->CI->config->item('webmaster_email', 'tank_auth'), $this->CI->config->item('website_name', 'tank_auth'));
		$this->CI->email->to($email);
		$this->CI->email->subject(sprintf('تنبيه بردود جديدة',$this->CI->config->item('website_name', 'tank_auth')));
		$this->CI->email->message($this->CI->load->view('email/'.$type.'-html', $data, TRUE));
		$this->CI->email->set_alt_message($this->CI->load->view('email/'.$type.'-txt', $data, TRUE));
		$this->CI->email->send();
	}



}