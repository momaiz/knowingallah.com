<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "intro";
$route['404_override']       = 'my404error';
//$route['(\w{2})/(.*)']     = '$2';
$route['(\w{2})']            = $route['default_controller'];

$route['(\w{2})/sitemap/(:any)'] = 'sitemap/$2';
#$route['(\w{2})/slug/(:any)']    = 'slug/$2';

$route['(\w{2})/home']        = 'pages/home';
$route['(\w{2})/latest']      = 'pages/new_site';
$route['(\w{2})/contact']     = 'pages/contact_us';
$route['(\w{2})/contactSend'] = 'pages/sendContactUs';
$route['(\w{2})/search']      = 'pages/search';
$route['(\w{2})/about']       = 'pages/page';
$route['(\w{2})/gallery']     = 'pages/photo_gallery';
$route['(\w{2})/page/(:num)'] = 'pages/page/$2';
$route['(\w{2})/pages']       = 'pages/index';
//$route['(\w{2})/articlesWithDefaultPhoto'] = 'pages/getAllArticlesWithDefaultPhoto';

$route['(\w{2})/articles']                        = 'articles';
$route['(\w{2})/articles/category']               = 'articles/category';
$route['(\w{2})/articles/category/(:num)']        = 'articles/category/$2';
$route['(\w{2})/articles/category/(:num)/(:num)'] = 'articles/category/$2/$3';
$route['(\w{2})/articles/all_articles']           = 'articles/all_articles';
$route['(\w{2})/articles/all_articles/(:num)']    = 'articles/all_articles/$2';
$route['(\w{2})/articles/download_article/(:num)']= 'articles/download_article/$2';
$route['(\w{2})/articles/latest_articles']        = 'articles/latest_articles';
$route['(\w{2})/articles/(:num)']                 = 'articles/article/$2';
$route['(\w{2})/articles/(:any)']                 = 'articles/getArticleBySlug/$2';

$route['(\w{2})/books']        = 'books/index';
$route['(\w{2})/books/(:num)'] = 'books/index/$2';
$route['(\w{2})/books/(:any)'] = 'books/getBookBySlug/$2';
$route['(\w{2})/books/incrementDownloads'] = 'books/incrementDownloads';

$route['(\w{2})/audios']                        = 'audios';
$route['(\w{2})/audios/category']               = 'audios/category';
$route['(\w{2})/audios/category/(:num)']        = 'audios/category/$2';
$route['(\w{2})/audios/category/(:num)/(:num)'] = 'audios/category/$2/$3';
$route['(\w{2})/audios/all_audios']             = 'audios/all_audios';
$route['(\w{2})/audios/all_audios/(:num)']      = 'audios/all_audios/$2';
$route['(\w{2})/audios/latest_audios']          = 'audios/latest_audios';
$route['(\w{2})/audios/(:num)']                 = 'audios/audio/$2';
$route['(\w{2})/audios/(:any)']                 = 'audios/getAudioBySlug/$2';

$route['(\w{2})/videos']                        = 'videos';
$route['(\w{2})/videos/category']               = 'videos/category';
$route['(\w{2})/videos/category/(:num)']        = 'videos/category/$2';
$route['(\w{2})/videos/category/(:num)/(:num)'] = 'videos/category/$2/$3';
$route['(\w{2})/videos/all_videos']             = 'videos/all_videos';
$route['(\w{2})/videos/all_videos/(:num)']      = 'videos/all_videos/$2';
$route['(\w{2})/videos/latest_videos']          = 'videos/latest_videos';
$route['(\w{2})/videos/(:num)']                 = "videos/video/$2";
$route['(\w{2})/videos/(:any)']                 = 'videos/getVideoBySlug/$2';

$route['(\w{2})/add_like']     = 'likes/add_like';
$route['(\w{2})/likes_get']    = 'likes/likes_get';
$route['(\w{2})/add_dislike']  = 'likes/add_dislike';
$route['(\w{2})/dislikes_get'] = 'likes/dislikes_get';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
