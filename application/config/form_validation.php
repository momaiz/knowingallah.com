<?php

$config = array(
                 'posts' => array(
                                array(
                                	'field'=>'post_title',
                                	'label'=>'Post_title',
                                	'rules'=>'required|trim|xss_clean'
                                ),
								array(
                                	'field'=>'post_url',
                                	'label'=>'Post_url',
                                	'rules'=>'required|trim|xss_clean'
                                ),
								array(
                                	'field'=>'post_text',
                                	'label'=>'Post_text',
                                	'rules'=>'trim|xss_clean'
                                ),
								array(
                                	'field'=>'cat_id',
                                	'label'=>'Cat_id',
                                	'rules'=>'required|trim|xss_clean'
                                )
				)
			   );
			   
?>