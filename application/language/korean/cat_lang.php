<?php
$lang['return_to_parent_cat']      = 'Back to supreme category';
$lang['sub_cats']                  = 'Categories under this category';
$lang['translated_to_other_langs'] = 'Category translated to';
$lang['hits']                      = 'Hits';

$lang['sub_articles']              = 'View articles under this category';
$lang['article_title']             = 'Article Title';

$lang['sub_videos']                = 'View videos under this category';
$lang['video_title']               = 'Video Title';

$lang['sub_audios']                = 'View lessons under this category';
$lang['audio_title']               = 'Lesson Title';

$lang['sub_flashs']                = 'View flashes under this category';
$lang['flash_title']               = 'Flash Title';

$lang['sub_books']                 = 'View books under this category';
$lang['book_title']                = 'Book Title';