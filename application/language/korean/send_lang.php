<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

$lang['name']            = '당신의 이름: ';
$lang['email']           = '당신의 이메일: ';
$lang['friend_email']    = '친구 이메일: ';
$lang['comment']         = '의견 남기기 (선택사항): ';
$lang['submit']          = '보내기';
$lang['subject']         = '친구로부터의 메시지: 선지자께 하나님의 평화와 축북을 보내세요';
$lang['message1']        = '하나님의 평화가 당신께 깃들길';
$lang['message2']        = '친구로부터 메시지가 도착하였습니다 ';
$lang['message3']        = ' 중 ';
$lang['message4']        = '예언자 무함마드 웹싸이트 지원';
$lang['message5']        = '그가 당신께 사이트를 둘러보도록 초대합니다';
$lang['message6']        = '메시지 : ';
$lang['conferm_massage'] = '성공적으로 발송. 하나님의 축복이 깃들길';