<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

$lang['name']            = 'Je naam: ';
$lang['email']           = 'Jouw e-mail: ';
$lang['friend_email']    = 'Stuur e-mail naar een vriend(in): ';
$lang['comment']         = 'Ruimte voor opmerkingen (optioneel): ';
$lang['submit']          = 'Verstuur';
$lang['subject']         = 'Een bericht van een vriend: Stuur Vrede en Zegeningen aan de Profeet Mohammed';
$lang['message1']        = 'Vrede zij met je';
$lang['message2']        = 'Je hebt een bericht van een vriend ';
$lang['message3']        = ' van ';
$lang['message4']        = 'Steun de Profeet Mohammed website';
$lang['message5']        = 'Hij nodigt je uit een kijkje op de website te nemen: ';
$lang['message6']        = 'Bericht : ';
$lang['conferm_massage'] = 'Versturen gelukt, Jazakum Allah khayr';