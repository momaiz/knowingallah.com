<?php
$lang['return_to_parent_cat']      = 'Retour à la catégorie Haut';
$lang['sub_cats']                  = 'Sous catégories';
$lang['translated_to_other_langs'] = 'Catégorie traduite en';
$lang['hits']                      = 'visité';

$lang['sub_articles']              = 'Afficher les articles sous cette catégorie';
$lang['article_title']             = "Titre de l'article";

$lang['sub_videos']                = 'Afficher les videos sous cette catégorie';
$lang['video_title']               = 'Titre de la video';

$lang['sub_audios']                = 'Afficher les audios sous cette catégorie';
$lang['audio_title']               = "Titre de l'audio";

$lang['sub_flashs']                = 'Afficher les flashs sous cette catégorie';
$lang['flash_title']               = 'Titre de la flashe';

$lang['sub_books']                 = 'View books under this category';
$lang['book_title']                = 'Book Title';