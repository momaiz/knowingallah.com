<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

$lang['name']            = 'Nom: ';
$lang['email']           = 'Email: ';
$lang['friend_email']    = 'E-mail de votre ami: ';
$lang['comment']         = 'Votre commentaire (facultatif): ';
$lang['submit']          = 'Envoyer';
$lang['subject']         = "Priez sur le Messager d'Allah : Un message de votre ami";
$lang['message1']        = "Que la paix d'Allah et sa miséricorde vous accompagnent";
$lang['message2']        = 'Votre ami ';
$lang['message3']        = ' a envoyé cette lettre ';
$lang['message4']        = 'Site de soutien au Messager d\'Allah Muhammad';
$lang['message5']        = 'Vous invite à visiter cette page: ';
$lang['message6']        = 'Le message : ';
$lang['conferm_massage'] = 'Qu\'Allah vous récompense par le bien ,envoi de message réussi';