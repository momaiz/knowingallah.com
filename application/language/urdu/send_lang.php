<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

$lang['name']            = 'آپکا نام: ';
$lang['email']           = 'ايميل پتہ: ';
$lang['friend_email']    = 'تمہارے دوست کا ایمیل پتہ: ';
$lang['comment']         = 'تبصرہ (اختیاری): ';
$lang['submit']          = 'بھیجیں';
$lang['subject']         = 'دوست کی طرف سے پیغام: رسول اللہ پردرووسلام ہو.';
$lang['message1']        = 'السلام علیکم ورحمۃ اللہ وبرکاتہ';
$lang['message2']        = 'تمہارے دوست ';
$lang['message3']        = ' نے پیغام بھیجا ہے اور تمہیں ';
$lang['message4']        = 'نصرت محمد رسول اللہ ویب سائٹ';
$lang['message5']        = 'ویب سائٹ کے فلاں پیج کی زیارت کی دعوت دے رہا ہے: ';
$lang['message6']        = 'پیغام';
$lang['conferm_massage'] = 'کامیا بی سے خط بھیجا گیا: اللہ تمہیں نیک صلہ عطا کرے.';