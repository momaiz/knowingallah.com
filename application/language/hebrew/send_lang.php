<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

$lang['name']            = 'שמך: ';
$lang['email']           = 'Your Email: ';
$lang['friend_email']    = 'דואר אליקטרוני של חברך: ';
$lang['comment']         = 'תגובתך ( אם אתה רוצה ): ';
$lang['submit']          = 'Send';
$lang['subject']         = 'ברכה ושלום על שליחו של אאללה : הודעה מידידך';
$lang['message1']        = 'שלום וברכה עליכם';
$lang['message2']        = 'שלח לך את זה ';
$lang['message3']        = ' חברך ';
$lang['message4']        = 'האתר של הנביא מוחמד עליו שלום וברכה';
$lang['message5']        = 'קורא לך לקרוא מאמר זה';
$lang['message6']        = 'המסר : ';
$lang['conferm_massage'] = 'תודה רבה , שה נזלח בהצלחה';