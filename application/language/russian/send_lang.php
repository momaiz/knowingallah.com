<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

$lang['name']            = 'Ваше имя: ';
$lang['email']           = 'Your Email: ';
$lang['friend_email']    = 'Почтовый адрес вашего друга: ';
$lang['comment']         = 'Ваш комментарий (добровольно): ';
$lang['submit']          = 'Send';
$lang['subject']         = 'Сообщение от друга: Скажи: да благословит его Аллах и приветствует';
$lang['message1']        = 'Ассаляму аляйкум ва рахмату Аллахи ва баракатуху';
$lang['message2']        = 'Твой друг отправил тебе ';
$lang['message3']        = ' Это сообщение от ';
$lang['message4']        = 'Сайт поддержки Мухаммада посланника Аллаха';
$lang['message5']        = 'Приглашение посетить одну из страниц сайта';
$lang['message6']        = 'Сообщение : ';
$lang['conferm_massage'] = 'Да вознаградит Аллах вас добром Сообщение успешно отправлено';