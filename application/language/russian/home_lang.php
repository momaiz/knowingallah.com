<?php
$lang['categories']       = 'Разделы';
$lang['last_articles']    = 'New Articles';
$lang['last_videos']      = 'New Videos';
$lang['last_audios']      = 'New Audios';
$lang['last_flashs']      = 'New Flashes';
$lang['site_new']         = 'New Site';
$lang['others']           = 'Подробно';
$lang['siteTeam']         = 'Site Team';

$lang['choose_lang']      = 'Choose Language';
$lang['search']           = 'Search';

$lang['whoIsAllah']        = 'Who is Allah';
$lang['whoIsAllahDesc']    = 'Where is God? And what do we know about the same? And how his attributes? Do you get angry and rejoice? And the meanings of names? And how to contact God and we do not see it? And what of us likes and dislikes .. answer questions about it here';
$lang['existenceGod']      = 'Evidence of the existence of God';
$lang['existenceGodDesc']  = 'We do not see with our own eyes what God prove its existence? Is his presence right irrefutable or doubt in his presence? Or presence of man-made? Is there evidence of the fact of the existence of God? Here you know the answer to these questions';
$lang['godAndMan']         = 'God and man';
$lang['godAndManDesc']     = 'What kind of relationship slave Balaha? God does not want human misery and between him and the man\'s struggle to impose his authority and strength it? Mother of God, a God merciful cream-loving man? Bam advantage over the rest of the creatures? And why he did it and what he wants from him';
$lang['sonAtheist']        = 'My son atheist';
$lang['sonAtheistDesc']    = 'How do you deal with your son atheist who denies the existence of God? How making the voice logic and convince him substantiated the existence of the Creator? Here you will find the answer to all these questions, we have suggestions that will help you overcome those stumbling block in peace';
$lang['browse']            = 'Browse';
$lang['specialVideo']      = 'Special Video';
$lang['returnersToAllah']  = 'Returnees to God';
$lang['godJudgments']      = 'God\'s judgments';
$lang['questionsAboutGod'] = 'Questions about God';
$lang['watchVideo']        = 'Watch Video';
$lang['mostVisited']       = 'Most Visited';

$lang['watchMore']        = 'More';
$lang['creationDate']     = 'Creation Date';
$lang['visits']           = 'Visits';
$lang['addComment']       = 'Add Comment';
$lang['listen']           = 'Listen';
$lang['photos']           = 'Photos';
$lang['bookForDownload']  = 'Books For Download';