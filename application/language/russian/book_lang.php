<?php
/**
 * Copyright (c) 2013 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @copyright     copyright 2013
 * @author        programmer/ Mohammed Fawzy Zayan <admin@zayanit.com>
 * @link          http://www.zayanit.com
 * @version       1.0
 * Created by JetBrains PhpStorm 6.0
 */

$lang['book_translated_to'] = 'Book translated to';
$lang['books_in_same_cat']  = 'Books in same category';
$lang['download_book']      = 'Download Book .. Here';
$lang['book_photo']         = 'Book Photo';
$lang['moreDownloaded']     = 'Most Downloaded';
$lang['noBooks']            = 'Sorry, there are no books.';
$lang['downloadTheBook']    = 'Click here to download the book';
$lang['shareBookIn']        = 'Share Book In :';
$lang['relatedBooks']       = 'Related Books';
