<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

$lang['name']            = 'Your Name: ';
$lang['email']           = 'Your Email: ';
$lang['friend_email']    = 'Your Friend Email: ';
$lang['comment']         = 'Your Comment (optional): ';
$lang['submit']          = 'Send';
$lang['subject']         = 'Frieden und Segen dem Verkünder Allahs: Ein Brief von Ihrem Freund';
$lang['message1']        = 'Assalamu Alaikom Wa Rahmatu Allah Wa Barakatoh (Frieden sei mit Ihnen)';
$lang['message2']        = 'Ihr Freund ';
$lang['message3']        = ' sandte Ihnen diesen Brief Von ';
$lang['message4']        = 'Die "Prophet Mohammed, Verkünder Allahs" Webseite.';
$lang['message5']        = 'Um Sie einzuladen, eine der Seiten dieser Website zu besuchen: ';
$lang['message6']        = 'Die Mitteilung : ';
$lang['conferm_massage'] = 'Sent Successfully. Jazakum Allah Kheiran';