<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

$lang['title']             = 'DataBase For the Team Work';
$lang['name']              = 'Name';
$lang['phone']             = 'Phone';
$lang['email']             = 'E-mail';
$lang['age']               = 'Age';
$lang['qualification']     = 'Qualification';
$lang['nationality']       = 'Nationality';
$lang['location']          = 'Location';
$lang['city']              = 'City';
$lang['country']           = 'Country';
$lang['otherExperience']   = 'Studies and other certifications';
$lang['activities']        = 'Activities';
$lang['mainlanguage']      = 'Mother Language';
$lang['otherLanguages']    = 'Other Languages';
$lang['profession']        = 'Essential job';
$lang['skills']            = 'Other abilities interesting for the work team';
$lang['forumName']         = 'Forum dislay name';
$lang['forum']             = 'Supporting Prophet Muhammad website Forum';
$lang['forumTeam']         = 'Your team in the forum or the website';
$lang['otherInformation']  = 'Other Informations';
$lang['popularTV']         = 'Most famous tv channel in your region';
$lang['popularPlace']      = 'The most important streets and fields where banners can be put and prices if possible';
$lang['websites']          = 'Do your area have a page or a group on the facebook (or any other site) to reach them through it';
$lang['comments']          = 'Comments';
$lang['submit']            = ' Send ';
$lang['selectLang']        = '-- please select a language --';
$lang['mainInformation']   = ' Main Information ';
$lang['confirm_message']   = 'May Allah reward you, you have been added to our database';
$lang['error_message']     = 'Sorry, an error occurred please try again';
$lang['duplicate_message'] = 'Oops you are registered before';