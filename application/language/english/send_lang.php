<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

$lang['name']            = 'Your Name: ';
$lang['email']           = 'Your Email: ';
$lang['friend_email']    = 'Your Friend Email: ';
$lang['comment']         = 'Your Comment (optional): ';
$lang['submit']          = 'Send';
$lang['subject']         = 'A message from a friend: Send peace and blessings on the Messenger of Allah';
$lang['message1']        = 'Peace be upon you ';
$lang['message2']        = 'You have got a message from a friend ';
$lang['message3']        = ' this message from ';
$lang['message4']        = 'Supporting Prophet Muhammad website ';
$lang['message5']        = 'He invites you to browse the website ';
$lang['message6']        = 'Message : ';
$lang['conferm_massage'] = 'Sent Successfully. Jazakum Allah Kheiran';

$lang['books']             = 'Books';