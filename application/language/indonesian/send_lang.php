<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

$lang['name']            = 'nama anda: ';
$lang['email']           = 'Your Email: ';
$lang['friend_email']    = 'Email untuk teman anda: ';
$lang['comment']         = 'komentar (pilihan): ';
$lang['submit']          = 'Send';
$lang['subject']         = 'Bershalawatlah kepada Rasulullah saw.:  sebuah surat dari teman anda';
$lang['message1']        = 'Assalamu \'alaikum wa rahmatullahi wa barakaatuh';
$lang['message2']        = 'Teman Anda telah mengirimkan ';
$lang['message3']        = ' pesan ini dari ';
$lang['message4']        = 'Website Muhammad Rasulullah saw.';
$lang['message5']        = 'Mengundang Anda untuk mengunjungi salah satu halaman web: ';
$lang['message6']        = 'pesan : ';
$lang['conferm_massage'] = 'terima kasih, pesan telah terkirim';