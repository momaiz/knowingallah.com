<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

$lang['name']            = 'Your Name: ';
$lang['email']           = 'Your Email: ';
$lang['friend_email']    = 'Your Friend Email: ';
$lang['comment']         = 'Your Comment (optional): ';
$lang['submit']          = 'Send';
$lang['subject']         = 'che la pace e la benedizione siano sul Messaggero di Allah: una lettera di un amico';
$lang['message1']        = 'Assalamu alaikom wa rahmatu Allah wa barakatoh (La Pace sia con te)';
$lang['message2']        = 'Il tuo amico ';
$lang['message3']        = ' ti ha inviato questa lettera Da ';
$lang['message4']        = 'Supporting Prophet Muhammad website ';
$lang['message5']        = 'e ti ha invitato a visitare una delle pagine del sito web';
$lang['message6']        = 'Messaggio : ';
$lang['conferm_massage'] = 'Sent Successfully. Jazakum Allah Kheiran';