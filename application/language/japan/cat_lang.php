<?php
$lang['return_to_parent_cat']      = '戻る';
$lang['sub_cats']                  = 'このセクションの下に';
$lang['translated_to_other_langs'] = 'このセクションは～～に翻訳された';
$lang['hits']                      = '再生回数';

$lang['sub_articles']              = 'このセクションの記事をビュー';
$lang['article_title']             = '記事の見出し';

$lang['sub_videos']                = 'このセクションの動画をビュー';
$lang['video_title']               = '動画の見出し';

$lang['sub_audios']                = 'このセクションのレッスンをビュー';
$lang['audio_title']               = 'レッスンの見出し';

$lang['sub_flashs']                = 'このセクションのフラッシュファイルをビュー';
$lang['flash_title']               = 'フラッシュファイルの見出し';

$lang['sub_books']                 = 'View books under this category';
$lang['book_title']                = 'Book Title';