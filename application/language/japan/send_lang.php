<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

$lang['name']            = '氏名: ';
$lang['email']           = 'メールアドレス: ';
$lang['friend_email']    = '友人のメールアドレス: ';
$lang['comment']         = 'コメント（随意）: ';
$lang['submit']          = '提出';
$lang['subject']         = 'アッラーの使徒にアッラーの平安と祝福があるようにお祈りし、友人からのメッセージ';
$lang['message1']        = 'あなたにアッラーの慈悲と祝福があるように ';
$lang['message2']        = 'あなたの友人があなたに～～を送りました ';
$lang['message3']        = ' このメッセージは～～から ';
$lang['message4']        = 'アッラーの使徒ムハンマドの後援サイト ';
$lang['message5']        = '～～がサイトのあるページのご覧を進めました。 ';
$lang['message6']        = 'メッセージ : ';
$lang['conferm_massage'] = 'メッセージが届きました。ありがとうございました。';