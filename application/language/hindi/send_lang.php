﻿<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

$lang['name']            = 'आपका नाम: ';
$lang['email']           = 'आपका ई मेल एड्रेस: ';
$lang['friend_email']    = 'आपके मित्र का इ मेल एड्रेस: ';
$lang['comment']         = 'आपकी टिप्पणी ऐच्छिक है: ';
$lang['submit']          = 'भेजें';
$lang['subject']         = 'अल्लाह के पैगंबर मुहम्मद के लिये अल्लाह से आशीर्वाद मांग लीजिये. यह देखये आपके मित्र का एक संदेश';
$lang['message1']        = 'अस्सलामोअलेकुम अल्लाह की दया और उसका कृपा हो आपके लिये';
$lang['message2']        = 'यह संदेश ';
$lang['message3']        = ' की ओर से है आपके मित्र ने आपके लिये एक संदेशा भेजा है. ';
$lang['message4']        = 'पैगंबर हज़रत मुहम्मद के समर्थन की वेबसाइट';
$lang['message5']        = 'उन्होंने आपको इस वेबसाइट के एक पन्ना को देखने के लिये  आमंत्रित किया है: ';
$lang['message6']        = 'संदेशा : ';
$lang['conferm_massage'] = 'अल्लाह आपको अच्छा रखे आप का मेल भेज दिया गया';