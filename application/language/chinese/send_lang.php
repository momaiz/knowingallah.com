<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

$lang['name']            = '你的名字：';
$lang['email']           = 'Your Email：';
$lang['friend_email']    = '给你的朋友发送电子邮件：';
$lang['comment']         = '你的评论（可选）：';
$lang['submit']          = 'Send：';
$lang['subject']         = '祝福安拉的使者：来自你朋友的一封信';
$lang['message1']        = '祈安拉赐你平安、慈悯与吉庆';
$lang['message2']        = '你的朋友 ';
$lang['message3']        = ' 给你发送了这封邮件从 ';
$lang['message4']        = '安拉的使者网站';
$lang['message5']        = '邀请你访问网页';
$lang['message6']        = '信息';
$lang['conferm_massage'] = '祈安拉回赐你，邮件已正确发送';