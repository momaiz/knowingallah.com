<?php
$lang['websiteName']       = 'Allah\'ı Tanıyalım';

$lang['meta_description']  = 'To all those who search for the truth, wish to get closer to Allah and increase their knowledge of Him';
$lang['meta_keywords']     = 'God ,Jesus , Is Jesus God, Our Real God , Who Is My God , Allah , Know Allah , How to know Allah , Who is allah , Allah';

$lang['homepage']          = 'Anasayfa';
$lang['aboutus']           = 'About Site';
$lang['forum']             = 'Forum';
$lang['articles']          = 'Makaleler';
$lang['books']             = 'Kitaplar';
$lang['audios']            = 'Sesli Yayınlar';
$lang['videos']            = 'Videolar';
$lang['flashs']            = 'Flashs';
$lang['photoalbum']        = 'Albüm';
$lang['contribute']        = 'Contributed to the victory';
$lang['contactus']         = 'Bize ulaşın';
$lang['publications']      = 'Site Publications';
$lang['menu']              = 'Ana menü';
$lang['software']          = 'You need the following programs';
$lang['copyright']         = 'Müslüman kardeşim! Bütün haklarından yararlanabilirsin';
$lang['midade']            = 'Web devlopement by Midade';
$lang['sabeqoon']          = 'Sabeqoon e-alışveriş';
$lang['hits']              = 'Number of viewing of the website';
$lang['mailListJoin']      = 'Mail listesine üye olun';
$lang['ok']                = 'Onayla';
$lang['mailListJoined']    = 'Mail listesine üyeliğiniz tamamlanmıştır. Allah razı olsun.';
$lang['mailListNotJoined'] = 'This email address is registered';
$lang['books']             = 'Books';

$lang['relatedSites']      = 'Related Sites';
$lang['services']          = 'Our Services';
$lang['news']              = 'News';
$lang['prev']              = 'Prev';
$lang['next']              = 'Next';
$lang['relatedVideos']     = 'Related Videos';
$lang['playVideo']         = 'Wathc Video';
$lang['relatedAudios']     = 'Related Audios';
$lang['aboutSites']        = 'About Sites';
$lang['notFound']          = 'Page Not Found';
$lang['404message1']       = 'Sorry it seems that the page has been deleted for some reason';
$lang['404message2']       = 'You can return to ';
$lang['404homepage']       = 'Home Page';
$lang['search_message']    = 'You can also search for the topic you want from here';

$lang['liked_before']      = 'You have liked this before';
$lang['not_found']         = 'Not Found';
$lang['disliked_before']   = 'You have disliked this before';
$lang['followUs']          = 'Follow US';
$lang['recentPost']        = 'Resent Post';
$lang['relatedArticles']   = 'Related Articles with';

$lang['shareArticle']       = 'Share article';
$lang['shareVideo']         = 'Share video';
$lang['shareAudio']         = 'Share audio';
$lang['mostViewedArticles'] = 'Most viewed articles in category';
$lang['mostViewedVideos']   = 'Most viewed videos in category';
$lang['watchToo']           = 'Watch too';
$lang['mostViewed']         = 'Most viewed videos';
$lang['listen']             = 'Listen';
$lang['mostViewedAudio']    = 'Most listened';
$lang['listenToo']          = 'Listen too';
$lang['mostListen']         = 'Most listened';
$lang['mostWatched']        = 'Most watched';
$lang['facebook']           = 'Facebook';
$lang['twitter']            = 'Twitter';
$lang['googlePuls']         = 'Google plus';

$lang['madaTitle']          = 'Title';
$lang['shikh']              = 'Auther';
$lang['category']           = 'Category';

$lang['download']           = 'Download';