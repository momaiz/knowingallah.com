<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

$lang['name']            = 'Adınız: ';
$lang['email']           = 'e-mail adresiniz: ';
$lang['friend_email']    = 'Arkadaşınızın e-mail adresi: ';
$lang['comment']         = 'Yorumunuz (Tercihli): ';
$lang['submit']          = 'Gönder';
$lang['subject']         = 'Rasulullah\'a salat getirin! Arkadaşınızdan gelen mesaj';
$lang['message1']        = 'Esselamu aleykum ve rahmetullahi ve berakatuhu';
$lang['message2']        = 'Arkadaşınız ';
$lang['message3']        = 'Size şu mesajı göndermiştir of ';
$lang['message4']        = 'Muhammed Rasulullah web sitesi';
$lang['message5']        = 'Web sitesinin bir sayfasını ziyaret etmenizi istiyor: ';
$lang['message6']        = 'Mesajınız : ';
$lang['conferm_massage'] = 'Mesajınız başarıyla gönderilmiştir. Allah razı olsun';