<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

$lang['name']            = 'Su nombre: ';
$lang['email']           = 'Your Email: ';
$lang['friend_email']    = 'El correo electrónico de su amigo: ';
$lang['comment']         = 'Su comentario (opcional)';
$lang['submit']          = 'Send';
$lang['subject']         = 'Bendiciones sobre el Mensajero de Allah: Un mensaje de su amigo';
$lang['message1']        = 'Assalamu alaikom wa rahmatu Allah wa barakatoh (La paz sea con usted)';
$lang['message2']        = 'Su amigo ';
$lang['message3']        = ' le ha enviado esta carta de ';
$lang['message4']        = 'El sitio web de apoyo a Muhammad el Mensajero de Allah';
$lang['message5']        = 'Le invita a visitar una de las páginas del sitio web';
$lang['message6']        = 'El mensaje';
$lang['conferm_massage'] = 'Que Allah le recompense, el mensaje ha sido enviado con éxito';