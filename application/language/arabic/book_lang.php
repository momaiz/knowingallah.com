<?php
/**
 * Copyright (c) 2013 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @copyright     copyright 2013
 * @author        programmer/ Mohammed Fawzy Zayan <admin@zayanit.com>
 * @link          http://www.zayanit.com
 * @version       1.0
 * Created by JetBrains PhpStorm 6.0
 */

$lang['book_translated_to'] = 'الكتاب مترجم إلى';
$lang['books_in_same_cat']  = 'كتب فى نفس القسم';
$lang['download_book']      = 'لتحميل الكتاب .. من هنا';
$lang['book_photo']         = 'صورة الكتاب';
$lang['moreDownloaded']     = 'الأكثر تحميلا';
$lang['noBooks']            = 'عذرا، لا يوجد كتب.';
$lang['downloadTheBook']    = 'اضغط هنا لتحميل الكتاب';
$lang['shareBookIn']        = 'شارك الكتاب عبر :';
$lang['relatedBooks']       = 'كتب متعلقة';
