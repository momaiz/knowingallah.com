<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

$lang['name']            = 'اسمك: ';
$lang['email']           = 'بريدك الإلكترونى: ';
$lang['friend_email']    = 'البريد الإلكترونى لصديقك: ';
$lang['comment']         = 'تعليقك (اختيارى): ';
$lang['submit']          = 'إرسال';
$lang['subject']         = 'صل على رسول الله: رسالة من صديقك ';
$lang['message1']        = 'السلام عليكم ورحمة الله وبركاته';
$lang['message2']        = 'لقد أرسل لك صديقك ';
$lang['message3']        = ' هذه الرسالة من ';
$lang['message4']        = 'موقع نصرة محمد رسول الله';
$lang['message5']        = 'يدعوك فيها لزيارة إحدى صفحات الموقع: ';
$lang['message6']        = 'الرسالة : ';
$lang['conferm_massage'] = 'جزاكم الله خيرا تم إرسال رسالتك';
