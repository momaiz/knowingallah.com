﻿<?php
$lang['author']                = 'الكاتب';
$lang['underCategory']         = 'تحت قسم';
$lang['creationDate']          = 'تاريخ الاضافة';
$lang['article_translated_to'] = 'المقال مترجم الى';
$lang['print']                 = 'اطبع';
$lang['word']                  = 'حمل المقال بصيغة وورد';
$lang['articles_in_same_cat']  = 'مقالات في نفس القسم';
$lang['hits']                  = 'المشاهدات';
$lang['sendToFriend']          = 'أرسل الى صديق';
$lang['sendToFriendinLang']    = 'أرسل هذه الصفحة إلى صديق باللغة';
$lang['download']              = 'تحميل المرفقات';
$lang['link_error']            = 'للابلاغ عن درس لا يعمل';
$lang['share_campaign']        = 'ساهم فى دعم الموقع';
$lang['audios_in_same_cat']    = 'صوتيات فى نفس القسم';
$lang['videos_in_same_cat']    = 'مرئيات فى نفس القسم';
$lang['flashs_in_same_cat']    = 'فلاشات فى نفس القسم';
$lang['flash_translated_to']   = 'الفلاش مترجم إلى';
$lang['audio_translated_to']   = 'الدرس مترجم إلى';
$lang['video_translated_to']   = 'الفيديو مترجم إلى';
$lang['relatedPosts']          = 'مقالات من نفس القسم';

$lang['lastAdded']             = 'آخر المواد المضافة';
$lang['readToo']               = 'اقرأ ايضاً';
$lang['readMore']              = 'اقرأ المزيد';
$lang['viewToo']               = 'تصفح أيضاً';
$lang['catTranslation']        = 'ترجمات القسم';
