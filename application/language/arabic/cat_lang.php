<?php
$lang['return_to_parent_cat']      = 'العودة الى القسم الأعلى';
$lang['sub_cats']                  = 'الأقسام تحت هذا القسم';
$lang['translated_to_other_langs'] = 'القسم مترجم الى';
$lang['hits']                      = 'المشاهدات';

$lang['sub_articles']              = 'عرض المقالات تحت هذا القسم';
$lang['article_title']             = 'عنوان المقال';

$lang['sub_videos']                = 'عرض المرئيات تحت هذا القسم';
$lang['video_title']               = 'عنوان الفيديو';

$lang['sub_audios']                = 'عرض الدروس تحت هذا القسم';
$lang['audio_title']               = 'عنوان الدرس';

$lang['sub_flashs']                = 'عرض الفلاشات تحت هذا القسم';
$lang['flash_title']               = 'عنوان الفلاش';

$lang['sub_books']                 = 'عرض الكتب تحت هذا القسم';
$lang['book_title']                = 'عنوان الكتاب';