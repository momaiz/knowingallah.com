<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

$lang['title']             = 'قاعدة بيانات فرق العمل فى موقع نصرة رسول الله صلى الله عليه وسلم';
$lang['name']              = 'الاسم';
$lang['phone']             = 'رقم الهاتف';
$lang['email']             = 'البريد الإلكترونى';
$lang['age']               = 'العمر';
$lang['qualification']     = 'المؤهل';
$lang['nationality']       = 'الجنيسة';
$lang['location']          = 'مكان الإقامة';
$lang['city']              = 'المدينة';
$lang['country']           = 'البلد';
$lang['otherExperience']   = 'الدراسات والخبرات الأخرى';
$lang['activities']        = 'أنشطة';
$lang['mainlanguage']      = 'اللغة الأم';
$lang['otherLanguages']    = 'لغات أخرى';
$lang['profession']        = 'العمل الأساسى';
$lang['skills']            = 'مهارات أخرى تفيد العمل فى الفريق';
$lang['forumName']         = 'اسم الظهور فى';
$lang['forum']             = 'منتدى نصرة رسول الله صلى الله عليه وسلم';
$lang['forumTeam']         = 'اسم الفريق المشترك به في منتدي الموقع ان كان موجود';
$lang['otherInformation']  = 'بيانات إضافية';
$lang['popularTV']         = 'اشهر قناة تليفزيونية في منطقتك';
$lang['popularPlace']      = 'أنسب الأماكن في مدينتك التي يمكن عمل إعلان عن الموقع';
$lang['websites']          = 'هل لمنطقتك صفحة أو جروب علي الفيس بوك (أو أي موقع أخر ) يمكن الوصول اليهم عن طريقه';
$lang['comments']          = 'إضافات أخرى';
$lang['submit']            = ' إرسال ';
$lang['selectLang']        = '-- من فضلك اختر لغة --';
$lang['mainInformation']   = ' البيانات الأساسية ';
$lang['confirm_message']   = 'جزاكم الله خيرا تمت الإضافة بنجاح';
$lang['error_message']     = 'عفوا لقد حدث خطأ ما يرجى المحاولة مرة أخرى';
$lang['duplicate_message'] = 'عفوا أنت مسجل لدينا من قبل';
