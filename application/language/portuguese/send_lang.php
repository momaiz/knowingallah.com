<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

$lang['name']            = 'Your Name: ';
$lang['email']           = 'Your Email: ';
$lang['friend_email']    = 'Your Friend Email: ';
$lang['comment']         = 'Your Comment (optional): ';
$lang['submit']          = 'Send';
$lang['subject']         = 'a paz e as bençãos estejam com o Mensageiro de Alá: Uma carta do seu amigo';
$lang['message1']        = 'Assalamu alaikom wa rahmatu Allah wa barakatoh ( A paz esteja convosco)';
$lang['message2']        = 'O seu amigo ';
$lang['message3']        = ' enviou-lhe esta carta De ';
$lang['message4']        = 'O Website do Profeta Maomé, O Mensageiro de Alá';
$lang['message5']        = 'convidando-o a visitar uma das páginas do website: ';
$lang['message6']        = 'A mensagem : ';
$lang['conferm_massage'] = 'Sent Successfully. Jazakum Allah Kheiran';