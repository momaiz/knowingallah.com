<?php
class MY_Controller extends CI_Controller{

    function __construct(){
        parent::__construct();
        $current_lang = $this->config->item('language');
        $this->lang->load('article', $current_lang);
        $this->lang->load('cat', $current_lang);
        $this->lang->load('book', $current_lang);
    }

}