<?php
class MY_Model extends CI_Model
{
    protected $model = '';

    function increment_hits($id, $lang_id)
    {
        $this->db->set('hits', 'hits + 1', FALSE);
        $this->db->where(array($this->model."_id" =>$id, 'language_id' =>$lang_id));
        $this->db->update($this->model.'_translation');
    }

    function get_row_detail($model_id, $lang_id)
    {
        $this->db->select($this->model . '_translation.*, ' . $this->model . '_cat_translation.' . $this->model . '_cat_id, ' . $this->model . '_cat_translation.description as category');
        $this->db->from($this->model . '_translation');
        $this->db->join($this->model, $this->model . '_translation.' . $this->model . '_id = ' . $this->model . '.id');
        $this->db->join(
            $this->model . '_cat_translation',
            $this->model . '_cat_translation.' . $this->model . '_cat_id = ' . $this->model . '.' . $this->model . '_cat_id and ' . $this->model . '_cat_translation.language_id =' . $lang_id,
            'left outer');
        $this->db->where($this->model . '_id = ' . $model_id . ' and ' . $this->model . '_translation.language_id = ' . $lang_id);
        $query = $this->db->get();
        if ($query) {
            return $query->row_array();
        }
    }

    function get_row_detail_by_slug($slug, $lang_id)
    {
        $slug = urldecode($slug);
        $this->db->select($this->model . '_translation.*, ' . $this->model . '_cat_translation.' . $this->model . '_cat_id, ' . $this->model . '_cat_translation.description as category');
        $this->db->from($this->model . '_translation');
        $this->db->join($this->model, $this->model . '_translation.' . $this->model . '_id = ' . $this->model . '.id');
        $this->db->join(
            $this->model . '_cat_translation',
            $this->model . '_cat_translation.' . $this->model . '_cat_id = ' . $this->model . '.' . $this->model . '_cat_id and ' . $this->model . '_cat_translation.language_id =' . $lang_id,
            'left outer');
        $this->db->where('slug = ' . "'$slug'" . ' and ' . $this->model . '_translation.language_id = ' . $lang_id);
        $query = $this->db->get();
        if ($query)
            return $query->row_array();
    }

    function get_other_languages_for_row($model_id, $lang_id)
    {
        $this->db->select('language.code, language.description');
        $this->db->from($this->model . '_translation');
        $this->db->join('language', 'language.id = ' . $this->model . '_translation.language_id');
        $this->db->where(array($this->model . '_id' => $model_id, "language_id !=" => $lang_id));
        $query = $this->db->get();
        if ($query) {
            return $query->result_array();
        }
    }

    function get_rows_in_same_category($cat_id, $model_id, $lang_id ,$limit =null)
    {

        $this->db->select($this->model . '.*, ' . $this->model . '_translation.*');
        $this->db->from($this->model . '_translation');
        $this->db->join($this->model, $this->model . '.id = ' . $this->model . '_translation.' . $this->model . '_id');
        $this->db->where(array($this->model . '_cat_id' => $cat_id, $this->model . ".id !=" => $model_id, "language_id =" => $lang_id, "visible = " => 1));
        $this->db->order_by('RAND()');
        if($limit)$this->db->limit($limit);
        $query = $this->db->get();
        if ($query) {
            return $query->result_array();
        }
    }

    function get_category_data($cat_id, $lang_id)
    {
        $this->db->select('description, parent_id, cat_description');
        $this->db->from($this->model . '_cat_translation');
        $this->db->join($this->model . '_cat', $this->model . '_cat_translation.' . $this->model . '_cat_id = ' . $this->model . '_cat.id');
        $this->db->where(array($this->model . '_cat_id' => $cat_id, "language_id =" => $lang_id));
        $query = $this->db->get();
        if ($query) {
            return $query->row_array();
        }
    }

    function get_sub_categories($cat_id, $lang_id)
    {
        $this->db->select('description, ' . $this->model . '_cat.id');
        $this->db->from($this->model . '_cat_translation');
        $this->db->join($this->model . '_cat', $this->model . '_cat_translation.' . $this->model . '_cat_id = ' . $this->model . '_cat.id');
        $this->db->where(array('parent_id' => $cat_id, "language_id =" => $lang_id));
        $query = $this->db->get();
        if ($query) {
            return $query->result_array();
        }
    }

    function get_rows($cat_id, $lang_id, $limit=null, $from=null)
    {
        $this->db->select($this->model . '_translation.' . $this->model . '_id, ' . $this->model . '_translation.title, ' . $this->model . '_translation.slug, '  . $this->model . '_translation.hits, ' . $this->model . '_translation.author,'. $this->model .'_translation.creation_date, '. $this->model . '_translation.description');
        if($this->model == 'article') $this->db->select('article_translation.article_photo');
        if($this->model == 'video') $this->db->select('video_translation.youtube_code');
        $this->db->from($this->model . '_translation');
        $this->db->join($this->model, $this->model . '_translation.' . $this->model . '_id = ' . $this->model . '.id');
        $this->db->where(array($this->model . '_cat_id' => $cat_id, "language_id =" => $lang_id, "visible = " => 1));
        $this->db->order_by($this->model .'_translation.creation_date','desc');
        if($limit)
        {
            $this->db->select($this->model . '_translation.content');
            if($from) $this->db->limit($limit, $from);
            $this->db->limit($limit);
        }
        $query = $this->db->get();
        if ($query) {
            return $query->result_array();
        }
    }

    function get_most_viewed_rows($cat_id=null, $lang_id, $limit=null, $from=null)
    {
        $this->db->select($this->model . '_translation.' . $this->model . '_id, ' . $this->model . '_translation.title, ' . $this->model . '_translation.slug, ' . $this->model . '_translation.hits, ' . $this->model . '_translation.author, ' . $this->model . '_translation.creation_date, ' . $this->model . '_translation.description');
        $this->db->from($this->model . '_translation');
        $this->db->join($this->model, $this->model . '_translation.' . $this->model . '_id = ' . $this->model . '.id');
        $this->db->where(array("language_id =" => $lang_id, "visible = " => 1));
        if ($this->model == 'article' && $cat_id == null) {
            $this->db->select('article_translation.article_photo');
            $this->db->where('hits > ', 25000);
            $this->db->order_by($this->model .'_translation.hits','RANDOM');
        } else {
            $this->db->order_by($this->model .'_translation.hits','desc');
        }
        if($this->model == 'video') $this->db->select('video_translation.youtube_code');
        if($cat_id) $this->db->where($this->model . '_cat_id', $cat_id);
        if($limit)
        {
            $this->db->select($this->model . '_translation.content');
            if($from) $this->db->limit($limit, $from);
            $this->db->limit($limit);
        }
        $query = $this->db->get();
        if ($query) {
            return $query->result_array();
        }
    }

    function get_other_languages_for_category($cat_id, $lang_id)
    {
        $this->db->select('language.code, language.description');
        $this->db->from($this->model.'_cat_translation');
        $this->db->join('language', $this->model.'_cat_translation.language_id = language.id');
        $this->db->where(array($this->model.'_cat_id' => $cat_id, "language_id <>" => $lang_id));
        $query = $this->db->get();
        if ($query) {
            return $query->result_array();
        }

    }

    function get_random_rows($lang_id, $limit, $special='no')
    {
        $this->db->where('language_id', $lang_id);
        $this->db->where('visible', 1);
        if($this->model == 'article')
            $this->db->where('special', $special);
        $this->db->order_by('id', 'RANDOM');
        $this->db->limit($limit);
        $query=$this->db->get($this->model.'_translation');
        if ($query) {
            return $query->result_array();
        }
    }

    function get_lastest_rows($current_id = null, $lang_id, $limit)
    {
        $this->db->where('language_id', $lang_id);
        $this->db->where('visible', 1);
        if($current_id != '')
            $this->db->where($this->model.'_id != ', $current_id);
        $this->db->order_by($this->model.'_id', 'DESC');
        $this->db->limit($limit);
        $query=$this->db->get($this->model.'_translation');
        if ($query) {
            return $query->result_array();
        }
    }

    function get_all_rows($lang_id,$per_page,$from,$order=null) {
        $this->db->where('language_id', $lang_id);
        $this->db->where('visible', 1);
        if($order==null)$this->db->order_by('creation_date','desc');
        $this->db->limit($per_page, $from);
        $query=$this->db->get($this->model.'_translation');
        if ($query) {
            return $query->result_array();
        }
    }

    function get_all_rows_num($lang_id, $cat_id = null) {
        if($cat_id){
            $this->db->join($this->model, $this->model . '_translation.' . $this->model . '_id = ' . $this->model . '.id');
            $this->db->where(array($this->model . '_cat_id' => $cat_id));
        }
        $this->db->where('language_id', $lang_id);
        $this->db->where('visible', 1);
        $this->db->order_by($this->model.'_translation.creation_date','desc');
        $query=$this->db->get($this->model.'_translation');
        if ($query) {
            return $query->num_rows();
        }
    }

    function get_property($select, $model_id, $lang_id){
        $this->db->select($select);
        $this->db->from($this->model.'_translation');
        $this->db->where($this->model.'_id = ' .$model_id. ' and language_id = ' . $lang_id);
        $query=$this->db->get();
        if ($query) {
            return $query->row_array();
        }
    }

    function get_prev_model($model_id, $cat_id, $lang_id){
        $this->db->select($this->model.'.id, '.$this->model.'_translation.title');
        $this->db->from($this->model.'_translation');
        $this->db->join($this->model,$this->model.'.id = '.$this->model.'_translation.'.$this->model.'_id');
        $this->db->where(array($this->model.'_cat_id'=>$cat_id,$this->model.".id < " =>$model_id,"language_id =" =>$lang_id, "visible = " => 1));
        $this->db->order_by($this->model.'_id','desc');
        $this->db->limit(1);
        $query=$this->db->get();
        if($query){
            return $query->row_array();
        }
    }

    function get_next_model($model_id, $cat_id, $lang_id){
        $this->db->select($this->model.'.id, '.$this->model.'_translation.title');
        $this->db->from($this->model.'_translation');
        $this->db->join($this->model,$this->model.'.id = '.$this->model.'_translation.'.$this->model.'_id');
        $this->db->where(array($this->model.'_cat_id'=>$cat_id,$this->model.".id > " =>$model_id,"language_id =" =>$lang_id, "visible = " => 1));
        $this->db->order_by($this->model.'_id');
        $this->db->limit(1);
        $query=$this->db->get();
        if($query){
            return $query->row_array();
        }
    }
}