<?php
class my404error extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('articles_model');
    }

    public function index()
    {
        $data = $this->tools_model->get_page_config();
        $data['page_title'] = '404';
        $data['mostViewed'] = $this->articles_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
        $this->output->set_status_header('404');
        $data['main_content'] = 'errors/error_404';
        $this->load->view('layouts/default',$data);
    }
}