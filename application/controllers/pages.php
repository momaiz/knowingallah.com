<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Pages extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
        $this->load->model('articles_model');
        $this->load->model('videos_model');
        $this->load->model('audios_model');
        $this->load->model('books_model');
	}

    /**
     * Pages controller index calls the home method to show site homepage.
     */
    function index()
    {
        $this->home();
    }

    /**
     * Site homepage.
     */
	function home()
	{
        $this->load->helper('audio_helper');
        $data                        = $this->tools_model->get_page_config();
        $data['site_description']    = lang('meta_description'); //lang('homepage').' - '.lang('websiteName');
        $data['site_new']            = $this->tools_model->get_site_new($data['current_lang_id'], 4);
        $data['home_video']          = $this->tools_model->get_home_video($data['current_lang_id']);
        $data['special_articles']    = $this->articles_model->get_random_rows($data['current_lang_id'], 4, 'yes');
        $data['islamStory']          = $this->articles_model->get_rows(69, $data['current_lang_id'], 3);
        $data['books']               = $this->articles_model->get_rows(163, $data['current_lang_id'], 4);
        $data['questionsAboutGod']   = $this->articles_model->get_rows(198, $data['current_lang_id'], 4);
        $data['latest_videos']       = $this->videos_model->get_lastest_rows(null, $data['current_lang_id'], 4);
        $data['latestAudios']        = $this->audios_model->get_lastest_rows(null, $data['current_lang_id'], 6);
        //$data['most_viwed_articles'] = $this->articles_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
        $data['mostViewedItems']     = $this->tools_model->getMostViewedItems($data['current_lang_id'], 6);
        $data['books']               = $this->books_model->getLastestBooks($data['current_lang_id'], 4);
        $data['main_content']        = "pages/home";
        $this->load->view("layouts/default", $data);
	}

    function new_site()
    {
        $data                     = $this->tools_model->get_page_config();
        $data['page']             = 'site_new';
        $data['site_new']         = $this->tools_model->get_site_new($data['current_lang_id'], 30);
        $data['page_title']       = lang('site_new');
        $data['site_description'] = $data['page_title'].' - '.lang('websiteName');
        $data['main_content']     = "pages/page";
        $data['mostViewedItems']  = $this->tools_model->getMostViewedItems($data['current_lang_id'], 6);
        $this->load->view("layouts/default", $data);
    }

    function contact_us()
    {
        $this->lang->load('contact');
        $data = $this->tools_model->get_page_config();
        $data['page_title']       = lang('contactus');
        $data['site_description'] = $data['page_title'].' - '.lang('websiteName');
        $data['main_content']     = "pages/contact_us";
        $data['mostViewed'] = $this->articles_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
        $this->load->view("layouts/default", $data);
    }

    function sendContactUs()
    {
        $config = Array(
            'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'midade.contact@gmail.com',
            'smtp_pass' => '!f0r!C0nt@ct!U$',
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'wordwrap'  => TRUE
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $message = '<b>Username:</b> '.$this->input->post('name')."\r\n";
        $message .= '<b>E-mail:</b> '.$this->input->post('email')."\r\n";
        $message .= '<b>Message:</b>'."\r\n";
        $message .= $this->input->post('message');
        $message = str_replace ("\r\n", "<br>", $message);
        $this->email->from('midade.contact@gmail.com', 'Knowing Allah site');
        $this->email->to('contact@midade.com');
        $this->email->subject('Knowing Allah site new contact message');
        $this->email->message($message);
        if($this->email->send()){
            $success     = lang('success');
            $redirectURL = site_url('contact');
            echo "<script>
                    alert(\"$success\");
                    window.location.href=\"$redirectURL\";
                </script>";
        }else{
            show_error($this->email->print_debugger());
        }
    }

    function search()
    {
        $data                     = $this->tools_model->get_page_config();
        $data['page']             = 'search';
        $data['page_title']       = lang('search');
        $data['site_description'] = $data['page_title'].' - '.lang('websiteName');
        $data['main_content']     = "pages/page";
        $this->load->view("layouts/default", $data);
    }

    function page()
    {
        $page_id = (int)$this->uri->segment(2);
        $data    = $this->tools_model->get_page_config();
        if($page_id == '')
        {
            if($data['current_lang_abbr'] == 'ar')
                $page_id = 5;
            else
                $page_id = 2;
        }
        $data                     = $this->tools_model->get_page_config();
        $data['page']             = $this->tools_model->get_page_detail($page_id);
        $data['page_title']       = $data['page']['title'];
        $data['site_description'] = $data['page_title'].' - '.lang('websiteName');
        $data['mostViewed'] = $this->articles_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
        $data['main_content']     = "pages/page";
        $this->load->view("layouts/default", $data);
    }

    function photo_gallery(){
        $data = $this->tools_model->get_page_config();
        $data['gallery_photos'] = $this->tools_model->get_photos($data['current_lang_id']);
        $data['page']           = 'gallery';
        $data['page_title']     = lang('photoalbum');
        $data['site_description'] = $data['page_title'].' - '.lang('websiteName');
        $data['main_content']   = 'pages/page';
        $this->load->view('layouts/default', $data);
    }

    /**
     * Gets all articles with default photo.
     * By using this link
     * http://domain/[ar] or any language code/articlesWithDefaultPhoto
     */
    /*public function getAllArticlesWithDefaultPhoto() {
        $articles = $this->tools_model->getAllArticlesWithDefaultPhoto();
        if ($articles) {
            $this->load->library("phpexcel");
            date_default_timezone_set("GMT");
            $filename = "articles-with-default-photo-".date("Y-m-d");

            $this->phpexcel->getProperties()->setCreator("Mohammed Zayan")
                ->setLastModifiedBy("Mohammed Zayan")
                ->setTitle("Site Articles With Default Photo");

            $this->phpexcel->setActiveSheetIndex(0);

            $this->phpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
            $this->phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
            $this->phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);

            $this->phpexcel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->phpexcel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('005599');
            $this->phpexcel->getActiveSheet()->setCellValue('A1', "عنوان المقال");
            $this->phpexcel->getActiveSheet()->setCellValue('B1', "رابط المقالة");
            $this->phpexcel->getActiveSheet()->setCellValue('C1', "كود اللغة");

            $therow = 1;

            foreach ($articles as $article) {
                ++$therow ;
                $this->phpexcel->getActiveSheet()->setCellValue('A'.$therow, $article['articleTitle']);
                $this->phpexcel->getActiveSheet()->setCellValue('B'.$therow, $article['articleLink']);
                $this->phpexcel->getActiveSheet()->setCellValue('C'.$therow, $article['laguageCode']);
            }

            $ty = "Excel2007";
            $fname = $filename.".xlsx";
            $headertype = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            header('Content-Type: '.$headertype);
            header('Content-Disposition: attachment;filename="'.$fname.'"');
            header('Cache-Control: max-age=0');

            $objWriter = PHPExcel_IOFactory::createWriter($this->phpexcel, $ty);
            ob_clean();

            $objWriter->save('php://output');
            exit;
        } else {
            echo "No data ". anchor(base_url(),"go back");
        }
    }*/

}