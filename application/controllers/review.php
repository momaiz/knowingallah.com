<?php
class Review extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('articles_model');
    }

    function index(){
        $this->all();
    }

    function all(){
        $data = $this->tools_model->get_page_config();
        $this->load->library('pagination');
        $config['base_url']       = site_url('review/all');
        $config['total_rows']     = $this->articles_model->get_all_rows_num($data['current_lang_id']);
        $config['per_page']       = 15;
        $config['num_links']      = 4;
        $this->pagination->initialize($config);
        $uri_segment3 = (int)$this->uri->segment(3);
        if(isset($uri_segment3) && $uri_segment3 != ''){
            $from = $uri_segment3;
        }else{
            $from = 0;
        }
        $data['articles']= $this->articles_model->get_all_rows($data['current_lang_id'],$config['per_page'],$from,'asc');
        if(count($data['articles'])) {
            $this->load->view("pages/review/all", $data);
        }else{
            echo 'خطأ في الرابط.';
        }

    }

    function article($id=null){
        $data = $this->tools_model->get_page_config();
        $data['articleId'] = $id;
        if($id){
            $data['article'] =  $this->articles_model->get_row_detail($id, $data['current_lang_id']);
            if($data['article']){
                $this->load->view("pages/review/article", $data);
            }else{
                echo 'خطأ في الرابط.';
            }
        }else{
            echo 'خطأ في الرابط.';
        }

    }


}