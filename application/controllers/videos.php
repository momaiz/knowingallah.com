<?php
ob_start();
class Videos extends MY_Controller{

	function __construct(){
            parent::__construct();
            $this->load->model('videos_model');

	}

	function index()
    {
        $data                          = $this->tools_model->get_page_config();
        $data['cat_description']       = $this->lang->line('videos');
        // Get sub-categories
        $data['query_sub_cats']        = $this->videos_model->get_sub_categories(0, $data['current_lang_id']);
        //Others Languages
        $data['query_other_languages'] = $this->videos_model->get_other_languages_for_category(0, $data['current_lang_id']);
        $data['mostViewed']            = $this->videos_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
        $data['main_content']          = "pages/categories";
        $data['model']                 = 'videos';
        $data['page_title']            = lang('videos');
        $data['site_description']      = lang('videos').' - '.lang('websiteName');
        $this->load->view("layouts/default", $data);
	}

	function category($id=null){
            $data = $this->tools_model->get_page_config();
            $data['categoryId'] = $id;
            if($id)
            {
                // Get Category description
                $row = $this->videos_model->get_category_data($id, $data['current_lang_id']);
                if($row)
                {
                    $data['cat_name'] = $row['description'];
                    $data['site_description'] = $row['description'];
                    $data['cat_description'] = $row['description'];
                    $data['parent_id'] = $row['parent_id'];
                    $data['page_title'] = $data['cat_description'];
                    $data['parent_cat_description']=$this->videos_model->get_category_data($data['parent_id'],$data['current_lang_id']);
                    // Get sub-categories
                    $data['query_sub_cats'] = $this->videos_model->get_sub_categories($id, $data['current_lang_id']);
                    // Get videos
                    $this->load->library('pagination');
                    $config['base_url']       = site_url('videos/category').'/'.$id;
                    $config['suffix']         = '/';
                    $config['total_rows']     = $this->videos_model->get_all_rows_num($data['current_lang_id'], $id);
                    $config['per_page']       = 12;
                    $config['num_links']      = 3;
                    $config['uri_segment']    = 4;
                    $config['num_tag_open']   = '<li>';
                    $config['num_tag_close']  = '</li>';
                    $config['cur_tag_open']   = "<li class='active'><a href='#'>";
                    $config['cur_tag_close']  = '</a></li>';
                    $config['first_link']     = false;
                    $config['last_link']      = false;
                    $config['next_tag_open']  = "<li>";
                    $config['next_link']      = "<span aria-hidden='true'><i class='fa fa-angle-double-right'></i></span>";
                    $config['next_tag_close'] = '</li>';
                    $config['prev_tag_open']  = '<li>';
                    $config['prev_link']      = "<span aria-hidden='true'><i class='fa fa-angle-double-left'></i></span>";
                    $config['prev_tag_close'] = '</li>';
                    $config['full_tag_open']  = "<ul class='pagination'>";
                    $config['full_tag_close'] = '</ul>';
                    $this->pagination->initialize($config);
                    $uri_segment4 = (int)$this->uri->segment(4);
                    if(isset($uri_segment4) && $uri_segment4 != ''){
                        $from = $uri_segment4;
                    }else{
                        $from = 0;
                    }
                $data['query_videos'] =  $this->videos_model->get_rows($id, $data['current_lang_id'], $config['per_page'], $from);
                //Others Languages
                $data['query_other_languages'] = $this->videos_model->get_other_languages_for_category($id, $data['current_lang_id']);
                $data['mostViewed'] = $this->videos_model->get_most_viewed_rows($id, $data['current_lang_id'], 4);
                $data['randVideos'] = $this->videos_model->get_random_rows($data['current_lang_id'], 6);
                $data['randVideo'] = $this->tools_model->get_home_video($data['current_lang_id'], 'yes');
                $data['main_content'] = "pages/videos/category";
            }else{
                $data['page_title'] = '404';
                $this->output->set_status_header('404');
                $data['mostViewed'] = $this->videos_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
                $data['main_content'] = "errors/error_404";
            }
            $this->load->view("layouts/default", $data);
        }else{
            $this->index();
        }
    }

    function all_videos(){
        $data = $this->tools_model->get_page_config();
        $this->load->library('pagination');
        $config['base_url']       = site_url('videos/all_videos');
        $config['total_rows']     = $this->videos_model->get_all_rows_num($data['current_lang_id']);
        $config['per_page']       = 8;
        $config['num_links']      = 3;
        $config['suffix']         = '/';
        $config['num_tag_open']   = '<li>';
        $config['num_tag_close']  = '</li>';
        $config['cur_tag_open']   = "<li class='active'><a href='#'>";
        $config['cur_tag_close']  = '</a></li>';
        $config['first_link']     = false;
        $config['last_link']      = false;
        $config['next_tag_open']  = "<li>";
        $config['next_link']      = "<span aria-hidden='true'>&raquo;</span><span class='sr-only'>Next</span>";
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open']  = '<li>';
        $config['prev_link']      = "<span aria-hidden='true'>&laquo;</span><span class='sr-only'>Previous</span>";
        $config['prev_tag_close'] = '</li>';
        $config['full_tag_open']  = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $this->pagination->initialize($config);
        $uri_segment3 = (int)$this->uri->segment(3);
        if(isset($uri_segment3) && $uri_segment3 != ''){
            $from = $uri_segment3;
        }else{
            $from = 0;
        }
        $data['videos']     = $this->videos_model->get_all_rows($data['current_lang_id'],$config['per_page'],$from);
        $data['page_title'] = lang('videos');
        $data['site_description'] = lang('videos').' - '.lang('websiteName');
        if(count($data['videos'])) {
            $data['main_content'] = "pages/videos/videos";
        }else{
            $data['page_title'] = '404';
            $this->output->set_status_header('404');
            $data['mostViewed'] = $this->videos_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
            $data['main_content'] = "errors/error_404";
        }
        $this->load->view("layouts/default", $data);
    }

    function video($id=null){
        $data = $this->tools_model->get_page_config();
        $data['videoId'] = $id;
        if($id)
        {
            // Get Video Detail
            $data['video_row']  =  $this->videos_model->get_row_detail($id, $data['current_lang_id']);
            if($data['video_row'])
            {
                redirect('videos/'.$data['video_row']['slug']);
            }else{
                $data['page_title'] = '404';
                $this->output->set_status_header('404');
                $data['mostViewed'] = $this->videos_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
                $data['main_content'] = "errors/error_404";
            }
        }else{
            $data['page_title'] = '404';
            $this->output->set_status_header('404');
            $data['mostViewed'] = $this->videos_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
            $data['main_content'] = "errors/error_404";
        }
        $this->load->view("layouts/default", $data);
    }

    function getVideoBySlug($slug=null)
    {
        $data = $this->tools_model->get_page_config();
        $data['videoSlug'] = $slug;
        if ($slug) {
            // Get Video Detail
            $data['video_row']  =  $this->videos_model->get_row_detail_by_slug($slug, $data['current_lang_id']);
            if ($data['video_row']) {
                $data['page_title'] = $data['video_row']['title'];
                $id = $data['video_row']['video_id'];
                $data['videoId'] = $id;
                if($data['video_row']['description'] == '')
                    $data['site_description'] = $data['video_row']['title'].' - '.$data['video_row']['author'];
                else
                    $data['site_description'] = $data['video_row']['description'];
                $data['file_ext'] = $this->tools_model->get_file_extension_from_url($data['video_row']['link']);
                // Increment video hits
                $this->videos_model->increment_hits($id, $data['current_lang_id']);
                // Get videos in Same Category
                //$data['query_videos'] = $this->videos_model->get_rows_in_same_category($data['video_row']['video_cat_id'], $id, $data['current_lang_id'], 3);
                $data['latest_videos']    = $this->videos_model->get_lastest_rows($data['videoId'], $data['current_lang_id'],4);
                //$data['related_articles'] = $this->tools_model->get_related_articles($data['video_row']['title'], $data['current_lang_id'], 3);
                //$data['relatedVideos'] = $this->tools_model->getRelatedVideos($data['video_row']['title'], $data['current_lang_id'], 3);
                $data['relatedVideos'] = $this->videos_model->get_rows_in_same_category($data['video_row']['video_cat_id'], $id, $data['current_lang_id'], 3);
                $data['mostViewed'] = $this->videos_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
                $data['query_other_languages'] = $this->videos_model->get_other_languages_for_row($id, $data['current_lang_id']);
                $data['main_content'] = "pages/videos/video";
            }else{
                $data['page_title'] = '404';
                $this->output->set_status_header('404');
                $data['mostViewed'] = $this->videos_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
                $data['main_content'] = "errors/error_404";
            }
        }else{
            $data['page_title'] = '404';
            $this->output->set_status_header('404');
            $data['mostViewed'] = $this->videos_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
            $data['main_content'] = "errors/error_404";
        }
        $this->load->view("layouts/default", $data);
    }

    function latest_videos(){
        $data                 = $this->tools_model->get_page_config();
        $data['videos']       = $this->videos_model->get_lastest_rows($data['current_lang_id'],18);
        $data['main_content'] = "pages/videos/videos";
        $data['page_title']   = lang('last_videos');
        $data['site_description'] = lang('last_videos').' - '.lang('websiteName');
        $data['latest_videos'] = 'latest_videos';
        $this->load->view("layouts/default", $data);
    }
}