<?php

/**
 * Copyright (c) 2016 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author   Mohammed Zayan <zayanit@gmail.com>
 * @link     http://www.zayanit.com
 * @version  1.0
 * Created by PhpStorm
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Books extends MY_Controller
{
    /**
     * Books constructor.
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('books_model');
    }

    function index()
    {
        $data                        = $this->tools_model->get_page_config();
        $data['booksMoreDownloaded'] = $this->books_model->booksMoreDownloaded($data['current_lang_id']);
        $this->load->library('pagination');
        $config['base_url']       = site_url('books').'/';
        $config['suffix']         = '/';
        $config['total_rows']     = $this->books_model->getAllRowsNum($data['current_lang_id']);
        $config['per_page']       = 12;
        $config['num_links']      = 3;
        $config['uri_segment']    = 2;
        $config['num_tag_open']   = '<li>';
        $config['num_tag_close']  = '</li>';
        $config['cur_tag_open']   = "<li class='active'><a href='#'>";
        $config['cur_tag_close']  = '</a></li>';
        $config['first_link']     = false;
        $config['last_link']      = false;
        $config['next_tag_open']  = "<li>";
        $config['next_link']      = "<span aria-hidden='true'><i class='fa fa-angle-double-right'></i></span>";
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open']  = '<li>';
        $config['prev_link']      = "<span aria-hidden='true'><i class='fa fa-angle-double-left'></i></span>";
        $config['prev_tag_close'] = '</li>';
        $config['full_tag_open']  = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $this->pagination->initialize($config);
        $uri_segment2 = (int)$this->uri->segment(2);
        if (isset($uri_segment2) && $uri_segment2 != '')
            $from = $uri_segment2;
        else
            $from = 0;
        $data['allBooks']            = $this->books_model->getBooks($data['current_lang_id'], $config['per_page'], $from);
        $data['cat_description']     = $this->lang->line('books');
        $data['page_title']          = lang('books');
        $data['site_description']    = lang('books').' - '.lang('websiteName');
        $data['main_content']        = "/pages/books/all_books";
        $this->load->view("layouts/default", $data);
    }

    /**
     * Get book data by it's slug.
     * @param $slug
     */
    function getBookBySlug($slug)
    {
        $data                        = $this->tools_model->get_page_config();
        $data['booksMoreDownloaded'] = $this->books_model->booksMoreDownloaded($data['current_lang_id']);
        $data['book']                = $this->books_model->getBookBySlug($slug, $data['current_lang_id']);
        // Increment book hits
        $this->books_model->increment_hits($data['book']['id'], $data['current_lang_id']);
        $data['relatedBooks']        = $this->books_model->getRelatedBooks($slug, $data['current_lang_id'], 3);
        $data['cat_description']     = $this->lang->line('books');
        $data['page_title']          = lang('books');
        if( isset($data['book']['name']{3}) ){
           $data['page_title'] =  $data['book']['name'] ;
        }
        $data['site_description']    = lang('books').' - '.lang('websiteName');
        $data['main_content']        = "/pages/books/book";
        $this->load->view("layouts/default", $data);
    }

    /**
     * Increment book downloads number
     */
    function incrementDownloads()
    {
        $bookID = (int)$this->input->post('bookID');
        $this->books_model->incrementDownloads($bookID);
    }
}
