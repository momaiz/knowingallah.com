<?php
ob_start();
class Audios extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('audios_model');
        $this->load->helper('audio_helper');
    }

    function index(){
        $data                          = $this->tools_model->get_page_config();
        $data['cat_description']       = $this->lang->line('audios');
        // Get sub-categories
        $data['query_sub_cats']        = $this->audios_model->get_sub_categories(0, $data['current_lang_id']);
        //Others Languages
        $data['query_other_languages'] = $this->audios_model->get_other_languages_for_category(0, $data['current_lang_id']);
        $data['mostViewed']            = $this->audios_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
        $data['model']='audios';
        $data['main_content']          = "pages/categories";
        $data['page_title']            = lang('audios');
        $data['site_description']      = lang('audios').' - '.lang('websiteName');
        $this->load->view("layouts/default", $data);

    }

    function category($id = null) {
        $data = $this->tools_model->get_page_config();
        $data['categoryId'] = $id;
        if ($id) {
            // Get Category description
            $row = $this->audios_model->get_category_data($id, $data['current_lang_id']);
            if ($row) {
                $data['cat_name']               = $row['description'];
                $data['site_description']       = $row['cat_description'];
                $data['cat_description']        = $row['cat_description'];
                $data['parent_id']              = $row['parent_id'];
                $data['page_title']             = $data['cat_name'];
                $data['parent_cat_description'] = $this->audios_model->get_category_data($data['parent_id'],$data['current_lang_id']);
                // Get sub-categories
                $data['query_sub_cats'] = $this->audios_model->get_sub_categories($id, $data['current_lang_id']);
                // Get audios
                $this->load->library('pagination');
                $config['base_url']       = site_url('audios/category').'/'.$id;
                $config['suffix']         = '/';
                $config['total_rows']     = $this->audios_model->get_all_rows_num($data['current_lang_id'], $id);
                $config['per_page']       = 22;
                $config['num_links']      = 3;
                $config['uri_segment']    = 4;
                $config['num_tag_open']   = '<li>';
                $config['num_tag_close']  = '</li>';
                $config['cur_tag_open']   = "<li class='active'><a href='#'>";
                $config['cur_tag_close']  = '</a></li>';
                $config['first_link']     = false;
                $config['last_link']      = false;
                $config['next_tag_open']  = "<li>";
                $config['next_link']      = "<span aria-hidden='true'><i class='fa fa-angle-double-right'></i></span>";
                $config['next_tag_close'] = '</li>';
                $config['prev_tag_open']  = '<li>';
                $config['prev_link']      = "<span aria-hidden='true'><i class='fa fa-angle-double-left'></i></span>";
                $config['prev_tag_close'] = '</li>';
                $config['full_tag_open']  = "<ul class='pagination'>";
                $config['full_tag_close'] = '</ul>';
                $this->pagination->initialize($config);
                $uri_segment4 = (int)$this->uri->segment(4);
                if(isset($uri_segment4) && $uri_segment4 != ''){
                    $from = $uri_segment4;
                }else{
                    $from = 0;
                }
                $data['query_audios'] =  $this->audios_model->get_rows($id, $data['current_lang_id'], $config['per_page'], $from);
                //Others Languages
                $data['query_other_languages'] = $this->audios_model->get_other_languages_for_category($id, $data['current_lang_id']);
                $data['mostViewed'] = $this->audios_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
                //$data['randAudios'] = $this->audios_model->get_random_rows($data['current_lang_id'], 6);
                //$data['randVideo'] = $this->tools_model->get_home_video($data['current_lang_id'], 'yes');
                $data['main_content'] = "pages/audios/category";
            }else{
                $data['page_title'] = '404';
                $this->output->set_status_header('404');
                $data['mostViewed'] = $this->audios_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
                $data['main_content'] = "errors/error_404";
            }

            $this->load->view("layouts/default", $data);
        }else{
            $this->index();
        }
    }

    function all_audios(){
        $data = $this->tools_model->get_page_config();
        $this->load->library('pagination');
        $config['base_url']       = site_url('audios/all_audios');
        $config['suffix']         = '/';
        $config['total_rows']     = $this->audios_model->get_all_rows_num($data['current_lang_id']);
        $config['per_page']       = 22;
        $config['num_links']      = 3;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='active'><a href='#'>";
        $config['cur_tag_close'] = '</a></li>';
        $config['first_link']     = false;
        $config['last_link']      = false;
        $config['next_tag_open'] = "<li>";
        $config['next_link']      = "<span aria-hidden='true'>&raquo;</span><span class='sr-only'>Next</span>";
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_link']      = "<span aria-hidden='true'>&laquo;</span><span class='sr-only'>Previous</span>";
        $config['prev_tag_close'] = '</li>';
        $config['full_tag_open']  = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $this->pagination->initialize($config);
        $uri_segment3 = (int)$this->uri->segment(3);
        if(isset($uri_segment3) && $uri_segment3 != ''){
            $from = $uri_segment3;
        }else{
            $from = 0;
        }
        $data['audios']     = $this->audios_model->get_all_rows($data['current_lang_id'],$config['per_page'],$from);
        $data['page_title'] = lang('audios');
        $data['site_description'] = lang('audios').' - '.lang('websiteName');
        if(count($data['audios'])) {
            $data['main_content'] = "pages/audios/audios";
        }else{
            $data['page_title'] = '404';
            $this->output->set_status_header('404');
            $data['mostViewed'] = $this->audios_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
            $data['main_content'] = "errors/error_404";
        }
        $this->load->view("layouts/default", $data);
    }

    function audio($id=null){
        $data = $this->tools_model->get_page_config();
        $data['audioId'] = $id;
        if($id)
        {
            // Get audio Detail
            $data['audio_row']  =  $this->audios_model->get_row_detail($id, $data['current_lang_id']);
            if($data['audio_row'])
            {
                redirect('audios/'.$data['audio_row']['slug']);
            }else{
                $data['page_title'] = '404';
                $this->output->set_status_header('404');
                $data['mostViewed'] = $this->audios_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
                $data['main_content'] = "errors/error_404";
            }
        }else{
            $data['page_title'] = '404';
            $this->output->set_status_header('404');
            $data['mostViewed'] = $this->audios_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
            $data['main_content'] = "errors/error_404";
        }
        $this->load->view("layouts/default", $data);
    }

    function getAudioBySlug($slug = null)
    {
        $data = $this->tools_model->get_page_config();
        $data['audioSlug'] = $slug;
        if ($slug) {
            // Get audio Detail
            $data['audio_row']  =  $this->audios_model->get_row_detail_by_slug($slug, $data['current_lang_id']);
            if ($data['audio_row']) {
                $data['page_title'] = $data['audio_row']['title'];
                $id = $data['audio_row']['audio_id'];
                $data['audioId'] = $id;
                if ($data['audio_row']['description'] == '')
                    $data['site_description'] = $data['audio_row']['title'].' - '.$data['audio_row']['author'];
                else
                    $data['site_description'] = $data['audio_row']['description'];
                $data['file_ext'] = $this->tools_model->get_file_extension_from_url($data['audio_row']['link']);
                // Increment audio hits
                $this->audios_model->increment_hits($id, $data['current_lang_id']);
                // Get audios in Same Category
                $data['query_audios'] = $this->audios_model->get_rows_in_same_category($data['audio_row']['audio_cat_id'], $id, $data['current_lang_id'], 3);
                //Others Languages
                $data['query_other_languages'] = $this->audios_model->get_other_languages_for_row($id, $data['current_lang_id']);
                //$data['related_articles'] = $this->tools_model->get_related_articles($data['audio_row']['title'], $data['current_lang_id'], 3);
                $data['mostViewed'] = $this->audios_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
                $data['relatedAudios'] = $this->audios_model->get_rows_in_same_category($data['audio_row']['audio_cat_id'], $id, $data['current_lang_id'], 3);
                $data['main_content'] = "pages/audios/audio";
            } else {
                $data['page_title'] = '404';
                $this->output->set_status_header('404');
                $data['mostViewed'] = $this->audios_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
                $data['main_content'] = "errors/error_404";
            }
        } else {
            $data['page_title'] = '404';
            $this->output->set_status_header('404');
            $data['mostViewed'] = $this->audios_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
            $data['main_content'] = "errors/error_404";
        }
        $this->load->view("layouts/default", $data);
    }

    function latest_audios(){
        $data                 = $this->tools_model->get_page_config();
        $data['audios']       = $this->audios_model->get_lastest_rows($data['current_lang_id'],10000000);
        $data['main_content'] = "pages/audios/audios";
        $data['page_title']   = lang('last_audios');
        $data['site_description'] = lang('last_audios').' - '.lang('websiteName');
        $this->load->view("layouts/default", $data);
    }
}