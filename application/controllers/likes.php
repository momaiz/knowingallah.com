<?php

/**
 * Copyright (c) 2015 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author        Mohammed Zayan <admin@zayanit.com>
 * @link          http://www.zayanit.com
 * @version       1.0
 * Created by Sublime Text 2
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Likes extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('likes_model');
	}

	function index(){}

	function add_like()
	{
		$item_id = (int)$this->input->post('item_id');
		$module  = $this->input->post('module');
		$lang    = $this->input->post('lang');
		$result  = $this->likes_model->checkItem($item_id, $module, $lang);
		switch ($result) {
			case 'liked':
				$data =  lang('liked_before');
				break;
			case 'fail':
				$data =  lang('not_found');
				break;
			case 'success':
				$data = $result;
				break;
		}
		echo $data;
	}

	function likes_get()
	{
		$item_id = (int)$this->input->post('item_id');
		$module  = $this->input->post('module');
		$lang    = $this->input->post('lang');
		$likes   = $this->likes_model->increse_likes($item_id, $module, $lang);
		echo $likes;
	}

	function add_dislike()
	{
		$item_id = (int)$this->input->post('item_id');
		$module  = $this->input->post('module');
		$lang    = $this->input->post('lang');
		$result  = $this->likes_model->checkElement($item_id, $module, $lang);
		switch ($result) {
			case 'disliked':
				echo lang('disliked_before');
				break;
			case 'fail':
				echo lang('not_found');
				break;
			case 'success':
				echo $result;		
				break;
		}
	}

	function dislikes_get()
	{
		$item_id  = (int)$this->input->post('item_id');
		$module   = $this->input->post('module');
		$lang     = $this->input->post('lang');
		$dislikes = $this->likes_model->increse_dislikes($item_id, $module, $lang);
		echo $dislikes;
	}

}