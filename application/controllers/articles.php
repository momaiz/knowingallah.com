<?php
ob_start();
class Articles extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('articles_model');
    }

    function index(){
        $data                          = $this->tools_model->get_page_config();
        $data['cat_description']       = $this->lang->line('articles');
        $data['query_sub_cats']        = $this->articles_model->get_sub_categories(0, $data['current_lang_id']);
        $data['query_other_languages'] = $this->articles_model->get_other_languages_for_category(0, $data['current_lang_id']);
        $data['mostViewed']            = $this->articles_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
        $data['model']                 = 'articles';
        $data['main_content']          = "/pages/categories";
        $data['page_title']            = lang('articles');
        $data['site_description']      = lang('articles').' - '.lang('websiteName');
        $this->load->view("layouts/default", $data);
    }

    function category($id=null) {
        $data = $this->tools_model->get_page_config();
        $data['categoryId'] = $id;
        if ($id) {
            $row = $this->articles_model->get_category_data($id, $data['current_lang_id']);
            if ($row) {
                $data['cat_name']               = $row['description'];
                $data['cat_description']        = $row['description'];
                $data['site_description']       = $row['description'];
                $data['parent_id']              = $row['parent_id'];
                $data['parent_cat_description'] = $this->articles_model->get_category_data($data['parent_id'],$data['current_lang_id']);
                $data['page_title']             = $data['cat_description'];
                $data['query_sub_cats']         = $this->articles_model->get_sub_categories($id, $data['current_lang_id']);
                //get article in pagination
                $this->load->library('pagination');
                $config['base_url']       = site_url('articles/category').'/'.$id;
                $config['suffix']         = '/';
                $config['total_rows']     = $this->articles_model->get_all_rows_num($data['current_lang_id'], $id);
                $config['per_page']       = 9;
                $config['num_links']      = 3;
                $config['uri_segment']    = 4;
                $config['num_tag_open']   = '<li>';
                $config['num_tag_close']  = '</li>';
                $config['cur_tag_open']   = "<li class='active'><a href='#'>";
                $config['cur_tag_close']  = '</a></li>';
                $config['first_link']     = false;
                $config['last_link']      = false;
                $config['next_tag_open']  = "<li>";
                $config['next_link']      = "<span aria-hidden='true'><i class='fa fa-angle-double-right'></i></span>";
                $config['next_tag_close'] = '</li>';
                $config['prev_tag_open']  = '<li>';
                $config['prev_link']      = "<span aria-hidden='true'><i class='fa fa-angle-double-left'></i></span>";
                $config['prev_tag_close'] = '</li>';
                $config['full_tag_open']  = "<ul class='pagination'>";
                $config['full_tag_close'] = '</ul>';
                $this->pagination->initialize($config);
                $uri_segment4 = (int)$this->uri->segment(4);
                if (isset($uri_segment4) && $uri_segment4 != '')
                    $from = $uri_segment4;
                else
                    $from = 0;
                $data['query_articles'] =  $this->articles_model->get_rows($id, $data['current_lang_id'], $config['per_page'], $from);
                //Other languages
                $data['query_other_languages'] = $this->articles_model->get_other_languages_for_category($id, $data['current_lang_id']);
                $data['mostViewed'] = $this->articles_model->get_most_viewed_rows($id, $data['current_lang_id'], 4);
                //$data['randArticles'] = $this->articles_model->get_random_rows($data['current_lang_id'], 6);
                //$data['randVideo'] = $this->tools_model->get_home_video($data['current_lang_id'], 'yes');
                $data['main_content']          = "pages/articles/category";
            } else {
                $data['page_title']            = '404';
                $this->output->set_status_header('404');
                $data['mostViewed'] = $this->articles_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
                $data['main_content']          = "errors/error_404";
            }
            $this->load->view("layouts/default", $data);
        } else {
            $this->index();
        }
    }

    function all_articles(){
        $data = $this->tools_model->get_page_config();
        $this->load->library('pagination');
        $config['base_url']       = site_url('articles/all_articles');
        $config['suffix']         = '/';
        $config['total_rows']     = $this->articles_model->get_all_rows_num($data['current_lang_id']);
        $config['per_page']       = 15;
        $config['num_links']      = 3;
        $config['num_tag_open']   = '<li>';
        $config['num_tag_close']  = '</li>';
        $config['cur_tag_open']   = "<li class='active'><a href='#'>";
        $config['cur_tag_close']  = '</a></li>';
        $config['first_link']     = false;
        $config['last_link']      = false;
        $config['next_tag_open']  = "<li>";
        $config['next_link']      = "<span aria-hidden='true'>&raquo;</span><span class='sr-only'>Next</span>";
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open']  = '<li>';
        $config['prev_link']      = "<span aria-hidden='true'>&laquo;</span><span class='sr-only'>Previous</span>";
        $config['prev_tag_close'] = '</li>';
        $config['full_tag_open']  = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $this->pagination->initialize($config);
        $uri_segment3 = (int)$this->uri->segment(3);
        if(isset($uri_segment3) && $uri_segment3 != ''){
            $from = $uri_segment3;
        }else{
            $from = 0;
        }
        $data['articles']   = $this->articles_model->get_all_rows($data['current_lang_id'],$config['per_page'],$from);
        $data['page_title'] = lang('articles');
        $data['site_description'] = lang('articles').' - '.lang('websiteName');
        if(count($data['articles'])) {
            $data['main_content'] = "pages/articles/articles";
        }else{
            $data['page_title'] = '404';
            $this->output->set_status_header('404');
            $data['mostViewed'] = $this->articles_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
            $data['main_content'] = "errors/error_404";
        }
        $this->load->view("layouts/default", $data);
    }

    function article($id=null)
    {
        $data = $this->tools_model->get_page_config();
        $data['articleId'] = $id;
        if($id){
            $data['article_row'] =  $this->articles_model->get_row_detail($id, $data['current_lang_id']);
            if($data['article_row']){
                redirect('articles/'.$data['article_row']['slug']);
            }else{
                $data['page_title'] = '404';
                $this->output->set_status_header('404');
                $data['mostViewed'] = $this->articles_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
                $data['main_content'] = "errors/error_404";
            }
        }else{
            $data['page_title'] = '404';
            $this->output->set_status_header('404');
            $data['mostViewed'] = $this->articles_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
            $data['main_content'] = "errors/error_404";
        }
        $this->load->view("layouts/default", $data);
    }

    /**
     * Get article data by it's slug.
     * @param null $slug
     */
    function getArticleBySlug($slug = null) {
        $data                = $this->tools_model->get_page_config();
        $data['articleSlug'] = $slug;
        if ($slug) {
            $data['article_row'] =  $this->articles_model->get_row_detail_by_slug($slug, $data['current_lang_id']);
            if ($data['article_row']) {
                $data['page_title']  = $data['article_row']['title'];
                $id                  = $data['article_row']['article_id'];
                $data['articleId']   = $id;
                if ($data['article_row']['description'] == '')
                    $data['site_description'] = $data['article_row']['title'].' '.substr_word_end($data['article_row']['content'], 0, 105, 'UTF-8');
                else
                    $data['site_description'] = $data['article_row']['description'];
                $this->articles_model->increment_hits($id, $data['current_lang_id']);
                //$data['query_articles']  = $this->articles_model->get_rows_in_same_category($data['article_row']['article_cat_id'], $id, $data['current_lang_id'], 9);
                $data['mostViewed'] = $this->articles_model->get_most_viewed_rows($data['article_row']['article_cat_id'], $data['current_lang_id'], 4);
                $data['latest_articles']    = $this->articles_model->get_lastest_rows($id, $data['current_lang_id'],4);
                $data['related_articles'] = $this->tools_model->get_related_articles($data['article_row']['title'], $data['current_lang_id'], 3);
                $data['query_other_languages'] = $this->articles_model->get_other_languages_for_row($id, $data['current_lang_id']);
                $data['main_content']          = "pages/articles/article";
            } else {
                $data['page_title'] = '404';
                $this->output->set_status_header('404');
                $data['mostViewed'] = $this->articles_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
                $data['main_content'] = "errors/error_404";
            }
        } else {
            $data['page_title'] = '404';
            $this->output->set_status_header('404');
            $data['mostViewed']   = $this->articles_model->get_most_viewed_rows(null, $data['current_lang_id'], 4);
            $data['main_content'] = "errors/error_404";
        }
        $this->load->view("layouts/default", $data);
    }

    function latest_articles(){
        $data                     = $this->tools_model->get_page_config();
        $data['articles']         = $this->articles_model->get_lastest_rows($data['current_lang_id'],18);
        $data['main_content']     = "pages/articles/articles";
        $data['page_title']       = lang('last_articles');
        $data['latest_articles']  = 'latest_articles';
        $data['site_description'] = lang('last_articles').' - '.lang('websiteName');
        $this->load->view("layouts/default", $data);
    }

    function download_article(){
        $this->load->helper('download');
        $articleID = (int)$this->uri->segment(3);
        $data      = $this->tools_model->get_current_language_config();
        $result    = $this->articles_model->get_property('content ,title ',$articleID, $data['current_lang_id']);
        if($result){
            $text  = strip_tags($result['content']);
            force_download($result['title'].".docx", $text);
        }
        $this->article($articleID);
    }
}