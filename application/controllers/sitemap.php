<?php

/**
 * Copyright (c) 2015 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author        programmer/ Mohammed Fawzy Zayan <admin@zayanit.com>
 * @link          http://www.zayanit.com
 * @version       1.0
 * Created by Sublime Text 2
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sitemap extends CI_Controller
{

	function __construct()
	{
        parent::__construct();
        $this->load->model('sitemap_model');
    }

    function articles_categories()
    {
    	$lang_id = $this->sitemap_model->get_lang_id();
    	$data['model'] = 'article';
    	$data['categories'] = $this->sitemap_model->get_categories($lang_id, $data['model']);
    	$this->load->view('sitemap/categories', $data);
    }

    function articles()
    {
    	$lang_id = $this->sitemap_model->get_lang_id();
    	$data['model'] = 'article';
    	$data['rows']  = $this->sitemap_model->get_rows($lang_id, $data['model']);
    	$this->load->view('sitemap/items', $data);
    }

    function videos_categories()
    {
    	$lang_id = $this->sitemap_model->get_lang_id();
    	$data['model'] = 'video';
    	$data['categories'] = $this->sitemap_model->get_categories($lang_id, $data['model']);
    	$this->load->view('sitemap/categories', $data);
    }

    function videos()
    {
    	$lang_id = $this->sitemap_model->get_lang_id();
    	$data['model'] = 'video';
    	$data['rows']  = $this->sitemap_model->get_rows($lang_id, $data['model']);
    	$this->load->view('sitemap/items', $data);
    }

    function audios_categories()
    {
    	$lang_id = $this->sitemap_model->get_lang_id();
    	$data['model'] = 'audio';
    	$data['categories'] = $this->sitemap_model->get_categories($lang_id, $data['model']);
    	$this->load->view('sitemap/categories', $data);
    }

    function audios()
    {
    	$lang_id = $this->sitemap_model->get_lang_id();
    	$data['model'] = 'audio';
    	$data['rows']  = $this->sitemap_model->get_rows($lang_id, $data['model']);
    	$this->load->view('sitemap/items', $data);
    }

}