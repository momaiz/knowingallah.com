<style type="text/css">
    table{
        width: 600px;
        text-align: center;
        margin: auto;
    }
    td{
        width: 200px;
        margin: auto;
        border: 1px solid #ccc;
    }
    tr td:first-child{text-align: right; font-size: 18px; padding: 3px 8px 3px 1px;}
    fieldset{width: 700px; background-color: #F5F5F5;}
    .a{background-color: #E7E7E7;}
    .butn{
        border: 1px solid #DBEAF9;
        background: #1B4552;
        padding: .5em;
        cursor: pointer;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        color: #fff;
        font-weight: bold;
        text-decoration: none;
    }
    .butn:hover{background: #DBEAF9; color:#1B4552;}
</style>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript">
    $(function(){
        $('#unfavor').click(function(){
            $('#volunteer_container').append('<img src="<?php echo base_url().'assets/images/ajax-loader.gif' ?>" alt="Loading Now" id="loading" />');
            var id = ('#volunteer_unfavor').val();
            $.ajax({
                url: 'volunteers/make_unfavor',
                type: 'POST',
                data: 'volunteer_id=' + id,

                success: function(result){
                    $('#loading').fadeOut(500);
                    $('#unfavor_form').fadeOut(500);
                    $('#favor_form').fadeIn(500);
                }
            });
            return false;
        });
    });
</script>
<div id="volunteer_container">
<?php
foreach ($data_rows as $row){
    $id = $this->uri->segment(3);
    echo "<br />";
    $edit_url = site_url().'/volunteers/volunteers_/edit/'.$id;
    echo "<a href=\"$edit_url\" class='butn'>تعــــــديل</a>";
    echo "<br /><br />";
    echo form_fieldset(' البيانات الأساسية ');
    ?>
<table>
    <tr>
        <td>الاسم : </td>
        <td><?=$row->name?></td>
    </tr>
    <tr class="a">
        <td>مميز : </td>
        <td><?=$row->favor?></td>
    </tr>
    <tr>
        <td>تاريخ التسجيل : </td>
        <td><?=$row->printDate?></td>
    </tr>
    <tr class="a">
        <td>رقم الهاتف : </td>
        <td><?=$row->phoneNumber?></td>
    </tr>
    <tr>
        <td>البريد الإلكترونى : </td>
        <td><?=$row->mail?></td>
    </tr>
    <tr class="a">
        <td>العمر : </td>
        <td><?=$row->age?></td>
    </tr>
    <tr>
        <td>المؤهل : </td>
        <td><?=$row->qualification?></td>
    </tr>
    <tr class="a">
        <td>الجنسية : </td>
        <td><?=$row->nationality?></td>
    </tr>
    <tr>
        <td>مكان الإقامة : </td>
        <td><?=$row->location?></td>
    </tr>
    <tr class="a">
        <td>المدينة : </td>
        <td><?=$row->city?></td>
    </tr>
    <tr>
        <td>البلد : </td>
        <td>
            <?php
            $country="";
            switch ($row->country) {
            case "AF": $country="Afghanistan"; break;
            case "AX": $country="Aland Islands"; break;
            case "AL": $country="Albania"; break;
            case "DZ": $country="Algeria"; break;
            case "AS": $country="American Samoa"; break;
            case "AD": $country="Andorra"; break;
            case "AO": $country="Angola"; break;
            case "AI": $country="Anguilla"; break;
            case "AQ": $country="Antarctica"; break;
            case "AG": $country="Antigua and Barbuda"; break;
            case "AR": $country="Argentina"; break;
            case "AM": $country="Armenia"; break;
            case "AW": $country="Aruba"; break;
            case "AU": $country="Australia"; break;
            case "AT": $country="Austria"; break;
            case "AZ": $country="Azerbaijan"; break;
            case "BS": $country="Bahamas"; break;
            case "BH": $country="Bahrain"; break;
            case "BD": $country="Bangladesh"; break;
            case "BB": $country="Barbados"; break;
            case "BY": $country="Belarus"; break;
            case "BE": $country="Belgium"; break;
            case "BZ": $country="Belize"; break;
            case "BJ": $country="Benin"; break;
            case "BM": $country="Bermuda"; break;
            case "BT": $country="Bhutan"; break;
            case "BO": $country="Bolivia"; break;
            case "BA": $country="Bosnia and Herzegovina"; break;
            case "BW": $country="Botswana"; break;
            case "BV": $country="Bouvet Island"; break;
            case "BR": $country="Brazil"; break;
            case "IO": $country="British Indian Ocean Territory"; break;
            case "BN": $country="Brunei Darussalam"; break;
            case "BG": $country="Bulgaria"; break;
            case "BF": $country="Burkina Faso"; break;
            case "BI": $country="Burundi"; break;
            case "KH": $country="Cambodia"; break;
            case "CM": $country="Cameroon"; break;
            case "CA": $country="Canada"; break;
            case "CV": $country="Cape Verde"; break;
            case "KY": $country="Cayman Islands"; break;
            case "CF": $country="Central African Republic"; break;
            case "TD": $country="Chad"; break;
            case "CL": $country="Chile"; break;
            case "CN": $country="China"; break;
            case "CX": $country="Christmas Island"; break;
            case "CC": $country="Cocos (Keeling) Islands"; break;
            case "CO": $country="Colombia"; break;
            case "KM": $country="Comoros"; break;
            case "CG": $country="Congo"; break;
            case "CD": $country="Congo, The Democratic Republic of the"; break;
            case "CK": $country="Cook Islands"; break;
            case "CR": $country="Costa Rica"; break;
            case "CI": $country="Cote d'Ivoire"; break;
            case "HR": $country="Croatia"; break;
            case "CU": $country="Cuba"; break;
            case "CY": $country="Cyprus"; break;
            case "CZ": $country="Czech Republic"; break;
            case "DK": $country="Denmark"; break;
            case "DJ": $country="Djibouti"; break;
            case "DM": $country="Dominica"; break;
            case "DO": $country="Dominican Republic"; break;
            case "EC": $country="Ecuador"; break;
            case "EG": $country="Egypt"; break;
            case "SV": $country="El Salvador"; break;
            case "GQ": $country="Equatorial Guinea"; break;
            case "ER": $country="Eritrea"; break;
            case "EE": $country="Estonia"; break;
            case "ET": $country="Ethiopia"; break;
            case "FK": $country="Falkland Islands (Malvinas)"; break;
            case "FO": $country="Faroe Islands"; break;
            case "FJ": $country="Fiji"; break;
            case "FI": $country="Finland"; break;
            case "FR": $country="France"; break;
            case "GF": $country="French Guiana"; break;
            case "PF": $country="French Polynesia"; break;
            case "TF": $country="French Southern Territories"; break;
            case "GA": $country="Gabon"; break;
            case "GM": $country="Gambia"; break;
            case "GE": $country="Georgia"; break;
            case "DE": $country="Germany"; break;
            case "GH": $country="Ghana"; break;
            case "GI": $country="Gibraltar"; break;
            case "GR": $country="Greece"; break;
            case "GL": $country="Greenland"; break;
            case "GD": $country="Grenada"; break;
            case "GP": $country="Guadeloupe"; break;
            case "GU": $country="Guam"; break;
            case "GT": $country="Guatemala"; break;
            case "GG": $country="Guernsey"; break;
            case "GN": $country="Guinea"; break;
            case "GW": $country="Guinea-Bissau"; break;
            case "GY": $country="Guyana"; break;
            case "HT": $country="Haiti"; break;
            case "HM": $country="Heard Island and McDonald Islands"; break;
            case "VA": $country="Holy See (Vatican City State)"; break;
            case "HN": $country="Honduras"; break;
            case "HK": $country="Hong Kong"; break;
            case "HU": $country="Hungary"; break;
            case "IS": $country="Iceland"; break;
            case "IN": $country="India"; break;
            case "ID": $country="Indonesia"; break;
            case "IR": $country="Iran, Islamic Republic of"; break;
            case "IQ": $country="Iraq"; break;
            case "IE": $country="Ireland"; break;
            case "IM": $country="Isle of Man"; break;
            case "IL": $country="Israel"; break;
            case "IT": $country="Italy"; break;
            case "JM": $country="Jamaica"; break;
            case "JP": $country="Japan"; break;
            case "JE": $country="Jersey"; break;
            case "JO": $country="Jordan"; break;
            case "KZ": $country="Kazakhstan"; break;
            case "KE": $country="Kenya"; break;
            case "KI": $country="Kiribati"; break;
            case "KP": $country="Korea, Democratic People's Republic of"; break;
            case "KR": $country="Korea, Republic of"; break;
            case "KW": $country="Kuwait"; break;
            case "KG": $country="Kyrgyzstan"; break;
            case "LA": $country="Lao People's Democratic Republic"; break;
            case "LV": $country="Latvia"; break;
            case "LB": $country="Lebanon"; break;
            case "LS": $country="Lesotho"; break;
            case "LR": $country="Liberia"; break;
            case "LY": $country="Libyan Arab Jamahiriya"; break;
            case "LI": $country="Liechtenstein"; break;
            case "LT": $country="Lithuania"; break;
            case "LU": $country="Luxembourg"; break;
            case "MO": $country="Macao"; break;
            case "MK": $country="Macedonia, The Former Yugoslav Republic of"; break;
            case "MG": $country="Madagascar"; break;
            case "MW": $country="Malawi"; break;
            case "MY": $country="Malaysia"; break;
            case "MV": $country="Maldives"; break;
            case "ML": $country="Mali"; break;
            case "MT": $country="Malta"; break;
            case "MH": $country="Marshall Islands"; break;
            case "MQ": $country="Martinique"; break;
            case "MR": $country="Mauritania"; break;
            case "MU": $country="Mauritius"; break;
            case "YT": $country="Mayotte"; break;
            case "MX": $country="Mexico"; break;
            case "FM": $country="Micronesia, Federated States of"; break;
            case "MD": $country="Moldova, Republic of"; break;
            case "MC": $country="Monaco"; break;
            case "MN": $country="Mongolia"; break;
            case "MS": $country="Montserrat"; break;
            case "ME": $country="Montenegro"; break;
            case "MA": $country="Morocco"; break;
            case "MZ": $country="Mozambique"; break;
            case "MM": $country="Myanmar"; break;
            case "NA": $country="Namibia"; break;
            case "NR": $country="Nauru"; break;
            case "NP": $country="Nepal"; break;
            case "NL": $country="Netherlands"; break;
            case "AN": $country="Netherlands Antilles"; break;
            case "NC": $country="New Caledonia"; break;
            case "NZ": $country="New Zealand"; break;
            case "NI": $country="Nicaragua"; break;
            case "NE": $country="Niger"; break;
            case "NG": $country="Nigeria"; break;
            case "NU": $country="Niue"; break;
            case "NF": $country="Norfolk Island"; break;
            case "MP": $country="Northern Mariana Islands"; break;
            case "NO": $country="Norway"; break;
            case "OM": $country="Oman"; break;
            case "PK": $country="Pakistan"; break;
            case "PW": $country="Palau"; break;
            case "PS": $country="Palestinian Territories, Occupied"; break;
            case "PA": $country="Panama"; break;
            case "PG": $country="Papua New Guinea"; break;
            case "PY": $country="Paraguay"; break;
            case "PE": $country="Peru"; break;
            case "PH": $country="Philippines"; break;
            case "PN": $country="Pitcairn"; break;
            case "PL": $country="Poland"; break;
            case "PT": $country="Portugal"; break;
            case "PR": $country="Puerto Rico"; break;
            case "QA": $country="Qatar"; break;
            case "RE": $country="Reunion"; break;
            case "RO": $country="Romania"; break;
            case "RU": $country="Russian Federation"; break;
            case "RW": $country="Rwanda"; break;
            case "BL": $country="Saint Barthelemy"; break;
            case "SH": $country="Saint Helena"; break;
            case "KN": $country="Saint Kitts and Nevis"; break;
            case "LC": $country="Saint Lucia"; break;
            case "MF": $country="Saint Martin"; break;
            case "PM": $country="Saint Pierre and Miquelon"; break;
            case "VC": $country="Saint Vincent and the Grenadines"; break;
            case "WS": $country="Samoa"; break;
            case "SM": $country="San Marino"; break;
            case "ST": $country="Sao Tome and Principe"; break;
            case "SA": $country="Saudi Arabia"; break;
            case "SN": $country="Senegal"; break;
            case "RS": $country="Serbia"; break;
            case "SC": $country="Seychelles"; break;
            case "SL": $country="Sierra Leone"; break;
            case "SG": $country="Singapore"; break;
            case "SK": $country="Slovakia"; break;
            case "SI": $country="Slovenia"; break;
            case "SB": $country="Solomon Islands"; break;
            case "SO": $country="Somalia"; break;
            case "ZA": $country="South Africa"; break;
            case "GS": $country="South Georgia and the South Sandwich Islands"; break;
            case "ES": $country="Spain"; break;
            case "LK": $country="Sri Lanka"; break;
            case "SD": $country="Sudan"; break;
            case "SR": $country="Suriname"; break;
            case "SJ": $country="Svalbard and Jan Mayen"; break;
            case "SZ": $country="Swaziland"; break;
            case "SE": $country="Sweden"; break;
            case "CH": $country="Switzerland"; break;
            case "SY": $country="Syrian Arab Republic"; break;
            case "TW": $country="Taiwan, Province of China"; break;
            case "TJ": $country="Tajikistan"; break;
            case "TZ": $country="Tanzania, United Republic of"; break;
            case "TH": $country="Thailand"; break;
            case "TL": $country="Timor-Leste"; break;
            case "TG": $country="Togo"; break;
            case "TK": $country="Tokelau"; break;
            case "TO": $country="Tonga"; break;
            case "TT": $country="Trinidad and Tobago"; break;
            case "TN": $country="Tunisia"; break;
            case "TR": $country="Turkey"; break;
            case "TM": $country="Turkmenistan"; break;
            case "TC": $country="Turks and Caicos Islands"; break;
            case "TV": $country="Tuvalu"; break;
            case "UG": $country="Uganda"; break;
            case "UA": $country="Ukraine"; break;
            case "AE": $country="United Arab Emirates"; break;
            case "GB": $country="United Kingdom"; break;
            case "US": $country="United States"; break;
            case "UM": $country="United States Minor Outlying Islands"; break;
            case "UY": $country="Uruguay"; break;
            case "UZ": $country="Uzbekistan"; break;
            case "VU": $country="Vanuatu"; break;
            case "VE": $country="Venezuela"; break;
            case "VN": $country="Viet Nam"; break;
            case "VG": $country="Virgin Islands, British"; break;
            case "VI": $country="Virgin Islands, U.S."; break;
            case "WF": $country="Wallis and Futuna"; break;
            case "EH": $country="Western Sahara"; break;
            case "YE": $country="Yemen"; break;
            case "ZM": $country="Zambia"; break;
            case "ZW": $country="Zimbabwe"; break;
            }
            echo $country;
            ?>
        </td>
    </tr>
    <tr class="a">
        <td>الدراسات والخبرات الأخرى : </td>
        <td><?=$row->otherExperience?></td>
    </tr>
    <tr>
        <td>أنشطة : </td>
        <td><?=$row->activities?></td>
    </tr>
    <tr class="a">
        <td>اللغة الأم : </td>
        <td>
            <?php foreach($main_lang as $lang): ?>
            <?php echo $lang->description ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>لغات أخرى : </td>
        <td>
        	<?php if(isset($volunteer_languages)): ?>
            <?php foreach($volunteer_languages as $other_language): ?>
            <?php echo $other_language->description . "<br />"; ?>
            <?php endforeach; ?>
            <?php endif; ?>
        </td>
    </tr>
    <tr class="a">
        <td>العمل الأساسى : </td>
        <td><?=$row->profession?></td>
    </tr>
    <tr>
        <td>مهارات أخرى تفيد العمل فى الفريق : </td>
        <td>
        	<?php if(isset($skills)): ?>
            <?php foreach($skills as $skill): ?>
            <?php echo $skill->arabicDesc . "<br />"; ?>
            <?php endforeach; ?>
            <?php endif; ?>
        </td>
    </tr>
    <tr class="a">
        <td>اسم الظهور فى المنتدى : </td>
        <td><?=$row->forumName?></td>
    </tr>
    <tr>
        <td>اسم الفريق المشترك به فى المنتدى : </td>
        <td><?=$row->forumTeam?></td>
    </tr>
</table>
    <?php echo form_fieldset_close(); ?>
<br />
<?php echo form_fieldset(' بيانات إضافية '); ?>
<table>
    <tr class="a">
        <td>أشهر قناة تلفزيونية فى منطقته : </td>
        <td><?=$row->popularTV?></td>
    </tr>
    <tr>
        <td>أنسب الأماكن فى المدينة لعمل الدعاية : </td>
        <td><?=$row->popularPlace?></td>
    </tr>
    <tr class="a">
        <td>صفحة أو جروب المنطقة على الفيس أو أى موقع آخر : </td>
        <td><?=$row->websites?></td>
    </tr>
    <tr>
        <td>إضافات أخرى : </td>
        <td><?=$row->comment?></td>
    </tr>
</table>
<?php echo form_fieldset_close(); ?>
<?php
} 
?>
<br /><br /><br /><br /><br /><br /><br />
</div>