		<ul class="sf-menu">
			<li>
				<a href="#a">صفحة المقدمة</a>
				<ul>
					<li><a href="<?= site_url('intro/intro_text') ?>">نص المقدمة</a></li>
					<li><a href="<?= site_url('intro/intro_videos') ?>">فيديوهات المقدمة</a></li>
				</ul>
			</li>
			<li>
				<a href="#a">المقالات</a>
				<ul>
					<li>
						<a href="#">الأقسام الرئيسية</a>
						<ul>
						<?php foreach($languages as $lang): ?>
							<li><a href="<?=site_url("articles/articles_category").'/'.$lang->id.'/'?>"><?=$lang->description?></a></li>
						<?php endforeach; ?>						
						</ul>
					</li>

					<li>
						<a href="<?=site_url("articles/search_cat")?>">البحث عن قسم</a>					
					</li>
					<li>
						<a href="<?=site_url("articles/search_article")?>">البحث عن مقال</a>
					</li>					
				</ul>
			</li>
			<li>
				<a href="#a">الفيديوهات</a>
				<ul>
					<li>
						<a href="<?= site_url('videos/home_videos') ?>">فيديوهات الرئيسية</a>
					</li>
					<li>
						<a href="#">الأقسام الرئيسية</a>
						<ul>
<?php 
foreach($languages as $lang): ?>
							<li><a href="<?=site_url("videos/videos_category/" . $lang->id)?>"><?=$lang->description?></a></li>
<?php endforeach; ?>						
						</ul>
					</li>

					<li>
						<a href="<?=site_url("videos/search_cat")?>">البحث عن قسم</a>					
					</li>
					<li>
						<a href="<?=site_url("videos/search_video")?>">البحث عن فيديو</a>
					</li>					
				</ul>
			</li>			
			<li>
				<a href="#a">الصوتيات</a>
				<ul>
					<li>
						<a href="#">الأقسام الرئيسية</a>
						<ul>
<?php 
foreach($languages as $lang): ?>
							<li><a href="<?=site_url("audios/audios_category/" . $lang->id)?>"><?=$lang->description?></a></li>
<?php endforeach; ?>						
						</ul>
					</li>

					<li>
						<a href="<?=site_url("audios/search_cat")?>">البحث عن قسم</a>					
					</li>
					<li>
						<a href="<?=site_url("audios/search_audio")?>">البحث عن صوتيات</a>
					</li>					
				</ul>
			</li>	

			<li>
				<a href="<?= site_url('books/books_for_dowinload') ?>">الكتيبات</a>
			</li>		
            			
			<li>
				<a href="<?=site_url('languages')?>">اللغات</a>
			</li>

            <li>
            	<a href="<?php echo site_url('pages'); ?>">الصفحات الثابتة</a>
            </li>
                        
            <li>
                <a href="<?php echo site_url('menu'); ?>">القائمة</a>
            </li>

            <li>
            	<a href="#a">الصور</a>
            	<ul>
            		<li>
                        <a href="<?php echo site_url('slider'); ?>">الاسلايدر</a>
                    </li>
					<li>
                        <a href="<?php echo site_url('photo_gallery'); ?>">معرض الصور</a>
                    </li>
                    <li>
                    	<a href="<?php echo site_url('banners'); ?>">البنرات</a>
            		</li>
            	</ul>
            </li>
                        
            <li>
                <a href="<?=site_url('login/logout')?>">تسجيل الخروج</a>
            </li>
		</ul>

<!--<li>
        <a href="<?=site_url('blocks')?>">البلوكات</a>
    </li>
    <li>
        <a href="#a">المتطوعين</a>
        <ul>
            <li>
                <a href="<?=site_url("volunteers/volunteers_")?>">قائمة الأعضاء</a>
            </li>
            <li>
                <a href="<?=site_url("volunteers/skills")?>">المهارات</a>
            </li>
            <li>
                <a href="<?=site_url('volunteers/search')?>">البحث</a>
            </li>
            <li>
                <a href="<?=site_url('volunteers/favor_volunteers')?>">المتطوعين المميزين</a>
            </li>
        </ul>
    </li>
    <li>
		<a href="#a">الفلاشات</a>
		<ul>
			<li>
				<a href="#">الأقسام الرئيسية</a>
				<ul>
<?php 
foreach($languages as $lang): ?>
					<li><a href="<?=site_url("flashs/flashs_category/" . $lang->id)?>"><?=$lang->description?></a></li>
<?php endforeach; ?>						
				</ul>
			</li>

			<li>
				<a href="<?=site_url("flashs/search_cat")?>">البحث عن قسم</a>					
			</li>
			<li>
				<a href="<?=site_url("flashs/search_flash")?>">البحث عن فلاش</a>
			</li>					
		</ul>
	</li> 
    
    <li>
        <a href="#a">الكتب</a>
        <ul>
            <li>
                <a href="#">الأقسام الرئيسية</a>
                <ul>
                    <?php foreach($languages as $lang): ?>
                        <li><a href="<?=site_url("books/books_category/" . $lang->id)?>"><?=$lang->description?></a></li>
                    <?php endforeach; ?>
                </ul>
            </li>
            <li>
                <a href="<?=site_url("books/search_cat")?>">البحث عن قسم</a>
            </li>
            <li>
                <a href="<?=site_url("books/search_book")?>">البحث عن كتاب</a>
            </li>
        </ul>
    </li>-->