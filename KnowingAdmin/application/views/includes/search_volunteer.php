<br/><br/>
<script type="text/javascript" src="<?= base_url()?>assets/menu/js/tools.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/menu/js/jquery-1.7.1.min.js"></script>
<style type="text/css">
    #country_div{display: none;}
    #skill_div{display: none;}
    #language_div{display: none;}
    #email_div{display: none;}
    span{font-size: 16px; font-weight: bold;}
    #result{
    	width: 700px;
    	margin: 5px 20px;
    }
    table{width: 100%}
    td{border: 1px solid #CCC;}
    th{
    	background: #A8C3BE;
    	border: 1px solid #5C4511;
    }
    #pagination a,#pagination strong{
    	background: #005599;
    	color: #fff;
    	padding: 4px 8px;
    	border: 1px solid #CCC;
    }
    #pagination a:hover, #pagination strong{
    	background: #BBCAD2;
    	color: #000;
    }
</style>
<script type="text/javascript">
    $(function(){
        $('select').change(function(){
            $('option:selected#sc_country').each(function(){
                $('#all_divs div').hide();
                $('#country_div').show();
            });
            $('option:selected#sc_skill').each(function(){
                $('#all_divs div').hide();
                $('#skill_div').show();
            });
            $('option:selected#sc_language').each(function(){
                $('#all_divs div').hide();
                $('#language_div').show();
            });
            $('option:selected#sc_email').each(function(){
                $('#all_divs div').hide();
                $('#email_div').show();
            });
        });
        $('tr:odd').css('background', '#E7E7E7');
    });
</script>

<select>
    <option>اختر طريقة البحث</option>
    <option id="sc_country">البحث بالبلد</option>
    <option id="sc_skill">البحث بالمهارة</option>
    <option id="sc_language">البحث باللغة</option>
    <option id="sc_email">البحث بالبريد الإلكترونى</option>
</select>

<br /><br />

<div id="all_divs">
    <?php echo form_open('volunteers/search_by_country'); ?>
    <div id="country_div">
        <span>برجاء اختيار البلد : </span>
        <select size=1 width="100%" id="country" name="country" >
            <option value="AF" >Afghanistan</option>
            <option value="AX" >Aland Islands</option>
            <option value="AL" >Albania</option>
            <option value="DZ" >Algeria</option>
            <option value="AS" >American Samoa</option>
            <option value="AD" >Andorra</option>
            <option value="AO" >Angola</option>
            <option value="AI" >Anguilla</option>
            <option value="AQ" >Antarctica</option>
            <option value="AG" >Antigua and Barbuda</option>
            <option value="AR" >Argentina</option>
            <option value="AM" >Armenia</option>
            <option value="AW" >Aruba</option>
            <option value="AU" >Australia</option>
            <option value="AT" >Austria</option>
            <option value="AZ" >Azerbaijan</option>
            <option value="BS" >Bahamas</option>
            <option value="BH" >Bahrain</option>
            <option value="BD" >Bangladesh</option>
            <option value="BB" >Barbados</option>
            <option value="BY" >Belarus</option>
            <option value="BE" >Belgium</option>
            <option value="BZ" >Belize</option>
            <option value="BJ" >Benin</option>
            <option value="BM" >Bermuda</option>
            <option value="BT" >Bhutan</option>
            <option value="BO" >Bolivia</option>
            <option value="BA" >Bosnia and Herzegovina</option>
            <option value="BW" >Botswana</option>
            <option value="BV" >Bouvet Island</option>
            <option value="BR" >Brazil</option>
            <option value="IO" >British Indian Ocean Territory</option>
            <option value="BN" >Brunei Darussalam</option>
            <option value="BG" >Bulgaria</option>
            <option value="BF" >Burkina Faso</option>
            <option value="BI" >Burundi</option>
            <option value="KH" >Cambodia</option>
            <option value="CM" >Cameroon</option>
            <option value="CA" >Canada</option>
            <option value="CV" >Cape Verde</option>
            <option value="KY" >Cayman Islands</option>
            <option value="CF" >Central African Republic</option>
            <option value="TD" >Chad</option>
            <option value="CL" >Chile</option>
            <option value="CN" >China</option>
            <option value="CX" >Christmas Island</option>
            <option value="CC" >Cocos (Keeling) Islands</option>
            <option value="CO" >Colombia</option>
            <option value="KM" >Comoros</option>
            <option value="CG" >Congo</option>
            <option value="CD" >Congo, The Democratic Republic of the</option>
            <option value="CK" >Cook Islands</option>
            <option value="CR" >Costa Rica</option>
            <option value="CI" >Cote d'Ivoire</option>
            <option value="HR" >Croatia</option>
            <option value="CU" >Cuba</option>
            <option value="CY" >Cyprus</option>
            <option value="CZ" >Czech Republic</option>
            <option value="DK" >Denmark</option>
            <option value="DJ" >Djibouti</option>
            <option value="DM" >Dominica</option>
            <option value="DO" >Dominican Republic</option>
            <option value="EC" >Ecuador</option>
            <option value="EG" >Egypt</option>
            <option value="SV" >El Salvador</option>
            <option value="GQ" >Equatorial Guinea</option>
            <option value="ER" >Eritrea</option>
            <option value="EE" >Estonia</option>
            <option value="ET" >Ethiopia</option>
            <option value="FK" >Falkland Islands (Malvinas)</option>
            <option value="FO" >Faroe Islands</option>
            <option value="FJ" >Fiji</option>
            <option value="FI" >Finland</option>
            <option value="FR" >France</option>
            <option value="GF" >French Guiana</option>
            <option value="PF" >French Polynesia</option>
            <option value="TF" >French Southern Territories</option>
            <option value="GA" >Gabon</option>
            <option value="GM" >Gambia</option>
            <option value="GE" >Georgia</option>
            <option value="DE" >Germany</option>
            <option value="GH" >Ghana</option>
            <option value="GI" >Gibraltar</option>
            <option value="GR" >Greece</option>
            <option value="GL" >Greenland</option>
            <option value="GD" >Grenada</option>
            <option value="GP" >Guadeloupe</option>
            <option value="GU" >Guam</option>
            <option value="GT" >Guatemala</option>
            <option value="GG" >Guernsey</option>
            <option value="GN" >Guinea</option>
            <option value="GW" >Guinea-Bissau</option>
            <option value="GY" >Guyana</option>
            <option value="HT" >Haiti</option>
            <option value="HM" >Heard Island and McDonald Islands</option>
            <option value="VA" >Holy See (Vatican City State)</option>
            <option value="HN" >Honduras</option>
            <option value="HK" >Hong Kong</option>
            <option value="HU" >Hungary</option>
            <option value="IS" >Iceland</option>
            <option value="IN" >India</option>
            <option value="ID" >Indonesia</option>
            <option value="IR" >Iran, Islamic Republic of</option>
            <option value="IQ" >Iraq</option>
            <option value="IE" >Ireland</option>
            <option value="IM" >Isle of Man</option>
            <option value="IL" >Israel</option>
            <option value="IT" >Italy</option>
            <option value="JM" >Jamaica</option>
            <option value="JP" >Japan</option>
            <option value="JE" >Jersey</option>
            <option value="JO" >Jordan</option>
            <option value="KZ" >Kazakhstan</option>
            <option value="KE" >Kenya</option>
            <option value="KI" >Kiribati</option>
            <option value="KP" >Korea, Democratic People's Republic of</option>
            <option value="KR" >Korea, Republic of</option>
            <option value="KW" >Kuwait</option>
            <option value="KG" >Kyrgyzstan</option>
            <option value="LA" >Lao People's Democratic Republic</option>
            <option value="LV" >Latvia</option>
            <option value="LB" >Lebanon</option>
            <option value="LS" >Lesotho</option>
            <option value="LR" >Liberia</option>
            <option value="LY" >Libyan Arab Jamahiriya</option>
            <option value="LI" >Liechtenstein</option>
            <option value="LT" >Lithuania</option>
            <option value="LU" >Luxembourg</option>
            <option value="MO" >Macao</option>
            <option value="MK" >Macedonia, The Former Yugoslav Republic of</option>
            <option value="MG" >Madagascar</option>
            <option value="MW" >Malawi</option>
            <option value="MY" >Malaysia</option>
            <option value="MV" >Maldives</option>
            <option value="ML" >Mali</option>
            <option value="MT" >Malta</option>
            <option value="MH" >Marshall Islands</option>
            <option value="MQ" >Martinique</option>
            <option value="MR" >Mauritania</option>
            <option value="MU" >Mauritius</option>
            <option value="YT" >Mayotte</option>
            <option value="MX" >Mexico</option>
            <option value="FM" >Micronesia, Federated States of</option>
            <option value="MD" >Moldova, Republic of</option>
            <option value="MC" >Monaco</option>
            <option value="MN" >Mongolia</option>
            <option value="MS" >Montserrat</option>
            <option value="ME" >Montenegro</option>
            <option value="MA" >Morocco</option>
            <option value="MZ" >Mozambique</option>
            <option value="MM" >Myanmar</option>
            <option value="NA" >Namibia</option>
            <option value="NR" >Nauru</option>
            <option value="NP" >Nepal</option>
            <option value="NL" >Netherlands</option>
            <option value="AN" >Netherlands Antilles</option>
            <option value="NC" >New Caledonia</option>
            <option value="NZ" >New Zealand</option>
            <option value="NI" >Nicaragua</option>
            <option value="NE" >Niger</option>
            <option value="NG" >Nigeria</option>
            <option value="NU" >Niue</option>
            <option value="NF" >Norfolk Island</option>
            <option value="MP" >Northern Mariana Islands</option>
            <option value="NO" >Norway</option>
            <option value="OM" >Oman</option>
            <option value="PK" >Pakistan</option>
            <option value="PW" >Palau</option>
            <option value="PS" >Palestinian Territories, Occupied</option>
            <option value="PA" >Panama</option>
            <option value="PG" >Papua New Guinea</option>
            <option value="PY" >Paraguay</option>
            <option value="PE" >Peru</option>
            <option value="PH" >Philippines</option>
            <option value="PN" >Pitcairn</option>
            <option value="PL" >Poland</option>
            <option value="PT" >Portugal</option>
            <option value="PR" >Puerto Rico</option>
            <option value="QA" >Qatar</option>
            <option value="RE" >Reunion</option>
            <option value="RO" >Romania</option>
            <option value="RU" >Russian Federation</option>
            <option value="RW" >Rwanda</option>
            <option value="BL" >Saint Barthelemy</option>
            <option value="SH" >Saint Helena</option>
            <option value="KN" >Saint Kitts and Nevis</option>
            <option value="LC" >Saint Lucia</option>
            <option value="MF" >Saint Martin</option>
            <option value="PM" >Saint Pierre and Miquelon</option>
            <option value="VC" >Saint Vincent and the Grenadines</option>
            <option value="WS" >Samoa</option>
            <option value="SM" >San Marino</option>
            <option value="ST" >Sao Tome and Principe</option>
            <option value="SA">Saudi Arabia</option>
            <option value="SN" >Senegal</option>
            <option value="RS" >Serbia</option>
            <option value="SC" >Seychelles</option>
            <option value="SL" >Sierra Leone</option>
            <option value="SG" >Singapore</option>
            <option value="SK" >Slovakia</option>
            <option value="SI" >Slovenia</option>
            <option value="SB" >Solomon Islands</option>
            <option value="SO" >Somalia</option>
            <option value="ZA" >South Africa</option>
            <option value="GS" >South Georgia and the South Sandwich Islands</option>
            <option value="ES" >Spain</option>
            <option value="LK" >Sri Lanka</option>
            <option value="SD" >Sudan</option>
            <option value="SR" >Suriname</option>
            <option value="SJ" >Svalbard and Jan Mayen</option>
            <option value="SZ" >Swaziland</option>
            <option value="SE" >Sweden</option>
            <option value="CH" >Switzerland</option>
            <option value="SY" >Syrian Arab Republic</option>
            <option value="TW" >Taiwan, Province of China</option>
            <option value="TJ" >Tajikistan</option>
            <option value="TZ" >Tanzania, United Republic of</option>
            <option value="TH" >Thailand</option>
            <option value="TL" >Timor-Leste</option>
            <option value="TG" >Togo</option>
            <option value="TK" >Tokelau</option>
            <option value="TO" >Tonga</option>
            <option value="TT" >Trinidad and Tobago</option>
            <option value="TN" >Tunisia</option>
            <option value="TR" >Turkey</option>
            <option value="TM" >Turkmenistan</option>
            <option value="TC" >Turks and Caicos Islands</option>
            <option value="TV" >Tuvalu</option>
            <option value="UG" >Uganda</option>
            <option value="UA" >Ukraine</option>
            <option value="AE" >United Arab Emirates</option>
            <option value="GB" >United Kingdom</option>
            <option value="US">United States</option>
            <option value="UM" >United States Minor Outlying Islands</option>
            <option value="UY" >Uruguay</option>
            <option value="UZ" >Uzbekistan</option>
            <option value="VU" >Vanuatu</option>
            <option value="VE" >Venezuela</option>
            <option value="VN" >Viet Nam</option>
            <option value="VG" >Virgin Islands, British</option>
            <option value="VI" >Virgin Islands, U.S.</option>
            <option value="WF" >Wallis and Futuna</option>
            <option value="EH" >Western Sahara</option>
            <option value="YE" >Yemen</option>
            <option value="ZM" >Zambia</option>
            <option value="ZW" >Zimbabwe</option>
        </select>
        <input type="submit" name="submit" value=" بحث " /> 
        <?php echo form_close(); ?>
    </div>
    <div id="skill_div">
        <?php echo form_open('volunteers/search_by_skill'); ?>
        <span>برجاء اختيار المهارة : </span>
        <select size=1 width="100%" name="volunteer_skill">
            <?php foreach($allSkills as $oneSkill): ?>
            <option value="<?=$oneSkill->skillId?>"><?=$oneSkill->arabicDesc?></option>
            <?php endforeach; ?>
        </select>
        <input type="submit" name="submit" value=" بحث " /> 
        <?php echo form_close(); ?>
    </div>
    <div id="language_div">
        <?php echo form_open('volunteers/search_by_language'); ?>
        <span>برجاء اختيار اللغة : </span>
        <select size=1 width="100%" name="volunteer_lang">
            <?php foreach($allLanguages as $onelang): ?>
            <option value="<?=$onelang->id?>"><?=$onelang->description?></option>
            <?php endforeach; ?>
        </select>
        <input type="submit" name="submit" value=" بحث " />
        <?php echo form_close(); ?>
    </div>
    <div id="email_div">
        <?php echo form_open('volunteers/search_by_email'); ?>
        <span>برجاء إدخال البريد الإلكترونى : </span>
        <input type="text" name="volunteer_mail" size="40" />
        <input type="submit" name="submit" value=" بحث " />
        <?php echo form_close(); ?>
    </div>
    
</div>
<br />
<div id="result">
<?php if(isset($sk_volunteers)): ?>
	<?php //echo $this->table->generate($sk_volunteers); ?>
    <table border="1" cellpadding="2" cellspacing="1" class="mytable">
        <tr>
            <th>الاسم</th>
            <th>البريد الإلكترونى</th>
            <th>تاريخ الاشتراك</th>
            <th>البلد</th>
        </tr>
    <?php
        foreach($sk_volunteers as $sv):
        $country="";
        switch ($sv->country) {
            case "AF": $country="Afghanistan"; break;
            case "AX": $country="Aland Islands"; break;
            case "AL": $country="Albania"; break;
            case "DZ": $country="Algeria"; break;
            case "AS": $country="American Samoa"; break;
            case "AD": $country="Andorra"; break;
            case "AO": $country="Angola"; break;
            case "AI": $country="Anguilla"; break;
            case "AQ": $country="Antarctica"; break;
            case "AG": $country="Antigua and Barbuda"; break;
            case "AR": $country="Argentina"; break;
            case "AM": $country="Armenia"; break;
            case "AW": $country="Aruba"; break;
            case "AU": $country="Australia"; break;
            case "AT": $country="Austria"; break;
            case "AZ": $country="Azerbaijan"; break;
            case "BS": $country="Bahamas"; break;
            case "BH": $country="Bahrain"; break;
            case "BD": $country="Bangladesh"; break;
            case "BB": $country="Barbados"; break;
            case "BY": $country="Belarus"; break;
            case "BE": $country="Belgium"; break;
            case "BZ": $country="Belize"; break;
            case "BJ": $country="Benin"; break;
            case "BM": $country="Bermuda"; break;
            case "BT": $country="Bhutan"; break;
            case "BO": $country="Bolivia"; break;
            case "BA": $country="Bosnia and Herzegovina"; break;
            case "BW": $country="Botswana"; break;
            case "BV": $country="Bouvet Island"; break;
            case "BR": $country="Brazil"; break;
            case "IO": $country="British Indian Ocean Territory"; break;
            case "BN": $country="Brunei Darussalam"; break;
            case "BG": $country="Bulgaria"; break;
            case "BF": $country="Burkina Faso"; break;
            case "BI": $country="Burundi"; break;
            case "KH": $country="Cambodia"; break;
            case "CM": $country="Cameroon"; break;
            case "CA": $country="Canada"; break;
            case "CV": $country="Cape Verde"; break;
            case "KY": $country="Cayman Islands"; break;
            case "CF": $country="Central African Republic"; break;
            case "TD": $country="Chad"; break;
            case "CL": $country="Chile"; break;
            case "CN": $country="China"; break;
            case "CX": $country="Christmas Island"; break;
            case "CC": $country="Cocos (Keeling) Islands"; break;
            case "CO": $country="Colombia"; break;
            case "KM": $country="Comoros"; break;
            case "CG": $country="Congo"; break;
            case "CD": $country="Congo, The Democratic Republic of the"; break;
            case "CK": $country="Cook Islands"; break;
            case "CR": $country="Costa Rica"; break;
            case "CI": $country="Cote d'Ivoire"; break;
            case "HR": $country="Croatia"; break;
            case "CU": $country="Cuba"; break;
            case "CY": $country="Cyprus"; break;
            case "CZ": $country="Czech Republic"; break;
            case "DK": $country="Denmark"; break;
            case "DJ": $country="Djibouti"; break;
            case "DM": $country="Dominica"; break;
            case "DO": $country="Dominican Republic"; break;
            case "EC": $country="Ecuador"; break;
            case "EG": $country="Egypt"; break;
            case "SV": $country="El Salvador"; break;
            case "GQ": $country="Equatorial Guinea"; break;
            case "ER": $country="Eritrea"; break;
            case "EE": $country="Estonia"; break;
            case "ET": $country="Ethiopia"; break;
            case "FK": $country="Falkland Islands (Malvinas)"; break;
            case "FO": $country="Faroe Islands"; break;
            case "FJ": $country="Fiji"; break;
            case "FI": $country="Finland"; break;
            case "FR": $country="France"; break;
            case "GF": $country="French Guiana"; break;
            case "PF": $country="French Polynesia"; break;
            case "TF": $country="French Southern Territories"; break;
            case "GA": $country="Gabon"; break;
            case "GM": $country="Gambia"; break;
            case "GE": $country="Georgia"; break;
            case "DE": $country="Germany"; break;
            case "GH": $country="Ghana"; break;
            case "GI": $country="Gibraltar"; break;
            case "GR": $country="Greece"; break;
            case "GL": $country="Greenland"; break;
            case "GD": $country="Grenada"; break;
            case "GP": $country="Guadeloupe"; break;
            case "GU": $country="Guam"; break;
            case "GT": $country="Guatemala"; break;
            case "GG": $country="Guernsey"; break;
            case "GN": $country="Guinea"; break;
            case "GW": $country="Guinea-Bissau"; break;
            case "GY": $country="Guyana"; break;
            case "HT": $country="Haiti"; break;
            case "HM": $country="Heard Island and McDonald Islands"; break;
            case "VA": $country="Holy See (Vatican City State)"; break;
            case "HN": $country="Honduras"; break;
            case "HK": $country="Hong Kong"; break;
            case "HU": $country="Hungary"; break;
            case "IS": $country="Iceland"; break;
            case "IN": $country="India"; break;
            case "ID": $country="Indonesia"; break;
            case "IR": $country="Iran, Islamic Republic of"; break;
            case "IQ": $country="Iraq"; break;
            case "IE": $country="Ireland"; break;
            case "IM": $country="Isle of Man"; break;
            case "IL": $country="Israel"; break;
            case "IT": $country="Italy"; break;
            case "JM": $country="Jamaica"; break;
            case "JP": $country="Japan"; break;
            case "JE": $country="Jersey"; break;
            case "JO": $country="Jordan"; break;
            case "KZ": $country="Kazakhstan"; break;
            case "KE": $country="Kenya"; break;
            case "KI": $country="Kiribati"; break;
            case "KP": $country="Korea, Democratic People's Republic of"; break;
            case "KR": $country="Korea, Republic of"; break;
            case "KW": $country="Kuwait"; break;
            case "KG": $country="Kyrgyzstan"; break;
            case "LA": $country="Lao People's Democratic Republic"; break;
            case "LV": $country="Latvia"; break;
            case "LB": $country="Lebanon"; break;
            case "LS": $country="Lesotho"; break;
            case "LR": $country="Liberia"; break;
            case "LY": $country="Libyan Arab Jamahiriya"; break;
            case "LI": $country="Liechtenstein"; break;
            case "LT": $country="Lithuania"; break;
            case "LU": $country="Luxembourg"; break;
            case "MO": $country="Macao"; break;
            case "MK": $country="Macedonia, The Former Yugoslav Republic of"; break;
            case "MG": $country="Madagascar"; break;
            case "MW": $country="Malawi"; break;
            case "MY": $country="Malaysia"; break;
            case "MV": $country="Maldives"; break;
            case "ML": $country="Mali"; break;
            case "MT": $country="Malta"; break;
            case "MH": $country="Marshall Islands"; break;
            case "MQ": $country="Martinique"; break;
            case "MR": $country="Mauritania"; break;
            case "MU": $country="Mauritius"; break;
            case "YT": $country="Mayotte"; break;
            case "MX": $country="Mexico"; break;
            case "FM": $country="Micronesia, Federated States of"; break;
            case "MD": $country="Moldova, Republic of"; break;
            case "MC": $country="Monaco"; break;
            case "MN": $country="Mongolia"; break;
            case "MS": $country="Montserrat"; break;
            case "ME": $country="Montenegro"; break;
            case "MA": $country="Morocco"; break;
            case "MZ": $country="Mozambique"; break;
            case "MM": $country="Myanmar"; break;
            case "NA": $country="Namibia"; break;
            case "NR": $country="Nauru"; break;
            case "NP": $country="Nepal"; break;
            case "NL": $country="Netherlands"; break;
            case "AN": $country="Netherlands Antilles"; break;
            case "NC": $country="New Caledonia"; break;
            case "NZ": $country="New Zealand"; break;
            case "NI": $country="Nicaragua"; break;
            case "NE": $country="Niger"; break;
            case "NG": $country="Nigeria"; break;
            case "NU": $country="Niue"; break;
            case "NF": $country="Norfolk Island"; break;
            case "MP": $country="Northern Mariana Islands"; break;
            case "NO": $country="Norway"; break;
            case "OM": $country="Oman"; break;
            case "PK": $country="Pakistan"; break;
            case "PW": $country="Palau"; break;
            case "PS": $country="Palestinian Territories, Occupied"; break;
            case "PA": $country="Panama"; break;
            case "PG": $country="Papua New Guinea"; break;
            case "PY": $country="Paraguay"; break;
            case "PE": $country="Peru"; break;
            case "PH": $country="Philippines"; break;
            case "PN": $country="Pitcairn"; break;
            case "PL": $country="Poland"; break;
            case "PT": $country="Portugal"; break;
            case "PR": $country="Puerto Rico"; break;
            case "QA": $country="Qatar"; break;
            case "RE": $country="Reunion"; break;
            case "RO": $country="Romania"; break;
            case "RU": $country="Russian Federation"; break;
            case "RW": $country="Rwanda"; break;
            case "BL": $country="Saint Barthelemy"; break;
            case "SH": $country="Saint Helena"; break;
            case "KN": $country="Saint Kitts and Nevis"; break;
            case "LC": $country="Saint Lucia"; break;
            case "MF": $country="Saint Martin"; break;
            case "PM": $country="Saint Pierre and Miquelon"; break;
            case "VC": $country="Saint Vincent and the Grenadines"; break;
            case "WS": $country="Samoa"; break;
            case "SM": $country="San Marino"; break;
            case "ST": $country="Sao Tome and Principe"; break;
            case "SA": $country="Saudi Arabia"; break;
            case "SN": $country="Senegal"; break;
            case "RS": $country="Serbia"; break;
            case "SC": $country="Seychelles"; break;
            case "SL": $country="Sierra Leone"; break;
            case "SG": $country="Singapore"; break;
            case "SK": $country="Slovakia"; break;
            case "SI": $country="Slovenia"; break;
            case "SB": $country="Solomon Islands"; break;
            case "SO": $country="Somalia"; break;
            case "ZA": $country="South Africa"; break;
            case "GS": $country="South Georgia and the South Sandwich Islands"; break;
            case "ES": $country="Spain"; break;
            case "LK": $country="Sri Lanka"; break;
            case "SD": $country="Sudan"; break;
            case "SR": $country="Suriname"; break;
            case "SJ": $country="Svalbard and Jan Mayen"; break;
            case "SZ": $country="Swaziland"; break;
            case "SE": $country="Sweden"; break;
            case "CH": $country="Switzerland"; break;
            case "SY": $country="Syrian Arab Republic"; break;
            case "TW": $country="Taiwan, Province of China"; break;
            case "TJ": $country="Tajikistan"; break;
            case "TZ": $country="Tanzania, United Republic of"; break;
            case "TH": $country="Thailand"; break;
            case "TL": $country="Timor-Leste"; break;
            case "TG": $country="Togo"; break;
            case "TK": $country="Tokelau"; break;
            case "TO": $country="Tonga"; break;
            case "TT": $country="Trinidad and Tobago"; break;
            case "TN": $country="Tunisia"; break;
            case "TR": $country="Turkey"; break;
            case "TM": $country="Turkmenistan"; break;
            case "TC": $country="Turks and Caicos Islands"; break;
            case "TV": $country="Tuvalu"; break;
            case "UG": $country="Uganda"; break;
            case "UA": $country="Ukraine"; break;
            case "AE": $country="United Arab Emirates"; break;
            case "GB": $country="United Kingdom"; break;
            case "US": $country="United States"; break;
            case "UM": $country="United States Minor Outlying Islands"; break;
            case "UY": $country="Uruguay"; break;
            case "UZ": $country="Uzbekistan"; break;
            case "VU": $country="Vanuatu"; break;
            case "VE": $country="Venezuela"; break;
            case "VN": $country="Viet Nam"; break;
            case "VG": $country="Virgin Islands, British"; break;
            case "VI": $country="Virgin Islands, U.S."; break;
            case "WF": $country="Wallis and Futuna"; break;
            case "EH": $country="Western Sahara"; break;
            case "YE": $country="Yemen"; break;
            case "ZM": $country="Zambia"; break;
            case "ZW": $country="Zimbabwe"; break;
        }
    ?>
    <tr>
        <td><a href="<?php echo site_url().'/volunteers/volunteer/'.$sv->profileId; ?>"><?php echo $sv->name; ?></a></td>
        <td><?php echo $sv->mail; ?></td>
        <td><?php echo $sv->printDate; ?></td>
        <td><?php echo $country; ?></td>
    </tr>
    <?php endforeach; ?>
    </table>
	<br />
	<?php echo $this->pagination->create_links(); ?>
<?php endif; ?>

<?php if(isset($la_volunteers)): ?>
<?php echo $this->table->generate($la_volunteers); ?>
<br />
<?php echo $this->pagination->create_links(); ?>
<?php endif; ?>
</div>

