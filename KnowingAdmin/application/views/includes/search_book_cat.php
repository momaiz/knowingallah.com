<?php
/**
 * Copyright (c) 2013 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @copyright     copyright 2013
 * @author        programmer/ Mohammed Fawzy Zayan <admin@zayanit.com>
 * @link          http://www.zayanit.com
 * @version       1.0
 * Created by JetBrains PhpStorm 6.0
 */
?>
<br/><br/>
<script type="text/javascript" src="<?= base_url()?>assets/menu/js/tools.js"></script>
<script type="text/javascript">
    function Search_book()
    {
        var id = trim(document.getElementById('txtSearch').value);
        if(id == "أدخل رقم القسم" || id == "")
        {
            alert('أدخل رقم القسم');
            return false;
        }
        var e = document.getElementById("srcLanguages");
        var lang = e.options[e.selectedIndex].value;
        self.location = '<?=site_url("books/search_cat_result")?>/' + lang + '/' + id;
    }
</script>
<form onsubmit="Search_book(); return false;">
    <input type="text" id="txtSearch" value="أدخل رقم القسم" style="width:200px;" onfocus="if(this.value=='أدخل رقم القسم') this.value='';"
           onblur="if(this.value=='') this.value='أدخل رقم القسم';" onkeypress='numericKey(event)' />
    <select name="srcLanguages" id="srcLanguages">
        <?php
        foreach($languages as $lang): ?>
            <option value="<?=$lang->id?>"><?=$lang->description?></option>
        <?php endforeach; ?>
    </select>
    <input type="submit" value="أرسل" />
</form>