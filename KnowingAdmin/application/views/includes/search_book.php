<?php
/**
 * Copyright (c) 2013 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @copyright     copyright 2013
 * @author        programmer/ Mohammed Fawzy Zayan <admin@zayanit.com>
 * @link          http://www.zayanit.com
 * @version       1.0
 * Created by JetBrains PhpStorm 6.0
 */
?>

<br/><br/>
<script type="text/javascript" src="<?= base_url()?>assets/menu/js/tools.js"></script>
<script type="text/javascript">
    function Search_book()
    {
        var id = trim(document.getElementById('txtSearch').value);
        if(id == "أدخل رقم الكتاب" || id == "")
        {
            alert('أدخل رقم الكتاب');
            return false;
        }
        self.location = '<?=site_url("books/book")?>/' + id;
    }
</script>
<form onsubmit="Search_book(); return false;">
    <input type="text" id="txtSearch" value="أدخل رقم الكتاب" style="width:200px;" onfocus="if(this.value=='أدخل رقم الكتاب') this.value='';"
           onblur="if(this.value=='') this.value='أدخل رقم الكتاب';" onkeypress='numericKey(event)' />
    <input type="submit" value="أرسل" />
</form>