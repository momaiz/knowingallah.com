<style type="text/css">
    .export_link{
        background: #1B4552;
        -moz-border-radius:5px;
        -webkit-border-radius:5px;
        border-radius:5px;
        text-decoration: none;
        color: #A8C3BE;
        padding: 8px;
        margin-bottom: 2px;
    }
    .export_link:hover{
        background: #A8C3BE;
        color: #1B4552;
    }
</style>
<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 * Created by JetBrains PhpStorm
 */

//echo form_open('volunteers/export');
//    echo form_submit('submit', 'اســتيراد');
//echo form_close();
echo   anchor(base_url()."index.php/volunteers/download/xls/","download as xls", array('class'=>'export_link'));
echo "&nbsp;&nbsp;&nbsp;";
echo   anchor(base_url()."index.php/volunteers/download/xlsx/","download as xlsx", array('class'=>'export_link'));
echo "&nbsp;&nbsp;&nbsp;";
echo   anchor(base_url()."index.php/volunteers/download/csv/","download as csv", array('class'=>'export_link'));
echo "&nbsp;&nbsp;&nbsp;";
echo   anchor(base_url()."index.php/volunteers/download/html/","download as html", array('class'=>'export_link'));
echo "&nbsp;&nbsp;&nbsp;";
echo anchor(site_url()."/volunteers/download_txt", "download as txt", array('class'=>'export_link'));