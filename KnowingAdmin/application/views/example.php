<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style type='text/css'>
body
{
	font-family: Arial;
	font-size: 14px;
    background: #CCC;
}
#container{
    direction: rtl;
    width: 90%;
    margin: auto;
    border: 1px solid #005599;
    padding: 10px;
    background: #F9F9FF;
}
ul{
    list-style-type: none;
    padding: 0;
    margin: 0;
}
ul#nav li{
    float: right;
    background-color: #F1F1F1;
}
ul#nav li a{
    display: block;
    width: 105px;
    padding: 8px;
    color: #555;
    text-align: center;
    text-decoration: none;
    font-size: 14px;
}
ul#nav li a:hover{
    background: #555961;
    color: #fff;
    font-weight: bold;
}
ul#nav li ul li{float: none;}
ul#nav li ul{
    position: absolute;
    display: none;
}
ul#nav li:hover ul{
    display: block;
}
.clear{clear: both;}
</style>
<title>لوحة تحكم الإدارة</title>
</head>
<body>
<div id="container">
    <br />

        <ul id="nav">
		    <li>
                <a href='#'>صفحات الموقع <img src="<?php echo base_url().'/assets/icons/arrow_down.png' ?>"></a>
                <ul>
                    <li><a href="<?php echo site_url('pages'); ?>">الصفحات الرئيسية</a></li>
                    <li><a href="<?php echo site_url('pages/pages_'); ?>">الصفحات الإضافية</a></li>
                </ul>
            </li>
            <li><a href='<?php echo site_url('islamic_ads'); ?>'>الإعلانات الدعوية</a></li>
            <li>
                <a href='#'>دليل المواقع <img src="<?php echo base_url().'/assets/icons/arrow_down.png' ?>" /></a>
                <ul>
                    <li><a href="<?php echo site_url('dalil_categores'); ?>">أقسام الدليل</a></li>
                    <li><a href='<?php echo site_url('dalil'); ?>'>مواقع الدليل</a></li>
                </ul>
            </li>
            <li>
                <a href='#'>الدليل الثابت <img src="<?php echo base_url().'/assets/icons/arrow_down.png' ?>" /></a>
                <ul>
                    <li><a href="<?php echo site_url('dalil_categores/fixed_categors'); ?>">أقسام الدليل</a></li>
                    <li><a href="<?php echo site_url('dalil/sites_fixed_dalil'); ?>">مواقع الدليل</a></li>
                </ul>
            </li>
            <li>
                <a href="<?php echo site_url('customer_sites'); ?>">مواقع العملاء</a>
            </li>
            <li>
                <a href='#'>الإعلانات <img src="<?php echo base_url().'/assets/icons/arrow_down.png' ?>" /></a>
                <ul>
                    <li><a href="<?php echo base_url('ads/requests'); ?>">طلبات الإعلانات</a></li>
                    <li><a href='<?php echo site_url('ads/active'); ?>'>الإعلانات المفعلة</a></li>
                    <li><a href='<?php echo site_url('ads/un_active'); ?>'>الإعلانات المنتهية</a></li>
                </ul>
            </li>
            <li><a href='<?php echo site_url('slider'); ?>'>الاسلايدر</a></li>
            <li><a href='<?php echo site_url('site_config'); ?>'>الإعدادات</a></li>
            <li><a href='<?php echo site_url('login/logout')?>'>تسجيل الخروج</a></li>
        </ul>

    <div style='height:60px;' class="clear"></div>
    <div>
        <?php if(isset($my_view)){$this->load->view($my_view);} ?>
        <br /><br />
		<?php echo $output; ?>
        <br /><br />
    </div>
</div>
</body>
</html>
