<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title>Login</title>
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/login_style.css" type="text/css" />
    </head>
    <body>
        <div id="login">
            <h2>Login</h2>
            <?php
            echo form_open('login/log_in');
            echo "<p>" . form_label('User Name: ', 'username');
            $data = array(
                'name'  => 'username',
                'id'    => 'username',
                'value' => set_value('username')
            );
            echo form_input($data) . "</p>";
            echo "<p>" . form_label('Password: ', 'password');
            $data = array(
                'name' => 'password',
                'id'   => 'password'
            );
            echo form_password($data) . "</p>";
            echo "<p>" . form_submit('submit', '  Login!  ') . "</p>";
            echo form_close();
            ?>
        </div>
    </body>
</html>