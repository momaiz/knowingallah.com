<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<title>ادارة الموقع</title>
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
	<link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/menu/css/superfish.css" media="screen">
	<script type="text/javascript" src="<?= base_url()?>assets/menu/js/hoverIntent.js"></script>
	<script type="text/javascript" src="<?= base_url()?>assets/menu/js/superfish.js"></script>
		<script type="text/javascript">

		// initialise plugins
		jQuery(function(){
			jQuery('ul.sf-menu').superfish();
		});

		</script>	
		<style type="text/css">
		body{
			background: #CCC;
			direction: rtl;
		}
		#container{
			margin: auto;
			width: 95%;
			padding: 10px;
			border: 1px solid #059;
			background: #FFF;
			min-height:800px;
		}
		</style>
</head>
<body>
	<div id="container">
<?php
$this->load->view("includes/menu");
?>

	<div style='height:50px;'></div> 
<?php 
	if(isset($module_name))
	{
	?>	
<h1><?=$module_name ?></h1> 
<?php
	}
	if(isset($page_title))
	{
	?>	
<h3><?=$page_title ?></h3> 
<?php
	}
?>
    <div>
<?php
if(isset($my_view))
	$this->load->view($my_view);
?>
		<?php echo $output; ?>
    </div>
</div>
</body>
</html>
