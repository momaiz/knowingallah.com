<?php

/**
 * Copyright (c) 2015 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @author        Mohammed Zayan <admin@zayanit.com>
 * @link          http://www.zayanit.com
 * @version       1.0
 * Created by Sublime Text 2
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * [Function to change file name before upload it]
 * @param  [string] $fieldName [html input file name]
 * @return [string]            [the file name after change]
 */
function changeFileName($fieldName)
{
	$fileTitle = $_FILES[$fieldName]['name'];
	//Get file extension
    //$ext_arr   = explode(".", basename($fileTitle));
    //$ext       = strtolower($ext_arr[count($ext_arr) - 1]);
    $uniqer    = substr(md5(uniqid(rand(), 1)), 0, 5);
    $fileTitle = md5($uniqer . "mohammedZayan" . time() . md5("midadeMohammedZayan"));
    $fileTitle = substr($fileTitle, 4, 9) ; //. "." . $ext;
    $fileName  = $uniqer . '_' . $fileTitle;
    return $fileName;
}

/**
 * [Function to upload files to media server]
 * @param  [string] $upload_directory [full path to upload directory with file name]
 * @param  [string] $source_directory [full path to the file you want to upload]
 * @return 
 */
function uploadToMedia($upload_directory, $upload_file, $source_directory)
{
	$ftp_server     = "media.knowingallah.com";
	$ftp_username   = "mediaka@media.knowingallah.com";
	$ftp_password   = "1234RooT@#$"; 
	$ftp_connection = ftp_connect($ftp_server) or die('Can\'t connect with server');
	ftp_login($ftp_connection, $ftp_username, $ftp_password) or die('Can\'t login to server');
	ftp_chdir($ftp_connection, $upload_directory) or die('Can\'t open upload directory');
	ftp_put($ftp_connection, $upload_file, $source_directory, FTP_BINARY) or die('Can\'t upload to server');
	ftp_close($ftp_connection);
}

/**
 * [fileNameAsItemName is a function which make the filename equal to the title field]
 * @param  [string] $fieldName [upload field name]
 * @param  [string] $itemName  [title field value]
 * @return [string]            [file name after changes]
 */
function fileNameAsItemName($fieldName, $itemName)
{
	$fileTitle = $_FILES[$fieldName]['name'];
	//Get file extension
    $ext_arr   = explode(".", basename($fileTitle));
    $ext       = strtolower($ext_arr[count($ext_arr) - 1]);
    $fileTitle = str_replace(' ', '-', $itemName);
    $fileTitle = $fileTitle.".".$ext;
    return $fileTitle;
}

/**
 * [compressImage function to compress uploaded images]
 * @param  [file]   $source      [temp file name]
 * @param  [string] $destination [the name of the image which you want]
 * @param  [int]    $quality     [the quality you want]
 * @return [string]              [the compressed image name]
 */
function compressImage($source, $destination, $quality)
{ 
	$info = getimagesize($source);

	if ($info['mime'] == 'image/jpeg')
		$image = imagecreatefromjpeg($source);
	elseif ($info['mime'] == 'image/gif') 
		$image = imagecreatefromgif($source); 
	elseif ($info['mime'] == 'image/png') 
		$image = imagecreatefrompng($source); 

	imagejpeg($image, $destination, $quality);
	return $destination;
}