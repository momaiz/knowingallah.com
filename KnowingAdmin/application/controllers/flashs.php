<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Flashs extends MY_Controller {

    public $data = array();
    
	function __construct()
	{
		parent::__construct();
	}
	
	function _admin_output($output = null)
	{
		$output->languages = $this->admin_model->get_languages_list();
		$output->module_name = "الفلاشات";
		$this->load->view('admin.php',$output);	
	}

	function index()
	{
		$this->_admin_output(array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}	

	function flashs_category($lang = null, $cat = null)
	{
		try{
			if($lang == null || $lang == 'add')
				$lang = 1;
			if($cat == null || $cat == 'add')
				$cat = 0;
			
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('flash_cat_translation');
			$crud->set_subject('قسم');
			$crud->required_fields('description');
			$crud->add_fields('flash_cat_id','language_id','parent_id','description');
			$crud->edit_fields('language_id','description');
			$crud->set_relation('flash_cat_id','flash_cat', 'id');
			$crud->set_relation('language_id','language','description');
			$crud->where('language_id', $lang);
			$crud->where('parent_id', $cat);
			$crud->columns('flash_cat_id','description');
			$crud->display_as('description','وصف القسم')->display_as('language_id','اللغة')->display_as('parent_id','رقم القسم الأب')->display_as('flash_cat_id','رقم القسم');

			$crud->change_field_type('flash_cat_id', 'hidden', '0');
			$crud->change_field_type('language_id', 'hidden', $lang);
			$this->session->set_userdata('art_cat_parent_Id', $cat);
			$crud->callback_add_field('parent_id',array($this,'add_field_callback_1'));
			$crud->callback_before_insert(array($this,'callback_before_insert_flash_cat'));

			$crud->add_action('الأقسام تحت هذا القسم', '', '','ui-icon-plus',array($this,'sub_cats'));
			$crud->add_action('ترجمات', '', '','ui-icon-plus',array($this,'cat_trans'));
			$crud->add_action('الفلاشات تحت هذا القسم', '', '','ui-icon-plus',array($this,'sub_flashs'));
			$crud->unset_export();
			$crud->unset_print();
			$output = $crud->render();

			$output->page_title = $this->admin_model->get_flash_cat_page_title($lang,$cat);			
			$this->_admin_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	function add_field_callback_1()
	{
		return '<input type="hidden" value="'. $this->session->userdata('art_cat_parent_Id') .'" name="parent_id" >' . $this->session->userdata('art_cat_parent_Id');
	}
	function callback_before_insert_flash_cat($post_array, $primary_key = null)
	{
		$insert = $this->db->insert('flash_cat', array('parent_id' => $post_array['parent_id']));
		$post_array['flash_cat_id'] = $this->db->insert_id(); 	
		unset($post_array['parent_id']);
		return $post_array;
	}	

	function sub_cats($primary_key , $row)
	{
	    return site_url('flashs/flashs_category').'/'.$row->language_id.'/'.$row->flash_cat_id;
	}
	function cat_trans($primary_key , $row)
	{
	    return site_url('flashs/flash_category').'/'.$row->flash_cat_id;
	}
	function sub_flashs($primary_key , $row)
	{
	    return site_url('flashs/flashs_').'/'.$row->language_id.'/'.$row->flash_cat_id;
	}
	function flash_category($cat)
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('flash_cat_translation');
			$crud->set_subject('ترجمة');
			$crud->required_fields('description','language_id');
			$crud->add_fields('flash_cat_id','description','language_id');
			$crud->edit_fields('description','language_id');
			$crud->set_relation('language_id','language','description');
			$crud->where('flash_cat_id', $cat);
			$crud->columns('flash_cat_id','description','language_id');
			$crud->display_as('description','وصف القسم')->display_as('flash_cat_id','رقم القسم')->display_as('language_id','اللغة');
			
			$crud->change_field_type('flash_cat_id', 'hidden', $cat);

			$crud->unset_export();
			$crud->unset_print();
			$output = $crud->render();
			$output->page_title = "ترجمات القسم رقم: " . $cat;			
			$this->_admin_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function flashs_($lang = null, $cat = null)
	{
		try{
			if($lang == null || $lang == 'add')
				$lang = 1;
			if($cat == null || $cat == 'add')
				$cat = 0;
			
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('flash_translation');
			$crud->set_subject('فلاش');
			$crud->set_relation('flash_id','flash', 'id');
			$crud->set_relation('language_id','language','description');
			$crud->where('language_id', $lang);
			$crud->where('flash_cat_id', $cat);
			$crud->add_fields('language_id','flash_id','flash_cat_id','creation_date','title','content','link','author','visible');
			$crud->edit_fields('title','content','link','author','visible');
			$crud->required_fields('title','visible','link');
			$crud->columns('flash_id','title','author','visible','hits','creation_date');
			$crud->display_as('flash_cat_id','رقم القسم')->display_as('title','عنوان الفلاش')->display_as('language_id','اللغة')->display_as('flash_id','رقم الفلاش')->display_as('content','وصف الفلاش')
			->display_as('link','رابط الفلاش')
			->display_as('author','المحاضر')->display_as('visible','يظهر في الموقع')->display_as('hits','عدد مرات المشاهدة')->display_as('creation_date','تاريخ الاضافة');

			$crud->change_field_type('flash_id', 'hidden', '0');
			$crud->change_field_type('creation_date', 'hidden', '0');
			$crud->change_field_type('language_id', 'hidden', $lang);
			$this->session->set_userdata('art_cat_Id', $cat);
			$crud->callback_add_field('flash_cat_id',array($this,'add_field_callback_2'));
			$crud->callback_before_insert(array($this,'callback_before_insert_flash'));
			
			$crud->add_action('ترجمات', '', '','ui-icon-plus',array($this,'flash_trans'));
			$crud->unset_export();
			$crud->unset_print();
			$output = $crud->render();

			$output->page_title = $this->admin_model->get_flash_page_title($lang,$cat);			
			$this->_admin_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	function add_field_callback_2()
	{
		return '<input type="hidden" value="'. $this->session->userdata('art_cat_Id') .'" name="flash_cat_id" >' . $this->session->userdata('art_cat_Id');
	}	
	function callback_before_insert_flash($post_array, $primary_key = null)
	{
		$currentDate = date("Y-m-d H:i:s");
		$insert = $this->db->insert('flash', array('flash_cat_id' => $post_array['flash_cat_id'], 'creation_date' => $currentDate));
		$post_array['flash_id'] = $this->db->insert_id(); 	
		$post_array['creation_date'] = $currentDate; 	
		unset($post_array['flash_cat_id']);
		return $post_array;
	}		
	function flash_trans($primary_key , $row)
	{
	    return site_url('flashs/flash').'/'.$row->flash_id;
	}	
	function flash($art)
	{
		try{
			
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('flash_translation');
			$crud->set_subject('ترجمة');
			$crud->required_fields('title','visible','link');
			$crud->add_fields('language_id','flash_id','creation_date','title','content','link','author','visible');			
			$crud->edit_fields('title','content','link','author','visible');
			$crud->set_relation('language_id','language','description');
			$crud->where('flash_id', $art);
			$crud->columns('flash_id','language_id','title','author','visible','hits','creation_date');
			$crud->display_as('title','عنوان الفلاش')->display_as('language_id','اللغة')->display_as('flash_id','رقم الفلاش')->display_as('content','وصف الفلاش')
			->display_as('link','رابط الفلاش')
			->display_as('author','المحاضر')->display_as('visible','يظهر في الموقع')->display_as('hits','عدد مرات المشاهدة')->display_as('creation_date','تاريخ الاضافة');
			$crud->callback_before_insert(array($this,'callback_before_insert_flash_trans'));
			$crud->change_field_type('flash_id', 'hidden', $art);
			$crud->change_field_type('creation_date', 'hidden', '0');

			$crud->unset_export();
			$crud->unset_print();
			$output = $crud->render();

			$output->page_title = "ترجمات الفلاش رقم: " . $art;			
			$this->_admin_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}	
	function callback_before_insert_flash_trans($post_array, $primary_key = null)
	{
		$currentDate = date("Y-m-d H:i:s");
		$post_array['creation_date'] = $currentDate; 	
		unset($post_array['flash_cat_id']);
		return $post_array;
	}
	
	function search_cat()
	{
		$output->page_title = "البحث عن قسم للفلاشهات";			
		$output->my_view = "includes/search_flash_cat";			
		$output->css_files = array();			
		$output->js_files = array();			
		$output->output = "";			
		$this->_admin_output($output);
	}

	function search_flash()
	{
		$output->page_title = "البحث عن فلاش";			
		$output->my_view = "includes/search_flash";			
		$output->css_files = array();			
		$output->js_files = array();			
		$output->output = "";			
		$this->_admin_output($output);
	}
	
	//26-07-2012
	function search_cat_result($lang = null, $cat = null)
	{
		try{
			if($lang == null || $lang == 'add')
				$lang = 1;
			if($cat == null || $cat == 'add')
				$cat = 0;
			
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('flash_cat_translation');
			$crud->set_subject('قسم');
			$crud->required_fields('description');
			$crud->add_fields('flash_cat_id','language_id','parent_id','description');
			$crud->edit_fields('language_id','description');
			$crud->set_relation('flash_cat_id','flash_cat', 'id');
			$crud->set_relation('language_id','language','description');
			$crud->where('language_id', $lang);
			$crud->where('flash_cat_id', $cat);
			$crud->columns('flash_cat_id','description');
			$crud->display_as('description','وصف القسم')->display_as('language_id','اللغة')->display_as('parent_id','رقم القسم الأب')->display_as('flash_cat_id','رقم القسم');

			$crud->change_field_type('flash_cat_id', 'hidden', '0');
			$crud->change_field_type('language_id', 'hidden', $lang);
			$this->session->set_userdata('art_cat_parent_Id', $cat);
			$crud->callback_add_field('parent_id',array($this,'add_field_callback_1'));
			$crud->callback_before_insert(array($this,'callback_before_insert_flash_cat'));

			$crud->add_action('الأقسام تحت هذا القسم', '', '','ui-icon-plus',array($this,'sub_cats'));
			$crud->add_action('ترجمات', '', '','ui-icon-plus',array($this,'cat_trans'));
			$crud->add_action('المقالات تحت هذا القسم', '', '','ui-icon-plus',array($this,'sub_flashs'));
			$crud->unset_export();
			$crud->unset_print();
			$output = $crud->render();

			$output->page_title = "نتيجة البحث";			
			$this->_admin_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	
}