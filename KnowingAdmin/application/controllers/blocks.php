<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

class Blocks extends MY_Controller{
    
    function __construct() {
        parent::__construct();
    }
    
    function _admin_output($output = null){
        $output->languages = $this->admin_model->get_languages_list();
        $this->load->view('admin.php',$output);	
    }
    
    function index(){
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_table('site_bloks');
        $crud->set_subject('بلوك جديد');
        $crud->required_fields('cat_id');
        $crud->columns('id' ,'cat_id', 'module', 'show', 'language');
        $crud->display_as('module', 'الموديول')->display_as('show', 'الإظهار')->display_as('language', 'اللغة')->display_as('cat_id', 'رقم القسم')->display_as('id', 'مسلسل');
        $crud->set_relation('language','language','description');
        $crud->callback_field('module',array($this,'field_callback_1'));
        $crud->callback_field('show',array($this,'field_callback_2'));
        $crud->unset_export();
            $crud->unset_print();  
        $output = $crud->render();
        $output->page_title = "البلوكات";
        $this->_admin_output($output);
    }
    
    function field_callback_1(){
        return "
            <select name='module'>
                <option value='article'>مقالات</option>
                <option value='video'>فيديو</option>
                <option value='audio'>أوديو</option>
                <option value='flash'>فلاش</option>
            <select>
        ";
    }
    
    function field_callback_2(){
        return "
            <select name='show'>
                <option value='categors'>أقسام</option>
                <option value='articles'>مقالات</option>
            <select>
        ";
    }
    
}