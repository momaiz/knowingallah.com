<?php
/**
 * Copyright (c) 2013 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @copyright     copyright 2013
 * @author        programmer/ Mohammed Fawzy Zayan <admin@zayanit.com>
 * @link          http://www.zayanit.com
 * @version       1.0
 * Created by JetBrains PhpStorm
 */

class Books extends MY_Controller{

    public $data = array();

    function __construct(){
        parent::__construct();
    }

    function _admin_output($output = null){
        $output->languages = $this->admin_model->get_languages_list();
        $output->module_name = "الكتب";
        $this->load->view('admin.php',$output);
    }

    function index(){
        $this->_admin_output(array('output' => '' , 'js_files' => array() , 'css_files' => array()));
    }

    function books_for_dowinload()
    {
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_table('books_for_dowinload');
        $crud->set_subject('كتيب');
        $crud->columns('id', 'language_id', 'name', 'photo', 'visible');
        $crud->add_fields('language_id', 'name', 'slug', 'author', 'photo', 'download_link', 'visible', 'creation_date');
        $crud->edit_fields('language_id', 'name', 'slug', 'author', 'photo', 'download_link', 'visible');
        $crud->display_as('id', '#')->display_as('language_id', 'اللغة')->display_as('name', 'اسم الكتيب')->display_as('slug', 'السلج')->display_as('photo', 'صورة الكتيب')->display_as('visible', 'الظهور فى الموقع')->display_as('creation_date', 'تاريخ الإضافة')->display_as('download_link', 'رابط التحميل')->display_as('author', 'الكاتب')->display_as('hits', 'المشاهدات')->display_as('downloads', 'عدد مرات التحميل');
        $crud->required_fields('name', 'language_id', 'photo', 'download_link');
        $crud->set_relation('language_id','language','description');
        $crud->callback_column('name', array($this, '_callback_download_link'));
        $crud->callback_column('photo', array($this, '_callback_show_photo'));
        $crud->change_field_type('creation_date', 'hidden', date("Y-m-d"));
        $crud->callback_before_insert(array($this,'_callback_before_insert_book'));
        $crud->unset_export();
        $crud->unset_print();
        $output = $crud->render();
        $this->_admin_output($output);
    }

    function _callback_download_link($value, $row)
    {
        return "<a href=\"$row->download_link\" target='_blank'>$value</a>";
    }

    function _callback_show_photo($value)
    {
        return "<img src='".$value."' width='153' height='90' />"; 
    }

    function _callback_before_insert_book($post_array, $primary_key = null)
    {
        $this->load->helper('my_text_helper');
        if ($post_array['slug'] == '')
            $post_array['slug'] = url_slug($post_array['title']);
        else
            $post_array['slug'] = url_slug($post_array['slug']);
        return $post_array;
    }

//    function books_category($lang = null, $cat = null){
//        error_reporting(E_ALL ^ E_WARNING);
//        try{
//            if($lang == null || $lang == 'add')
//                $lang = 1;
//            if($cat == null || $cat == 'add')
//                $cat = 0;
//
//            $crud = new grocery_CRUD();
//            $crud->set_theme('datatables');
//            $crud->set_table('book_cat_translation');
//            $crud->set_subject('قسم');
//            $crud->required_fields('description');
//            $crud->add_fields('book_cat_id','language_id','parent_id','description');
//            $crud->edit_fields('language_id','description');
//            $crud->set_relation('book_cat_id','book_cat', 'id');
//            $crud->set_relation('language_id','language','description');
//            $crud->where('language_id', $lang);
//            $crud->where('parent_id', $cat);
//            $crud->columns('book_cat_id','description');
//            $crud->display_as('description','وصف القسم')->display_as('language_id','اللغة')->display_as('parent_id','رقم القسم الأب')->display_as('book_cat_id','رقم القسم');
//
//            $crud->change_field_type('book_cat_id', 'hidden', '0');
//            $crud->change_field_type('language_id', 'hidden', $lang);
//            $this->session->set_userdata('bok_cat_parent_Id', $cat);
//            $crud->callback_field('parent_id',array($this,'add_field_callback_1'));
//            $crud->callback_before_insert(array($this,'callback_before_insert_book_cat'));
//
//            $crud->add_action('الأقسام تحت هذا القسم', '', '','ui-icon-plus',array($this,'sub_cats'));
//            $crud->add_action('ترجمات', '', '','ui-icon-plus',array($this,'cat_trans'));
//            $crud->add_action('الكتب تحت هذا القسم', '', '','ui-icon-plus',array($this,'sub_books'));
//            $crud->unset_export();
//            $crud->unset_print();
//            $output = $crud->render();
//
//            $output->page_title = $this->admin_model->get_book_cat_page_title($lang,$cat);
//            $this->_admin_output($output);
//
//        }catch(Exception $e){
//            show_error($e->getMessage().' --- '.$e->getTraceAsString());
//        }
//    }

    function add_field_callback_1(){
        return '<input type="text" value="'. $this->session->userdata('bok_cat_parent_Id') .'" name="parent_id" >';
    }

//    function callback_before_insert_book_cat($post_array, $primary_key = null){
//        $insert = $this->db->insert('book_cat', array('parent_id' => $post_array['parent_id']));
//        $post_array['book_cat_id'] = $this->db->insert_id();
//        unset($post_array['parent_id']);
//        return $post_array;
//    }

//    function sub_cats($primary_key , $row){
//        return site_url('books/books_category').'/'.$row->language_id.'/'.$row->book_cat_id;
//    }

    function cat_trans($primary_key , $row){
        return site_url('books/book_category').'/'.$row->book_cat_id;
    }

    function sub_books($primary_key , $row){
        return site_url('books/books_').'/'.$row->language_id.'/'.$row->book_cat_id;
    }

//    function book_category($cat){
//        try{
//            $crud = new grocery_CRUD();
//
//            $crud->set_theme('datatables');
//            $crud->set_table('book_cat_translation');
//            $crud->set_subject('ترجمة');
//            $crud->required_fields('description','language_id');
//            $crud->add_fields('book_cat_id','description','language_id');
//            $crud->edit_fields('description','language_id');
//            $crud->set_relation('language_id','language','description');
//            $crud->where('book_cat_id', $cat);
//            $crud->columns('book_cat_id','description','language_id');
//            $crud->display_as('description','وصف القسم')->display_as('book_cat_id','رقم القسم')->display_as('language_id','اللغة');
//
//            $crud->change_field_type('book_cat_id', 'hidden', $cat);
//
//            $crud->unset_export();
//            $crud->unset_print();
//            $output = $crud->render();
//            $output->page_title = "ترجمات القسم رقم: " . $cat;
//            $this->_admin_output($output);
//
//        }catch(Exception $e){
//            show_error($e->getMessage().' --- '.$e->getTraceAsString());
//        }
//    }

    function books_($lang = null, $cat = null){
        try{
            if($lang == null || $lang == 'add')
                $lang = 1;
            if($cat == null || $cat == 'add')
                $cat = 0;

            $crud = new grocery_CRUD();
            $crud->set_theme('datatables');
            $crud->set_table('book_translation');
            $crud->set_subject('كتاب');
            $crud->set_relation('book_id','book', 'id');
            $crud->set_relation('language_id','language','description');
            $crud->where('language_id', $lang);
            $crud->where('book_cat_id', $cat);
            $crud->add_fields('language_id','book_id','book_cat_id','creation_date','title','book_photo','author','visible','base_url', 'download_url');
            $crud->edit_fields('title','book_photo','author','visible','base_url');
            $crud->required_fields('title','download_url','visible');
            $crud->columns('book_id','title','author','visible','hits','creation_date','base_url');
            $crud->display_as('book_cat_id','رقم القسم')->display_as('title','عنوان الكتاب')->display_as('language_id','اللغة')->display_as('book_id','رقم الكتاب')->display_as('book_photo','صورة الكتاب')
                ->display_as('author','الكاتب')->display_as('visible','يظهر في الموقع')->display_as('hits','عدد مرات المشاهدة')->display_as('creation_date','تاريخ الاضافة')->display_as('base_url','الرابط الأصلى')->display_as('download_url', 'رابط تحميل الكتاب');
            $crud->set_field_upload('book_photo','../assets/uploads/books');
            $crud->callback_column('book_photo', array($this, '_callback_view_photo'));
            $crud->change_field_type('book_id', 'hidden', '0');
            $crud->change_field_type('creation_date', 'hidden', '0');
            $crud->change_field_type('language_id', 'hidden', $lang);
            $this->session->set_userdata('bok_cat_Id', $cat);
            $crud->callback_field('book_cat_id',array($this,'add_field_callback_2'));
            $crud->callback_before_insert(array($this,'callback_before_insert_book'));

            $crud->add_action('ترجمات', '', '','ui-icon-plus',array($this,'book_trans'));
            $crud->unset_export();
            $crud->unset_print();
            $output = $crud->render();

            $output->page_title = $this->admin_model->get_book_page_title($lang,$cat);
            $output->editor = $this->data;
            $this->_admin_output($output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    function _callback_view_photo($value, $row){
        return "<a href='".$row->download_url."' target='_blank'><img src='".base_url()."../assets/uploads/books/".$value."' width='250' height='150' /></a>";
    }

    function add_field_callback_2(){
        return '<input type="text" value="'. $this->session->userdata('bok_cat_Id') .'" name="book_cat_id" >';
    }

    function callback_before_insert_book($post_array, $primary_key = null){
        $currentDate = date("Y-m-d H:i:s");
        $insert = $this->db->insert('book', array('book_cat_id' => $post_array['book_cat_id'], 'creation_date' => $currentDate));
        $post_array['book_id'] = $this->db->insert_id();
        $post_array['creation_date'] = $currentDate;
        unset($post_array['book_cat_id']);
        return $post_array;
    }

    function book_trans($primary_key , $row){
        return site_url('books/book').'/'.$row->book_id;
    }

    function book($art){
        try{
            $crud = new grocery_CRUD();
            $crud->set_theme('datatables');
            $crud->set_table('book_translation');
            $crud->set_subject('ترجمة');
            $crud->required_fields('title','content','visible');
            $crud->add_fields('language_id','book_id','creation_date','title','content','author','visible');
            $crud->edit_fields('title','content','author','visible');
            $crud->set_relation('language_id','language','description');
            $crud->where('book_id', $art);
            $crud->columns('book_id','language_id','title','author','visible','hits','creation_date');
            $crud->display_as('title','عنوان الكتاب')->display_as('language_id','اللغة')->display_as('book_id','رقم الكتاب')->display_as('content','محتوى الكتاب')
                ->display_as('author','الكاتب')->display_as('visible','يظهر في الموقع')->display_as('hits','عدد مرات المشاهدة')->display_as('creation_date','تاريخ الاضافة');
            $crud->callback_before_insert(array($this,'callback_before_insert_book_trans'));
            $crud->change_field_type('book_id', 'hidden', $art);
            $crud->change_field_type('creation_date', 'hidden', '0');

            $crud->unset_export();
            $crud->unset_print();
            $output = $crud->render();

            $output->page_title = "ترجمات الكتاب رقم: " . $art;
            $this->_admin_output($output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    function callback_before_insert_book_trans($post_array, $primary_key = null){
        $currentDate = date("Y-m-d H:i:s");
        $post_array['creation_date'] = $currentDate;
        unset($post_array['book_cat_id']);
        return $post_array;
    }

    function search_cat(){
        $output->page_title = "البحث عن قسم للكتب";
        $output->my_view = "includes/search_book_cat";
        $output->css_files = array();
        $output->js_files = array();
        $output->output = "";
        $this->_admin_output($output);
    }

    function search_cat_result($lang = null, $cat = null){
        try{
            if($lang == null || $lang == 'add')
                $lang = 1;
            if($cat == null || $cat == 'add')
                $cat = 0;

            $crud = new grocery_CRUD();
            $crud->set_theme('datatables');
            $crud->set_table('book_cat_translation');
            $crud->set_subject('قسم');
            $crud->required_fields('description');
            $crud->add_fields('book_cat_id','language_id','parent_id','description');
            $crud->edit_fields('language_id','description');
            $crud->set_relation('book_cat_id','book_cat', 'id');
            $crud->set_relation('language_id','language','description');
            $crud->where('language_id', $lang);
            $crud->where('book_cat_id', $cat);
            $crud->columns('book_cat_id','description');
            $crud->display_as('description','وصف القسم')->display_as('language_id','اللغة')->display_as('parent_id','رقم القسم الأب')->display_as('book_cat_id','رقم القسم');

            $crud->change_field_type('book_cat_id', 'hidden', '0');
            $crud->change_field_type('language_id', 'hidden', $lang);
            $this->session->set_userdata('bok_cat_parent_Id', $cat);
            $crud->callback_add_field('parent_id',array($this,'add_field_callback_1'));
            $crud->callback_before_insert(array($this,'callback_before_insert_book_cat'));

            $crud->add_action('الأقسام تحت هذا القسم', '', '','ui-icon-plus',array($this,'sub_cats'));
            $crud->add_action('ترجمات', '', '','ui-icon-plus',array($this,'cat_trans'));
            $crud->add_action('الكتب تحت هذا القسم', '', '','ui-icon-plus',array($this,'sub_books'));
            $crud->unset_export();
            $crud->unset_print();
            $output = $crud->render();

            $output->page_title = "نتيجة البحث";
            $this->_admin_output($output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    function search_book(){
        $output->page_title = "البحث عن كتاب";
        $output->my_view = "includes/search_book";
        $output->css_files = array();
        $output->js_files = array();
        $output->output = "";
        $this->_admin_output($output);
    }

}