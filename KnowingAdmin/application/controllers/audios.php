<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Audios extends MY_Controller {

    public $data = array();

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$this->_admin_output(array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	function audios_category($lang = null, $cat = null)
	{
		try{
			if($lang == null || $lang == 'add')
				$lang = 1;
			if($cat == null || $cat == 'add')
				$cat = 0;

			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('audio_cat_translation');
			$crud->set_subject('قسم');
			$crud->required_fields('description', 'cat_description');
			$crud->add_fields('audio_cat_id','language_id','parent_id','description', 'cat_description');
			$crud->edit_fields('language_id','description', 'cat_description');
			$crud->set_relation('audio_cat_id','audio_cat', 'id');
			$crud->set_relation('language_id','language','description');
			$crud->where('language_id', $lang);
			$crud->where('parent_id', $cat);
			$crud->columns('audio_cat_id','description');
			$crud->display_as('description','اسم القسم')->display_as('language_id','اللغة')->display_as('parent_id','رقم القسم الأب')->display_as('audio_cat_id','رقم القسم')->display_as('cat_description', 'وصف القسم');
			$crud->callback_field('cat_description', array($this, '_callback_description'));
			$crud->change_field_type('audio_cat_id', 'hidden', '0');
			$crud->change_field_type('language_id', 'hidden', $lang);
			$this->session->set_userdata('art_cat_parent_Id', $cat);
			$crud->callback_add_field('parent_id',array($this,'add_field_callback_1'));
			$crud->callback_before_insert(array($this,'callback_before_insert_audio_cat'));
			$crud->add_action('الأقسام تحت هذا القسم', '', '','ui-icon-plus',array($this,'sub_cats'));
			$crud->add_action('ترجمات', '', '','ui-icon-plus',array($this,'cat_trans'));
			$crud->add_action('الصوتيات تحت هذا القسم', '', '','ui-icon-plus',array($this,'sub_audios'));
			$crud->unset_export();
			$crud->unset_print();
			$output = $crud->render();
			$output->page_title = $this->admin_model->get_audio_cat_page_title($lang,$cat);
			$this->_admin_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function audio_category($cat)
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('audio_cat_translation');
			$crud->set_subject('ترجمة');
			$crud->required_fields('description','language_id', 'cat_description');
			$crud->add_fields('audio_cat_id','description','language_id', 'cat_description');
			$crud->edit_fields('description','language_id', 'cat_description');
			$crud->set_relation('language_id','language','description');
			$crud->where('audio_cat_id', $cat);
			$crud->columns('audio_cat_id','description','language_id');
			$crud->display_as('description','اسم القسم')->display_as('audio_cat_id','رقم القسم')->display_as('language_id','اللغة')->display_as('cat_description', 'وصف القسم');
			$crud->callback_field('cat_description', array($this, '_callback_description'));
			$crud->change_field_type('audio_cat_id', 'hidden', $cat);
			$crud->unset_export();
			$crud->unset_print();
			$output = $crud->render();
			$output->page_title = "ترجمات القسم رقم: " . $cat;
			$this->_admin_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function audios_($lang = null, $cat = null)
	{
		try{
			if($lang == null || $lang == 'add')
				$lang = 1;
			if($cat == null || $cat == 'add')
				$cat = 0;

			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('audio_translation');
			$crud->set_subject('تسجيل صوتي');
			$crud->set_relation('audio_id','audio', 'id');
			$crud->set_relation('language_id','language','description');
			$crud->where('language_id', $lang);
			$crud->where('audio_cat_id', $cat);
			$crud->add_fields('language_id','audio_id','audio_cat_id','creation_date','title','slug','description','link','audio_code','author','visible','base_url');
			$crud->edit_fields('title','slug','description','link','audio_code','author','visible','base_url');
			$crud->required_fields('title','visible');
			$crud->columns('audio_id','title','author','visible','hits','creation_date','base_url');
			$crud->display_as('audio_cat_id','رقم القسم')->display_as('title','عنوان التسجيل صوتي')->display_as('language_id','اللغة')->display_as('audio_id','رقم التسجيل صوتي')->display_as('description','وصف التسجيل صوتي')
			->display_as('link','رابط التسجيل صوتي')->display_as('base_url','الرابط الأصلى')
			->display_as('author','المحاضر')->display_as('visible','يظهر في الموقع')->display_as('hits','عدد مرات المشاهدة')->display_as('creation_date','تاريخ الاضافة')->display_as('audio_code', 'كود الصوت')->display_as('slug', 'رابط الظهور فى الموقع');
			$crud->callback_field('description', array($this, '_callback_description'));
			$crud->change_field_type('audio_id', 'hidden', '0');
			$crud->change_field_type('creation_date', 'hidden', '0');
			$crud->change_field_type('language_id', 'hidden', $lang);
			$this->session->set_userdata('art_cat_Id', $cat);
			$crud->callback_add_field('audio_cat_id',array($this,'add_field_callback_2'));
			$crud->callback_before_insert(array($this,'callback_before_insert_audio'));
			$crud->add_action('ترجمات', '', '','ui-icon-plus',array($this,'audio_'));
			$crud->callback_field('link', array($this, '_callback_link_field'));
			$crud->callback_column('base_url', array($this, '_callback_base_url_view'));
			$crud->callback_before_update(array($this, 'callback_before_update_audio'));
			$crud->unset_export();
			$crud->unset_print();
			$output = $crud->render();

			$output->page_title = $this->admin_model->get_audio_page_title($lang,$cat);
			$this->_admin_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function audio_trans($art)
	{
		try{

			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('audio_translation');
			$crud->set_subject('ترجمة');
			$crud->required_fields('title','visible','link');
			$crud->add_fields('language_id','audio_id','creation_date','title','slug','description','link','author','visible');
			$crud->edit_fields('title','slug','description','link','author','visible');
			$crud->set_relation('language_id','language','description');
			$crud->where('audio_id', $art);
			$crud->columns('audio_id','language_id','title','author','visible','hits','creation_date');
			$crud->display_as('title','عنوان التسجيل صوتي')->display_as('language_id','اللغة')->display_as('audio_id','رقم التسجيل صوتي')->display_as('description','وصف التسجيل صوتي')
			->display_as('link','رابط التسجيل صوتي')->display_as('slug', 'رابط الظهور فى الموقع')
			->display_as('author','المحاضر')->display_as('visible','يظهر في الموقع')->display_as('hits','عدد مرات المشاهدة')->display_as('creation_date','تاريخ الاضافة');
			$crud->callback_before_insert(array($this,'callback_before_insert_audio_trans'));
			$crud->change_field_type('audio_id', 'hidden', $art);
			$crud->change_field_type('creation_date', 'hidden', '0');
			$crud->callback_field('description', array($this, '_callback_description'));
			$crud->callback_field('link', array($this, '_callback_link_field'));
			$crud->callback_before_update(array($this, 'callback_before_update_audio'));
			$crud->unset_export();
			$crud->unset_print();
			$output = $crud->render();

			$output->page_title = "ترجمات التسجيل صوتي رقم: " . $art;
			$this->_admin_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function search_cat()
	{
		$output->page_title = "البحث عن قسم للتسجيل صوتيهات";
		$output->my_view = "includes/search_audio_cat";
		$output->css_files = array();
		$output->js_files = array();
		$output->output = "";
		$this->_admin_output($output);
	}

	function search_cat_result($lang = null, $cat = null)
	{
		try{
			if($lang == null || $lang == 'add')
				$lang = 1;
			if($cat == null || $cat == 'add')
				$cat = 0;

			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('audio_cat_translation');
			$crud->set_subject('قسم');
			$crud->required_fields('description');
			$crud->add_fields('audio_cat_id','language_id','parent_id','description');
			$crud->edit_fields('language_id','description');
			$crud->set_relation('audio_cat_id','audio_cat', 'id');
			$crud->set_relation('language_id','language','description');
			$crud->where('language_id', $lang);
			$crud->where('audio_cat_id', $cat);
			$crud->columns('audio_cat_id','description');
			$crud->display_as('description','وصف القسم')->display_as('language_id','اللغة')->display_as('parent_id','رقم القسم الأب')->display_as('audio_cat_id','رقم القسم');

			$crud->change_field_type('audio_cat_id', 'hidden', '0');
			$crud->change_field_type('language_id', 'hidden', $lang);
			$this->session->set_userdata('art_cat_parent_Id', $cat);
			$crud->callback_add_field('parent_id',array($this,'add_field_callback_1'));
			$crud->callback_before_insert(array($this,'callback_before_insert_audio_cat'));

			$crud->add_action('الأقسام تحت هذا القسم', '', '','ui-icon-plus',array($this,'sub_cats'));
			$crud->add_action('ترجمات', '', '','ui-icon-plus',array($this,'cat_trans'));
			$crud->add_action('المقالات تحت هذا القسم', '', '','ui-icon-plus',array($this,'sub_audios'));
			$crud->unset_export();
			$crud->unset_print();
			$output = $crud->render();

			$output->page_title = "نتيجة البحث";
			$this->_admin_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function search_audio()
	{
		$output->page_title = "البحث عن تسجيل صوتي";
		$output->my_view = "includes/search_audio";
		$output->css_files = array();
		$output->js_files = array();
		$output->output = "";
		$this->_admin_output($output);
	}

	function search_audio_result($art)
	{
		try{

			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('audio_translation');
			$crud->required_fields('title','visible','link');
			$crud->edit_fields('title','slug','description','link','author','visible');
			$crud->set_relation('language_id','language','description');
			$art   = urldecode($art);
			$where = "title like '%$art%' or slug like '%$art%'";
			$crud->where($where);
			$crud->columns('audio_id','language_id','title','author','visible','hits','creation_date','description');
			$crud->display_as('title','عنوان التسجيل صوتي')->display_as('language_id','اللغة')->display_as('audio_id','رقم التسجيل صوتي')->display_as('description','وصف التسجيل صوتي')
				->display_as('link','رابط التسجيل صوتي')->display_as('slug', 'رابط الظهور فى الموقع')
				->display_as('author','المحاضر')->display_as('visible','يظهر في الموقع')->display_as('hits','عدد مرات المشاهدة')->display_as('creation_date','تاريخ الاضافة');
			$crud->change_field_type('audio_id', 'hidden', $art);
			$crud->change_field_type('creation_date', 'hidden', '0');
			$crud->callback_field('description', array($this, '_callback_description'));
			$crud->callback_field('link', array($this, '_callback_link_field'));
			$crud->callback_before_update(array($this, 'callback_before_update_audio'));
			$crud->unset_add();
			$crud->unset_print();
			$crud->unset_export();
			$output = $crud->render();
			$output->page_title = "نتيجة البحث عن التسجيل الصوتى : " . $art;




			$this->_admin_output($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function _admin_output($output = null)
	{
		$output->languages = $this->admin_model->get_languages_list();
		$output->module_name = "الصوتيات";
		$this->load->view('admin.php' , $output);
	}

	function sub_cats($primary_key , $row)
	{
	    return site_url('audios/audios_category').'/'.$row->language_id.'/'.$row->audio_cat_id;
	}

	function cat_trans($primary_key , $row)
	{
	    return site_url('audios/audio_category').'/'.$row->audio_cat_id;
	}

	function sub_audios($primary_key , $row)
	{
	    return site_url('audios/audios_').'/'.$row->language_id.'/'.$row->audio_cat_id;
	}

	function _callback_description($value)
	{
		return "
			<textarea maxlength='160' id='textarea' name='description'>$value</textarea>
			<lable id='textarea_feedback'></lable>
			<script type='text/javascript'>
			$(document).ready(function() {
			    var text_max = 160;
			    $('#textarea_feedback').html('الأحرف المتبقية ' + text_max);

			    $('#textarea').keyup(function() {
			        var text_length = $('#textarea').val().length;
			        var text_remaining = text_max - text_length;

			        $('#textarea_feedback').html('الأحرف المتبقية ' + text_remaining);
			    });
			});
			</script>
		";
	}

	function add_field_callback_1()
	{
		return '<input type="hidden" value="'. $this->session->userdata('art_cat_parent_Id') .'" name="parent_id" >' . $this->session->userdata('art_cat_parent_Id');
	}

	function callback_before_insert_audio_cat($post_array, $primary_key = null)
	{
		$insert = $this->db->insert('audio_cat', array('parent_id' => $post_array['parent_id']));
		$post_array['audio_cat_id'] = $this->db->insert_id();
		unset($post_array['parent_id']);
		return $post_array;
	}

	function add_field_callback_2()
	{
		return '<input type="hidden" value="'. $this->session->userdata('art_cat_Id') .'" name="audio_cat_id" >' . $this->session->userdata('art_cat_Id');
	}

	function callback_before_insert_audio($post_array, $primary_key = null)
	{
		if($post_array['link'] == '' && $_FILES['link']['name'] != '')
			$post_array['link'] = $this->uploadAudio($post_array['title']);
		$currentDate = date("Y-m-d H:i:s");
		$insert = $this->db->insert('audio', array('audio_cat_id' => $post_array['audio_cat_id'], 'creation_date' => $currentDate));
		$post_array['audio_id']      = $this->db->insert_id();
		$post_array['creation_date'] = $currentDate;
		$this->load->helper('my_text_helper');
		if($post_array['slug'] == '')
			$post_array['slug'] = url_slug($post_array['title']);
		else
			$post_array['slug'] = url_slug($post_array['slug']);
		unset($post_array['audio_cat_id']);
		return $post_array;
	}

	function audio_($primary_key , $row)
	{
	    return site_url('audios/audio_trans').'/'.$row->audio_id;
	}

	function callback_before_insert_audio_trans($post_array, $primary_key = null)
	{
		if($post_array['link'] == '' && $_FILES['link']['name'] != '')
			$post_array['link'] = $this->uploadAudio($post_array['title']);
		$currentDate = date("Y-m-d H:i:s");
		$post_array['creation_date'] = $currentDate;
		$this->load->helper('my_text_helper');
		if($post_array['slug'] == '')
			$post_array['slug'] = url_slug($post_array['title']);
		else
			$post_array['slug'] = url_slug($post_array['slug']);
		unset($post_array['audio_cat_id']);
		return $post_array;
	}

	function callback_before_update_audio($post_array, $primary_key = null)
	{
		if($_FILES['link']['name'] != '')
			$post_array['link'] = $this->uploadAudio($post_array['title']);
		$this->load->helper('my_text_helper');
		if($post_array['slug'] == '')
			$post_array['slug'] = url_slug($post_array['title']);
		else
			$post_array['slug'] = url_slug($post_array['slug']);
		return $post_array;
	}

	function _callback_link_field()
	{
		return '<input type="text" name="link" style="width: 300px;" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			تحميل الملف الصوتى: &nbsp; <input type="file" name="link" accept="audio/*">';
	}

	function _callback_base_url_view($value)
	{
		if($value != '')
			return "<a href=\"$value\" target='_blank'>الرابط اﻷصلى</a>";
		else
			return '';
	}

	function uploadAudio($itemName)
	{
		$this->load->helper('my_uploader_helper');
		$audioName  = fileNameAsItemName('link', $itemName);
		$uploadfile = $_FILES['link']['tmp_name'];
		$remoteDir  = '/public_html/media/audios/';
		uploadToMedia($remoteDir, $audioName, $uploadfile);
		return 'http://media.knowingallah.com/media/audios/'.$audioName;
	}

}
