<?php

class Pages extends MY_Controller{
	
	function __construct(){
		parent::__construct();
	}
	
	function _admin_output($output = null){
		$output->languages = $this->admin_model->get_languages_list();
		$output->module_name = "الصفحات الثابتة";
		$this->load->view('admin.php',$output);	
	}
	
	function index(){
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('page');
		$crud->set_subject('صفحة');
        $crud->columns('id', 'title', 'content');
        $crud->display_as('id', '#')->display_as('title', 'عنوان الصفحة')->display_as('content', 'محتوى الصفحة');
        $crud->unset_export();
        $crud->unset_print();
		$output = $crud->render();
		$this->_admin_output($output);
	}
	
}
