<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

class Login extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->database();
        $this->load->library('session');
    }


    function index(){
        $this->load->view('login_form');
    }
    
    function log_in(){
        $this->load->model('login_model');
        $query = $this->login_model->validate();
        if($query){
            $data = array(
                'username'     => $this->input->post('username'),
                'is_logged_in' => true
            );
            $this->session->set_userdata($data);
            redirect('main');
        }else{
            $this->index();
        }
    }
    
    function logout(){
        $this->session->sess_destroy();
        $this->index();
    }
    
}