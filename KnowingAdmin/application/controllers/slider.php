<?php
/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 * Created by JetBrains PhpStorm
 */

class Slider extends MY_Controller{

    function __construct(){
        parent::__construct();
    }

    function _admin_output($output = null){
        $output->languages = $this->admin_model->get_languages_list();
        $output->module_name = "الاسلايدر";
        $this->load->view('admin.php',$output);
    }

    function index(){
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_table('slider');
        $crud->set_subject('سلايدر');
        $crud->columns('id', 'language_id', 'content');
        $crud->display_as('id', '#')->display_as('language_id', 'اللغة')->display_as('photo', 'الصورة')->display_as('photo_link', 'رابط الصورة')->display_as('content', 'المحتوى');
        $crud->add_fields('language_id', 'content');
        $crud->edit_fields('language_id', 'content');
        $crud->callback_add_field('content',array($this,'content_add_field_callback'));
        $crud->callback_edit_field('content',array($this,'content_edit_field_callback'));
        /*$crud->set_field_upload('photo','../assets/uploads/slider');
        $crud->callback_column('photo', array($this, '_callback_view_photo'));*/
        $crud->set_relation('language_id', 'language', 'description');
        $crud->required_fields('language_id', 'content');
        $crud->unset_export();
        $crud->unset_print();
        $output = $crud->render();
        $this->_admin_output($output);
    }

    function content_add_field_callback(){
        return '<textarea name="content" rows="15"></textarea>';
    }

    function content_edit_field_callback($value){
        return "<textarea name='content' rows='15'>$value</textarea>";
    }

    function _callback_view_photo($value, $row){
        return "<a href='".$row->photo_link."' target='_blank'><img src='".base_url()."../assets/uploads/slider/".$value."' width='553' height='123' /></a>";
    }



}