<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 * Created by JetBrains PhpStorm
 */


class Menu extends MY_Controller{

    function __construct(){
        parent::__construct();
    }

    function _admin_output($output = null){
        $output->languages = $this->admin_model->get_languages_list();
        $output->module_name = "المقالات";
        $this->load->view('admin.php',$output);
    }

    function index(){
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_table('site_menu');
        $crud->set_subject('رابط للقائمة');
        $crud->columns('id', 'language_id', 'title');
        $crud->display_as('id', '#')->display_as('language_id', 'اللغة')->display_as('title', 'العنوان')->display_as('link', 'الرابط')->display_as('parent_id', 'رقم الرابط الأب');
        $crud->set_relation('language_id', 'language', 'description');
        $crud->change_field_type('parent_id', 'hidden', '0');
        $crud->callback_column('title', array($this, 'add_link_to_title_callback'));
        //$crud->add_action('إضافة روابط تحت هذا الربط', '', '', 'ui-icon-plus', array($this, 'add_link_to_menu'));
        $crud->add_action('عرض الروابط تحت هذا الرابط' ,'assets/clipboard_list.png', 'menu/sub_links');
        $crud->where('parent_id', 0);
        $crud->callback_add_field('link', array($this, 'add_link_callback'));
        $crud->required_fields('language_id', 'title', 'link');
        $crud->unset_export();
        $crud->unset_print();
        $output = $crud->render();
        $this->_admin_output($output);
    }

    function add_link_to_title_callback($value, $row){
        return "<a href='".$row->link."' target='_blank' style='text-decoration: none;'>$value</a>";
    }

    function sub_links($parent_id = null){
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_table('site_menu');
        $crud->set_subject('رابط فرعى');
        $crud->where('parent_id', $parent_id);
        $crud->columns('id', 'language_id', 'title');
        $crud->display_as('id', '#')->display_as('language_id', 'اللغة')->display_as('title', 'العنوان')->display_as('link', 'الرابط')->display_as('parent_id', 'رقم الرابط الأب');
        $crud->change_field_type('parent_id', 'hidden', $parent_id);
        $crud->set_relation('language_id', 'language', 'description');
        $crud->callback_column('title', array($this, 'add_link_to_title_callback'));
        $crud->required_fields('language_id', 'title', 'link');
        $crud->unset_export();
        $crud->unset_print();
        $output = $crud->render();
        $this->_admin_output($output);
    }

    function add_link_callback(){
        return "<input type='text' name='link' value='#' />";
    }

}