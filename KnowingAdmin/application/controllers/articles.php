﻿<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Articles extends MY_Controller {

    public $data = array();

	function __construct(){
        parent::__construct();
    }

	function index(){
		$this->_admin_output(array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	function articles_category($lang = null, $cat = null){
		error_reporting(E_ALL ^ E_WARNING);
		try{
			if($lang == null || $lang == 'add')
				$lang = 1;
			if($cat == null || $cat == 'add')
				$cat = 0;

			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('article_cat_translation');
			$crud->set_subject('قسم');
			$crud->required_fields('description', 'cat_description');
			$crud->add_fields('article_cat_id','language_id','parent_id','description', 'cat_description');
			$crud->edit_fields('language_id','description', 'cat_description');
			$crud->set_relation('article_cat_id','article_cat', 'id');
			$crud->set_relation('language_id','language','description');
			$crud->where('language_id', $lang);
			$crud->where('parent_id', $cat);
			$crud->columns('article_cat_id','description');
			$crud->display_as('description','اسم القسم')->display_as('language_id','اللغة')->display_as('parent_id','رقم القسم الأب')->display_as('article_cat_id','رقم القسم')->display_as('cat_description', 'وصف القسم');

			$crud->change_field_type('article_cat_id', 'hidden', '0');
			$crud->change_field_type('language_id', 'hidden', $lang);
			$this->session->set_userdata('art_cat_parent_Id', $cat);
			$crud->callback_field('parent_id',array($this,'add_field_callback_1'));
			$crud->callback_field('cat_description', array($this, '_callback_description'));
			$crud->callback_before_insert(array($this,'callback_before_insert_article_cat'));

			$crud->add_action('الأقسام تحت هذا القسم', '', '','ui-icon-plus',array($this,'sub_cats'));
			$crud->add_action('ترجمات', '', '','ui-icon-plus',array($this,'cat_trans'));
			$crud->add_action('المقالات تحت هذا القسم', '', '','ui-icon-plus',array($this,'sub_articles'));
			$crud->unset_export();
			$crud->unset_print();
			$output = $crud->render();

			$output->page_title = $this->admin_model->get_article_cat_page_title($lang,$cat);
			$this->_admin_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function article_category($cat){
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('article_cat_translation');
			$crud->set_subject('ترجمة');
			$crud->required_fields('language_id', 'description', 'cat_description');
			$crud->add_fields('article_cat_id', 'language_id', 'description', 'cat_description');
			$crud->edit_fields('language_id', 'description', 'cat_description');
			$crud->set_relation('language_id','language','description');
			$crud->where('article_cat_id', $cat);
			$crud->columns('article_cat_id','description','language_id');
			$crud->display_as('description','اسم القسم')->display_as('article_cat_id','رقم القسم')->display_as('language_id','اللغة')->display_as('cat_description', 'وصف القسم');
			$crud->callback_field('cat_description', array($this, '_callback_description'));
			$crud->change_field_type('article_cat_id', 'hidden', $cat);
			$crud->unset_export();
			$crud->unset_print();
			$output = $crud->render();
			$output->page_title = "ترجمات القسم رقم: " . $cat;
			$this->_admin_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function articles_($lang = null, $cat = null){
		try{
			if($lang == null || $lang == 'add')
				$lang = 1;
			if($cat == null || $cat == 'add')
				$cat = 0;

			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('article_translation');
			$crud->set_subject('مقال');
			$crud->set_relation('article_id','article', 'id');
			$crud->set_relation('language_id','language','description');
			$crud->where('language_id', $lang);
			$crud->where('article_cat_id', $cat);
			$crud->add_fields('language_id','article_id','article_cat_id','creation_date','title', 'slug','content', 'description','author','visible','special', 'article_photo','base_url');
			$crud->edit_fields('title', 'slug', 'content', 'description','author','visible','special', 'article_photo','base_url');
			$crud->required_fields('title','content','visible');
			$crud->columns('article_id','title','author','visible','special','hits','creation_date','article_photo');
			$crud->display_as('article_cat_id','رقم القسم')->display_as('title','عنوان المقال')->display_as('language_id','اللغة')->display_as('article_id','رقم المقال')->display_as('content','محتوى المقال')
			->display_as('author','الكاتب')->display_as('visible','يظهر في الموقع')->display_as('special', 'مميز')->display_as('hits','المشاهدات')->display_as('creation_date','تاريخ الاضافة')->display_as('base_url','الرابط الأصلى')->display_as('description', 'وصف المقال')->display_as('article_photo', 'صورة المقال')->display_as('slug', 'رابط المقال');
			$crud->change_field_type('article_id', 'hidden', '0');
			$crud->change_field_type('creation_date', 'hidden', '0');
			$crud->change_field_type('language_id', 'hidden', $lang);
			$this->session->set_userdata('art_cat_Id', $cat);
			$crud->callback_field('article_cat_id',array($this,'add_field_callback_2'));
			$crud->callback_before_insert(array($this,'callback_before_insert_article'));
			$crud->callback_before_update(array($this, 'callback_before_update_article'));
			$crud->callback_field('description', array($this, '_callback_article_description'));
			$crud->callback_field('special', array($this, '_callback_special'));
			$crud->callback_field('article_photo', array($this, '_callback_article_photo'));
			$crud->callback_column('article_photo', array($this, '_callback_article_photo_view'));
			$crud->add_action('ترجمات', '', '','ui-icon-plus',array($this,'article_'));
			$crud->unset_export();
			$crud->unset_print();
			$output = $crud->render();

			$output->page_title = $this->admin_model->get_article_page_title($lang,$cat);
            $output->editor = $this->data;
			$this->_admin_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function article_trans($art){
		try{

			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('article_translation');
			$crud->set_subject('ترجمة');
			$crud->required_fields('title','content','visible');
			$crud->add_fields('language_id','article_id','creation_date','title', 'slug', 'content', 'description','author','visible','special', 'article_photo');
			$crud->edit_fields('title', 'slug', 'content', 'description','author','visible','special', 'article_photo');
			$crud->set_relation('language_id','language','description');
			$crud->where('article_id', $art);
			$crud->columns('article_id','language_id','title','author','visible','special','hits','creation_date', 'article_photo');
			$crud->display_as('title','عنوان المقال')->display_as('language_id','اللغة')->display_as('article_id','رقم المقال')->display_as('content','محتوى المقال')->display_as('special', 'مميز')
			->display_as('author','الكاتب')->display_as('visible','يظهر في الموقع')->display_as('hits','عدد مرات المشاهدة')->display_as('creation_date','تاريخ الاضافة')->display_as('description', 'وصف المقال')->display_as('article_photo', 'صورة المقال')->display_as('slug', 'رابط المقال');
			$crud->callback_before_insert(array($this,'callback_before_insert_article_trans'));
			$crud->callback_before_update(array($this, 'callback_before_update_article'));
			$crud->change_field_type('article_id', 'hidden', $art);
			$crud->change_field_type('creation_date', 'hidden', '0');
			$crud->callback_field('description', array($this, '_callback_article_description'));
			$crud->callback_field('special', array($this, '_callback_special'));
			$crud->callback_field('article_photo', array($this, '_callback_article_photo'));
			$crud->callback_column('article_photo', array($this, '_callback_article_photo_view'));
			$crud->unset_export();
			$crud->unset_print();
			$output = $crud->render();

			$output->page_title = "ترجمات المقال رقم: " . $art;
			$this->_admin_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function search_cat(){
		$output->page_title = "البحث عن قسم للمقالات";
		$output->my_view = "includes/search_article_cat";
		$output->css_files = array();
		$output->js_files = array();
		$output->output = "";
		$this->_admin_output($output);
	}

	function search_cat_result($lang = null, $cat = null){
		try{
			if($lang == null || $lang == 'add')
				$lang = 1;
			if($cat == null || $cat == 'add')
				$cat = 0;

			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('article_cat_translation');
			$crud->set_subject('قسم');
			$crud->required_fields('description');
			$crud->add_fields('article_cat_id','language_id','parent_id','description');
			$crud->edit_fields('language_id','description');
			$crud->set_relation('article_cat_id','article_cat', 'id');
			$crud->set_relation('language_id','language','description');
			$crud->where('language_id', $lang);
			$crud->where('article_cat_id', $cat);
			$crud->columns('article_cat_id','description');
			$crud->display_as('description','وصف القسم')->display_as('language_id','اللغة')->display_as('parent_id','رقم القسم الأب')->display_as('article_cat_id','رقم القسم');

			$crud->change_field_type('article_cat_id', 'hidden', '0');
			$crud->change_field_type('language_id', 'hidden', $lang);
			$this->session->set_userdata('art_cat_parent_Id', $cat);
			$crud->callback_add_field('parent_id',array($this,'add_field_callback_1'));
			$crud->callback_before_insert(array($this,'callback_before_insert_article_cat'));

			$crud->add_action('الأقسام تحت هذا القسم', '', '','ui-icon-plus',array($this,'sub_cats'));
			$crud->add_action('ترجمات', '', '','ui-icon-plus',array($this,'cat_trans'));
			$crud->add_action('المقالات تحت هذا القسم', '', '','ui-icon-plus',array($this,'sub_articles'));
			$crud->unset_export();
			$crud->unset_print();
			$output = $crud->render();

			$output->page_title = "نتيجة البحث";
			$this->_admin_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function search_article(){
		$output->page_title = "البحث عن مقال";
		$output->my_view = "includes/search_article";
		$output->css_files = array();
		$output->js_files = array();
		$output->output = "";
		$this->_admin_output($output);
	}

	function search_article_result($art){
		try{

			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('article_translation');
			$crud->required_fields('title','content','visible');
			$crud->edit_fields('title', 'slug', 'content', 'description','author','visible','special', 'article_photo');
			$crud->set_relation('language_id','language','description');
			$art   = urldecode($art);
			$where = "title like '%$art%' or slug like '%$art%'";
			$crud->where($where);
			$crud->columns('article_id','language_id','title','author','visible','special','hits','creation_date', 'article_photo');
			$crud->display_as('title','عنوان المقال')->display_as('language_id','اللغة')->display_as('article_id','رقم المقال')->display_as('content','محتوى المقال')->display_as('special', 'مميز')
				->display_as('author','الكاتب')->display_as('visible','يظهر في الموقع')->display_as('hits','عدد مرات المشاهدة')->display_as('creation_date','تاريخ الاضافة')->display_as('description', 'وصف المقال')->display_as('article_photo', 'صورة المقال')->display_as('slug', 'رابط المقال');
			$crud->callback_before_update(array($this, 'callback_before_update_article'));
			$crud->change_field_type('article_id', 'hidden', $art);
			$crud->change_field_type('creation_date', 'hidden', '0');
			$crud->callback_field('description', array($this, '_callback_article_description'));
			$crud->callback_field('special', array($this, '_callback_special'));
			$crud->callback_field('article_photo', array($this, '_callback_article_photo'));
			$crud->callback_column('article_photo', array($this, '_callback_article_photo_view'));
			$crud->unset_add();
			$crud->unset_print();
			$crud->unset_export();
			$output = $crud->render();
			$output->page_title = "نتيجة البحث عن المقال : " . $art;
			$this->_admin_output($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function _admin_output($output = null){
		$output->languages = $this->admin_model->get_languages_list();
		$output->module_name = "المقالات";
		$this->load->view('admin.php',$output);
	}

	function add_field_callback_1(){
		return '<input type="text" value="'. $this->session->userdata('art_cat_parent_Id') .'" name="parent_id" >';
	}

	function _callback_description($value)
	{
		return "
			<textarea maxlength='160' id='textarea' name='cat_description'>$value</textarea>
			<lable id='textarea_feedback'></lable>
			<script type='text/javascript'>
			$(document).ready(function() {
			    var text_max = 160;
			    $('#textarea_feedback').html('الأحرف المتبقية ' + text_max);

			    $('#textarea').keyup(function() {
			        var text_length = $('#textarea').val().length;
			        var text_remaining = text_max - text_length;

			        $('#textarea_feedback').html('الأحرف المتبقية ' + text_remaining);
			    });
			});
			</script>
		";
	}

	function _callback_article_description($value)
	{
		return "
			<textarea maxlength='160' id='textarea' name='description'>$value</textarea>
			<lable id='textarea_feedback'></lable>
			<script type='text/javascript'>
			$(document).ready(function() {
			    var text_max = 160;
			    $('#textarea_feedback').html('الأحرف المتبقية ' + text_max);

			    $('#textarea').keyup(function() {
			        var text_length = $('#textarea').val().length;
			        var text_remaining = text_max - text_length;

			        $('#textarea_feedback').html('الأحرف المتبقية ' + text_remaining);
			    });
			});
			</script>
		";
	}

	function callback_before_insert_article_cat($post_array, $primary_key = null){
		$insert = $this->db->insert('article_cat', array('parent_id' => $post_array['parent_id']));
		$post_array['article_cat_id'] = $this->db->insert_id();
		unset($post_array['parent_id']);
		return $post_array;
	}

	function sub_cats($primary_key , $row){
	    return site_url('articles/articles_category').'/'.$row->language_id.'/'.$row->article_cat_id;
	}

	function cat_trans($primary_key , $row){
	    return site_url('articles/article_category').'/'.$row->article_cat_id;
	}

	function sub_articles($primary_key , $row){
	    return site_url('articles/articles_').'/'.$row->language_id.'/'.$row->article_cat_id.'/';
	}

	function _callback_article_photo()
	{
		return '<input type="file" name="article_photo" accept="image/*">';
	}

	function _callback_special($value)
	{
		if($value == 'yes')
		{
			return "
				<lable><input type='radio' name='special' value='yes' checked>نعم</lable>
				<lable style='padding-right:24px'><input type='radio' name='special' value='no'>لا</lable>
			";
		}else{
			return "
				<lable><input type='radio' name='special' value='yes'>نعم</lable>
				<lable style='padding-right:24px'><input type='radio' name='special' value='no' checked>لا</lable>
			";
		}
	}

	function add_field_callback_2(){
		return '<input type="text" value="'. $this->session->userdata('art_cat_Id') .'" name="article_cat_id1" disabled ><input type="hidden" value="'. $this->session->userdata('art_cat_Id') .'" name="article_cat_id">';
	}

	function callback_before_insert_article($post_array, $primary_key = null)
	{
		if($_FILES['article_photo']['name'] != ''){
			$post_array['article_photo'] = $this->uploadArticlePhoto();
		}
		$currentDate = date("Y-m-d H:i:s");
		$insert = $this->db->insert('article', array('article_cat_id' => $post_array['article_cat_id'], 'creation_date' => $currentDate));
		$post_array['article_id']    = $this->db->insert_id();
		$post_array['creation_date'] = $currentDate;
		$this->load->helper('my_text_helper');
		if($post_array['slug'] == '')
			$post_array['slug'] = url_slug($post_array['title']);
		else
			$post_array['slug'] = url_slug($post_array['slug']);
		unset($post_array['article_cat_id']);
		return $post_array;
	}

	function callback_before_update_article($post_array, $primary_key = null)
	{
		if($_FILES['article_photo']['name'] != '')
			$post_array['article_photo'] = $this->uploadArticlePhoto();
		$this->load->helper('my_text_helper');
		if($post_array['slug'] == '')
			$post_array['slug'] = url_slug($post_array['title']);
		else
			$post_array['slug'] = url_slug($post_array['slug']);
		return $post_array;
	}

	function article_($primary_key , $row){
	    return site_url('articles/article_trans').'/'.$row->article_id.'/';
	}

	function callback_before_insert_article_trans($post_array, $primary_key = null)
	{
		if($_FILES['article_photo']['name'] != ''){
			$post_array['article_photo'] = $this->uploadArticlePhoto();
		}
		$currentDate = date("Y-m-d H:i:s");
		$post_array['creation_date'] = $currentDate;
		$post_array['slug']          = strtolower(str_replace(' ', '-', $post_array['title']));
		unset($post_array['article_cat_id']);
		return $post_array;
	}

	function _callback_article_photo_view($value, $row)
	{
		if($value != ''){
			$images        = explode(',', $value);
			$article_photo = $images[3];
		}else{
			$article_photo = base_url('../assets/images/article_photo_last.jpg');
		}
		return "<img src=\"$article_photo\" width=110 height=80 alt=\"$row->title\" />";
	}

	function uploadArticlePhoto()
	{
		$this->load->helper('my_uploader_helper');
		$photoName  = changeFileName('article_photo');
		$image      = $_FILES['article_photo']['tmp_name'];
		$remoteDir  = '/public_html/media/articles/';

		$this->load->library('image_lib');
		$config['image_library']  = 'gd2';
		$config['source_image']   = $image;
		$config['maintain_ratio'] = FALSE;
		$config['width']          = 750;
    	$config['height']         = 350;
    	$this->image_lib->initialize($config);
		$this->image_lib->resize();
		$photoToUpload = compressImage($image, 'temp_upload/'.$photoName.'.jpg', 75);
		uploadToMedia($remoteDir, $photoName.'.jpg', $photoToUpload);

		$config['width']          = 233;
    	$config['height']         = 180;
    	$this->image_lib->initialize($config);
		$this->image_lib->resize();
		$photoToUpload = compressImage($image, 'temp_upload/'.$photoName.'_relat.jpg', 75);
		uploadToMedia($remoteDir, $photoName.'_relat.jpg', $photoToUpload);

		$config['width']          = 262;
    	$config['height']         = 150;
    	$this->image_lib->initialize($config);
		$this->image_lib->resize();
		$photoToUpload = compressImage($image, 'temp_upload/'.$photoName.'_sam.jpg', 75);
		uploadToMedia($remoteDir, $photoName.'_sam.jpg', $photoToUpload);

		$config['width']          = 120;
    	$config['height']         = 80;
    	$this->image_lib->initialize($config);
		$this->image_lib->resize();
		$photoToUpload = compressImage($image, 'temp_upload/'.$photoName.'_last.jpg', 75);
		uploadToMedia($remoteDir, $photoName.'_last.jpg', $photoToUpload);

		unlink(realpath(FCPATH)."/temp_upload/$photoName.jpg");
		unlink(realpath(FCPATH)."/temp_upload/$photoName"."_relat.jpg");
		unlink(realpath(FCPATH)."/temp_upload/$photoName"."_sam.jpg");
		unlink(realpath(FCPATH)."/temp_upload/$photoName"."_last.jpg");

		$photoURL = 'http://media.knowingallah.com/media/articles/';
		return $photoURL.$photoName.'.jpg'.','.$photoURL.$photoName.'_relat.jpg'.','.$photoURL.$photoName.'_sam.jpg'.','.$photoURL.$photoName.'_last.jpg';
	}

}
