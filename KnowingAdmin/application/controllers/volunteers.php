﻿<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

class Volunteers extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->model('volunteer_model');
        $this->load->helper('form');
        $this->load->helper('download');
        $this->load->helper('file');
    }
    
    function index(){}
    
    function _admin_output($output = null){
        $output->languages = $this->admin_model->get_languages_list();
        $output->module_name = "المتطوعين";
        $this->load->view('admin.php',$output);	
    }
    
    function volunteers_(){
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_table('volunteers');
        $crud->unset_add();
        $crud->columns('profileId', 'name', 'country', 'printDate', 'mail', 'favor');
        $crud->callback_column('name',array($this,'_callback_webpage_url'));
        $crud->callback_column('country',array($this,'_callback_languages'));
        $crud->display_as('profileId', 'تسلسل')->display_as('favor', 'مميز')->display_as('name',
            'الاسم')->display_as('country',
            'البلد')->display_as('printDate', 'تاريخ التسجيل')->display_as('mail', 'البريد الإلكترونى');
        $crud->unset_export();
        $crud->unset_print();
        $output = $crud->render();
        $output->page_title = "قائمة المتطوعين";
        $output->my_view = 'includes/export_form';
        $this->_admin_output($output);
    }
    function favor_volunteers(){
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_table('volunteers');
        $crud->unset_add();
        $crud->where('favor', 'yes');
        $crud->columns('profileId', 'name', 'country', 'printDate', 'mail', 'favor');
        $crud->callback_column('name',array($this,'_callback_webpage_url'));
        $crud->callback_column('country',array($this,'_callback_languages'));
        $crud->display_as('profileId', 'تسلسل')->display_as('favor', 'مميز')->display_as('name',
            'الاسم')->display_as('country',
            'البلد')->display_as('printDate', 'تاريخ التسجيل')->display_as('mail', 'البريد الإلكترونى');
        $crud->unset_export();
        $crud->unset_print();
        $output = $crud->render();
        $output->page_title = "قائمة المتطوعين";
        $this->_admin_output($output);
    }
    
    function _callback_webpage_url($value, $row){
        return "<a href='".site_url('volunteers/volunteer/'.$row->profileId)."'>$value</a>";
    }
    
    function _callback_languages($value){
        $country="";
	switch ($value) {
	case "AF": $country="Afghanistan"; break;
	case "AX": $country="Aland Islands"; break;
	case "AL": $country="Albania"; break;
	case "DZ": $country="Algeria"; break;
	case "AS": $country="American Samoa"; break;
	case "AD": $country="Andorra"; break;
	case "AO": $country="Angola"; break;
	case "AI": $country="Anguilla"; break;
	case "AQ": $country="Antarctica"; break;
	case "AG": $country="Antigua and Barbuda"; break;
	case "AR": $country="Argentina"; break;
	case "AM": $country="Armenia"; break;
	case "AW": $country="Aruba"; break;
	case "AU": $country="Australia"; break;
	case "AT": $country="Austria"; break;
	case "AZ": $country="Azerbaijan"; break;
	case "BS": $country="Bahamas"; break;
	case "BH": $country="Bahrain"; break;
	case "BD": $country="Bangladesh"; break;
	case "BB": $country="Barbados"; break;
	case "BY": $country="Belarus"; break;
	case "BE": $country="Belgium"; break;
	case "BZ": $country="Belize"; break;
	case "BJ": $country="Benin"; break;
	case "BM": $country="Bermuda"; break;
	case "BT": $country="Bhutan"; break;
	case "BO": $country="Bolivia"; break;
	case "BA": $country="Bosnia and Herzegovina"; break;
	case "BW": $country="Botswana"; break;
	case "BV": $country="Bouvet Island"; break;
	case "BR": $country="Brazil"; break;
	case "IO": $country="British Indian Ocean Territory"; break;
	case "BN": $country="Brunei Darussalam"; break;
	case "BG": $country="Bulgaria"; break;
	case "BF": $country="Burkina Faso"; break;
	case "BI": $country="Burundi"; break;
	case "KH": $country="Cambodia"; break;
	case "CM": $country="Cameroon"; break;
	case "CA": $country="Canada"; break;
	case "CV": $country="Cape Verde"; break;
	case "KY": $country="Cayman Islands"; break;
	case "CF": $country="Central African Republic"; break;
	case "TD": $country="Chad"; break;
	case "CL": $country="Chile"; break;
	case "CN": $country="China"; break;
	case "CX": $country="Christmas Island"; break;
	case "CC": $country="Cocos (Keeling) Islands"; break;
	case "CO": $country="Colombia"; break;
	case "KM": $country="Comoros"; break;
	case "CG": $country="Congo"; break;
	case "CD": $country="Congo, The Democratic Republic of the"; break;
	case "CK": $country="Cook Islands"; break;
	case "CR": $country="Costa Rica"; break;
	case "CI": $country="Cote d'Ivoire"; break;
	case "HR": $country="Croatia"; break;
	case "CU": $country="Cuba"; break;
	case "CY": $country="Cyprus"; break;
	case "CZ": $country="Czech Republic"; break;
	case "DK": $country="Denmark"; break;
	case "DJ": $country="Djibouti"; break;
	case "DM": $country="Dominica"; break;
	case "DO": $country="Dominican Republic"; break;
	case "EC": $country="Ecuador"; break;
	case "EG": $country="Egypt"; break;
	case "SV": $country="El Salvador"; break;
	case "GQ": $country="Equatorial Guinea"; break;
	case "ER": $country="Eritrea"; break;
	case "EE": $country="Estonia"; break;
	case "ET": $country="Ethiopia"; break;
	case "FK": $country="Falkland Islands (Malvinas)"; break;
	case "FO": $country="Faroe Islands"; break;
	case "FJ": $country="Fiji"; break;
	case "FI": $country="Finland"; break;
	case "FR": $country="France"; break;
	case "GF": $country="French Guiana"; break;
	case "PF": $country="French Polynesia"; break;
	case "TF": $country="French Southern Territories"; break;
	case "GA": $country="Gabon"; break;
	case "GM": $country="Gambia"; break;
	case "GE": $country="Georgia"; break;
	case "DE": $country="Germany"; break;
	case "GH": $country="Ghana"; break;
	case "GI": $country="Gibraltar"; break;
	case "GR": $country="Greece"; break;
	case "GL": $country="Greenland"; break;
	case "GD": $country="Grenada"; break;
	case "GP": $country="Guadeloupe"; break;
	case "GU": $country="Guam"; break;
	case "GT": $country="Guatemala"; break;
	case "GG": $country="Guernsey"; break;
	case "GN": $country="Guinea"; break;
	case "GW": $country="Guinea-Bissau"; break;
	case "GY": $country="Guyana"; break;
	case "HT": $country="Haiti"; break;
	case "HM": $country="Heard Island and McDonald Islands"; break;
	case "VA": $country="Holy See (Vatican City State)"; break;
	case "HN": $country="Honduras"; break;
	case "HK": $country="Hong Kong"; break;
	case "HU": $country="Hungary"; break;
	case "IS": $country="Iceland"; break;
	case "IN": $country="India"; break;
	case "ID": $country="Indonesia"; break;
	case "IR": $country="Iran, Islamic Republic of"; break;
	case "IQ": $country="Iraq"; break;
	case "IE": $country="Ireland"; break;
	case "IM": $country="Isle of Man"; break;
	case "IL": $country="Israel"; break;
	case "IT": $country="Italy"; break;
	case "JM": $country="Jamaica"; break;
	case "JP": $country="Japan"; break;
	case "JE": $country="Jersey"; break;
	case "JO": $country="Jordan"; break;
	case "KZ": $country="Kazakhstan"; break;
	case "KE": $country="Kenya"; break;
	case "KI": $country="Kiribati"; break;
	case "KP": $country="Korea, Democratic People's Republic of"; break;
	case "KR": $country="Korea, Republic of"; break;
	case "KW": $country="Kuwait"; break;
	case "KG": $country="Kyrgyzstan"; break;
	case "LA": $country="Lao People's Democratic Republic"; break;
	case "LV": $country="Latvia"; break;
	case "LB": $country="Lebanon"; break;
	case "LS": $country="Lesotho"; break;
	case "LR": $country="Liberia"; break;
	case "LY": $country="Libyan Arab Jamahiriya"; break;
	case "LI": $country="Liechtenstein"; break;
	case "LT": $country="Lithuania"; break;
	case "LU": $country="Luxembourg"; break;
	case "MO": $country="Macao"; break;
	case "MK": $country="Macedonia, The Former Yugoslav Republic of"; break;
	case "MG": $country="Madagascar"; break;
	case "MW": $country="Malawi"; break;
	case "MY": $country="Malaysia"; break;
	case "MV": $country="Maldives"; break;
	case "ML": $country="Mali"; break;
	case "MT": $country="Malta"; break;
	case "MH": $country="Marshall Islands"; break;
	case "MQ": $country="Martinique"; break;
	case "MR": $country="Mauritania"; break;
	case "MU": $country="Mauritius"; break;
	case "YT": $country="Mayotte"; break;
	case "MX": $country="Mexico"; break;
	case "FM": $country="Micronesia, Federated States of"; break;
	case "MD": $country="Moldova, Republic of"; break;
	case "MC": $country="Monaco"; break;
	case "MN": $country="Mongolia"; break;
	case "MS": $country="Montserrat"; break;
	case "ME": $country="Montenegro"; break;
	case "MA": $country="Morocco"; break;
	case "MZ": $country="Mozambique"; break;
	case "MM": $country="Myanmar"; break;
	case "NA": $country="Namibia"; break;
	case "NR": $country="Nauru"; break;
	case "NP": $country="Nepal"; break;
	case "NL": $country="Netherlands"; break;
	case "AN": $country="Netherlands Antilles"; break;
	case "NC": $country="New Caledonia"; break;
	case "NZ": $country="New Zealand"; break;
	case "NI": $country="Nicaragua"; break;
	case "NE": $country="Niger"; break;
	case "NG": $country="Nigeria"; break;
	case "NU": $country="Niue"; break;
	case "NF": $country="Norfolk Island"; break;
	case "MP": $country="Northern Mariana Islands"; break;
	case "NO": $country="Norway"; break;
	case "OM": $country="Oman"; break;
	case "PK": $country="Pakistan"; break;
	case "PW": $country="Palau"; break;
	case "PS": $country="Palestinian Territories, Occupied"; break;
	case "PA": $country="Panama"; break;
	case "PG": $country="Papua New Guinea"; break;
	case "PY": $country="Paraguay"; break;
	case "PE": $country="Peru"; break;
	case "PH": $country="Philippines"; break;
	case "PN": $country="Pitcairn"; break;
	case "PL": $country="Poland"; break;
	case "PT": $country="Portugal"; break;
	case "PR": $country="Puerto Rico"; break;
	case "QA": $country="Qatar"; break;
	case "RE": $country="Reunion"; break;
	case "RO": $country="Romania"; break;
	case "RU": $country="Russian Federation"; break;
	case "RW": $country="Rwanda"; break;
	case "BL": $country="Saint Barthelemy"; break;
	case "SH": $country="Saint Helena"; break;
	case "KN": $country="Saint Kitts and Nevis"; break;
	case "LC": $country="Saint Lucia"; break;
	case "MF": $country="Saint Martin"; break;
	case "PM": $country="Saint Pierre and Miquelon"; break;
	case "VC": $country="Saint Vincent and the Grenadines"; break;
	case "WS": $country="Samoa"; break;
	case "SM": $country="San Marino"; break;
	case "ST": $country="Sao Tome and Principe"; break;
	case "SA": $country="Saudi Arabia"; break;
	case "SN": $country="Senegal"; break;
	case "RS": $country="Serbia"; break;
	case "SC": $country="Seychelles"; break;
	case "SL": $country="Sierra Leone"; break;
	case "SG": $country="Singapore"; break;
	case "SK": $country="Slovakia"; break;
	case "SI": $country="Slovenia"; break;
	case "SB": $country="Solomon Islands"; break;
	case "SO": $country="Somalia"; break;
	case "ZA": $country="South Africa"; break;
	case "GS": $country="South Georgia and the South Sandwich Islands"; break;
	case "ES": $country="Spain"; break;
	case "LK": $country="Sri Lanka"; break;
	case "SD": $country="Sudan"; break;
	case "SR": $country="Suriname"; break;
	case "SJ": $country="Svalbard and Jan Mayen"; break;
	case "SZ": $country="Swaziland"; break;
	case "SE": $country="Sweden"; break;
	case "CH": $country="Switzerland"; break;
	case "SY": $country="Syrian Arab Republic"; break;
	case "TW": $country="Taiwan, Province of China"; break;
	case "TJ": $country="Tajikistan"; break;
	case "TZ": $country="Tanzania, United Republic of"; break;
	case "TH": $country="Thailand"; break;
	case "TL": $country="Timor-Leste"; break;
	case "TG": $country="Togo"; break;
	case "TK": $country="Tokelau"; break;
	case "TO": $country="Tonga"; break;
	case "TT": $country="Trinidad and Tobago"; break;
	case "TN": $country="Tunisia"; break;
	case "TR": $country="Turkey"; break;
	case "TM": $country="Turkmenistan"; break;
	case "TC": $country="Turks and Caicos Islands"; break;
	case "TV": $country="Tuvalu"; break;
	case "UG": $country="Uganda"; break;
	case "UA": $country="Ukraine"; break;
	case "AE": $country="United Arab Emirates"; break;
	case "GB": $country="United Kingdom"; break;
	case "US": $country="United States"; break;
	case "UM": $country="United States Minor Outlying Islands"; break;
	case "UY": $country="Uruguay"; break;
	case "UZ": $country="Uzbekistan"; break;
	case "VU": $country="Vanuatu"; break;
	case "VE": $country="Venezuela"; break;
	case "VN": $country="Viet Nam"; break;
	case "VG": $country="Virgin Islands, British"; break;
	case "VI": $country="Virgin Islands, U.S."; break;
	case "WF": $country="Wallis and Futuna"; break;
	case "EH": $country="Western Sahara"; break;
	case "YE": $country="Yemen"; break;
	case "ZM": $country="Zambia"; break;
	case "ZW": $country="Zimbabwe"; break;
	}
	return $country;
    }
    
    function volunteer(){
        $output->page_title = "بيانات المتطوع";			
        $output->my_view = "includes/volunteer";			
        $output->css_files = array();			
        $output->js_files = array();
        $output->data_rows = $this->volunteer_model->getVolunteerData();
        $output->main_lang = $this->volunteer_model->getVolunteerMainLanguage();
        $output->skills = $this->volunteer_model->getVolunteerSkills();
        $output->volunteer_languages = $this->volunteer_model->getVolunteerOtherLanguages();
        $output->output = "";			
        $this->_admin_output($output);
    }
    
    function skills(){
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_table('ist_skill');
        $crud->set_subject('مهارة جديدة');
        $crud->columns('skillId', 'arabicDesc', 'englishDesc');
        $crud->display_as('skillId', 'الترتيب')->display_as('arabicDesc', 'الاسم العربى')->display_as('englishDesc', 'الاسم الإنجليزى');
        $crud->required_fields('arabicDesc', 'englishDesc');
        $crud->unset_export();
        $crud->unset_print();
        $output = $crud->render();
        $output->page_title = "قائمة المهارات";
        $this->_admin_output($output);
    }
    
    function search(){
        $output->page_title = "البحث فى المتطوعين";			
        $output->my_view = "includes/search_volunteer";			
        $output->css_files = array();			
        $output->js_files = array();			
        $output->output = "";			
        $output->allSkills = $this->volunteer_model->getAllSkills();
        $output->allLanguages = $this->volunteer_model->getAllLanguages();
        $this->_admin_output($output);
    }
    
    function search_by_country(){
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->where('country', $_POST['country']);
        $crud->set_table('volunteers');
        $crud->unset_operations();
        $crud->columns('profileId', 'name', 'country', 'printDate', 'mail');
        $crud->callback_column('name',array($this,'_callback_webpage_url'));
        $crud->callback_column('country',array($this,'_callback_languages'));
        $crud->display_as('profileId', 'تسلسل')->display_as('name', 'الاسم')->display_as('country', 'البلد')->display_as('printDate', 'تاريخ التسجيل')->display_as('mail', 'البريد الإلكترونى');
        $crud->unset_export();
        $crud->unset_print();
        $output = $crud->render();
        $data['page_title'] = "البحث فى المتطوعين";			
        $data['my_view'] = "includes/search_volunteer";			
        $data['css_files'] = array();			
        $data['js_files'] = array();			
        $data['output'] = "";			
        $data['allSkills'] = $this->volunteer_model->getAllSkills();
        $data['allLanguages'] = $this->volunteer_model->getAllLanguages();
        $output = array_merge($data,(array)$output);
        $this->_admin_output($output);
    }
    
    function search_by_skill(){
    	$this->load->library('table');
		$tmpl = array ( 'table_open'  => '<table border="1" cellpadding="2" cellspacing="1" class="mytable">' );
		$this->table->set_template($tmpl);
		if($this->input->post('volunteer_skill')){
			$skill_id = $this->input->post('volunteer_skill');
		}else{
			$skill_id = $this->uri->segment(3);
		}
    	
        //$data['sk_volunteers'] = $this->volunteer_model->get_volunteers_by_skill($skill_id);
		$data['page_title']    = "البحث فى المتطوعين";			
        $data['my_view']       = "includes/search_volunteer";			
        $data['css_files']     = array();			
        $data['js_files']      = array();			
        $data['output']        = "";			
        $data['allSkills']     = $this->volunteer_model->getAllSkills();
        $data['allLanguages']  = $this->volunteer_model->getAllLanguages();
		
		$data['id'] = $this->input->post('volunteer_skill');
		$this->load->library('pagination');
		$config['base_url']       = site_url().'/'.'volunteers/search_by_skill/'.$skill_id;
		$config['total_rows']     = $this->volunteer_model->get_volunteers_by_skill($skill_id)->num_rows();
		$config['per_page']       = 15;
		$config['num_links']      = 5;
		$config['full_tag_open']  = "<div id='pagination'>";
		$config['full_tag_close'] = "</div>";
		$config['next_link']      = 'next';
		$config['prev_link']      = 'prev';
		
		if($this->uri->segment(4)){
			$limit = $this->uri->segment(4);
		}else{
			$limit = 0;
		}
		
		$data['sk_volunteers'] = $this->volunteer_model->get_volunteers_by_skill_pagination($skill_id, $limit, $config['per_page']);
		
		$this->pagination->initialize($config);
		$this->_admin_output($data);
	}
    
    function search_by_language(){
        $this->load->library('table');
        $tmpl = array ( 'table_open'  => '<table border="1" cellpadding="2" cellspacing="1" class="mytable">' );
        $this->table->set_template($tmpl);

        if($this->input->post('volunteer_lang')){
            $lang_id = $this->input->post('volunteer_lang');
        }else{
            $lang_id = $this->uri->segment(3);
        }

        $data['page_title']    = "البحث فى المتطوعين";
        $data['my_view']       = "includes/search_volunteer";
        $data['css_files']     = array();
        $data['js_files']      = array();
        $data['output']        = "";
        $data['allSkills']     = $this->volunteer_model->getAllSkills();
        $data['allLanguages']  = $this->volunteer_model->getAllLanguages();
        $data['id']            = $this->input->post('volunteer_lang');

        $this->load->library('pagination');
        $config['base_url']       = site_url().'/'.'volunteers/search_by_language/'.$lang_id;
        $config['total_rows']     = $this->volunteer_model->get_volunteers_by_lang($lang_id)->num_rows();
        $config['per_page']       = 15;
        $config['num_links']      = 5;
        $config['full_tag_open']  = "<div id='pagination'>";
        $config['full_tag_close'] = "</div>";
        $config['next_link']      = 'next';
        $config['prev_link']      = 'prev';

        if($this->uri->segment(4)){
            $limit = $this->uri->segment(4);
        }else{
            $limit = 0;
        }

        $data['la_volunteers'] = $this->volunteer_model->get_volunteers_by_lang_pagination($lang_id, $limit, $config['per_page']);

        $this->pagination->initialize($config);
        $this->_admin_output($data);
    }
    
    function search_by_email(){
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->where('mail', $_POST['volunteer_mail']);
        $crud->set_table('volunteers');
        $crud->unset_operations();
        $crud->columns('profileId', 'name', 'country', 'printDate', 'mail');
        $crud->callback_column('name',array($this,'_callback_webpage_url'));
        $crud->callback_column('country',array($this,'_callback_languages'));
        $crud->display_as('profileId', 'تسلسل')->display_as('name', 'الاسم')->display_as('country', 'البلد')->display_as('printDate', 'تاريخ التسجيل')->display_as('mail', 'البريد الإلكترونى');
        $crud->unset_export();
        $crud->unset_print();
        $output = $crud->render();
        $data['page_title'] = "البحث فى المتطوعين";			
        $data['my_view'] = "includes/search_volunteer";			
        $data['css_files'] = array();			
        $data['js_files'] = array();			
        $data['output'] = "";			
        $data['allSkills'] = $this->volunteer_model->getAllSkills();
        $data['allLanguages'] = $this->volunteer_model->getAllLanguages();
        $output = array_merge($data,(array)$output);
        $this->_admin_output($output);
    }
    
    function download($type=""){
        //ob_start();
        $volunteers_data = $this->volunteer_model->get_all_volunteers();
        if($volunteers_data){
            $this->load->library("phpexcel");
            date_default_timezone_set("GMT");
            $filename = "filename".date("Y-m-d");

            $this->phpexcel->getProperties()->setCreator("Mohammed Zayan")
                ->setLastModifiedBy("Mohammed Zayan")
                ->setTitle("Volunteers Data")
                ->setSubject("Export Volunteers Data")
                ->setDescription("This is volunteers data")
                ->setKeywords("Volunteers")
                ->setCategory("Volunteers");

            $this->phpexcel->setActiveSheetIndex(0);

            $this->phpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $this->phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $this->phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
            $this->phpexcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
            $this->phpexcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);

            $this->phpexcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $this->phpexcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

            $this->phpexcel->getActiveSheet()->getStyle('A1:G1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->phpexcel->getActiveSheet()->getStyle('A1:G1')->getFill()->getStartColor()->setARGB('FF808080');
            $this->phpexcel->getActiveSheet()->setCellValue('A1', "profileId");
            $this->phpexcel->getActiveSheet()->setCellValue('B1', "الاسم");
            $this->phpexcel->getActiveSheet()->setCellValue('C1', "الإيميل");
            $this->phpexcel->getActiveSheet()->setCellValue('D1', "print data");
            $this->phpexcel->getActiveSheet()->setCellValue('E1', "age");
            $this->phpexcel->getActiveSheet()->setCellValue('F1', "qualification");
            $this->phpexcel->getActiveSheet()->setCellValue('G1', "location");

            $therow = 1;

            foreach($volunteers_data as $row){
                ++$therow ;
                $this->phpexcel->getActiveSheet()->setCellValue('A'.$therow, $row->profileId);
                $this->phpexcel->getActiveSheet()->setCellValue('B'.$therow, $row->name);
                $this->phpexcel->getActiveSheet()->setCellValue('C'.$therow, $row->mail);
                $this->phpexcel->getActiveSheet()->setCellValue('D'.$therow, $row->printDate);
                $this->phpexcel->getActiveSheet()->setCellValue('E'.$therow, $row->age);
                $this->phpexcel->getActiveSheet()->setCellValue('F'.$therow, $row->qualification);
                $this->phpexcel->getActiveSheet()->setCellValue('G'.$therow, $row->location);

            }
            switch ($type){
                case "xlsx":
                    $ty = "Excel2007";
                    $fname = $filename.".xlsx";
                    $headertype = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    break;

                case "csv":
                    $ty = "CSV";
                    $fname = $filename.".csv";
                    $headertype = "text/csv";
                    break;

                case "html":
                    $ty = "HTML";
                    $fname = $filename.".html";
                    $headertype = "text/html";
                    break;


                    break;

                default :
                    $ty = "Excel5";
                    $fname = $filename.".xls";
                    $headertype = "application/vnd.ms-excel";
            }
            header('Content-Type: '.$headertype);
            header('Content-Disposition: attachment;filename="'.$fname.'"');
            header('Cache-Control: max-age=0');

            $objWriter = PHPExcel_IOFactory::createWriter($this->phpexcel, $ty);
            //flush();
            ob_clean();
            
                        $objWriter->save('php://output');
            exit;
        }else{
            echo "No data ". anchor(base_url(),"go back");
        }
    }
    
    function download_txt(){
        //$this->load->model('site_model');
        $result = $this->volunteer_model->get_all_volunteers();
        //print_r($result); die();
        header("Pragma: public"); // required
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false); // required for certain browsers
        header("Content-Type: text/plain");
        header("Content-Disposition: attachment; filename=\"Volunteers.txt\";");
        header("Content-Transfer-Encoding: binary");
        ob_clean();
        foreach($result as $value){
            echo $value->name.",".$value->mail."\r\n";
        }
    }
    
}