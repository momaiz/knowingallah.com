<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 * Created by JetBrains PhpStorm
 */

class Photo_gallery extends MY_Controller{

    function __construct(){
        parent::__construct();
    }

    function _admin_output($output = null){
        $output->languages = $this->admin_model->get_languages_list();
        $output->module_name = "معرض الصور";
        $this->load->view('admin.php',$output);
    }

    function index(){
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_table('photo_gallery');
        $crud->set_subject('صورة');
        $crud->columns('id', 'language_id', 'photo');
        $crud->display_as('id', '#')->display_as('language_id', 'اللغة')->display_as('photo', 'رابط الصورة')->display_as('photo_thumb', 'رابط الصورة المصغرة');
        $crud->callback_column('photo', array($this, '_callback_view_photo'));
        $crud->set_relation('language_id', 'language', 'description');
        $crud->required_fields('language_id', 'photo', 'photo_thumb');
        $crud->unset_export();
        $crud->unset_print();
        $output = $crud->render();
        $this->_admin_output($output);
    }

    function _callback_view_photo($value, $row){
        return "<img src='".$value."' width='153' height='90' />";
    }

}