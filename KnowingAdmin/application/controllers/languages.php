<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Languages extends MY_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	function _admin_output($output = null)
	{
		$output->languages = $this->admin_model->get_languages_list();
		$this->load->view('admin.php',$output);	
	}

	function index()
	{
		try{
			
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('language');
			$crud->set_subject('لغة');
			$crud->required_fields('id','code','description', 'description_ar');
			$crud->columns('code','description', 'description_ar');
			$crud->display_as('description','اللغة')->display_as('code','رقم اللغة')->display_as('description_ar', 'الوصف العربى');
			$crud->unset_export();
    		$crud->unset_print();
			$output = $crud->render();
			$output->page_title = "اللغات";			

			$this->_admin_output($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
}