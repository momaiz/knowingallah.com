<?php

/**
 * Copyright (c) 2015 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @copyright     copyright 2015
 * @author        programmer/ Mohammed Fawzy Zayan <admin@zayanit.com>
 * @link          http://www.zayanit.com
 * @version       1.0
 * Created by Sublime Text 2
 */

class Banners extends MY_Controller
{

	function __construct()
	{
        parent::__construct();
    }

    function index()
    {
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_table('banners');
        $crud->set_subject('بنر');
        $crud->unset_export();
        $crud->unset_print();
        $crud->columns('id', 'language_id', 'banner_link', 'banner_photo', 'banner_place');
        $crud->display_as('id', '#')->display_as('language_id', 'اللغة')->display_as('banner_photo', 'صورة البنر')->display_as('banner_link', 'رابط البنر')->display_as('banner_place', 'مكان البنر');
        $crud->set_relation('language_id', 'language', 'description');
        $crud->required_fields('language_id', 'banner_link', 'banner_place');
        $crud->callback_column('banner_link', array($this, '_callback_banner_url'));
        $crud->callback_column('banner_photo', array($this, '_callback_show_banner_photo'));
        $crud->callback_column('banner_place', array($this, '_callback_show_banner_place'));
        $crud->callback_field('banner_photo', array($this, '_callback_banner_photo'));
        $crud->callback_field('banner_place', array($this, '_callback_banner_place'));
        $crud->callback_before_insert(array($this, '_callback_before_insert_update'));
        $crud->callback_before_update(array($this, '_callback_before_insert_update'));
        $output = $crud->render();
        $this->_admin_output($output);
    }

    function _admin_output($output = null)
    {
        $output->languages = $this->admin_model->get_languages_list();
        $output->module_name = "البنرات";
        $this->load->view('admin.php',$output);
    }

    function _callback_banner_url($value)
    {
    	return "<a href=\"$value\" target=\"_blank\">رابط الذى يفتحه البنر</a>";
    }

    function _callback_show_banner_photo($value, $row)
    {
        if($row->banner_place == 'top')
    	   return "<img src='".$value."' width='400' height='50' />";
        elseif($row->banner_place == 'side')
            return "<img src='".$value."' width='200' height='150' />";
    }

    function _callback_show_banner_place($value)
    {
        if($value == 'top')
            return 'فى اﻷعلى';
        elseif($value == 'side')
            return 'فى الجانب';
    }

    function _callback_banner_photo()
    {
        return '<input type="file" name="banner_photo" id="banner_photo" accept="image/*">';
    }

    function _callback_before_insert_update($post_array)
    {
        if($_FILES['banner_photo']['name'] != '')
            $post_array['banner_photo'] = $this->uploadBannerPhoto($post_array);
        return $post_array;
    }

    function _callback_banner_place($value)
    {
        if($value == 'top'){
            return "
                <lable><input type='radio' name='banner_place' value='top' checked>فى اﻷعلى</lable>
                <lable style='padding-right:24px'><input type='radio' name='banner_place' value='side'>فى الجانب</lable>
            ";
        }elseif($value == 'side'){
            return "
                <lable><input type='radio' name='banner_place' value='top'>فى اﻷعلى</lable>
                <lable style='padding-right:24px'><input type='radio' name='banner_place' value='side' checked>فى الجانب</lable>
            ";
        }else{
            return "
                <lable><input type='radio' name='banner_place' value='top'>فى اﻷعلى</lable>
                <lable style='padding-right:24px'><input type='radio' name='banner_place' value='side'>فى الجانب</lable>
            ";
        }
    }

    function uploadBannerPhoto($post_array)
    {
        $this->load->helper('my_uploader_helper');
        $photoName  = changeFileName('banner_photo');
        $image      = $_FILES['banner_photo']['tmp_name'];
        $remoteDir  = '/public_html/media/banners/';
        $this->load->library('image_lib');
        $config['image_library']  = 'gd2';
        $config['source_image']   = $image;
        $config['maintain_ratio'] = FALSE;
        if($post_array['banner_place'] == 'top'){
            $config['width']  = 847;
            $config['height'] = 90;
        }
        elseif($post_array['banner_place'] == 'side'){
            $config['width']  = 360;
            $config['height'] = 250;
        }
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
        $photoToUpload = compressImage($image, 'temp_upload/'.$photoName.'.jpg', 75);
        uploadToMedia($remoteDir, $photoName.'.jpg', $photoToUpload);
        unlink(realpath(FCPATH)."/temp_upload/$photoName.jpg");
        return 'http://media.knowingallah.com/media/banners/'.$photoName.'.jpg';
    }

}