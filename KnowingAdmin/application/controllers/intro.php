<?php

/**
 * Copyright (c) 2015 MOHAMMED ZAYAN Design, All Rights Reserved.
 *
 * @copyright     copyright 2015
 * @author        programmer/ Mohammed Fawzy Zayan <admin@zayanit.com>
 * @link          http://www.zayanit.com
 * @version       1.0
 * Created by Sublime Text 2
 */

class Intro extends MY_Controller
{

 	function __construct()
	{
        parent::__construct();
    }

    function _admin_output($output = null)
    {
        $output->languages = $this->admin_model->get_languages_list();
        $output->module_name = "مقدمة الموقع";
        $this->load->view('admin.php',$output);
    }

    function index()
    {
        $this->_admin_output(array('output' => '' , 'js_files' => array() , 'css_files' => array()));
    }

    function intro_text()
    {
    	$crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_table('intro_text');
        $crud->set_subject('نص');
        $crud->columns('id', 'text_content', 'visible');
        $crud->display_as('id', '#')->display_as('text_content', 'النص')->display_as('visible', 'الظهور فى الموقع');
        $crud->required_fields('text_content');
        $crud->callback_add_field('text_content', array($this,'text_content_add_field_callback'));
        $crud->callback_edit_field('text_content', array($this, 'text_content_edit_field_callback'));
        $crud->unset_export();
        $crud->unset_print();
        $output = $crud->render();
        $output->page_title = 'نص صفحة المقدمة';
        $this->_admin_output($output);
    }

    function intro_videos()
    {
    	$crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_table('intro_videos');
        $crud->set_subject('فيديو');
        $crud->columns('id', 'video_title', 'youtube_code', 'visible');
        $crud->display_as('id', '#')->display_as('video_title', 'عنوان الفيديو')->display_as('youtube_code', 'كود اليوتيوب')->display_as('visible', 'الظهور فى الموقع');
        $crud->required_fields('youtube_code');
        $crud->unset_export();
        $crud->unset_print();
        $output = $crud->render();
        $output->page_title = 'فيديو صفحة المقدمة';
        $this->_admin_output($output);
    }

    function text_content_add_field_callback()
    {
    	return '<textarea name="text_content" rows="15"></textarea>';
    }

    function text_content_edit_field_callback($value){
        return "<textarea name='text_content' rows='15'>$value</textarea>";
    }

}