<?php
/**
 * Copyright (c) 2015 MOHAMMED ZAYAN Design, All Rights Reserved.
 * @author        Mohammed Zayan <admin@zayanit.com>
 * @link          http://www.zayanit.com
 * @version       1.0
 * Created by PhpStorm.
 * Date: 11/08/15
 * Time: 01:35 م
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MY_Controller
{

    function __construct(){
        parent::__construct();
    }

    function index()
    {
        $output->css_files = array();
        $output->js_files = array();
        $output->output = "";
        $this->_admin_output($output);
    }

    function _admin_output($output = null){
        $output->languages = $this->admin_model->get_languages_list();
        $output->module_name = "مرحبا بك فى لوحة تحكم الإدارة";
        $this->load->view('admin.php',$output);
    }

}