<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Videos extends MY_Controller {

    public $data = array();

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$this->_admin_output(array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	function home_videos()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('home_videos');
		$crud->set_subject('فيديو');
		$crud->columns('id', 'language_id', 'video_title', 'youtube_code', 'visible');
		$crud->display_as('id', '#')->display_as('video_title', 'عنوان الفيديو')->display_as('youtube_code', 'كود اليوتيوب')->display_as('visible', 'الظهور فى الموقع')->display_as('language_id', 'االغة');
		$crud->required_fields('youtube_code');
		$crud->set_relation('language_id','language','description');
		$crud->unset_export();
        $crud->unset_print();
		$output = $crud->render();
		$output->page_title = 'فيديوهات الرئيسية';
		$this->_admin_output($output);
	}

	function videos_category($lang = null, $cat = null)
	{
		try{
			if($lang == null || $lang == 'add')
				$lang = 1;
			if($cat == null || $cat == 'add')
				$cat = 0;

			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('video_cat_translation');
			$crud->set_subject('قسم');
			$crud->required_fields('description', 'cat_description');
			$crud->add_fields('video_cat_id','language_id','parent_id','description', 'cat_description');
			$crud->edit_fields('language_id','description', 'cat_description');
			$crud->set_relation('video_cat_id','video_cat', 'id');
			$crud->set_relation('language_id','language','description');
			$crud->where('language_id', $lang);
			$crud->where('parent_id', $cat);
			$crud->columns('video_cat_id','description');
			$crud->display_as('description','اسم القسم')->display_as('language_id','اللغة')->display_as('parent_id','رقم القسم الأب')->display_as('video_cat_id','رقم القسم')->display_as('cat_description', 'وصف القسم');
			$crud->change_field_type('video_cat_id', 'hidden', '0');
			$crud->change_field_type('language_id', 'hidden', $lang);
			$this->session->set_userdata('art_cat_parent_Id', $cat);
			$crud->callback_add_field('parent_id',array($this,'add_field_callback_1'));
			$crud->callback_before_insert(array($this,'callback_before_insert_video_cat'));
			$crud->add_action('الأقسام تحت هذا القسم', '', '','ui-icon-plus',array($this,'sub_cats'));
			$crud->add_action('ترجمات', '', '','ui-icon-plus',array($this,'cat_trans'));
			$crud->add_action('الفيديوهات تحت هذا القسم', '', '','ui-icon-plus',array($this,'sub_videos'));
			$crud->callback_field('cat_description', array($this, '_callback_description'));
			$crud->unset_export();
        	$crud->unset_print();
			$output = $crud->render();

			$output->page_title = $this->admin_model->get_video_cat_page_title($lang,$cat);
			$this->_admin_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function video_category($cat)
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('video_cat_translation');
			$crud->set_subject('ترجمة');
			$crud->required_fields('description','language_id', 'cat_description');
			$crud->add_fields('video_cat_id','description','language_id', 'cat_description');
			$crud->edit_fields('description','language_id', 'cat_description');
			$crud->set_relation('language_id','language','description');
			$crud->where('video_cat_id', $cat);
			$crud->columns('video_cat_id','description','language_id');
			$crud->display_as('description','اسم القسم')->display_as('video_cat_id','رقم القسم')->display_as('language_id','اللغة')->display_as('cat_description', 'وصف القسم');
			$crud->callback_field('cat_description', array($this, '_callback_description'));
			$crud->change_field_type('video_cat_id', 'hidden', $cat);

			$crud->unset_export();
        $crud->unset_print();
			$output = $crud->render();
			$output->page_title = "ترجمات القسم رقم: " . $cat;
			$this->_admin_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function videos_($lang = null, $cat = null)
	{
		try{
			if($lang == null || $lang == 'add')
				$lang = 1;
			if($cat == null || $cat == 'add')
				$cat = 0;

			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('video_translation');
			$crud->set_subject('فيديو');
			$crud->set_relation('video_id','video', 'id');
			$crud->set_relation('language_id','language','description');
			$crud->where('language_id', $lang);
			$crud->where('video_cat_id', $cat);
			$crud->add_fields('language_id','video_id','video_cat_id','creation_date','title','slug','description','link','youtube_code','author','visible','base_url');
			$crud->edit_fields('title','slug','description','link','youtube_code','author','visible','base_url');
			$crud->required_fields('title','visible');
			$crud->columns('video_id','title','author','visible','hits','creation_date','base_url');
			$crud->display_as('video_cat_id','رقم القسم')->display_as('title','عنوان الفيديو')->display_as('language_id','اللغة')->display_as('video_id','رقم الفيديو')->display_as('description','وصف الفيديو')->display_as('slug', 'رابط الظهور فى الموقع')
			->display_as('link','رابط الفيديو')->display_as('youtube_code','كود الفيديو على اليوتيوب')
			->display_as('author','المحاضر')->display_as('visible','يظهر في الموقع')->display_as('hits','عدد مرات المشاهدة')->display_as('creation_date','تاريخ الاضافة')->display_as('base_url','الرابط الأصلى');
			$crud->callback_field('description', array($this, '_callback_description'));
			$crud->change_field_type('video_id', 'hidden', '0');
			$crud->change_field_type('creation_date', 'hidden', '0');
			$crud->change_field_type('language_id', 'hidden', $lang);
			$this->session->set_userdata('art_cat_Id', $cat);
			$crud->callback_add_field('video_cat_id',array($this,'add_field_callback_2'));
			$crud->callback_before_insert(array($this,'callback_before_insert_video'));
			$crud->callback_before_update(array($this, 'callback_before_update_video'));
			$crud->add_action('ترجمات', '', '','ui-icon-plus',array($this,'video_'));
			$crud->unset_export();
        $crud->unset_print();
			$output = $crud->render();

			$output->page_title = $this->admin_model->get_video_page_title($lang,$cat);
			$this->_admin_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function video_trans($art)
	{
		try{

			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('video_translation');
			$crud->set_subject('ترجمة');
			$crud->required_fields('title','visible');
			$crud->add_fields('language_id','video_id','creation_date','title','slug','description','link','youtube_code','author','visible');
			$crud->edit_fields('title','slug','description','link','youtube_code','author','visible');
			$crud->set_relation('language_id','language','description');
			$crud->where('video_id', $art);
			$crud->columns('video_id','language_id','title','author','visible','hits','creation_date');
			$crud->display_as('title','عنوان الفيديو')->display_as('language_id','اللغة')->display_as('video_id','رقم الفيديو')->display_as('description','وصف الفيديو')->display_as('slug', 'رابط الظهور فى الموقع')
			->display_as('link','رابط الفيديو')->display_as('youtube_code','كود الفيديو على اليوتيوب')
			->display_as('author','المحاضر')->display_as('visible','يظهر في الموقع')->display_as('hits','عدد مرات المشاهدة')->display_as('creation_date','تاريخ الاضافة');
			$crud->callback_before_insert(array($this,'callback_before_insert_video_trans'));
			$crud->callback_before_update(array($this, 'callback_before_update_video'));
			$crud->change_field_type('video_id', 'hidden', $art);
			$crud->change_field_type('creation_date', 'hidden', '0');
			$crud->callback_field('description', array($this, '_callback_description'));
			$crud->unset_export();
        	$crud->unset_print();
			$output = $crud->render();

			$output->page_title = "ترجمات الفيديو رقم: " . $art;
			$this->_admin_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function search_cat()
	{
		$output->page_title = "البحث عن قسم للفيديوهات";
		$output->my_view = "includes/search_video_cat";
		$output->css_files = array();
		$output->js_files = array();
		$output->output = "";
		$this->_admin_output($output);
	}

	function search_cat_result($lang = null, $cat = null)
	{
		try{
			if($lang == null || $lang == 'add')
				$lang = 1;
			if($cat == null || $cat == 'add')
				$cat = 0;

			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('video_cat_translation');
			$crud->set_subject('قسم');
			$crud->required_fields('description');
			$crud->add_fields('video_cat_id','language_id','parent_id','description');
			$crud->edit_fields('language_id','description');
			$crud->set_relation('video_cat_id','video_cat', 'id');
			$crud->set_relation('language_id','language','description');
			$crud->where('language_id', $lang);
			$crud->where('video_cat_id', $cat);
			$crud->columns('video_cat_id','description');
			$crud->display_as('description','وصف القسم')->display_as('language_id','اللغة')->display_as('parent_id','رقم القسم الأب')->display_as('video_cat_id','رقم القسم');

			$crud->change_field_type('video_cat_id', 'hidden', '0');
			$crud->change_field_type('language_id', 'hidden', $lang);
			$this->session->set_userdata('art_cat_parent_Id', $cat);
			$crud->callback_add_field('parent_id',array($this,'add_field_callback_1'));
			$crud->callback_before_insert(array($this,'callback_before_insert_video_cat'));

			$crud->add_action('الأقسام تحت هذا القسم', '', '','ui-icon-plus',array($this,'sub_cats'));
			$crud->add_action('ترجمات', '', '','ui-icon-plus',array($this,'cat_trans'));
			$crud->add_action('المقالات تحت هذا القسم', '', '','ui-icon-plus',array($this,'sub_videos'));
			$crud->unset_export();
        $crud->unset_print();
			$output = $crud->render();

			$output->page_title = "نتيجة البحث";
			$this->_admin_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function search_video()
	{
		$output->page_title = "البحث عن فيديو";
		$output->my_view = "includes/search_video";
		$output->css_files = array();
		$output->js_files = array();
		$output->output = "";
		$this->_admin_output($output);
	}

	function search_video_result($art)
	{
		try{

			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('video_translation');
			$crud->required_fields('title','visible');
			$crud->edit_fields('title','slug','description','link','youtube_code','author','visible');
			$crud->set_relation('language_id','language','description');
			$art   = urldecode($art);
			$where = "title like '%$art%' or slug like '%$art%'";
			$crud->where($where);
			$crud->columns('video_id','language_id','title','author','visible','hits','creation_date');
			$crud->display_as('title','عنوان الفيديو')->display_as('language_id','اللغة')->display_as('video_id','رقم الفيديو')->display_as('description','وصف الفيديو')->display_as('slug', 'رابط الظهور فى الموقع')
				->display_as('link','رابط الفيديو')->display_as('youtube_code','كود الفيديو على اليوتيوب')
				->display_as('author','المحاضر')->display_as('visible','يظهر في الموقع')->display_as('hits','عدد مرات المشاهدة')->display_as('creation_date','تاريخ الاضافة');
			$crud->callback_before_update(array($this, 'callback_before_update_video'));
			$crud->change_field_type('video_id', 'hidden', $art);
			$crud->change_field_type('creation_date', 'hidden', '0');
			$crud->callback_field('description', array($this, '_callback_description'));
			$crud->unset_add();
			$crud->unset_print();
			$crud->unset_export();
			$output = $crud->render();
			$output->page_title = "نتيجة البحث عن الفيديو : " . $art;
			$this->_admin_output($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function _admin_output($output = null)
	{
		$output->languages = $this->admin_model->get_languages_list();
		$output->module_name = "الفيديوهات";
		$this->load->view('admin.php',$output);
	}

	function _callback_description($value)
	{
		return "
			<textarea maxlength='160' id='textarea' name='description'>$value</textarea>
			<lable id='textarea_feedback'></lable>
			<script type='text/javascript'>
			$(document).ready(function() {
			    var text_max = 160;
			    $('#textarea_feedback').html('الأحرف المتبقية ' + text_max);

			    $('#textarea').keyup(function() {
			        var text_length = $('#textarea').val().length;
			        var text_remaining = text_max - text_length;

			        $('#textarea_feedback').html('الأحرف المتبقية ' + text_remaining);
			    });
			});
			</script>
		";
	}

	function add_field_callback_1()
	{
		return '<input type="hidden" value="'. $this->session->userdata('art_cat_parent_Id') .'" name="parent_id" >' . $this->session->userdata('art_cat_parent_Id');
	}

	function callback_before_insert_video_cat($post_array, $primary_key = null)
	{
		$insert = $this->db->insert('video_cat', array('parent_id' => $post_array['parent_id']));
		$post_array['video_cat_id'] = $this->db->insert_id();
		unset($post_array['parent_id']);
		return $post_array;
	}

	function sub_cats($primary_key , $row)
	{
	    return site_url('videos/videos_category').'/'.$row->language_id.'/'.$row->video_cat_id;
	}

	function cat_trans($primary_key , $row)
	{
	    return site_url('videos/video_category').'/'.$row->video_cat_id;
	}

	function sub_videos($primary_key , $row)
	{
	    return site_url('videos/videos_').'/'.$row->language_id.'/'.$row->video_cat_id;
	}

	function add_field_callback_2()
	{
		return '<input type="hidden" value="'. $this->session->userdata('art_cat_Id') .'" name="video_cat_id" >' . $this->session->userdata('art_cat_Id');
	}

	function callback_before_insert_video($post_array, $primary_key = null)
	{
		$currentDate = date("Y-m-d H:i:s");
		$insert = $this->db->insert('video', array('video_cat_id' => $post_array['video_cat_id'], 'creation_date' => $currentDate));
		$post_array['video_id']      = $this->db->insert_id();
		$post_array['creation_date'] = $currentDate;
		$this->load->helper('my_text_helper');
		if($post_array['slug'] == '')
			$post_array['slug'] = url_slug($post_array['title']);
		else
			$post_array['slug'] = url_slug($post_array['slug']);
		unset($post_array['video_cat_id']);
		return $post_array;
	}

	function video_($primary_key , $row)
	{
	    return site_url('videos/video_trans').'/'.$row->video_id;
	}

	function callback_before_insert_video_trans($post_array, $primary_key = null)
	{
		$currentDate = date("Y-m-d H:i:s");
		$post_array['creation_date'] = $currentDate;
		$this->load->helper('my_text_helper');
		if($post_array['slug'] == '')
			$post_array['slug'] = url_slug($post_array['title']);
		else
			$post_array['slug'] = url_slug($post_array['slug']);
		unset($post_array['video_cat_id']);
		return $post_array;
	}

	function callback_before_update_video($post_array)
	{
		$this->load->helper('my_text_helper');
		if($post_array['slug'] == '')
			$post_array['slug'] = url_slug($post_array['title']);
		else
			$post_array['slug'] = url_slug($post_array['slug']);
		return $post_array;
	}

}
