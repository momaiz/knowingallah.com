<?php

/**
 * Copyright (c) 2012 MOHAMMED ZAYAN Design, All Rights Reserved.
 * 
 * @copyright     copyright 2012
 * @author        programmer/ Mohammed Fawzy Zayan
 * @link          http://www.zayanit.com
 * @email         admin@zayanit.com
 * @version       1.0
 */

class Volunteer_model extends CI_Model{
    
    function getVolunteerData(){
        $id = $this->uri->segment(3);
        $this->db->where('profileId', $id);
        $query = $this->db->get('volunteers');
        if($query->num_rows == 1){
            return $query->result();
        }
    }
    
    function getVolunteerMainLanguage(){
        $id = $this->uri->segment(3);
        $sql = "SELECT `language`.`description` FROM `language` INNER JOIN `volunteers` ON `language`.`id` = `volunteers`.`mainlanguageId` WHERE `volunteers`.`profileId` = $id";
        $query = $this->db->query($sql);
        if($query->num_rows > 0){
            return $query->result();
        }
    }
    
    function getVolunteerOtherLanguages(){
        $id = $this->uri->segment(3);
        $sql = "SELECT `language`.`description` FROM `language` INNER JOIN `volunteer_languages` ON `language`.`id` = `volunteer_languages`.`language_id` WHERE `volunteer_languages`.`profileId` = $id";
        $query = $this->db->query($sql);
        if($query->num_rows > 0){
            return $query->result();
        }
    }
    
    function getVolunteerSkills(){
        $id = $this->uri->segment(3);
        $sql = "SELECT `ist_skill`.`arabicDesc` FROM `ist_skill` INNER JOIN `volunteer_skills` ON `ist_skill`.`skillId` = `volunteer_skills`.`skillId` WHERE `volunteer_skills`.`profileId` = $id";
        $query = $this->db->query($sql);
        if($query->num_rows > 0){
            return $query->result();
        }
    }
    
    function getAllSkills(){
        $this->db->select('skillId, arabicDesc');
        $query = $this->db->get('ist_skill');
        if($query->num_rows > 0){
            return $query->result();
        }
    }
    
    function getAllLanguages(){
        $this->db->select('id, description');
        $query = $this->db->get('language');
        if($query->num_rows > 0){
            return $query->result();
        }
    }
    
    function getVolunteerBySkill($skillID){
        $this->db->where('skillId', $skillID);
        $this->db->select('profileId');
        $query = $this->db->get('volunteer_skills');
        if($query->num_rows > 0){
            return $query->result_array();
        }
    }
    
    function get_volunteers_by_skill($skill_id){
    	$query = "SELECT V.`name` as `الاسم`, V.`mail` as `البريد الإلكترونى`, V.`printDate` as `تاريخ الاشتراك`, V.`country` as `البلد`
					FROM volunteers AS V
					INNER JOIN volunteer_skills AS VS ON V.profileId = VS.profileId
					INNER JOIN ist_skill AS S ON VS.skillId = S.skillId
					WHERE S.skillId = {$skill_id}";
		$result = $this->db->query($query);
		if($result->num_rows() > 0){
			return $result;
		}
    }
	
	function get_volunteers_by_skill_pagination($skill_id, $limit1, $limit2){
        $query = "SELECT V.`name`, V.`mail`, V.`printDate`, V.`country`, V.`profileId`
					FROM volunteers AS V
					INNER JOIN volunteer_skills AS VS ON V.profileId = VS.profileId
					INNER JOIN ist_skill AS S ON VS.skillId = S.skillId
					WHERE S.skillId = {$skill_id} LIMIT {$limit1},{$limit2}";
		$result = $this->db->query($query);
		if($result->num_rows() > 0){
			return $result->result();
		}
	}

    function get_volunteers_by_lang($lang_id){
        $query = "SELECT V.name, V.mail, V.printDate, V.country
                    FROM `volunteers` AS V
                    INNER JOIN `volunteer_languages` AS VL ON VL.profileId = V.profileId
                    INNER JOIN `language` AS L ON VL.language_id = L.id
                    WHERE L.id = {$lang_id}";
        $result = $this->db->query($query);
        if($result->num_rows() > 0){
            return $result;
        }
    }

    function get_volunteers_by_lang_pagination($lang_id, $limit1, $limit2){
        $query = "SELECT V.name, V.mail, V.printDate, V.country
                    FROM `volunteers` AS V
                    INNER JOIN `volunteer_languages` AS VL ON VL.profileId = V.profileId
                    INNER JOIN `language` AS L ON VL.language_id = L.id
                    WHERE L.id = {$lang_id} LIMIT {$limit1},{$limit2}";
        $result = $this->db->query($query);
        if($result->num_rows > 0){
            return $result;
        }
    }
    
    function get_all_volunteers(){
        $this->db->select('profileId,name,mail,printDate,age,qualification,location');
        $query = $this->db->get('volunteers');
        if($query->num_rows > 0){
            return $query->result_object();
        }
    }
    
}