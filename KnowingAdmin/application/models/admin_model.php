<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class admin_model extends CI_Model
{
	function get_languages_list()
	{
		return $this->db->get('language')->result();
	}

	function get_article_cat_page_title($lang, $cat)
	{
		$sql = "select description from language where id = " . $this->db->escape($lang);
		$row = $this->db->query($sql)->row_array();
		$title = "اللغة: <span style='color:green;'>" . $row['description'] . "</span><br />";

		if($cat == 0)
			$title .= "الأقسام الرئيسية";
		else
		{
			$sql = "select description from article_cat_translation where language_id = " . $this->db->escape($lang).
			" and article_cat_id = " . $this->db->escape($cat);
			$row = $this->db->query($sql)->row_array();
			$title .= "الأقسام تحت قسم: <span style='color:green;'>" . $row['description'] . "</span>";
		}
		return $title;
	}

	function get_article_page_title($lang, $cat)
	{
		$sql = "select description from language where id = " . $this->db->escape($lang);
		$row = $this->db->query($sql)->row_array();
		$title = "اللغة: <span style='color:green;'>" . $row['description'] . "</span><br />";

			$sql = "select description from article_cat_translation where language_id = " . $this->db->escape($lang).
			" and article_cat_id = " . $this->db->escape($cat);
			$row = $this->db->query($sql)->row_array();
			$title .= "المقالات تحت قسم: <span style='color:green;'>" . $row['description'] . "</span>";
		return $title;
	}

	function get_video_cat_page_title($lang, $cat)
	{
		$sql = "select description from language where id = " . $this->db->escape($lang);
		$row = $this->db->query($sql)->row_array();
		$title = "اللغة: <span style='color:green;'>" . $row['description'] . "</span><br />";

		if($cat == 0)
			$title .= "الأقسام الرئيسية";
		else
		{
			$sql = "select description from video_cat_translation where language_id = " . $this->db->escape($lang).
			" and video_cat_id = " . $this->db->escape($cat);
			$row = $this->db->query($sql)->row_array();
			$title .= "الأقسام تحت قسم: <span style='color:green;'>" . $row['description'] . "</span>";
		}
		return $title;
	}

	function get_video_page_title($lang, $cat)
	{
		$sql = "select description from language where id = " . $this->db->escape($lang);
		$row = $this->db->query($sql)->row_array();
		$title = "اللغة: <span style='color:green;'>" . $row['description'] . "</span><br />";

			$sql = "select description from video_cat_translation where language_id = " . $this->db->escape($lang).
			" and video_cat_id = " . $this->db->escape($cat);
			$row = $this->db->query($sql)->row_array();
			$title .= "الفيديوهات تحت قسم: <span style='color:green;'>" . $row['description'] . "</span>";
		return $title;
	}

	function get_audio_cat_page_title($lang, $cat)
	{
		$sql = "select description from language where id = " . $this->db->escape($lang);
		$row = $this->db->query($sql)->row_array();
		$title = "اللغة: <span style='color:green;'>" . $row['description'] . "</span><br />";

		if($cat == 0)
			$title .= "الأقسام الرئيسية";
		else
		{
			$sql = "select description from audio_cat_translation where language_id = " . $this->db->escape($lang).
			" and audio_cat_id = " . $this->db->escape($cat);
			$row = $this->db->query($sql)->row_array();
			$title .= "الأقسام تحت قسم: <span style='color:green;'>" . $row['description'] . "</span>";
		}
		return $title;
	}

	function get_audio_page_title($lang, $cat)
	{
		$sql = "select description from language where id = " . $this->db->escape($lang);
		$row = $this->db->query($sql)->row_array();
		$title = "اللغة: <span style='color:green;'>" . $row['description'] . "</span><br />";

			$sql = "select description from audio_cat_translation where language_id = " . $this->db->escape($lang).
			" and audio_cat_id = " . $this->db->escape($cat);
			$row = $this->db->query($sql)->row_array();
			$title .= "الصوتيات تحت قسم: <span style='color:green;'>" . $row['description'] . "</span>";
		return $title;
	}

	function get_flash_cat_page_title($lang, $cat)
	{
		$sql = "select description from language where id = " . $this->db->escape($lang);
		$row = $this->db->query($sql)->row_array();
		$title = "اللغة: <span style='color:green;'>" . $row['description'] . "</span><br />";

		if($cat == 0)
			$title .= "الأقسام الرئيسية";
		else
		{
			$sql = "select description from flash_cat_translation where language_id = " . $this->db->escape($lang).
			" and flash_cat_id = " . $this->db->escape($cat);
			$row = $this->db->query($sql)->row_array();
			$title .= "الأقسام تحت قسم: <span style='color:green;'>" . $row['description'] . "</span>";
		}
		return $title;
	}

	function get_flash_page_title($lang, $cat)
	{
		$sql = "select description from language where id = " . $this->db->escape($lang);
		$row = $this->db->query($sql)->row_array();
		$title = "اللغة: <span style='color:green;'>" . $row['description'] . "</span><br />";

			$sql = "select description from flash_cat_translation where language_id = " . $this->db->escape($lang).
			" and flash_cat_id = " . $this->db->escape($cat);
			$row = $this->db->query($sql)->row_array();
			$title .= "الفلاشات تحت قسم: <span style='color:green;'>" . $row['description'] . "</span>";
		return $title;
	}

    // functions for books
    function get_book_cat_page_title($lang, $cat)
    {
        $sql = "select description from language where id = " . $this->db->escape($lang);
        $row = $this->db->query($sql)->row_array();
        $title = "اللغة: <span style='color:green;'>" . $row['description'] . "</span><br />";

        if($cat == 0)
            $title .= "الأقسام الرئيسية";
        else
        {
            $sql = "select description from book_cat_translation where language_id = " . $this->db->escape($lang).
                " and book_cat_id = " . $this->db->escape($cat);
            $row = $this->db->query($sql)->row_array();
            $title .= "الأقسام تحت قسم: <span style='color:green;'>" . $row['description'] . "</span>";
        }
        return $title;
    }

    function get_book_page_title($lang, $cat)
    {
        $sql = "select description from language where id = " . $this->db->escape($lang);
        $row = $this->db->query($sql)->row_array();
        $title = "اللغة: <span style='color:green;'>" . $row['description'] . "</span><br />";

        $sql = "select description from book_cat_translation where language_id = " . $this->db->escape($lang).
            " and book_cat_id = " . $this->db->escape($cat);
        $row = $this->db->query($sql)->row_array();
        $title .= "الكتب تحت قسم: <span style='color:green;'>" . $row['description'] . "</span>";
        return $title;
    }

}