/*****************************************************/
/*** custom script for this project ***/
/*****************************************************/

$('.button-collapse').sideNav({
  menuWidth: 300, // Default is 240
  edge: 'right', // Choose the horizontal origin
  closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
});

$('.main-slider .owl-carousel').owlCarousel({
  rtl: true,
  loop: true,
  autoplay: true,
  autoplayTimeout: 6000,
  smartSpeed: 1000,
  items: 1,
  margin: 0,
  pagination: false,
  nav: false
});

$('.back-to-allah .owl-carousel').owlCarousel({
  rtl: true,
  loop: true,
  autoplay: true,
  autoplayTimeout: 6000,
  smartSpeed: 1000,
  items: 1,
  margin: 0,
  pagination: false,
  nav: false
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip({
    animation: true,
    delay: {
      show: 300,
      hide: 300
    }
  });
});


if ($(".jp-jplayer").length > 0) {
  $(".jp-jplayer").jPlayer({
    ready: function (event) {
      $(this).jPlayer("setMedia", {
        title: "Bubble",
        m4a: "http://quran.islamway.net/quran3/45/001.mp3",
        oga: "http://quran.islamway.net/quran3/45/001.mp3",
      });
    },
    swfPath: "../../dist/jplayer",
    supplied: "m4a, oga",
    wmode: "window",
    useStateClassSkin: true,
    autoBlur: true,
    smoothPlayBar: true,
    keyEnabled: true,
    remainingDuration: true,
    toggleDuration: true
  });

}
